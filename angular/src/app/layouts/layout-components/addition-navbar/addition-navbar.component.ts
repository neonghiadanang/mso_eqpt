import { Component, EventEmitter, HostBinding, Input, OnInit, Output } from '@angular/core';
import { AppSettings } from '../../../interfaces/settings';
import { Store } from '@ngrx/store';
import { AppState } from '../../../interfaces/app-state';
import { FormBuilder, FormGroup } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import * as SettingsActions from '../../../store/actions/app-settings.actions';

export interface SettingsSelect {
  label: string;
  value: string;
}
@Component({
  selector: 'app-addition-navbar',
  templateUrl: './addition-navbar.component.html',
  styleUrls: ['./addition-navbar.component.scss']
})
export class AdditionNavbarComponent implements OnInit {
  @HostBinding('class.app-addition-navbar') true;
  @HostBinding('class.open') get navbarOpen() {
    return  this.opened;
  };

  @Input() opened: boolean;
  @Input() title: string;

  @Output() open: EventEmitter<boolean>;

  appSettings: AppSettings;
  settingsForm: FormGroup;
  crumbsStyles: SettingsSelect[];
  menuStyles: SettingsSelect[];

  siteSettings: SettingsSelect[];
  operationSettings: SettingsSelect[];

  constructor(
    private store: Store<AppState>,
    private fb: FormBuilder
  ) {
    this.open = new EventEmitter<boolean>();
    this.opened = false;
    this.crumbsStyles = [
      {
        label: 'Default',
        value: 'default'
      },
      {
        label: 'Custom 1',
        value: 'custom1'
      },
      {
        label: 'Custom 2',
        value: 'custom2'
      }
    ];
    this.menuStyles = [
      {
        label: 'Style 1',
        value: 'menu-style-1'
      },
      {
        label: 'Style 2',
        value: 'menu-style-2'
      },
      {
        label: 'Style 3',
        value: 'menu-style-3'
      },
      {
        label: 'Style 4',
        value: 'menu-style-4'
      }
    ];
    // List Site Setting
    this.siteSettings = [
      {
        label: 'MSO',
        value: 'MSO'
      },
      {
        label: 'SMO',
        value: 'SMO'
      },
      {
        label: 'GWY',
        value: 'GWY'
      }
    ];

    this.operationSettings = [
      {
        label: 'DEV/TEST',
        value: 'TEST'
      },
      {
        label: 'UAT',
        value: 'UAT'
      },
      {
        label: 'PRODUCTION',
        value: 'PRODUCTION'
      }
    ];

  }

  ngOnInit() {
    this.store.select('appSettings').subscribe(settings => {
      this.appSettings = settings;
    });

    this.initSettingsForm(this.appSettings);
  }

  openNavbar() {
    this.opened = !this.opened;
    this.open.emit(this.opened);
  }

  initSettingsForm(data: AppSettings) {
    this.settingsForm = this.fb.group({
      boxed: [data.boxed ? data.boxed : false],
      sidebarCompress: [data.sidebarCompress ? data.sidebarCompress : false],
      rtl: [data.rtl ? data.rtl : false],
      showTitle: [data.showTitle ? data.showTitle : true],
      showTopNavbar: [data.showTopNavbar ? data.showTopNavbar : true],
      showCrumbs: [data.showCrumbs ? data.showCrumbs : true],
      crumbsType: [data.crumbsType ? data.crumbsType : 'custom1'],
      menuType: [data.menuType ? data.menuType : 'menu-style-3'],
      loginId: [data.loginId ? data.loginId : ''],
      siteId: [data.siteId ? data.siteId : 'MSO'],
      operationId: [data.operationId ? data.operationId : 'TEST'],
    });

    this.settingsForm.valueChanges.pipe(debounceTime(500)).subscribe((settingsObj) => {
        localStorage.setItem('site_id', settingsObj.siteId);
        localStorage.setItem('operation_id', settingsObj.operationId);
        console.log("initSettingsForm Set site_id="+settingsObj.siteId+" operation_id="+settingsObj.operationId)
        this.store.dispatch(new SettingsActions.Update(settingsObj));
    });
  }
}
