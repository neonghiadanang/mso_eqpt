import { Component, OnDestroy, OnInit } from '@angular/core';

import { Store } from '@ngrx/store';

import { BasePageComponent } from '../../base-page/base-page.component';
import { AppState } from '../../../interfaces/app-state';

@Component({
  selector: 'page-ng2-charts',
  templateUrl: './ng2-charts.component.html',
  styleUrls: ['./ng2-charts.component.scss']
})
export class PageNg2ChartsComponent extends BasePageComponent implements OnInit, OnDestroy {
  lineChartData: any[];
  lineChartLabels: any[];
  lineChartOptions: any;
  lineChartColors: any[];
  lineChartLegend: boolean;
  lineChartType: string;
  barChartOptions: any
  barChartLabels: string[];
  barChartType: string;
  barChartLegend: boolean;
  barChartData: any[];
  doughnutChartLabels: string[];
  doughnutChartData: number[];
  doughnutChartType: string;
  radarChartLabels: string[];
  radarChartData: any;
  radarChartType: string;
  pieChartLabels: string[];
  pieChartData: number[];
  pieChartType: string;
  polarAreaChartLabels: string[];
  polarAreaChartData: number[];
  polarAreaLegend: boolean;
  polarAreaChartType: string;
  polarAreaChartOptions: any;

  constructor(store: Store<AppState>) {
    super(store);

    this.pageData = {
      title: 'Ng2 Charts',
      loaded: true,
      breadcrumbs: [
        {
          title: 'UI elements',
          route: './dashboard'
        },
        {
          title: 'Charts',
          route: './dashboard'
        },
        {
          title: 'Ng2 Charts'
        }
      ]
    };

    // lineChart
    this.lineChartData = [
      {data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A'},
      {data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B'},
      {data: [18, 48, 77, 9, 100, 27, 40], label: 'Series C'}
    ];
    this.lineChartLabels = [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'June',
      'July'
    ];
    this.lineChartOptions = {
      responsive: true
    };
    this.lineChartColors = [
      {
        backgroundColor: 'rgba(255,116,19,0.2)',
        borderColor: 'rgba(255,116,19,1)',
        pointBackgroundColor: 'rgba(255,116,19,1)',
        pointBorderColor: '#fff',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: 'rgba(255,116,19,0.8)'
      },
      {
        backgroundColor: 'rgba(225,122,180,0.2)',
        borderColor: 'rgba(225,122,180,1)',
        pointBackgroundColor: 'rgba(225,122,180,1)',
        pointBorderColor: '#fff',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: 'rgba(225,122,180,1)'
      },
      {
        backgroundColor: 'rgba(145,55,63,0.2)',
        borderColor: 'rgba(145,55,63,1)',
        pointBackgroundColor: 'rgba(145,55,63,1)',
        pointBorderColor: '#fff',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: 'rgba(145,55,63,0.8)'
      }
    ];
    this.lineChartLegend = true;
    this.lineChartType = 'line';

    // Bar chart
    this.barChartOptions = {
      scaleShowVerticalLines: false,
      responsive: true
    };
    this.barChartLabels = [
      '2011',
      '2012',
      '2013',
      '2014',
      '2015',
      '2016',
      '2017'
    ];
    this.barChartType = 'bar';
    this.barChartLegend = true;
    this.barChartData = [
      {data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A'},
      {data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B'}
    ];

    // Doughnut chart
    this.doughnutChartLabels = [
      'Download Sales',
      'In-Store Sales',
      'Mail-Order Sales'
    ];
    this.doughnutChartData = [
      350,
      450,
      100
    ];
    this.doughnutChartType = 'doughnut';

    // Radar chart
    this.radarChartLabels = [
      'Eating',
      'Drinking',
      'Sleeping',
      'Designing',
      'Coding',
      'Cycling',
      'Running'
    ];
    this.radarChartData = [
      {data: [65, 59, 90, 81, 56, 55, 40], label: 'Series A'},
      {data: [28, 48, 40, 19, 96, 27, 100], label: 'Series B'}
    ];
    this.radarChartType = 'radar';

    // Pie
    this.pieChartLabels = [
      'Download Sales',
      'In-Store Sales',
      'Mail Sales'
    ];
    this.pieChartData = [
      300,
      500,
      100
    ];
    this.pieChartType = 'pie';

    // PolarArea
    this.polarAreaChartLabels = [
      'Download Sales',
      'In-Store Sales',
      'Mail Sales',
      'Telesales',
      'Corporate Sales'
    ];
    this.polarAreaChartData = [
      300,
      500,
      100,
      40,
      120
    ];
    this.polarAreaLegend = true;
    this.polarAreaChartType = 'polarArea';
    this.polarAreaChartOptions = {
      responsiveAnimationDuration: 500
    };
  }

  ngOnInit() {
    super.ngOnInit();
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }
}
