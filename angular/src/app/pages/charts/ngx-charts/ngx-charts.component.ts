import { Component, OnDestroy, OnInit } from '@angular/core';

import { Store } from '@ngrx/store';

import { single1, single2, multi1, multi2, multi3 } from './data';
import { BasePageComponent } from '../../base-page/base-page.component';
import { AppState } from '../../../interfaces/app-state';

@Component({
  selector: 'page-ngx-charts',
  templateUrl: './ngx-charts.component.html',
  styleUrls: ['./ngx-charts.component.scss']
})
export class PageNgxChartsComponent extends BasePageComponent implements OnInit, OnDestroy {
  single1: any[];
  single2: any[];
  multi1: any[];
  multi2: any[];
  multi3: any[];
  colorScheme: any;

  constructor(store: Store<AppState>) {
    super(store);

    this.pageData = {
      title: 'Ngx Charts',
      loaded: true,
      breadcrumbs: [
        {
          title: 'UI elements',
          route: './dashboard'
        },
        {
          title: 'Charts',
          route: './dashboard'
        },
        {
          title: 'Ngx Charts'
        }
      ]
    };
    this.colorScheme = {
      domain: ['#a8385d', '#7aa3e5', '#a27ea8', '#aae3f5', '#a95963', '#50abcc']
    };
    Object.assign(this, {single1, single2, multi1, multi2, multi3});
  }

  ngOnInit() {
    super.ngOnInit();
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }
}
