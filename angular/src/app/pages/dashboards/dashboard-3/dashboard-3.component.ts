import { Component, OnDestroy, OnInit } from '@angular/core';
import { AmChartsService } from '@amcharts/amcharts3-angular';
import { BasePageComponent } from '../../base-page/base-page.component';
import { AppState } from '../../../interfaces/app-state';
import { Store } from '@ngrx/store';
import { User } from '../../../ui/interfaces/user';
import { Message } from '../../../ui/interfaces/message';

const timelineData: any[] = [
  {
    'timeline': [
      {
        'date': '12 minutes ago',
        'content': `You <span class='text-info'>recommended</span> Sem B.`,
        'pointColor': '#ea8080'
      },
      {
        'date': '37 minutes ago',
        'content': `You followed Sydney N.`,
        'pointColor': '#915035'
      },
      {
        'date': '2 hours ago',
        'content': `You <span class='text-danger'>subscribed</span> to Harold Fuller`,
        'pointColor': '#B925FF'
      },
      {
        'date': '7 hours ago',
        'content': `You updated your profile picture`,
        'pointColor': '#C5CAE9'
      }
    ]
  }
];

@Component({
  selector: 'page-dashboard-3',
  templateUrl: './dashboard-3.component.html',
  styleUrls: ['./dashboard-3.component.scss']
})
export class PageDashboard3Component extends BasePageComponent implements OnInit, OnDestroy {
  timelineData: any[] = timelineData;
  chart: any;
  activeUser: User;
  messages: Message[];
  pieChartLabels: string[];
  pieChartData: any[];
  pieChartColors: any[];
  pieChartType: string;
  pieChartOptions: any;
  doughnutChartLabels: string[];
  doughnutChartData: number[];
  doughnutChartColors: any[];
  doughnutChartType: string;
  doughnutChartOptions: any;
  polarAreaChartLabels: string[];
  polarAreaChartData: number[];
  polarAreaChartColors: any[];
  polarAreaChartType: string;
  polarAreaChartOptions: any;

  constructor(
    store: Store<AppState>,
    private AmCharts: AmChartsService
  ) {
    super(store);

    this.pageData = {
      title: 'Dashboard 3',
      loaded: true,
      breadcrumbs: [
        {
          title: 'Main',
          route: './dashboard'
        },
        {
          title: 'Dashboards',
          route: './dashboard'
        },
        {
          title: 'Dashboard 3'
        }
      ]
    };

    this.activeUser = {
      name: 'Amanda Li',
      lastSeen: 'last seen 10 minutes ago',
      avatar: 'assets/content/avatar-4.jpg'
    };
    this.messages = [
      {
        'date': '8 hours ago',
        'content': `Aenean lacinia bibendum nulla sed consectetur. Nullam id dolor id nibh ultricies vehicula ut id elit.`,
        'my': false
      },
      {
        'date': '7 hours ago',
        'content': `Aenean lacinia bibendum nulla sed consectetur.`,
        'my': true
      },
      {
        'date': '2 hours ago',
        'content': `Contrary to popular belief,`,
        'my': false
      },
      {
        'date': '15 minutes ago',
        'content': `Lorem ipsum dolor sit.`,
        'my': true
      },
      {
        'date': '14 minutes ago',
        'content': `Aenean lacinia bibendum nulla sed consectetur. Nullam id dolor id nibh ultricies vehicula ut id elit.`,
        'my': false
      },
      {
        'date': '12 minutes ago',
        'content': `Aenean lacinia bibendum nulla sed consectetur.`,
        'my': true
      }
    ];
    // Pie
    this.pieChartLabels = [
      'Angular',
      'PHP',
      'HTML'
    ];
    this.pieChartData = [
      300,
      500,
      100
    ];
    this.pieChartColors = [
      {
        backgroundColor: ['#778391', '#5dade0', '#3c4e62']
      }
    ];
    this.pieChartType = 'pie';
    this.pieChartOptions = {
      elements: {
        arc : {
          borderWidth: 0
        }
      },
      tooltips: false
    };

    // Doughnut
    this.doughnutChartLabels = [
      'Angular',
      'PHP',
      'HTML'
    ];
    this.doughnutChartData = [
      350,
      450,
      100
    ];
    this.doughnutChartColors = [
      {
        backgroundColor: [
          '#778391',
          '#ff8c00',
          '#3c4e62'
        ],
      }
    ];
    this.doughnutChartType = 'doughnut';
    this.doughnutChartOptions = {
      elements: {
        arc : {
          borderWidth: 0
        }
      },
      tooltips: false
    };

    // PolarArea
    this.polarAreaChartLabels = [
      'Angular',
      'PHP',
      'HTML'
    ];
    this.polarAreaChartData = [
      300,
      400,
      500
    ];
    this.polarAreaChartColors = [
      {
        backgroundColor: [
          '#778391',
          '#dc143c',
          '#3c4e62'
        ]
      }
    ];
    this.polarAreaChartType = 'polarArea';
    this.polarAreaChartOptions = {
      elements: {
        arc : {
          borderWidth: 0
        }
      },
      tooltips: false
    };
  }

  ngOnInit() {
    super.ngOnInit();

    this.initCharts();
  }

  ngOnDestroy() {
    super.ngOnDestroy();

    this.AmCharts.destroyChart(this.chart);
  }

  initCharts() {
    this.chart = this.AmCharts.makeChart('amchart-1', {
      'type': 'pie',
      'theme': 'light',
      'dataProvider': [
        {
          'country': 'Lithuania',
          'litres': 501.9
        }, {
          'country': 'Czech Republic',
          'litres': 301.9
        }, {
          'country': 'Ireland',
          'litres': 201.1
        }, {
          'country': 'Germany',
          'litres': 165.8
        }, {
          'country': 'Australia',
          'litres': 139.9
        }, {
          'country': 'Austria',
          'litres': 128.3
        }, {
          'country': 'UK',
          'litres': 99
        }, {
          'country': 'Belgium',
          'litres': 60
        }, {
          'country': 'The Netherlands',
          'litres': 50
        }
      ],
      'pullOutRadius': 0,
      'labelRadius': -40,
      'valueField': 'litres',
      'titleField': 'country',
      'labelText': '[[litres]]',
      'balloon': {
        'fixedPosition': true
      }
    });

    this.chart = this.AmCharts.makeChart('amchart-2', {
      'type': 'pie',
      'theme': 'light',
      'dataProvider': [
        {
          'title': 'Chrome',
          'value': 70
        }, {
          'title': 'Firefox',
          'value': 15
        }, {
          'title': 'Opera',
          'value': 10
        }, {
          'title': 'Safari',
          'value': 12
        }, {
          'title': 'Edge',
          'value': 5
        }
      ],
      'titleField': 'title',
      'valueField': 'value',
      'labelRadius': -40,
      'radius': '46%',
      'innerRadius': '60%',
      'labelText': '[[title]]'
    });

    this.chart = this.AmCharts.makeChart('amchart-3', {
      'type': 'radar',
      'theme': 'light',
      'dataProvider': [
        {
          'country': 'Czech Republic',
          'litres': 156.9
        }, {
          'country': 'Ireland',
          'litres': 131.1
        }, {
          'country': 'Germany',
          'litres': 115.8
        }, {
          'country': 'Australia',
          'litres': 109.9
        }, {
          'country': 'Austria',
          'litres': 108.3
        }, {
          'country': 'UK',
          'litres': 99
        }
      ],
      'valueAxes': [ {
        'axisTitleOffset': 20,
        'minimum': 0,
        'axisAlpha': 0.15
      } ],
      'startDuration': 1,
      'graphs': [ {
        'balloonText': '[[value]] litres of beer per year',
        'bullet': 'round',
        'lineThickness': 2,
        'valueField': 'litres'
      } ],
      'categoryField': 'country'
    });
  }
}
