import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NexgenecEqptConfigComponent } from './eqpt-config.component';

describe('NexgenecEqptConfigComponent', () => {
  let component: NexgenecEqptConfigComponent;
  let fixture: ComponentFixture<NexgenecEqptConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NexgenecEqptConfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NexgenecEqptConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
