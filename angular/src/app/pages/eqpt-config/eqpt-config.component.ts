import { Component, ViewChild, OnInit, OnDestroy,Inject } from '@angular/core';
import { DataSource} from '@angular/cdk/collections';
import { MatPaginator } from '@angular/material/paginator';
import { BehaviorSubject, Observable, merge } from 'rxjs';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { map } from 'rxjs/operators';
import { BasePageComponent } from '../base-page/base-page.component';
import { Store } from '@ngrx/store';
import { AppState } from '../../interfaces/app-state';

import { MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatDialogModule } from '@angular/material/dialog';
import { NexgenecEqptConfig, NexgenecEqptConfigService, NexgenecEqptConnection, NexgenecEqptConnectionService, NexgenecMesSetting, NexgenecMesSettingService } from './eqpt-config.models';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { ConfirmationDialogComponent } from '../../ui/components/confirmation-dialog/confirmation-dialog.component';

export interface DialogData {
  animal: string;
  name: string;
}


@Component({
  selector: 'request-dialog',
  templateUrl: 'nexgeneceqptconfig.requestdialog.html',
})
export class DialogRequestApprove implements OnInit{
  action:string;
  local_data:any;
  // public ownerForm: FormGroup;
  listApprovedBy = [
  ]
  constructor(
    public dialogRef: MatDialogRef<DialogRequestApprove>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {
    this.local_data = {...data};
    console.log(this.local_data)
    this.listApprovedBy = this.local_data.list_user;
    this.action = this.local_data.action;
  }

  doAction(){
    this.dialogRef.close({result:true, commented:this.local_data.commented, approved_by:this.local_data.approved_by});
  }

  closeDialog(){
    this.dialogRef.close({event:'Cancel'});
  }

  ngOnInit() {
    // this.ownerForm = new FormGroup({
    //   commented: new FormControl(this.local_data.commented, [Validators.required, Validators.maxLength(256)]),
    // });
  }

  // public hasError = (controlName: string, errorName: string) =>{
  //   return this.ownerForm.controls[controlName].hasError(errorName);
  // }

}

@Component({
  selector: 'edit-dialog',
  templateUrl: 'nexgeneceqptconnection.editdialog.html',
})
export class DialogNexgenecEqptConnectionComponent implements OnInit{
  action:string;
  local_data:any;
  public ownerForm: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<DialogNexgenecEqptConnectionComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {
    //console.log(data);
    this.local_data = {...data};
    this.action = this.local_data.action;
  }

  doAction(){
    //this.dialogRef.close({data:this.local_data});
    this.dialogRef.close({data:this.ownerForm.value});
  }

  closeDialog(){
    this.dialogRef.close({event:'Cancel'});
  }

  ngOnInit() {
    this.ownerForm = new FormGroup({
      seq_id: new FormControl(this.local_data.seq_id, []),
      eqpt_id: new FormControl(this.local_data.eqpt_id, []),
      section_id: new FormControl(this.local_data.section_id, [Validators.required, Validators.maxLength(30)]),
      connection_mode: new FormControl(this.local_data.connection_mode, []),
      connection_type: new FormControl(this.local_data.connection_type, [Validators.required, Validators.maxLength(30)]),
      connection_host: new FormControl(this.local_data.connection_host, [Validators.required, Validators.maxLength(20)]),
      connection_port: new FormControl(this.local_data.connection_port, [Validators.required, Validators.maxLength(10)]),
      environment: new FormControl(this.local_data.environment, [Validators.required, Validators.maxLength(30)]),
      username: new FormControl(this.local_data.username, [Validators.required, Validators.maxLength(20)]),
      password_enc: new FormControl(this.local_data.password_enc, [Validators.required, Validators.maxLength(20)]),
      config: new FormControl(this.local_data.config, []),
      updated_by: new FormControl(this.local_data.updated_by, []),
      timestamp: new FormControl(this.local_data.timestamp, []),
    });
  }

  public hasError = (controlName: string, errorName: string) =>{
    return this.ownerForm.controls[controlName].hasError(errorName);
  }

}


@Component({
  selector: 'edit-dialog',
  templateUrl: 'nexgenecmessetting.editdialog.html',
})
export class DialogNexgenecMesSettingComponent implements OnInit{
  action:string;
  local_data:any;
  public ownerForm: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<DialogNexgenecMesSettingComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {
    //console.log(data);
    this.local_data = {...data};
    this.action = this.local_data.action;
  }

  doAction(){
    //this.dialogRef.close({data:this.local_data});
    this.dialogRef.close({data:this.ownerForm.value});
  }

  closeDialog(){
    this.dialogRef.close({event:'Cancel'});
  }

  ngOnInit() {
    this.ownerForm = new FormGroup({
      seq_id: new FormControl(this.local_data.seq_id, []),
      environment: new FormControl(this.local_data.environment, [Validators.required, Validators.maxLength(30)]),
      section_id: new FormControl(this.local_data.section_id, [Validators.required, Validators.maxLength(30)]),
      connection_mode: new FormControl(this.local_data.connection_mode, [Validators.required, Validators.maxLength(30)]),
      connection_host: new FormControl(this.local_data.connection_host, [Validators.required, Validators.maxLength(20)]),
      connection_port: new FormControl(this.local_data.connection_port, [Validators.required, Validators.maxLength(10)]),
      default_userid: new FormControl(this.local_data.default_userid, [Validators.maxLength(30)]),
      default_password: new FormControl(this.local_data.default_password, [Validators.maxLength(30)]),
      connection_type: new FormControl(this.local_data.connection_type, [Validators.maxLength(30)]),
      updated_by: new FormControl(this.local_data.updated_by, []),
      timestamp: new FormControl(this.local_data.timestamp, []),
      

    });
  }

  public hasError = (controlName: string, errorName: string) =>{
    return this.ownerForm.controls[controlName].hasError(errorName);
  }

}

@Component({
  selector: 'edit-dialog',
  templateUrl: 'nexgeneceqptconfig.editdialog.html',
})
export class DialogNexgenecEqptConfigComponent implements OnInit{
  action:string;
  local_data:any;
  public ownerForm: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<DialogNexgenecEqptConfigComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {
    console.log(data);
    this.local_data = {...data[0]};
    if(this.local_data.section_id==null){
      this.local_data.section_id = data[1].section;
    }
    this.action = this.local_data.action;
  }

  doAction(){
    this.dialogRef.close({data:this.ownerForm.value});
  }

  closeDialog(){
    this.dialogRef.close({event:'Cancel'});
  }

  ngOnInit() {
    this.ownerForm = new FormGroup({
      seq_id: new FormControl(this.local_data.seq_id, []),
      eqpt_id: new FormControl(this.local_data.eqpt_id, []),
      order_no: new FormControl(this.local_data.order_no, [Validators.maxLength(38)]),
      section_id: new FormControl(this.local_data.section_id, [Validators.required, Validators.maxLength(30)]),
      commented: new FormControl(this.local_data.commented, [Validators.maxLength(10)]),
      key: new FormControl(this.local_data.key, [Validators.required, Validators.maxLength(50)]),
      value: new FormControl(this.local_data.value, [Validators.required, Validators.maxLength(256)]),
      updated_by: new FormControl(this.local_data.updated_by, []),
      timestamp: new FormControl(this.local_data.timestamp, []),
      version: new FormControl(this.local_data.version, []),
      old_value: new FormControl(this.local_data.old_value, []),
      is_modified: new FormControl(this.local_data.is_modified, []),
      state: new FormControl(this.local_data.state, []),
    });
  }

  public hasError = (controlName: string, errorName: string) =>{
    return this.ownerForm.controls[controlName].hasError(errorName);
  }

}

@Component({
  selector: 'app-nexgeneceqptconfig',
  templateUrl: './eqpt-config.component.html',
  styleUrls: ['./eqpt-config.component.scss']
})

export class NexgenecEqptConfigComponent extends BasePageComponent implements OnInit, OnDestroy {
  // eqpt_id = "";

  displayedColumns: string[];
  database: NexgenecEqptConfigDatabase;
  dataSource: NexgenecEqptConfigDataSource | null;

  displayedColumns2: string[];
  database2: NexgenecEqptConnectionDatabase;
  dataSource2: NexgenecEqptConnectionDataSource | null;

  // displayedColumns3: string[];
  // database3: NexgenecMesSettingDatabase;
  // dataSource3: NexgenecMesSettingDataSource | null;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatPaginator, { static: true }) paginator2: MatPaginator;
  // @ViewChild(MatPaginator, { static: true }) paginator3: MatPaginator;

  eqpt_id = "";
  data = {
    eqpt_id: "",
    section: "",
    version: 1,
    state: "Editting",
  }

  //icon-sli-target':'icon-sli-note
  btnStateIcon = "icon-sli-target"
  btnStateText = "Request Approve"
  listSection = []
  listVersion = []

  constructor(store: Store<AppState>,
    private eqpSvConfig: NexgenecEqptConfigService,
    private eqpSvConnection: NexgenecEqptConnectionService,
    private eqpSvMes: NexgenecMesSettingService,
    public dialog: MatDialog,
    private actRoute: ActivatedRoute,
    private router: Router
  ) {
    super(store);
    
    

    this.pageData = {
      title: 'Equipment Configuration',
      loaded: true,
      breadcrumbs: [
      ]
    };
    
    this.displayedColumns = [
      // 'seq_id',
      // 'eqpt_id',
      // 'section_id',
      // 'order_no',
      'key',
      'value',
      'commented',
      // 'updated_by',
      // 'timestamp',
      'seq_id',
    ];
    this.database = new NexgenecEqptConfigDatabase(eqpSvConfig, this);

    this.displayedColumns2 = [
      'connection_type',
      'connection_host',
      'connection_port',
      'environment',
      'username',
      'password_enc',
      'seq_id',
    ];
    this.database2 = new NexgenecEqptConnectionDatabase(eqpSvConnection, this.data);

    this.actRoute.paramMap.subscribe(params => {
      this.data.eqpt_id = params.get('id');
      this.eqpt_id = this.data.eqpt_id;
      this.database.search(this.data)
      this.database2.search(this.data)
    });
  }

  ngOnInit() {
    super.ngOnInit();
    this.dataSource = new NexgenecEqptConfigDataSource(this.database, this.paginator);
    this.dataSource2 = new NexgenecEqptConnectionDataSource(this.database2, this.paginator2);
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }
  change_section(event){
    this.data.section =  event.value;
    this.search()
  }
  change_version(event){
    this.data.version =  event.value;
    this.search()
  }
  search(){
    this.database.search(this.data);
  }
  updateButtonState(){
    this.btnStateText = "Request Approve"
    if(this.data.state=="Editting"){
      this.btnStateText = "Request Approve"
    }
    else if(this.data.state=="Requesting"){
      this.btnStateText = "Clone new version"
    }
    else if(this.data.state=="Approved"){
      this.btnStateText = "Clone new version"
    }
    else if(this.data.state=="Cancelled"){
      this.btnStateText = "Request Approve"
    }
    else if(this.data.state=="Rejected"){
      this.btnStateText = "Request Approve"
    }
  }
  cloneNewversion(){
    let dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '500px',  
      data:"Do you confirm to do ["+this.btnStateText+"] ?"
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result.result == true) {
        this.eqpSvConfig.send(
          {
            action: this.btnStateText,
            version: this.data.version,
            eqpt_id: this.data.eqpt_id,
          }
        )
          .subscribe(
            response => {
              console.log(response)
              this.search()
            }
        );
      }
    })
  }
  approve(){
    if(this.btnStateText=='Request Approve'){
      this.requestApprove();
    }
    else{
      this.cloneNewversion();
    }
    
  }
  requestApprove(){
    this.eqpSvConfig.get(
      "/user/?"
    )
      .subscribe(
        response => {
          console.log(response);

          const dialogRef = this.dialog.open(DialogRequestApprove, {
            width: '500px',
            data:{
              commented:"Request comment", 
              approved_by:"",
              list_user: response
            }
          });
      
          dialogRef.afterClosed().subscribe(result => {
              console.log(result)
              if (result.result == true) {
                this.eqpSvConfig.send(
                  {
                    action: this.btnStateText,
                    version: this.data.version,
                    eqpt_id: this.data.eqpt_id,
                    commented: result.commented,
                    approved_by: result.approved_by
                  }
                )
                  .subscribe(
                    response => {
                      console.log(response)
                      this.search()
                    }
                );
              }
          });



        }
    );

    
  }

  openDialogEqptConfig(event, item) {
    console.log("openDialogEqptConfig "+event )
    console.log(item)
    if(event=="Delete"){
      let dialogRef = this.dialog.open(ConfirmationDialogComponent, {
        width: '500px',  
        data:"Do you confirm the deletion of this data?"
      });
      dialogRef.afterClosed().subscribe(result => {
        // NOTE: The result can also be nothing if the user presses the `esc` key or clicks outside the dialog
        console.log(result.result);
        if (result.result == true) {
            this.eqpSvConfig.delete(item.seq_id)
              .subscribe(
                response => {
                  console.log(response)
                  this.search()
                }
            );
        }
      })
    }
    else{
    const dialogRef = this.dialog.open(DialogNexgenecEqptConfigComponent, {
      width: '500px',
      data:[item, this.data]
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result==''){
      }
      else{
        if(event=="New"){
          if(result.data.eqpt_id==null){
            result.data.eqpt_id = this.eqpt_id;
          }
          console.log(result)
          this.eqpSvConfig.save(result)
            .subscribe(
                response => {
                  console.log(response);
                  this.search()
                }
            );
        }
        else if(event=="Update"){
          this.eqpSvConfig.update(result.data.seq_id, result)
            .subscribe(
                response => {
                  console.log(response);
                  this.search()
                }
            );
        }
      }
    });
    }
  }
  openDialogEqptConnection(event, item) {
    console.log("openDialogEqptConnection "+event )
    console.log(item)
    
    if(event=="Delete"){
      let dialogRef = this.dialog.open(ConfirmationDialogComponent, {
        width: '500px',  
        data:"Do you confirm the deletion of this data?"
      });
      dialogRef.afterClosed().subscribe(result => {
        // NOTE: The result can also be nothing if the user presses the `esc` key or clicks outside the dialog
        console.log(result.result);
        if (result.result == true) {
            this.eqpSvConnection.delete(item.seq_id)
              .subscribe(
                response => {
                  console.log(response)
                  this.database2.search(this.data.eqpt_id)
                }
            );
        }
      })
    }
    else{
    const dialogRef = this.dialog.open(DialogNexgenecEqptConnectionComponent, {
      width: '500px',
      data:item
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result==''){
      }
      else{
        if(event=="New"){
          if(result.data.eqpt_id==null){
            result.data.eqpt_id = this.eqpt_id;
          }
          console.log(result)
          this.eqpSvConnection.save(result)
            .subscribe(
                response => {
                  console.log(response);
                  this.database2.search(this.data.eqpt_id)
                }
            );
        }
        else if(event=="Update"){
          this.eqpSvConnection.update(result.data.seq_id, result)
            .subscribe(
                response => {
                  console.log(response);
                  this.database2.search(this.data.eqpt_id)
                }
            );
        }
      }
    });
    }
  }
}

/** An example database that the data source uses to retrieve data for the table. */
export class NexgenecEqptConfigDatabase {
  
  /** Stream that emits whenever the data has been modified. */
  dataChange: BehaviorSubject<NexgenecEqptConfig[]> = new BehaviorSubject<NexgenecEqptConfig[]>([]);

  get data(): NexgenecEqptConfig[] {
    return this.dataChange.value; 
  }
  objNexgenecEqptConfigComponent: any;
  constructor(private eqpSer: NexgenecEqptConfigService, objNexgenecEqptConfigComponent:NexgenecEqptConfigComponent) {
    this.eqpSer = eqpSer;
    this.objNexgenecEqptConfigComponent = objNexgenecEqptConfigComponent;
    this.search(this.objNexgenecEqptConfigComponent.data);
  }

  search(data: any) {
    console.log(data)
    if(data.eqpt_id.length==0){
      return;
    }
    this.eqpSer.findByQuery(
      { 
        eqpt_id:data.eqpt_id,
        section:data.section,
        version:data.version,
        view:"listmain",
      }).subscribe(
      data => {
        console.log(data)
        var arr = <Array<any>>data[0];

        // console.log(arr)
        this.objNexgenecEqptConfigComponent.listVersion = data[1]
        this.objNexgenecEqptConfigComponent.listSection = data[2]
        this.objNexgenecEqptConfigComponent.data.section = data[3]
        if(arr.length>0){
          this.objNexgenecEqptConfigComponent.data.state = arr[0].state;
        }

        this.objNexgenecEqptConfigComponent.updateButtonState();
        const copiedData = [];
        arr.forEach(function (value) {
            copiedData.push( value);
        });
        this.dataChange.next(copiedData);
        return data;
      },
      err => {
        console.log(err);
      },
      () => {
        //(callbackFnName && typeof this[callbackFnName] === 'function') ? this[callbackFnName](this[dataName]) : null;
      }
    );
  }

}

/**
 * Data source to provide what data should be rendered in the table. Note that the data source
 * can retrieve its data in any way. In this case, the data source is provided a reference
 * to a common data base, NexgenecEqptConfigDatabase. It is not the data source's responsibility to manage
 * the underlying data. Instead, it only needs to take the data and send the table exactly what
 * should be rendered.
 */
export class NexgenecEqptConfigDataSource extends DataSource<any> {
  constructor(private _database: NexgenecEqptConfigDatabase, private _paginator: MatPaginator) {
    super();
  }

  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<NexgenecEqptConfig[]> {


    const displayDataChanges = [
      this._database.dataChange,
      this._paginator.page,
    ];

    return merge(...displayDataChanges).pipe(
            map(() => {
                const data = this._database.data.slice();

                // Grab the page's slice of data.
                const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
                return data.splice(startIndex, this._paginator.pageSize);
            })
        );
  }

  disconnect() {}
}

/** An example database that the data source uses to retrieve data for the table. */
export class NexgenecEqptConnectionDatabase {
  
  /** Stream that emits whenever the data has been modified. */
  dataChange: BehaviorSubject<NexgenecEqptConnection[]> = new BehaviorSubject<NexgenecEqptConnection[]>([]);

  get data(): NexgenecEqptConnection[] {
    return this.dataChange.value; 
  }
  
  constructor(private eqpSer: NexgenecEqptConnectionService, data) {
    this.eqpSer = eqpSer;
    this.search(data);
  }

  search(eqpt_id: any) {
    if(eqpt_id.length==0){
      return;
    }
    this.eqpSer.findByQuery(
      { 
        eqpt_id:eqpt_id
      }).subscribe(
      data => {
        var arr = <Array<any>>data;
        const copiedData = [];
        arr.forEach(function (value) {
            copiedData.push( value);
        });
        this.dataChange.next(copiedData);
      },
      err => {
        console.log(err);
      },
      () => {
        //(callbackFnName && typeof this[callbackFnName] === 'function') ? this[callbackFnName](this[dataName]) : null;
      }
    );
  }

}

/**
 * Data source to provide what data should be rendered in the table. Note that the data source
 * can retrieve its data in any way. In this case, the data source is provided a reference
 * to a common data base, NexgenecEqptConnectionDatabase. It is not the data source's responsibility to manage
 * the underlying data. Instead, it only needs to take the data and send the table exactly what
 * should be rendered.
 */
export class NexgenecEqptConnectionDataSource extends DataSource<any> {
  constructor(private _database: NexgenecEqptConnectionDatabase, private _paginator: MatPaginator) {
    super();
  }

  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<NexgenecEqptConnection[]> {


    const displayDataChanges = [
      this._database.dataChange,
      this._paginator.page,
    ];

    return merge(...displayDataChanges).pipe(
            map(() => {
                const data = this._database.data.slice();

                // Grab the page's slice of data.
                const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
                return data.splice(startIndex, this._paginator.pageSize);
            })
        );
  }

  disconnect() {}
}
