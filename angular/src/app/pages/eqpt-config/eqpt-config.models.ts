import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CrudService } from '../../services/crud.service';
import { HttpResponse ,HttpHeaders} from '@angular/common/http';

export class NexgenecMesSetting {
    seq_id : any;
    environment : any;
    section_id : any;
    connection_mode : any;
    connection_host : any;
    connection_port : any;
    default_userid : any;
    default_password : any;
    connection_type : any;
    updated_by : any;
    timestamp : any;
}

@Injectable({
    providedIn: 'root',
  })
  export class NexgenecMesSettingService extends CrudService<NexgenecMesSetting, number> {
    constructor(protected _http: HttpClient) {
      super(_http, "eqp/mes");
    }
}

export class NexgenecEqptConnection {
  seq_id : any;
  eqpt_id : any;
  section_id : any;
  connection_mode : any;
  connection_type : any;
  connection_host : any;
  connection_port : any;
  environment : any;
  username : any;
  password_enc : any;
  config : any;
  updated_by : any;
  timestamp : any;
}

@Injectable({
  providedIn: 'root',
})
export class NexgenecEqptConnectionService extends CrudService<NexgenecEqptConnection, number> {
  constructor(protected _http: HttpClient) {
    super(_http, "eqp/connection");
  }
}

export class NexgenecEqptConfig {
  seq_id : any;
  eqpt_id : any;
  section_id : any;
  order_no : any;
  commented : any;
  key : any;
  value : any;
  updated_by : any;
  timestamp : any;
  site_id : any;
  operation_id : any;
  version : any;
  old_value : any;
  is_modified : any;
  state : any;
}

@Injectable({
  providedIn: 'root',
})
export class NexgenecEqptConfigService extends CrudService<NexgenecEqptConfig, number> {
  constructor(protected _http: HttpClient) {
    super(_http, "eqp/config");
  }
}