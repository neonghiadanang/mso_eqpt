import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NexgenecNexgenecEqptLoggerComponent } from './eqpt-logger.component';

describe('NexgenecEqptLoggerComponent', () => {
  let component: NexgenecEqptLoggerComponent;
  let fixture: ComponentFixture<NexgenecEqptLoggerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NexgenecEqptLoggerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NexgenecEqptLoggerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
