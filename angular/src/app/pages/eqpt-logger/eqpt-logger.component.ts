import { Component, ViewChild, OnInit, OnDestroy,Inject } from '@angular/core';
import { DataSource} from '@angular/cdk/collections';
import { MatPaginator } from '@angular/material/paginator';
import { BehaviorSubject, Observable, merge } from 'rxjs';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { map } from 'rxjs/operators';
import { BasePageComponent } from '../base-page/base-page.component';
import { Store } from '@ngrx/store';
import { AppState } from '../../interfaces/app-state';

import { MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatDialogModule } from '@angular/material/dialog';
import { NexgenecEqptLogger, NexgenecEqptLoggerService } from './eqpt-logger.models'
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { ConfirmationDialogComponent } from '../../ui/components/confirmation-dialog/confirmation-dialog.component';

export interface DialogData {
  animal: string;
  name: string;
}

@Component({
  selector: 'edit-dialog',
  templateUrl: 'edit.dialog.html',
})
export class DialogNexgenecEqptLoggerComponent implements OnInit{
  action:string;
  local_data:any;
  public ownerForm: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<DialogNexgenecEqptLoggerComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {
    //console.log(data);
    this.local_data = {...data};
    this.action = this.local_data.action;
  }

  doAction(){
    //this.dialogRef.close({data:this.local_data});
    this.dialogRef.close({data:this.ownerForm.value});
  }

  closeDialog(){
    this.dialogRef.close({event:'Cancel'});
  }

  ngOnInit() {
    this.ownerForm = new FormGroup({
      seq_id: new FormControl(this.local_data.seq_id, []),
      eqpt_id: new FormControl(this.local_data.eqpt_id, []),
      section_id: new FormControl(this.local_data.section_id, [Validators.required, Validators.maxLength(30)]),
      commented: new FormControl(this.local_data.commented, [Validators.maxLength(10)]),
      key: new FormControl(this.local_data.key, [Validators.required, Validators.maxLength(30)]),
      value: new FormControl(this.local_data.value, [Validators.required, Validators.maxLength(256)]),
      order_no: new FormControl(this.local_data.order_no, []),
      updated_by: new FormControl(this.local_data.updated_by, []),
      timestamp: new FormControl(this.local_data.timestamp, []),
    });
  }

  public hasError = (controlName: string, errorName: string) =>{
    return this.ownerForm.controls[controlName].hasError(errorName);
  }

}

@Component({
  selector: 'app-nexgeneceqptlogger',
  templateUrl: './eqpt-logger.component.html',
  styleUrls: ['./eqpt-logger.component.scss']
})

export class NexgenecEqptLoggerComponent extends BasePageComponent implements OnInit, OnDestroy {
  displayedColumns: string[];
  database: NexgenecEqptLoggerDatabase;
  dataSource: NexgenecEqptLoggerDataSource | null;
  eqpt_id = "";

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(store: Store<AppState>,
    private eqpSv: NexgenecEqptLoggerService,
    public dialog: MatDialog,
    private actRoute: ActivatedRoute,
    private router: Router
  ) {
    super(store);

    this.pageData = {
      title: 'Equipment Logger',
      loaded: true,
      breadcrumbs: [
      ]
    };
    
    this.displayedColumns = [
      // 'eqpt_id',
      'order_no',
      'section_id',
      'key',
      'value',
      'commented',
      // 'updated_by',
      // 'timestamp',
      'seq_id',
    ];
    this.database = new NexgenecEqptLoggerDatabase(eqpSv);
    this.actRoute.paramMap.subscribe(params => {
      this.eqpt_id = params.get('id');
      this.search('','')
    });
  }

  ngOnInit() {
    super.ngOnInit();
    this.dataSource = new NexgenecEqptLoggerDataSource(this.database, this.paginator);
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }
 
  search(id,name){
    console.log("====================================this.eqpt_id= "+this.eqpt_id)
    this.database.search(this.eqpt_id,id);
  }
  openDialog(event, item) {
    console.log("openDialog "+event )
    console.log(item)
    
    if(event=="Delete"){
      let dialogRef = this.dialog.open(ConfirmationDialogComponent, {
        width: '500px',  
        data:"Do you confirm the deletion of this data?"
      });
      dialogRef.afterClosed().subscribe(result => {
        // NOTE: The result can also be nothing if the user presses the `esc` key or clicks outside the dialog
        console.log(result.result);
        if (result.result == true) {
            this.eqpSv.delete(item.seq_id)
              .subscribe(
                response => {
                  console.log(response)
                  this.search('','')
                }
            );
        }
      })
    }
    else{
      const dialogRef = this.dialog.open(DialogNexgenecEqptLoggerComponent, {
        width: '500px',
        data:item
      });

      dialogRef.afterClosed().subscribe(result => {
        if(result==''){
        }
        else{
          if(event=="Update"){
            if(result.data.eqpt_id==null){
              result.data.eqpt_id = this.eqpt_id;
            }
            this.eqpSv.update(result.data.seq_id, result)
              .subscribe(
                  response => {
                    console.log(response);
                    this.search('','');
                  }
              );
          }
          else if(event=="New"){
            if(result.data.eqpt_id==null){
              result.data.eqpt_id = this.eqpt_id;
            }
            this.eqpSv.save(result)
              .subscribe(
                  response => {
                    console.log(response);
                    this.search('','');
                  }
              );
          }
        }
      });
    }
  }
}

/** An example database that the data source uses to retrieve data for the table. */
export class NexgenecEqptLoggerDatabase {
  
  /** Stream that emits whenever the data has been modified. */
  dataChange: BehaviorSubject<NexgenecEqptLogger[]> = new BehaviorSubject<NexgenecEqptLogger[]>([]);

  get data(): NexgenecEqptLogger[] {
    return this.dataChange.value; 
  }
  
  constructor(private eqpSer: NexgenecEqptLoggerService) {
    this.eqpSer = eqpSer;
    this.search('','');
  }

  search(eqpt_id: any, key: any) {
    if(eqpt_id.length==0){
      return;
    }
    this.eqpSer.findByQuery(
      { 
        "eqpt_id":eqpt_id,
        "key":key
      }).subscribe(
      data => {
        var arr = <Array<any>>data;
        const copiedData = [];
        arr.forEach(function (value) {
            copiedData.push( value);
        });
        this.dataChange.next(copiedData);
      },
      err => {
        console.log(err);
      },
      () => {
        //(callbackFnName && typeof this[callbackFnName] === 'function') ? this[callbackFnName](this[dataName]) : null;
      }
    );
  }

}

/**
 * Data source to provide what data should be rendered in the table. Note that the data source
 * can retrieve its data in any way. In this case, the data source is provided a reference
 * to a common data base, NexgenecEqptLoggerDatabase. It is not the data source's responsibility to manage
 * the underlying data. Instead, it only needs to take the data and send the table exactly what
 * should be rendered.
 */
export class NexgenecEqptLoggerDataSource extends DataSource<any> {
  constructor(private _database: NexgenecEqptLoggerDatabase, private _paginator: MatPaginator) {
    super();
  }

  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<NexgenecEqptLogger[]> {


    const displayDataChanges = [
      this._database.dataChange,
      this._paginator.page,
    ];

    return merge(...displayDataChanges).pipe(
            map(() => {
                const data = this._database.data.slice();

                // Grab the page's slice of data.
                const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
                return data.splice(startIndex, this._paginator.pageSize);
            })
        );
  }

  disconnect() {}
}