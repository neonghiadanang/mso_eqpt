import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CrudService } from '../../services/crud.service';
import { HttpResponse ,HttpHeaders} from '@angular/common/http';

export class NexgenecEqptLogger {
    seq_id : any;
    eqpt_id : any;
    section_id : any;
    commented : any;
    key : any;
    value : any;
    order_no : any;
    updated_by : any;
    timestamp : any;
}

@Injectable({
    providedIn: 'root',
  })
  export class NexgenecEqptLoggerService extends CrudService<NexgenecEqptLogger, number> {
    constructor(protected _http: HttpClient) {
      super(_http, "eqptlogger");
    }
}