import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EqptMachineStateComponent } from './eqpt-machine-state.component';

describe('EqptMachineStateComponent', () => {
  let component: EqptMachineStateComponent;
  let fixture: ComponentFixture<EqptMachineStateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EqptMachineStateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EqptMachineStateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
