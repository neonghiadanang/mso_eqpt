import {  ViewEncapsulation } from '@angular/core';
import { Component, ViewChild, OnInit, OnDestroy,Inject } from '@angular/core';

import * as go from 'gojs';
import { DataSyncService } from 'gojs-angular';
import * as _ from 'lodash';
import { BasePageComponent } from '../base-page/base-page.component';
import { Store } from '@ngrx/store';
import { AppState } from '../../interfaces/app-state';
import { HttpService } from '../../services/http/http.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { FormBuilder, AbstractControl } from '@angular/forms';
import { ElementRef, Input, Output, EventEmitter } from '@angular/core';

import * as SettingsActions from '../../store/actions/app-settings.actions';

import {MachineStateModel } from './state-machine-model';
import {MachineStateDiagram } from './state-machine-diagram';
import { Configuration } from '../../../app/configuration';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { ConfirmationDialogComponent } from '../../ui/components/confirmation-dialog/confirmation-dialog.component';

export interface DialogData {
  animal: string;
  name: string;
}
@Component({
  selector: 'edit-dialog',
  templateUrl: 'edit.dialog.node.html',
})
export class DialogEditNodeComponent implements OnInit{
  action:string;
  local_data:any;
  public ownerForm: FormGroup;
  list_state:any;
  list_Group:any;
  loadStateName:any;
  group_state_text:any;
  constructor(
    public dialogRef: MatDialogRef<DialogEditNodeComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {
    this.local_data = {...data[0]};
    this.loadStateName = this.local_data.text;
    this.list_state = data[1];
    this.list_Group = data[2];
    this.action = this.local_data.action;
  }

  doAction(){
    this.dialogRef.close({data:this.ownerForm.value});
  }

  closeDialog(){
    this.dialogRef.close({event:'Cancel'});
  }

  ngOnInit() {
    console.log(this.local_data);
    this.ownerForm = new FormGroup({
      id: new FormControl(this.local_data.id, []),
      loc: new FormControl(this.local_data.loc, []),
      //text: new FormControl(this.local_data.text, [this.checkExistNodename, Validators.required, Validators.maxLength(50)]),
      text: new FormControl(this.local_data.text, []),
      group_state: new FormControl(this.local_data.group, []),
      category: new FormControl(this.local_data.category?this.local_data.category:"", []),
      color: new FormControl(this.local_data.color, []),
      group_state_text: new FormControl(this.group_state_text, []),
    });
  }
  public hasError = (controlName: string, errorName: string) =>{
    return this.ownerForm.controls[controlName].hasError(errorName);
  }

  public checkExistNodename = (input: FormControl) => {
    for( let key in this.list_state){
      //console.log(this.list_state[key].value)
      if( input.value==this.list_state[key].value && this.loadStateName!=input.value){
        return { existnodename: true }
      }
    }
    return null;
  }
}


@Component({
  selector: 'edit-dialog-link',
  templateUrl: 'edit.dialog.link.html',
})
export class DialogEditLinkComponent implements OnInit{
  action:string;
  local_data:any;
  public ownerForm: FormGroup;
  list_state:any;
  
  constructor(
    public dialogRef: MatDialogRef<DialogEditLinkComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {
    console.log(data);
    this.local_data = {...data[0]};
    this.list_state = data[1];
    this.action = this.local_data.action;
    // console.log(this.list_state);
  }

  doAction(){
    this.dialogRef.close({data:this.ownerForm.value});
  }

  closeDialog(){
    this.dialogRef.close({event:'Cancel'});
  }

  ngOnInit() {
    this.ownerForm = new FormGroup({
      from: new FormControl(this.local_data.from, [Validators.required, Validators.maxLength(50)]),
      to: new FormControl(this.local_data.to, [Validators.required, Validators.maxLength(50)]),
      Trigger: new FormControl(this.local_data.Trigger, [Validators.required, Validators.maxLength(50)]),
      Function: new FormControl(this.local_data.Function, [Validators.required, Validators.maxLength(50)]),
      GuardCond: new FormControl(this.local_data.GuardCond, [Validators.maxLength(50)]),
      GuardVal: new FormControl(this.local_data.GuardVal, [Validators.maxLength(50)]),
      isPass: new FormControl(this.local_data.isPass, []),
      __gohashid: new FormControl(this.local_data.__gohashid, [])
    });
  }
  public hasError = (controlName: string, errorName: string) =>{
    return this.ownerForm.controls[controlName].hasError(errorName);
  }
}


export interface state_machine{
  statemachine_id : any;
  view_mode : any;
  view_type : any;
}

@Component({
  selector: 'app-eqpt-machine-state',
  templateUrl: './eqpt-machine-state.component.html',
  styleUrls: ['./eqpt-machine-state.component.scss']
})
export class EqptMachineStateComponent extends BasePageComponent implements OnInit, OnDestroy {
  model: any;
  listDiagram: any;
  private _diagram: go.Diagram;
  eqpt_id = "";

  state_machine = {
    statemachine_id : "",
    view_mode:"Ladder",
    view_type:"good",
    view_function:"simple",
  }
  listMode = [
    "Ladder",
    "Zigzag",
    "Auto"
  ]
  constructor(store: Store<AppState>
    ,private httpSv: HttpService,
    private actRoute: ActivatedRoute,
    private fb: FormBuilder,
    public dialog: MatDialog
  ) {
    super(store);

    this.pageData = {
      title: 'State Machine',
      loaded: true,
      breadcrumbs: [
      ]
    };

    this.actRoute.paramMap.subscribe(params => {
      this.eqpt_id = params.get('id');
    });
  }

  ngOnInit() {
    super.ngOnInit();
    this.state_machine = {
      statemachine_id : "",
      view_mode:"Ladder",
      view_type:"good",
      view_function: "simple"
    }
    this.loadDiagram()
    console.log("end init")
  }
  change_view_mode(event){
    this.state_machine.view_mode =  event.value;
    this.loadDiagram()
  }
  change_view_type(event){
    console.log(event)
    this.state_machine.view_type =  event.value;
    this.loadDiagram()
    // this.redrawModel();
  }
  change_view(event){
    console.log(event)
    this.state_machine.view_function =  event.value;
    if(this.state_machine.view_function=="overview"){
      this.state_machine.view_type = "all"
      this.loadDiagram()
    }
    else{
      this.loadDiagram()
    }
  }

  btnTextSaving = "Save Layout";
  saveModel(){
    console.log("Save model");
    const json = MachineStateDiagram.toJson();
    console.log(json);
    if(json==null){
      return;
    }
    this.btnTextSaving = "Saving...";
    var src = Configuration.server + "/eqpt/statemachine/?"+
            "eqpt_id="+this.eqpt_id+
            "&statemachine_id="+this.state_machine.statemachine_id+
            "&view_mode="+this.state_machine.view_mode+
            "&view_function="+this.state_machine.view_function+
            "&view_type="+this.state_machine.view_type+
            "&view_line=no";
      // console.log("Load diagram: " +src)
      this.httpSv.postData(src, {layout_json:json}).subscribe(
        data => {
          console.log( data );
          this.btnTextSaving = "Save Layout";
        },
        err => {
          this.btnTextSaving = "Save Layout"; 
          console.error(err)
        }
      );
  }
  change_statemachine_id(event){
    this.state_machine.statemachine_id =  event.value;
    this.loadDiagram()
  }
  loadDiagram(){
    var src = Configuration.server + "/eqpt/statemachine/?"+
            "eqpt_id="+this.eqpt_id+
            "&statemachine_id="+this.state_machine.statemachine_id+
            "&view_mode="+this.state_machine.view_mode+
            "&view_function="+this.state_machine.view_function+
            "&view_type="+this.state_machine.view_type+
            "&view_line=no";
      console.log("Load diagram: " +src)
      this.httpSv.getData(src).subscribe(
        data => {
          console.log( data );
          this.listDiagram = data.list;
          this.state_machine.statemachine_id = data.statemachine_id;
          // if(data.model!=null)
          // {
              this.model = data.model;
              const modelJson = this.model==null?data.jsonModel:this.buildModelFromTable(this.model)
              // if(this._diagram!=null){
              //   this._diagram.div = null;
              // }
              this._diagram = this.initDiagram(modelJson, this.model);
          // }
        },
        err => console.error(err)
      );
  }

  initDiagram(modelJson,model){
    var autoLayout = true;
    if(model!=null){
      if(this.state_machine.view_mode=="Auto"){

      }
      else{
        autoLayout = false;
      }
    }
    else{
      autoLayout = false;
    }
    if(autoLayout==true){
      console.log("autoLayout="+autoLayout)
    }
    else{
      console.log("autoLayout="+autoLayout)
    }
    // console.log(modelJson)
    // if(MachineStateDiagram.dia!=null){
    //   console.log("Reset diagram div")
    //   MachineStateDiagram.dia.div=null;
    // }
    // autoLayout = true;
    if(this.state_machine.view_function=="overview")
      return MachineStateDiagram.initDiagramOverview(this, modelJson, autoLayout);
    if(this.state_machine.view_function=="simple")
      return MachineStateDiagram.initDiagramSimple(this, modelJson, autoLayout);
    return MachineStateDiagram.initDiagramFullMode(this, modelJson, autoLayout);
  }

  buildModelFromTable(model){
    var showMode = this.state_machine.view_mode;
    var showPASS = this.state_machine.view_type=='good';
    var showFAIL = this.state_machine.view_type=='fail';
    var showALL = this.state_machine.view_type=='all';
    var showSimple = this.state_machine.view_function=='simple' || this.state_machine.view_function=='overview';              
    var showGroup = this.state_machine.view_function=='overview';
    return MachineStateModel.buildModelFromTable(this.model, showMode, showPASS, showFAIL, showALL, showSimple, showGroup)
  }

  editNode(e, data){
    return this.openDialogNode("Update", data)
  }
  editLink(e, data){
    return this.openDialogLink("Update", data)
  }

  openDialogNode(event, item) {
    // console.log("openDialogNode "+event )
    // console.log(item)
    
    if(event=="Delete"){
      let dialogRef = this.dialog.open(ConfirmationDialogComponent, {
        width: '500px',  
        data:"Do you confirm the deletion of this data?"
      });
      dialogRef.afterClosed().subscribe(result => {
        // NOTE: The result can also be nothing if the user presses the `esc` key or clicks outside the dialog
        console.log(result.result);
        if (result.result == true) {
            // this.eqpSv.delete(item.seq_id)
            //   .subscribe(
            //     response => {
            //       console.log(response)
            //     }
            // );
        }
      })
    }
    else{
      const arrNode = [];
      const arrGroup = new Map();
      for( let nod of this._diagram.model.nodeDataArray){
        console.log(nod)
        if('group' in nod && nod.group.length>0){
          const groupName = nod.group.trim();
          arrGroup.set(groupName, {
            "value":nod.group.trim(),
            "text":this.replaceNodeText(nod.group.trim())
          });
        }
        else{
          arrNode.push({
            "value":nod.id.trim(),
            "text":this.replaceNodeText(nod.text.trim())
          });
        }
      }
      item.text = this.replaceNodeText(item.text.trim())
      const dialogRef = this.dialog.open(DialogEditNodeComponent, {
        width: '500px',
        data:[item,arrNode,Array.from(arrGroup.values())]
      });

      dialogRef.afterClosed().subscribe(result => {
          if(result==''){
          }
          else{
            if(event=="Update"){
              console.log(result)
              // this.eqpSv.save(result)
              //   .subscribe(
              //       response => {
              //         console.log(response);
              //       }
              //   );
              this.updateModelNode(result);
              return result;
            }
            else if(event=="New"){
              console.log(result)
              // this.eqpSv.update(result.data.seq_id, result)
              //   .subscribe(
              //       response => {
              //         console.log(response);
              //       }
              //   );
              return result;
            }
          }
        });
      }
  }


  openDialogLink(event, item) {
    console.log("openDialogLink "+event )
    console.log(item)
    const arrNode = [];
    for( let nod of this._diagram.model.nodeDataArray){
      arrNode.push({
        "value":nod.id.trim(),
        "text":this.replaceNodeText(nod.text.trim())
      });
    }
    if(event=="Delete"){
      let dialogRef = this.dialog.open(ConfirmationDialogComponent, {
        width: '500px',  
        data:"Do you confirm the deletion of this data?"
      });
      dialogRef.afterClosed().subscribe(result => {
        // NOTE: The result can also be nothing if the user presses the `esc` key or clicks outside the dialog
        console.log(result.result);
        if (result.result == true) {
            // this.eqpSv.delete(item.seq_id)
            //   .subscribe(
            //     response => {
            //       console.log(response)
            //     }
            // );
        }
      })
    }
    else{
      item.GuardCond = item.GuardCond==null?"":item.GuardCond;
      item.GuardVal = item.GuardVal==null?"":item.GuardVal;
      item.isPass = item.isPass==null?"PASS":item.isPass;
      const dialogRef = this.dialog.open(DialogEditLinkComponent, {
          width: '500px',
          data:[item, arrNode]
        }
      );

    dialogRef.afterClosed().subscribe(result => {
      if(result==''){
      }
      else{
        if(event=="Update"){
          console.log(result)
          // this.eqpSv.save(result)
          //   .subscribe(
          //       response => {
          //         console.log(response);
          //       }
          //   );
          this.updateModelLink(result.data);
          return result;
        }
        else if(event=="New"){
          console.log(result)
          // this.eqpSv.update(result.data.seq_id, result)
          //   .subscribe(
          //       response => {
          //         console.log(response);
          //       }
          //   );
          return result;
        }
      }
    });
    }
  }

  updateModelNode(node){
    // update node
    for( let nod of this._diagram.model.nodeDataArray){
      if( nod.id==node.data.id){
        var text = node.data.text;//.replace(/[^\x00-\x7F]/g, "");
        nod.text = this.replaceNodeText(text);
        break;
      }
    }
    this._diagram.updateAllTargetBindings();
  }
  replaceNodeText(text){
    text =text.replace('\uf0a3', '')
    text =text.replace('\uf1ce', '')
    text =text.replace(' ', '')
    return text;
  }
  updateModelLink(linkModel){
    // update Link
    // console.log(linkModel);
    // console.log(linkModel.from);
    // console.log(linkModel.__gohashid);
    var nodefrom = this._diagram.findNodeForKey(linkModel.from);
    var nodeto = this._diagram.findNodeForKey(linkModel.to);
    // console.log(nodefrom, nodeto);
    if (nodefrom !== null && nodeto !== null) {
      var it = nodefrom.findLinksBetween(nodeto); // direction matters
      // console.log("The links: ", it);
      while (it.next()) {
          var link = it.value;
          console.log(link.data);
          console.log(link.data.__gohashid);
          if( linkModel.__gohashid==link.data.__gohashid ){
            // update node info
            link.data.GuardCond = linkModel.GuardCond==null?"":linkModel.GuardCond;
            link.data.GuardVal = linkModel.GuardVal==null?"":linkModel.GuardVal;
            link.data.Trigger = linkModel.Trigger;
            link.data.isPass = linkModel.isPass==null?"PASS":linkModel.isPass;
            link.data.Function = linkModel.Function;
            link.data.text = MachineStateModel.getTransition(linkModel.Trigger, linkModel.Function)
            link.data.GuardText = MachineStateModel.getGuardText(linkModel.GuardCond, linkModel.GuardVal)
            
            break;
          }
      }
    }
    this._diagram.updateAllTargetBindings();
  }

  // redrawModel(){
  //   const modelJson = this.buildModelFromTable(this.model);
  //   this._diagram.model = go.Model.fromJson(modelJson);
  // }
  
  ngOnDestroy() {
    super.ngOnDestroy();
  }
}