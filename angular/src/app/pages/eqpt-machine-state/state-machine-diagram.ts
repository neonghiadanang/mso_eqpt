import {  ViewEncapsulation } from '@angular/core';
import { Component, ViewChild, OnInit, OnDestroy,Inject } from '@angular/core';

import * as go from 'gojs';
import { DataSyncService } from 'gojs-angular';
import * as _ from 'lodash';
import { EqptMachineStateComponent } from './eqpt-machine-state.component'

export class MachineStateDiagram{
  static toJson() {
    if( MachineStateDiagram.dia ){
      return MachineStateDiagram.dia.model.toJson();
    }
    return null;
  }
  static myOverview: any;
  static dia: any;
  static eqptComponent:EqptMachineStateComponent;

  static initDiagramOverview(eqptMachineStateComponent:any, modelJson:any, autoLayout:boolean): go.Diagram {
    console.log("initDiagramOverview");
    MachineStateDiagram.eqptComponent = eqptMachineStateComponent;
    const $ = go.GraphObject.make;// for conciseness in defining templates
    // some constants that will be reused within templates
    const roundedRectangleParams = {
      parameter1: 10,  // set the rounded corner - radius conner
      spot1: go.Spot.TopLeft, spot2: go.Spot.BottomRight  // make content go all the way to inside edges of rounded corners
    };
    var idDiagramDiv = document.getElementById("idDiagramDiv");
    if (idDiagramDiv !== null) {
      console.log("Detele div Overview")
      var olddiag = go.Diagram.fromDiv(idDiagramDiv);
      if (olddiag !== null){
        console.log("Set div null")
        olddiag.div = null;
        MachineStateDiagram.dia = null;
        console.log(MachineStateDiagram.dia)
        // console.log(MachineStateDiagram.dia.div)
      } 
    }
    MachineStateDiagram.dia = 
      $(go.Diagram,  "idDiagramDiv",  // must name or refer to the DIV HTML element
        {
        "animationManager.initialAnimationStyle": go.AnimationManager.None,
        "InitialAnimationStarting": function(e) {
            var animation = e.subject.defaultAnimation;
            animation.easing = go.Animation.EaseOutExpo;
            animation.duration = 900;
            animation.add(e.diagram, 'scale', 0.1, 1);
            animation.add(e.diagram, 'opacity', 0, 1);
        },
        initialAutoScale: go.Diagram.UniformToFill,
        layout: autoLayout==true?$(go.LayeredDigraphLayout, {
            direction: 90,
            layerSpacing: 100,
            columnSpacing: 120,
            packOption: 4
        }):$(go.Layout),
        // have mouse wheel events zoom in and out instead of scroll up and down
        "toolManager.mouseWheelBehavior": go.ToolManager.WheelZoom,
        // support double-click in background creating a new node
        // enable undo & redo
        "undoManager.isEnabled": true,
      });

    // dia.commandHandler.archetypeGroupData = { key: 'Group', isGroup: true };
    // define the Node template
    MachineStateDiagram.dia.nodeTemplate =
      $(go.Node, 'Auto',
        {
          contextMenu:
            $('ContextMenu',
              $('ContextMenuButton',
                $(go.TextBlock, 'Group'),
                { click: function(e, obj) { e.diagram.commandHandler.groupSelection(); } },
                new go.Binding('visible', '', function(o) {

                  return o.diagram.selection.count > 1;
                }).ofObject())
            ),
            doubleClick : function(e, obj){
              MachineStateDiagram.editNode(e, obj);
            }
        },
        // Set location
        new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
        $(go.Panel, 'Auto',
          $(go.Shape, "RoundedRectangle", roundedRectangleParams,
          {
            name: "SHAPE", 
            strokeWidth: 0,
            stroke: null,
            portId: "",  // this Shape is the Node's port, not the whole Node
            // fromLinkable: true, fromLinkableSelfNode: true, fromLinkableDuplicates: true,
            // toLinkable: true, toLinkableSelfNode: true, toLinkableDuplicates: true,
            cursor: "pointer",
          }
          
          // ,new go.Binding('fill', 'color'), function(text) {
          //   return text.toUpperCase().indexOf("ERROR")>-1 ? "YELLOW":"GREEN";
          // }),
          ,new go.Binding("fill", "color")
        ),
        $(go.TextBlock, {
                font: "bold small-caps 12pt FontAwesome",
                margin: 17,
                stroke: "rgba(0, 0, 0, .87)",
            },
            new go.Binding('text', 'text', function(text) {
              var txt = text.toUpperCase();
              var icon  = '\uf0a3'; // good
              if(txt.indexOf("ERROR")>-1) icon = '\uf1ce';
              if( txt.indexOf(icon)>-1){
                return text;
              }
                return icon+" "+text;
            }).makeTwoWay())
        ),
      );
      MachineStateDiagram.dia.nodeTemplateMap.add("Start",
      $(go.Node, "Spot", { desiredSize: new go.Size(150, 75) },
        new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
        $(go.Shape, "Ellipse",
          {
            width: 130, height: 60, margin: 4,
            fill: "#52ce60", // green
            stroke: null,
            portId: "",
            // fromLinkable: true, fromLinkableSelfNode: true, fromLinkableDuplicates: true,
            // toLinkable: true, toLinkableSelfNode: true, toLinkableDuplicates: true,
            cursor: "pointer"
          }),
        $(go.TextBlock, 
          //"Start",
          new go.Binding("text").makeTwoWay(),
          {
            font: "bold 18pt helvetica, bold arial, sans-serif",
            stroke: "whitesmoke"
          }),
      )
    );
    MachineStateDiagram.dia.nodeTemplateMap.add("End",
      $(go.Node, "Spot", { desiredSize: new go.Size(100, 100) },
        new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
        $(go.Shape, "Circle",
          {
            fill: "maroon",
            stroke: null,
            portId: "",
            // fromLinkable: true, fromLinkableSelfNode: true, fromLinkableDuplicates: true,
            // toLinkable: true, toLinkableSelfNode: true, toLinkableDuplicates: true,
            cursor: "pointer"
          }),
        $(go.Shape, "Circle", { fill: null, desiredSize: new go.Size(100, 100), strokeWidth: 2, stroke: "whitesmoke" }),
        $(go.TextBlock, 
          new go.Binding("text").makeTwoWay(),
          {
            font: "bold 18pt helvetica, bold arial, sans-serif",
            stroke: "whitesmoke"
          })
      )
    );
    MachineStateDiagram.dia.groupTemplate =
      $(go.Group, "Auto", { // define the group's internal layout
        layout: $(go.TreeLayout, {
          angle: 90,
          arrangement: go.TreeLayout.ArrangementHorizontal,
          isRealtime: false
        }),
        // the group begins unexpanded;
        // upon expansion, a Diagram Listener will generate contents for the group
        isSubGraphExpanded: false,
        // when a group is expanded, if it contains no parts, generate a subGraph inside of it
        subGraphExpandedChanged: function(group) {
          if (group.memberParts.count === 0) {
            //randomGroup(group.data.key);
          }
        },
        doubleClick : function(e, obj){
          MachineStateDiagram.editNode(e, obj);
        }
      },
      new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
      $(go.Shape, "Rectangle", {
        fill: null,
        stroke: "blue",
        strokeWidth: 2
        },
        new go.Binding("fill", "", function(data) {
          return data.isGroup==true?'pink':data.color;
        })
      ),
      $(go.Panel, "Vertical", {
          defaultAlignment: go.Spot.Left,
          margin: 4
        },
        $(go.Panel, "Horizontal", {
            defaultAlignment: go.Spot.Top
          },
          // the SubGraphExpanderButton is a panel that functions as a button to expand or collapse the subGraph
          $("SubGraphExpanderButton"),
          $(go.TextBlock, {
              font: "Bold 18px Sans-Serif",
              margin: 4
            },
          new go.Binding("text", "text"))
        ),
        // create a placeholder to represent the area where the contents of the group are
        $(go.Placeholder, {
          padding: new go.Margin(0, 10)
        })
      ) // end Vertical Panel
    ); // end Group
    // replace the default Link template in the linkTemplateMap
    MachineStateDiagram.dia.linkTemplate =
      $(go.Link,  // the whole link panel
      { 
        curve: go.Link.Bezier, 
        adjusting: go.Link.Bezier, 
        reshapable: true,
        // relinkableFrom: true, 
        // relinkableTo: true, 
        toShortLength: 3,
        doubleClick : function(e, obj){
          MachineStateDiagram.editNode(e, obj);
        }
      },
      new go.Binding("points").makeTwoWay(),
      new go.Binding("curviness"),
      // Link sharp
      $(go.Shape,  // the link shape
        { 
          strokeWidth: 3.5,
        },
        // new go.Binding('strokeDashArray', 'isPass', function(progress) {
        //     return progress=="FAIL" ? [5,2]:null;
        // }),
        new go.Binding('stroke', 'isPass', function(progress) {
            return progress=="FAIL" ? "red":"BLUE";
        }),
        new go.Binding('strokeWidth', 'progress', function(progress) {
          return progress ? 3.5 : 2.5;
        })
      ),
      $(go.Shape,  // the arrowhead
        { toArrow: "standard", stroke: null },
        new go.Binding('fill', 'isPass', function(progress) {
          return progress=="FAIL" ? "red":"BLUE";
        }),
      ),
      $(go.Panel, "Auto",
          $(go.Shape,  // the label background, which becomes transparent around the edges
            {
              fill: $(go.Brush, "Radial",
                  { 0: "rgb(245, 245, 245)", 0.7: "rgb(245, 245, 245)", 1: "rgba(245, 245, 245, 0)" }),
              stroke: null
            },
            new go.Binding('stroke', 'isPass', function(progress) {
              return progress=="FAIL" ? "red":"BLUE";
            }),
          ),
          $(go.TextBlock, 
            {
              textAlign: "center",
              font: "9pt helvetica, arial, sans-serif",
              margin: 4,
            },
            // new go.Binding('height', 'GuardCond', function(value) {
            //     return value.length==0?40:50;
            // }),
            new go.Binding('stroke', 'isPass', function(progress) {
                return progress=="FAIL" ? "red":"BLUE";
            })
            ,new go.Binding('font', 'Trigger', function(progress) {
              var text = progress.toUpperCase();
              return text.indexOf("T_")>-1?"bold 10pt helvetica, bold arial, sans-serif":"10pt helvetica, bold arial, sans-serif";
            })
            ,new go.Binding('text', '', function(data) {
                var text = data.isPass.toUpperCase();
                if( data.Trigger.toUpperCase().indexOf("WAIT")>-1
                  || data.Function.toUpperCase().indexOf("WAIT")>-1 ){
                  text = "WAIT";
                }
                if( data.Trigger.toUpperCase().indexOf("T_")>-1
                  ){
                  text = data.Trigger;
                }
                return text;
            }).makeTwoWay(),
          ),
      ),
    );
    //go.Model.fromJson(modelJson)
    MachineStateDiagram.dia.model = go.Model.fromJson(modelJson);
    // Overview
    if(MachineStateDiagram.myOverview!=null){
      MachineStateDiagram.myOverview.div = null;
    }
    MachineStateDiagram.myOverview = $(go.Overview, "myOverviewDiv",  // the HTML DIV element for the Overview
      { observed: MachineStateDiagram.dia, contentAlignment: go.Spot.Center });   // tell it which Diagram to show and pan
    // console.log(dia)
    console.log("Init done")
    return MachineStateDiagram.dia;
  }
  static initDiagramSimple(eqptMachineStateComponent:any, modelJson:any, autoLayout:any): go.Diagram {
        console.log("initDiagramSimple autoLayout="+autoLayout);
        MachineStateDiagram.eqptComponent = eqptMachineStateComponent;
        const $ = go.GraphObject.make;// for conciseness in defining templates
        const roundedRectangleParams = {
          parameter1: 10,  // set the rounded corner - radius conner
          spot1: go.Spot.TopLeft, spot2: go.Spot.BottomRight  // make content go all the way to inside edges of rounded corners
        };

        var idDiagramDiv = document.getElementById("idDiagramDiv");
        if (idDiagramDiv !== null) {
          console.log("Detele div Simple")
          var olddiag = go.Diagram.fromDiv(idDiagramDiv);
          if (olddiag !== null){
            // console.log("Set div null")
            olddiag.div = null;
            MachineStateDiagram.dia = null;
            // console.log(MachineStateDiagram.dia)
            // console.log(MachineStateDiagram.dia.div)
          } 
        }
        // if(MachineStateDiagram.dia){
        //   console.log("Detele div Simple")
        //   MachineStateDiagram.dia.div = null;
        // }
    
        MachineStateDiagram.dia = 
          $(go.Diagram,  "idDiagramDiv",  // must name or refer to the DIV HTML element
            {
              "animationManager.initialAnimationStyle": go.AnimationManager.None,
              "InitialAnimationStarting": function(e) {
                  var animation = e.subject.defaultAnimation;
                  animation.easing = go.Animation.EaseOutExpo;
                  animation.duration = 900;
                  animation.add(e.diagram, 'scale', 0.1, 1);
                  animation.add(e.diagram, 'opacity', 0, 1);
              },
              initialAutoScale: go.Diagram.UniformToFill,
              layout: autoLayout==true?$(go.LayeredDigraphLayout, {
                  direction: 90,
                  layerSpacing: 170,
                  columnSpacing: 200,
                  packOption: 4
              }):$(go.Layout),
              // have mouse wheel events zoom in and out instead of scroll up and down
              "toolManager.mouseWheelBehavior": go.ToolManager.WheelZoom,
              // support double-click in background creating a new node
              //"clickCreatingTool.archetypeNodeData": { text: "new node" },
              // enable undo & redo
              "undoManager.isEnabled": true,
            });
      
          // dia.commandHandler.archetypeGroupData = { key: 'Group', isGroup: true };
          // define the Node template
          MachineStateDiagram.dia.nodeTemplate =
            $(go.Node, 'Auto',
              {
                contextMenu:
                  $('ContextMenu',
                    $('ContextMenuButton',
                      $(go.TextBlock, 'Group'),
                      { click: function(e, obj) { e.diagram.commandHandler.groupSelection(); } },
                      new go.Binding('visible', '', function(o) {
    
                        return o.diagram.selection.count > 1;
                      }).ofObject())
                  ),

              },
              // Set location
              new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
              $(go.Panel, 'Auto',
                $(go.Shape, "RoundedRectangle", roundedRectangleParams,
                {
                  name: "SHAPE", 
                  strokeWidth: 0,
                  stroke: null,
                  portId: "",  // this Shape is the Node's port, not the whole Node
                  // fromLinkable: true, fromLinkableSelfNode: true, fromLinkableDuplicates: true,
                  // toLinkable: true, toLinkableSelfNode: true, toLinkableDuplicates: true,
                  cursor: "pointer",
                }
                ,new go.Binding("fill", "color")
              ),
              $(go.TextBlock, {
                      font: "bold small-caps 12pt FontAwesome",
                      margin: 17,
                      stroke: "rgba(0, 0, 0, .87)",
                      //editable: true // editing the text automatically updates the model data
                      doubleClick : function(e, obj){
                        MachineStateDiagram.editNode(e, obj);
                      }
                  },
                  new go.Binding('text', 'text', function(text) {
                    var txt = text.toUpperCase();
                    var icon  = '\uf0a3'; // good
                    if(txt.indexOf("ERROR")>-1) icon = '\uf1ce';
                    if( txt.indexOf(icon)>-1){
                      return text;
                    }
                      return icon+" "+text;
                  }).makeTwoWay())
              ),
            );

            MachineStateDiagram.dia.nodeTemplateMap.add("Start",
              $(go.Node, "Spot", { desiredSize: new go.Size(150, 75) },
                new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
                $(go.Shape, "Ellipse",
                  {
                    width: 130, height: 60, margin: 4,
                    fill: "#52ce60", // green
                    stroke: null,
                    portId: "",
                    // fromLinkable: true, fromLinkableSelfNode: true, fromLinkableDuplicates: true,
                    // toLinkable: true, toLinkableSelfNode: true, toLinkableDuplicates: true,
                    cursor: "pointer"
                  }),
                $(go.TextBlock, 
                  //"Start",
                  new go.Binding("text").makeTwoWay(),
                  {
                    font: "bold 18pt helvetica, bold arial, sans-serif",
                    stroke: "whitesmoke"
                  }),
              )
          );
          MachineStateDiagram.dia.nodeTemplateMap.add("End",
            $(go.Node, "Spot", { desiredSize: new go.Size(100, 100) },
              new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
              $(go.Shape, "Circle",
                {
                  fill: "maroon",
                  stroke: null,
                  portId: "",
                  fromLinkable: true, fromLinkableSelfNode: true, fromLinkableDuplicates: true,
                  toLinkable: true, toLinkableSelfNode: true, toLinkableDuplicates: true,
                  cursor: "pointer"
                }),
              $(go.Shape, "Circle", { fill: null, desiredSize: new go.Size(100, 100), strokeWidth: 2, stroke: "whitesmoke" }),
              $(go.TextBlock, 
                new go.Binding("text").makeTwoWay(),
                {
                  font: "bold 18pt helvetica, bold arial, sans-serif",
                  stroke: "whitesmoke"
                })
            )
          );
          // replace the default Link template in the linkTemplateMap
          MachineStateDiagram.dia.linkTemplate =
            $(go.Link,  // the whole link panel
            //{ routing: go.Link.AvoidsNodes, corner: 10 },
            { 
              //routing: go.Link.AvoidsNodes, 
              //corner: 20,
              curve: go.Link.Bezier, 
              adjusting: go.Link.Bezier, 
              reshapable: true,
              relinkableFrom: false, 
              relinkableTo: false, 
              toShortLength: 3,
              doubleClick : function(e, obj){
                MachineStateDiagram.editNode(e, obj);
              },
            },
            new go.Binding("points").makeTwoWay(),
            new go.Binding("curviness"),
            // Link sharp
            $(go.Shape,  // the link shape
              { 
                strokeWidth: 3.5,
                
              },
              // new go.Binding('strokeDashArray', 'isPass', function(progress) {
              //     return progress=="FAIL" ? [5,10]:null;
              // }),
              new go.Binding('stroke', 'isPass', function(progress) {
                  return progress=="FAIL" ? "red":"BLUE";
              }),
              new go.Binding('strokeWidth', 'progress', function(progress) {
                return progress ? 3.5 : 2.5;
              })
            ),
            $(go.Shape,  // the arrowhead
              { toArrow: "standard", stroke: null },
              new go.Binding('fill', 'isPass', function(progress) {
                return progress=="FAIL" ? "red":"BLUE";
              }),
            ),
            $(go.Panel, "Auto",
                  $(go.Shape, "RoundedRectangle", roundedRectangleParams,
                    {
                      width: 200, height: 60, margin: 10, 
                      //fill: null,
                      name: "SHAPE", 
                      fill: "#ffffff", 
                      strokeDashArray: [3,3],
                      strokeWidth: 1,
                      stroke: 'black',
                      portId: "",
                      // fromLinkable: true, 
                      // fromLinkableSelfNode: true,
                      // fromLinkableDuplicates: true,
                      // toLinkable: true, 
                      // toLinkableSelfNode: true, 
                      // toLinkableDuplicates: true,
                      cursor: "pointer"
                    },
                    new go.Binding('stroke', 'isPass', function(progress) {
                      return progress=="FAIL" ? "red":"BLUE";
                    }),
                    new go.Binding('strokeDashArray', 'Trigger', function(value) {
                      return value.toUpperCase().indexOf("T_")>-1?null:[3,3];
                    }),
                    new go.Binding('height', '', function(data) {
                      var height=40;
                      if( data.GuardCond.length>0 && 
                        data.Trigger.toUpperCase().indexOf("T_")>-1
                        ){
                        // 3lines - Trigger + Function + Guard
                        height=80;
                      }
                      if( data.Trigger.toUpperCase().indexOf("T_")>-1 
                        && data.GuardCond.length==0){
                        // 2lines - Trigger + Function
                        height=60;
                      }
                      if( data.Trigger.toUpperCase().indexOf("T_")==-1 
                        && data.GuardCond.length>0){
                        // 2 lines - Function + GUard
                        height=80;
                      }
                      return height;
                        // var height=40;
                        // if( data.GuardCond.length>0 ){
                        //   height+=20;
                        // }
                        // if( data.Trigger.toUpperCase().indexOf("T_")>-1 ){
                        //   height+=20;
                        // }
                        // return height;
                    }),
                    // new go.Binding('height', 'GuardCond', function(value) {
                    //   return value.length==0?60:80;
                    // }),
                ),
                // ICON TRIGGER - LINE 1
                $(go.TextBlock, 
                    { 
                      text: '\uf0e7',
                      font: '16pt FontAwesome',
                      verticalAlignment: go.Spot.Top,
                      width: 170, 
                      height: 0
                    },
                    new go.Binding('height', 'GuardCond', function(value) {
                        return value.length==0?40:60;
                    }),
                    new go.Binding('stroke', 'isPass', function(progress) {
                      return progress=="FAIL" ? "red":"BLUE";
                    }),
                    new go.Binding('text', 'Trigger', function(progress) {
                        var text = progress.toUpperCase();
                        return text.indexOf("T_")>-1? '\uf0e7':(text.indexOf("*")>-1?'\uf069':'\uf103');//f0b2
                    }),
                    new go.Binding('visible', 'Trigger', function(value) {
                      var text = value.toUpperCase();
                      return text.indexOf("T_")>-1? true:false;
                    }),
                ),
                // TEXT TRIGGER - LINE 1
                $(go.TextBlock, 
                  {
                    text: "Trigger",
                    verticalAlignment: go.Spot.Top,
                    margin: new go.Margin(0, -40, 0, 0),
                    width: 170, 
                    height: 0
                  },
                    new go.Binding('height', 'GuardCond', function(value) {
                        return value.length==0?40:50;
                    }),
                    new go.Binding('stroke', 'isPass', function(progress) {
                        return progress=="FAIL" ? "red":"BLUE";
                    })
                    ,new go.Binding('font', 'Trigger', function(progress) {
                      var text = progress.toUpperCase();
                      return text.indexOf("T_")>-1?"bold 10pt helvetica, bold arial, sans-serif":"10pt helvetica, bold arial, sans-serif";
                    })
                    ,new go.Binding('text', 'Trigger', function(progress) {
                        var text = progress.toUpperCase();
                        return text.indexOf("*")>-1?"Any":progress;
                    }).makeTwoWay(),
                    new go.Binding('visible', 'Trigger', function(value) {
                      var text = value.toUpperCase();
                      return text.indexOf("T_")>-1? true:false;
                    }),
                ),
                // ICON - FUNCTION - LINE 2
                $(go.TextBlock, { 
                        text: '\uf1b2', 
                        font: '16pt FontAwesome',
                        verticalAlignment: go.Spot.Bottom,
                        width: 170, 
                        height: 60
                    },
                    new go.Binding('verticalAlignment', '', function(data) {
                      if( data.Trigger.toUpperCase().indexOf("T_")==-1 
                        && data.GuardCond.length>0){
                        return go.Spot.TopLeft;
                      }
                    }),
                    new go.Binding('height', '', function(data) {
                      var height=20;
                      if( data.GuardCond.length>0 && 
                        data.Trigger.toUpperCase().indexOf("T_")>-1
                        ){
                        height=25;
                      }
                      if( data.Trigger.toUpperCase().indexOf("T_")>-1 
                        && data.GuardCond.length==0){
                        height=40;
                      }
                      if( data.Trigger.toUpperCase().indexOf("T_")==-1 
                        && data.GuardCond.length>0){
                        // console.log(data.Function+" "+data.Trigger+" Height="+height)
                        height=20;
                      }
                      return height;
                    }),
                    new go.Binding('stroke', 'isPass', function(progress) {
                        return progress=="FAIL" ? "RED":"BLUE";
                    }),
                ),
                // TEXT - FUNCTION - LINE 2
                $(go.TextBlock, 
                  {   text: "Function Name",
                      font: '10pt Arial',
                      verticalAlignment: go.Spot.Bottom,
                      margin: new go.Margin(0, -50, 0, 0),
                      width: 170, 
                      height: 40,
                      stroke:'black'
                  },
                  new go.Binding('font', 'Trigger', function(value) {
                    var text = value.toUpperCase();
                    return text.indexOf("T_")>-1?'10pt Arial':'bold 10pt Arial';
                  }),
                  new go.Binding('height', '', function(data) {
                    var height=20;
                    if( data.GuardCond.length>0 && 
                      data.Trigger.toUpperCase().indexOf("T_")>-1
                      ){
                      height=25;
                    }
                    if( data.Trigger.toUpperCase().indexOf("T_")>-1 
                      && data.GuardCond.length==0){
                      height=40;
                    }
                    if( data.Trigger.toUpperCase().indexOf("T_")==-1 
                      && data.GuardCond.length>0){
                      height=10;
                    }
                    return height;
                  }),
                  new go.Binding('stroke', 'isPass', function(progress) {
                    return progress=="FAIL" ? "BLUE":"BLUE";
                  }),
                  new go.Binding("text", "Function").makeTwoWay(),
                  
                ),
                // ICON - GUARD CONDITION - LINE 3
                $(go.TextBlock, { 
                        text: '\uf132', 
                        font: '16pt FontAwesome',
                        verticalAlignment: go.Spot.Bottom,
                        width: 170, 
                        height: 80,
                        stroke: 'black',
                    },
                    new go.Binding('stroke', 'isPass', function(progress) {
                      return progress=="FAIL" ? "RED":"BLUE";
                    }),
                    new go.Binding('visible', 'GuardCond', function(value) {
                        return value.length==0?false:true;
                    }),
                ),
                // TEXT - GUARD CONDITION - LINE 3
                $(go.TextBlock, 
                  {   text: "GuardCondition",
                      verticalAlignment: go.Spot.Bottom,
                      margin: new go.Margin(0, -40, 0, 0),
                      width: 170, 
                      height: 70,
                      stroke: 'black',
                  },
                  new go.Binding('stroke', 'isPass', function(progress) {
                    return progress=="FAIL" ? "RED":"BLUE";
                  }),
                  new go.Binding('visible', 'GuardCond', function(value) {
                      return value.length==0?false:true;
                  }),
                  new go.Binding("text", "GuardText").makeTwoWay()
                ),
    
          ),
    
          );
    
          // dia.addModelChangedListener((e) => {
          //   if (e.isTransactionFinished) {
          //     //getting the below error when i upgrade gojs to 2.1.15 or higher
          //     //GraphLinksModel.linkKeyProperty must not be an empty string for .toIncrementalData() to succeed
          //     const dataChanges = e.model.toIncrementalData(e);
          //     if (dataChanges !== null) {
          //       this.diagramModelChange(dataChanges);
          //     }
          //   }
          // });
    
          function addNodeAndLink(e, obj) {
            var adornment = obj.part;
            var diagram = e.diagram;
            var model = diagram.model;
            diagram.startTransaction("Add State");
    
            // get the node data for which the user clicked the button
            var fromNode = adornment.adornedPart;
            var fromData = fromNode.data;
            // create a new "State" data object, positioned off to the right of the adorned Node
            var toData = { text: "s_NewState", 
                          loc:"", 
                          color:"yellow",
                          id:"s_NewState_" + (diagram.model.nodeDataArray.length+1)
                          };
            var p = fromNode.location.copy();
            p.x += 200;
            p.y += 200;
            toData.loc = go.Point.stringify(p);  // the "loc" property is a string, not a Point object
            // add the new node data to the model
            model.addNodeData(toData);
            console.log(toData)
    
            // create a link data from the old node data to the new node data
            var linkdata = {
              from: model.getKeyForNodeData(fromData),  // or just: fromData.id
              to: model.getKeyForNodeData(toData),
              text: "t_Trigger\\na_Function",
              GuardText: "",
              GuardVal:"",
              GuardCond:"",
              Function:"a_Function",
              Trigger:"t_Trigger",
              isPass:"PASS"
            };
            console.log(linkdata)
            // and add the link data to the model
            model.addLinkData(linkdata);
            console.log(linkdata)
    
            // select the new Node
            var newnode = diagram.findNodeForData(toData);
            diagram.select(newnode);
    
            diagram.commitTransaction("Add State");
    
            // if the new node is off-screen, scroll the diagram to show the new node
            diagram.scrollToRect(newnode.actualBounds);
          }
    
          // MachineStateDiagram.dia.nodeTemplate.selectionAdornmentTemplate =
          //   $(go.Adornment, "Spot",
          //     $(go.Panel, "Auto",
          //       $(go.Shape, "RoundedRectangle", roundedRectangleParams,
          //       { fill: null, stroke: "#7986cb", strokeWidth: 3 }),
          //       $(go.Placeholder)  // a Placeholder sizes itself to the selected Node
          //     ),
          //     // the button to create a "next" node, at the top-right corner
          //     $("Button",
          //       {
          //         alignment: go.Spot.TopRight,
          //         click: addNodeAndLink  // this function is defined below
          //       },
          //       $(go.Shape, "PlusLine", { width: 6, height: 6 })
          //     ) // end button
          //   ); // end Adornment
            // install custom linking tool, defined in PolylineLinkingTool.js
            // var tool = new PolylineLinkingTool();
            // tool.temporaryLink.routing = go.Link.Orthogonal;  // optional, but need to keep link template in sync, below
            // dia.toolManager.linkingTool = tool;

            //go.Model.fromJson(modelJson)
            MachineStateDiagram.dia.model = go.Model.fromJson(modelJson);
    
            // Overview
            if(MachineStateDiagram.myOverview!=null){
              MachineStateDiagram.myOverview.div = null;
            }
            MachineStateDiagram.myOverview = $(go.Overview, "myOverviewDiv",  // the HTML DIV element for the Overview
              { observed: MachineStateDiagram.dia, contentAlignment: go.Spot.Center });   // tell it which Diagram to show and pan
    
            // console.log(dia)
            console.log("Init done simple mode")
            return MachineStateDiagram.dia;
        }
  static initDiagramFullMode(eqptMachineStateComponent:any, modelJson:any, autoLayout:any): go.Diagram {
    console.log("initDiagram autoLayout="+autoLayout);
    MachineStateDiagram.eqptComponent = eqptMachineStateComponent;
    const $ = go.GraphObject.make;// for conciseness in defining templates
    // some constants that will be reused within templates
    const roundedRectangleParams = {
      parameter1: 10,  // set the rounded corner - radius conner
      spot1: go.Spot.TopLeft, spot2: go.Spot.BottomRight  // make content go all the way to inside edges of rounded corners
    };
    var idDiagramDiv = document.getElementById("idDiagramDiv");
      if (idDiagramDiv !== null) {
        console.log("Detele div Full")
        var olddiag = go.Diagram.fromDiv(idDiagramDiv);
        if (olddiag !== null){
          console.log("Set div null")
          olddiag.div = null;
          MachineStateDiagram.dia = null;
          console.log(MachineStateDiagram.dia)
          // console.log(MachineStateDiagram.dia.div)
        } 
      }

    MachineStateDiagram.dia = 
      $(go.Diagram,  "idDiagramDiv",  // must name or refer to the DIV HTML element
        {
          "animationManager.initialAnimationStyle": go.AnimationManager.None,
          "InitialAnimationStarting": function(e) {
              var animation = e.subject.defaultAnimation;
              animation.easing = go.Animation.EaseOutExpo;
              animation.duration = 900;
              animation.add(e.diagram, 'scale', 0.1, 1);
              animation.add(e.diagram, 'opacity', 0, 1);
          },
          initialAutoScale: go.Diagram.UniformToFill,
          layout: autoLayout==true?$(go.LayeredDigraphLayout, {
              direction: 90,
              layerSpacing: 170,
              columnSpacing: 200,
              packOption: 4
          }):$(go.Layout),
          // have mouse wheel events zoom in and out instead of scroll up and down
          "toolManager.mouseWheelBehavior": go.ToolManager.WheelZoom,
          // support double-click in background creating a new node
          //"clickCreatingTool.archetypeNodeData": { text: "new node" },
          // enable undo & redo
          "undoManager.isEnabled": true,
        });
        MachineStateDiagram.dia.groupTemplate =
        $(go.Group, "Auto", { // define the group's internal layout
          layout: $(go.TreeLayout, {
            angle: 90,
            arrangement: go.TreeLayout.ArrangementHorizontal,
            isRealtime: false
          }),
          // the group begins unexpanded;
          // upon expansion, a Diagram Listener will generate contents for the group
          isSubGraphExpanded: false,
          // when a group is expanded, if it contains no parts, generate a subGraph inside of it
          subGraphExpandedChanged: function(group) {
            if (group.memberParts.count === 0) {
              //randomGroup(group.data.key);
            }
          },
          doubleClick : function(e, obj){
            MachineStateDiagram.editNode(e, obj);
          }
        },
        new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
        $(go.Shape, "Rectangle", {
          fill: null,
          stroke: "blue",
          strokeWidth: 2
          },
          new go.Binding("fill", "", function(data) {
            return data.isGroup==true?'pink':data.color;
          })
        ),
        $(go.Panel, "Vertical", {
            defaultAlignment: go.Spot.Left,
            margin: 4
          },
          $(go.Panel, "Horizontal", {
              defaultAlignment: go.Spot.Top
            },
            // the SubGraphExpanderButton is a panel that functions as a button to expand or collapse the subGraph
            $("SubGraphExpanderButton"),
            $(go.TextBlock, {
                font: "Bold 18px Sans-Serif",
                margin: 4
              },
            new go.Binding("text", "text"))
          ),
          // create a placeholder to represent the area where the contents of the group are
          $(go.Placeholder, {
            padding: new go.Margin(0, 10)
          })
        ) // end Vertical Panel
      ); // end Group
      // dia.commandHandler.archetypeGroupData = { key: 'Group', isGroup: true };
      // define the Node template
      MachineStateDiagram.dia.nodeTemplate =
        $(go.Node, 'Auto',
          {
            contextMenu:
              $('ContextMenu',
                $('ContextMenuButton',
                  $(go.TextBlock, 'Group'),
                  { click: function(e, obj) { e.diagram.commandHandler.groupSelection(); } },
                  new go.Binding('visible', '', function(o) {

                    return o.diagram.selection.count > 1;
                  }).ofObject())
              ),

          },
          // Set location
          new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
          $(go.Panel, 'Auto',
            $(go.Shape, "RoundedRectangle", roundedRectangleParams,
            {
              name: "SHAPE", 
              strokeWidth: 0,
              stroke: null,
              portId: "",  // this Shape is the Node's port, not the whole Node
              // fromLinkable: true, fromLinkableSelfNode: true, fromLinkableDuplicates: true,
              // toLinkable: true, toLinkableSelfNode: true, toLinkableDuplicates: true,
              cursor: "pointer",
              
            }
            // ,new go.Binding('fill', 'text', function(text) {
            //     //return text.toUpperCase().indexOf("ERROR")>-1 ? "YELLOW":"rgb(0, 170, 239)";
            //     return text.toUpperCase().indexOf("ERROR")>-1 ? "YELLOW":"GREEN";
            // }),
            ,new go.Binding("fill", "color")
          ),
          $(go.TextBlock, {
                  font: "bold small-caps 12pt FontAwesome",
                  margin: 17,
                  stroke: "rgba(0, 0, 0, .87)",
                  //editable: true // editing the text automatically updates the model data
                  doubleClick : function(e, obj){
                    MachineStateDiagram.editNode(e, obj);
                  }
              },
              new go.Binding('text', 'text', function(text) {
                var txt = text.toUpperCase();
                var icon  = '\uf0a3'; // good
                if(txt.indexOf("ERROR")>-1) icon = '\uf1ce';
                if( txt.indexOf(icon)>-1){
                  return text;
                }
                  return icon+" "+text;
              }).makeTwoWay())
          ),
        );
        MachineStateDiagram.dia.nodeTemplateMap.add("Start",
        $(go.Node, "Spot", { desiredSize: new go.Size(150, 75) },
          new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
          $(go.Shape, "Ellipse",
            {
              width: 130, height: 60, margin: 4,
              fill: "#52ce60", // green
              stroke: null,
              portId: "",
              // fromLinkable: true, fromLinkableSelfNode: true, fromLinkableDuplicates: true,
              // toLinkable: true, toLinkableSelfNode: true, toLinkableDuplicates: true,
              cursor: "pointer"
            }),
          $(go.TextBlock, 
            //"Start",
            new go.Binding("text").makeTwoWay(),
            {
              font: "bold 18pt helvetica, bold arial, sans-serif",
              stroke: "whitesmoke"
            }),
        )
      );
      MachineStateDiagram.dia.nodeTemplateMap.add("End",
        $(go.Node, "Spot", { desiredSize: new go.Size(100, 100) },
          new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
          $(go.Shape, "Circle",
            {
              fill: "maroon",
              stroke: null,
              portId: "",
              // fromLinkable: true, fromLinkableSelfNode: true, fromLinkableDuplicates: true,
              // toLinkable: true, toLinkableSelfNode: true, toLinkableDuplicates: true,
              cursor: "pointer"
            }),
          $(go.Shape, "Circle", { fill: null, desiredSize: new go.Size(100, 100), strokeWidth: 2, stroke: "whitesmoke" }),
          $(go.TextBlock, 
            new go.Binding("text").makeTwoWay(),
            {
              font: "bold 18pt helvetica, bold arial, sans-serif",
              stroke: "whitesmoke"
            })
        )
      );
      // replace the default Link template in the linkTemplateMap
      MachineStateDiagram.dia.linkTemplate =
        $(go.Link,  // the whole link panel
        //{ routing: go.Link.AvoidsNodes, corner: 10 },
        { 
          //routing: go.Link.AvoidsNodes, 
          //corner: 20,
          curve: go.Link.Bezier, 
          //adjusting: go.Link.Bezier, 
          reshapable: true,
          relinkableFrom: true, 
          relinkableTo: true, 
          toShortLength: 3,
          doubleClick : function(e, obj){
            MachineStateDiagram.editNode(e, obj);
          },
        },
        new go.Binding("points").makeTwoWay(),
        new go.Binding("curviness"),
        // Link sharp
        $(go.Shape,  // the link shape
          { 
            strokeWidth: 3.5,
            
          },
          // new go.Binding('strokeDashArray', 'isPass', function(progress) {
          //     return progress=="FAIL" ? [5,10]:null;
          // }),
          new go.Binding('stroke', 'isPass', function(progress) {
              return progress=="FAIL" ? "red":"BLUE";
          }),
          new go.Binding('strokeWidth', 'progress', function(progress) {
            return progress ? 3.5 : 2.5;
          })
        ),
        $(go.Shape,  // the arrowhead
          { toArrow: "standard", stroke: null },
          new go.Binding('fill', 'isPass', function(progress) {
            return progress=="FAIL" ? "red":"BLUE";
          }),
        ),
        $(go.Panel, "Auto",
              $(go.Shape, "RoundedRectangle", roundedRectangleParams,
                {
                  width: 200, height: 60, margin: 10, 
                  //fill: null,
                  name: "SHAPE", 
                  fill: "#ffffff", 
                  strokeDashArray: [3,3],
                  strokeWidth: 1,
                  stroke: 'black',
                  portId: "",
                  // fromLinkable: true, fromLinkableSelfNode: true, fromLinkableDuplicates: true,
                  // toLinkable: true, toLinkableSelfNode: true, toLinkableDuplicates: true,
                  cursor: "pointer"
                },
                new go.Binding('stroke', 'isPass', function(progress) {
                  return progress=="FAIL" ? "red":"BLUE";
                }),
                new go.Binding('strokeDashArray', 'Trigger', function(value) {
                  return value.toUpperCase().indexOf("T_")>-1?null:[3,3];
                }),
                
            ),
            // ICON TRIGGER - LINE 1
            $(go.TextBlock, 
                { 
                  text: '\uf0e7',
                  font: '16pt FontAwesome',
                  verticalAlignment: go.Spot.Top,
                  width: 170, 
                  height: 40
                },
                new go.Binding('height', '', function(data) {
                    //console.log(data)
                    var value = data.GuardCond;
                    return value.length==0?40:60;
                }),
                new go.Binding('stroke', 'isPass', function(progress) {
                  return progress=="FAIL" ? "red":"BLUE";
                }),
                new go.Binding('text', 'Trigger', function(progress) {
                    var text = progress.toUpperCase();
                    return text.indexOf("T_")>-1? '\uf0e7':(text.indexOf("*")>-1?'\uf069':'\uf103');//f0b2
                }),
                
            ),
            // TEXT TRIGGER - LINE 1
            $(go.TextBlock, 
              {
                text: "Trigger",
                verticalAlignment: go.Spot.Top,
                margin: new go.Margin(0, -40, 0, 0),
                width: 170, 
                height: 40
              },
                new go.Binding('height', 'GuardCond', function(value) {
                    return value.length==0?40:50;
                }),
                new go.Binding('stroke', 'isPass', function(progress) {
                    return progress=="FAIL" ? "red":"BLUE";
                })
                ,new go.Binding('font', 'Trigger', function(progress) {
                  var text = progress.toUpperCase();
                  return text.indexOf("T_")>-1?"bold 10pt helvetica, bold arial, sans-serif":"10pt helvetica, bold arial, sans-serif";
                })
                ,new go.Binding('text', 'Trigger', function(progress) {
                    var text = progress.toUpperCase();
                    return text.indexOf("*")>-1?"Any":progress;
                }).makeTwoWay(),
            ),
            // ICON - FUNCTION - LINE 2
            $(go.TextBlock, { 
                    text: '\uf1b2', 
                    font: '16pt FontAwesome',
                    verticalAlignment: go.Spot.Bottom,
                    width: 170, 
                    height: 60
                },
                new go.Binding('height', 'GuardCond', function(value) {
                    return value.length==0?50:20;
                }),
                new go.Binding('stroke', 'isPass', function(progress) {
                    return progress=="FAIL" ? "RED":"BLUE";
                }),
            ),
            // TEXT - FUNCTION - LINE 2
            $(go.TextBlock, 
              {   text: "Function Name",
                  verticalAlignment: go.Spot.Bottom,
                  margin: new go.Margin(0, -40, 0, 0),
                  width: 170, 
                  height: 40
              },
                new go.Binding('height', 'GuardCond', function(value) {
                    return value.length==0?40:10;
                }),
              new go.Binding('stroke', 'isPass', function(progress) {
                return progress=="FAIL" ? "BLUE":"BLUE";
              }),

              new go.Binding("text", "Function").makeTwoWay()
            ),
            //GUARD CONDITION - LINE 3
            $(go.TextBlock, { 
                    text: '\uf132', 
                    font: '16pt FontAwesome',
                    verticalAlignment: go.Spot.Bottom,
                    width: 170, 
                    height: 70,
                    stroke: 'black',
                },
                new go.Binding('stroke', 'isPass', function(progress) {
                  return progress=="FAIL" ? "RED":"BLUE";
                }),
                new go.Binding('visible', 'GuardCond', function(value) {
                    return value.length==0?false:true;
                }),
            ),
            //GUARD CONDITION - LINE 3
            $(go.TextBlock, 
              {   text: "GuardCondition",
                  verticalAlignment: go.Spot.Bottom,
                  margin: new go.Margin(0, -40, 0, 0),
                    width: 170, 
                    height: 60,
                  stroke: 'black',
              },
              new go.Binding('stroke', 'isPass', function(progress) {
                return progress=="FAIL" ? "RED":"BLUE";
              }),
              new go.Binding('visible', 'GuardCond', function(value) {
                  return value.length==0?false:true;
              }),
              new go.Binding("text", "GuardText").makeTwoWay()
            ),

      ),

      );

      // dia.addModelChangedListener((e) => {
      //   if (e.isTransactionFinished) {
      //     //getting the below error when i upgrade gojs to 2.1.15 or higher
      //     //GraphLinksModel.linkKeyProperty must not be an empty string for .toIncrementalData() to succeed
      //     const dataChanges = e.model.toIncrementalData(e);
      //     if (dataChanges !== null) {
      //       this.diagramModelChange(dataChanges);
      //     }
      //   }
      // });

      function addNodeAndLink(e, obj) {
        var adornment = obj.part;
        var diagram = e.diagram;
        var model = diagram.model;
        diagram.startTransaction("Add State");

        // get the node data for which the user clicked the button
        var fromNode = adornment.adornedPart;
        var fromData = fromNode.data;
        // create a new "State" data object, positioned off to the right of the adorned Node
        var toData = { text: "s_NewState", 
                      loc:"", 
                      color:"yellow",
                      id:"s_NewState_" + (diagram.model.nodeDataArray.length+1)
                      };
        var p = fromNode.location.copy();
        p.x += 200;
        p.y += 200;
        toData.loc = go.Point.stringify(p);  // the "loc" property is a string, not a Point object
        // add the new node data to the model
        model.addNodeData(toData);
        console.log(toData)

        // create a link data from the old node data to the new node data
        var linkdata = {
          from: model.getKeyForNodeData(fromData),  // or just: fromData.id
          to: model.getKeyForNodeData(toData),
          text: "t_Trigger\\na_Function",
          GuardText: "",
          GuardVal:"",
          GuardCond:"",
          Function:"a_Function",
          Trigger:"t_Trigger",
          isPass:"PASS"
        };
        console.log(linkdata)
        // and add the link data to the model
        model.addLinkData(linkdata);
        console.log(linkdata)

        // select the new Node
        var newnode = diagram.findNodeForData(toData);
        diagram.select(newnode);

        diagram.commitTransaction("Add State");

        // if the new node is off-screen, scroll the diagram to show the new node
        diagram.scrollToRect(newnode.actualBounds);
      }

      // MachineStateDiagram.dia.nodeTemplate.selectionAdornmentTemplate =
      //   $(go.Adornment, "Spot",
      //     $(go.Panel, "Auto",
      //       $(go.Shape, "RoundedRectangle", roundedRectangleParams,
      //       { fill: null, stroke: "#7986cb", strokeWidth: 3 }),
      //       $(go.Placeholder)  // a Placeholder sizes itself to the selected Node
      //     ),
      //     // the button to create a "next" node, at the top-right corner
      //     $("Button",
      //       {
      //         alignment: go.Spot.TopRight,
      //         click: addNodeAndLink  // this function is defined below
      //       },
      //       $(go.Shape, "PlusLine", { width: 6, height: 6 })
      //     ) // end button
      //   ); // end Adornment
        // install custom linking tool, defined in PolylineLinkingTool.js
        // var tool = new PolylineLinkingTool();
        // tool.temporaryLink.routing = go.Link.Orthogonal;  // optional, but need to keep link template in sync, below
        // dia.toolManager.linkingTool = tool;

        //go.Model.fromJson(modelJson)
        MachineStateDiagram.dia.model = go.Model.fromJson(modelJson);

        // Overview
        if(MachineStateDiagram.myOverview!=null){
          MachineStateDiagram.myOverview.div = null;
        }
        MachineStateDiagram.myOverview = $(go.Overview, "myOverviewDiv",  // the HTML DIV element for the Overview
          { observed: MachineStateDiagram.dia, contentAlignment: go.Spot.Center });   // tell it which Diagram to show and pan

        // console.log(dia)
        console.log("Init done")
        return MachineStateDiagram.dia;
    }
  static editNode(e, obj){
    console.log(obj.part.data)
    var data = obj.part.data;
    console.log(data)
    if( "from" in data ){
      console.log("Edit link")
      MachineStateDiagram.eqptComponent.editLink(e, data);
    }
    else{
      console.log("Edit node")
      MachineStateDiagram.eqptComponent.editNode(e, data);
    }
  }
}