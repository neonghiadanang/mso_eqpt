

export class MachineStateModel{
  static buildModelFromTable(listData: any, showMode, showPASS, showFAIL, showALL, showSimple, showGroup) {
    var cfg_lines = "";
    listData.forEach(element => {
        cfg_lines += element.current_state +" ,\t"+
        element.trigger_action +" ,\t"+
        element.action_macro +" ,\t"+
        element.pass_state +" ,\t"+
        element.failed_state +" ,\t"+
        element.guard_cond +" ,\t"+
        element.guard_value +" ,\t"+
        element.group_state +"\n";
    });
    //console.log(cfg_lines)
    return MachineStateModel.parseModel(cfg_lines, showMode, showPASS, showFAIL, showALL, showSimple, showGroup);
  }

  static parseModel(conf_text: any, showMode, showPASS, showFAIL, showALL, showSimple, showGroup) {
    var arrayOfLines = conf_text.match(/[^\r\n]+/g);
    // console.log(arrayOfLines);
    var mapState = [];
    var mapStateId = [];
    var mapState2OK = [];
    var mapState2FAIL = [];
    var node2Group = {}
    var Groups = {}
    
    var X = 300;
    var Y = 0;
    var Y_ERROR = 0;
    var nodeIdNormal = 1;
    var nodeIdError = 1;
    var nodeId = 1;

    console.log( "showSimple="+showSimple+ " showGroup="+showGroup  );
    // Parse group 
    for (i = 0; i < arrayOfLines.length; i++) {
        var line = arrayOfLines[i].trim();
        if (line[0] == "#") continue;
        line = line.replace(/\t/g, '').replace(/  +/g, '');
        var arr = line.split(",");
        if (arr[0].trim().length > 0 &&
            arr[1].trim().length > 0
        ) {
            if(arr.length > 7){
                var groupName = arr[7].trim();
                var stateName = arr[0].trim();
                node2Group[stateName] = groupName;
                if (groupName in Groups){
                }
                else{
                    Groups[groupName] = []
                }
                var groupState={
                    "Name": groupName,
                    "Trigger": "",
                    "Function": "",
                    "OK": "",
                    "Fail": "",
                    "GuardCond": "",
                    "GuardVal": "",
                    "X": 0,
                    "Y": 0,
                    "nodeId": 0,
                }
                Groups[groupName].push(stateName);
            }
        }
    }
    for (var i = 0; i < arrayOfLines.length; i++) {
        var line = arrayOfLines[i].trim();
        // console.log( "line: " + line)
        if (line[0] == "#") continue;
        line = line.replace(/\t/g, '').replace(/  +/g, '');
        var arr = line.split(",");
        if (arr[0].trim().length > 0 &&
            arr[1].trim().length > 0
        ) {

            if (arr[0].toUpperCase().indexOf("ERROR") > -1 ||
                arr[0].toUpperCase().indexOf("REWORK") > -1
            ) {
                if (typeof(mapState[arr[0].trim()]) == "undefined") {
                    if( showMode=="Zigzag"){
                        X = nodeIdError % 3 == 1 ? 0 : (nodeIdError % 3 == 2 ? 300 : 600);
                        nodeIdError++;
                    }
                    else{
                        nodeIdError++;
						if (nodeIdError % 20 >= 10) {
							X = 600 - 100 * (20 - (nodeIdError % 20));
						} else if (nodeIdError % 20 < 10) {
							X = 600 - 100 * (nodeIdError % 20);
						}
                    }
                }
            } else {
                if (typeof(mapState[arr[0].trim()]) == "undefined") {
                    if( showMode=="Zigzag"){
                        X = nodeIdNormal % 3 == 1 ? 900 : (nodeIdNormal % 3 == 2 ? 1200 : 1500);
                        nodeIdNormal++;
                    }
                    else{
                        // Lagder
                        if (nodeIdNormal % 20 <= 10) {
							X = 900 + 300 * (nodeIdNormal % 20);
						} else if (nodeIdNormal % 20 > 10) {
							X = 900 + 300 * (20 - (nodeIdNormal % 20));
						} else {
							X = 900 + 300 * (20 - (nodeIdNormal % 20));
						}
						nodeIdNormal++;
                    }
                }
            }
            var state = {
                "Name": arr[0].trim(),
                "Trigger": arr[1].trim(),
                "Function": arr[2].trim(),
                "OK": arr[3].trim(),
                "Fail": arr[4].trim(),
                "GuardCond": arr.length>5?arr[5].trim():"",
                "GuardVal": arr.length>6?arr[6].trim():"",
                "X": X,
                "Y": X <= 600 ? Y_ERROR : Y,
                "nodeId": nodeId,
                "isGroup": false,
				"group":"",
            };
            if (typeof(mapState[state["Name"]]) == "undefined") {
                if (X >= 600) {
                    Y += 300;
                    if (Y_ERROR == 0) {
                        Y_ERROR = Y;
                    }
                } else {
                    Y_ERROR += 300;
                }
                //========================================
                if( showGroup==true && state["Name"] in node2Group && node2Group[state["Name"]].trim().length>0 ){
                    nodeId++;
                    var stateGroup = {
                        "Name": node2Group[state["Name"]],
                        "Trigger": arr[1].trim(),
                        "Function": arr[2].trim(),
                        "OK": arr[3].trim(),
                        "Fail": arr[4].trim(),
                        "GuardCond": arr.length > 5 ? arr[5].trim() : "",
                        "GuardVal": arr.length > 6 ? arr[6].trim() : "",
                        "GroupState": arr.length > 7 ? arr[7].trim() : "",
                        "X": X,
                        "Y": X < 900 ? Y_ERROR : Y,
                        "nodeId": nodeId,
                        "isGroup": true,
                        "group":"",
                    };
                    mapState[stateGroup["Name"]] = stateGroup;
                    mapStateId[nodeId] = stateGroup;
                    //
                    state["group"] = node2Group[state["Name"]];
                    //
                    var nameOk = stateGroup["Name"] + "=>" + stateGroup["OK"] + " by " + stateGroup["Function"] + "_" + stateGroup["Trigger"];
                    var nameFail = stateGroup["Name"] + "=>" + stateGroup["Fail"] + " by " + stateGroup["Function"] + "_" + stateGroup["Trigger"];
                    if (showSimple) {
                        nameOk = stateGroup["Name"] + "=>" + stateGroup["OK"] ;//+ " by " + stateGroup["Function"];
                        nameFail = stateGroup["Name"] + "=>" + stateGroup["Fail"];// + " by " + stateGroup["Function"];
                    }
                    mapState2OK[nameOk] = {
                        name: nameOk,
                        func: stateGroup["Function"],
                        trigger: stateGroup["Trigger"],
                        fromName: mapState[stateGroup["Name"]].Name,
                        fromId: mapState[stateGroup["Name"]].nodeId,
                        toNodeName: stateGroup["OK"]
                    };
                    mapState2FAIL[nameFail] = {
                        name: nameFail,
                        func: stateGroup["Function"],
                        trigger: stateGroup["Trigger"],
                        fromName: mapState[stateGroup["Name"]].Name,
                        fromId: mapState[stateGroup["Name"]].nodeId,
                        toNodeName: stateGroup["Fail"]
                    };
                    // End group
    
                }
                //========================================
                mapState[state["Name"]] = state;
                mapStateId[nodeId] = state;
                nodeId++;
            } else {

            }
            var nameOk = state["Name"] + "=>" + state["OK"] + " by " + state["Function"] + "_" + state["Trigger"];
            var nameFail = state["Name"] + "=>" + state["Fail"] + " by " + state["Function"] + "_" + state["Trigger"];
            if (showSimple) {
                nameOk = state["Name"] + "=>" + state["OK"] + " by " + state["Function"];
                nameFail = state["Name"] + "=>" + state["Fail"] + " by " + state["Function"];
            }
            //
            if (state["OK"].toUpperCase() == "A_NONE") {
                mapState2OK[nameOk] = {
                    name: nameOk,
                    func: state["Function"],
                    trigger: state["Trigger"],
                    fromName: mapState[state["Name"]].Name,
                    fromId: mapState[state["Name"]].nodeId,
                    toNodeName: mapState[state["Name"]].Name,
                    GuardCond: state["GuardCond"],
                    GuardVal:  state["GuardVal"],
                        };
            } else {
                mapState2OK[nameOk] = {
                    name: nameOk,
                    func: state["Function"],
                    trigger: state["Trigger"],
                    fromName: mapState[state["Name"]].Name,
                    fromId: mapState[state["Name"]].nodeId,
                    toNodeName: state["OK"],
                    GuardCond: state["GuardCond"],
                    GuardVal:  state["GuardVal"],
                    };
            }
            mapState2FAIL[nameFail] = {
                name: nameFail,
                func: state["Function"],
                trigger: state["Trigger"],
                fromName: mapState[state["Name"]].Name,
                fromId: mapState[state["Name"]].nodeId,
                toNodeName: state["Fail"],
                GuardCond: state["GuardCond"],
                GuardVal:  state["GuardVal"],
            };

            //
        }
    }

    // BEGIN BUILD
    var jsonData = '{ "class": "GraphLinksModel","nodeKeyProperty": "id",';
    jsonData += '"nodeDataArray": [';
    console.log(mapState)
    var idx = 0;
    var listEntry = Object.entries(mapState);
    for (let [key, value] of Object.entries(mapState)) {
        idx++;
        if (value.Name == "s_Initialize") {
            if(showGroup){
                jsonData += '{"id":"' + value.Name + '"'+ (value.isGroup==true?',"isGroup":true':(value.group.length>0?(',"group":"'+value.group+'"'):'')) +' ,"loc":"' + value.X + ' ' + value.Y + '", "text":"' + value.Name + '", "category":"Start", "color": "#7CFC00"}';
            }
            else{
                jsonData += '{"id":"' + value.Name + '","loc":"' + value.X + ' ' + value.Y + '", "text":"' + value.Name + '", "category":"Start", "color": "#7CFC00"}';                
            }
        } else {
            if (value.Name.toUpperCase().indexOf("ERROR") > -1 ||
                value.Name.toUpperCase().indexOf("REWORK") > -1
            ) {
                if(showGroup){
                    jsonData += '{"id":"' + value.Name +'"'+ (value.isGroup==true?',"isGroup":true':(value.group.length>0?(',"group":"'+value.group+'"'):''))+ ' ,"loc":"' + value.X + ' ' + value.Y + '", "text":"' + value.Name + '", "color": "#ffff00"}';
                }
                else{
                    jsonData += '{"id":"' + value.Name +'","loc":"' + value.X + ' ' + value.Y + '", "text":"' + value.Name + '", "color": "#ffff00"}';
                }

            } else {
                if(showGroup){
                    jsonData += '{"id":"' + value.Name +'"'+ (value.isGroup==true?',"isGroup":true':(value.group.length>0?(',"group":"'+value.group+'"'):''))+ ' ,"loc":"' + value.X + ' ' + value.Y + '", "text":"' + value.Name + '", "color": "#7CFC00"}';
                }
                else{
                    jsonData += '{"id":"' + value.Name +'","loc":"' + value.X + ' ' + value.Y + '", "text":"' + value.Name + '", "color": "#7CFC00"}';
                }

            }
        }
        if (idx < listEntry.length) {
            jsonData += ",\n";
        }
    };
    // End build node Id
    jsonData += '],';
    jsonData += "\n";

    jsonData += '"linkDataArray": [';
    jsonData += "\n";
    // Build node array link 
    if (showPASS || showALL) {
        idx = 0;
        var listEntry2 = Object.entries(mapState2OK);
        for (let [key, value] of listEntry2) {
            if (typeof(mapState[value.toNodeName]) == "undefined") {} else {
                var toNodeId = mapState[value.toNodeName].Name;
                var Function = mapState[value.toNodeName].Function;
                var Trigger = mapState[value.toNodeName].Trigger;
                var GuardCond = value.GuardCond;
                var GuardVal = value.GuardVal;
                var GuardText = MachineStateModel.getGuardText(value.GuardCond, value.GuardVal);
                var transition = MachineStateModel.getTransition(value.trigger, value.func);
                if (idx > 0) {
                    jsonData += ",";
                }
                jsonData += '{"from":"' + value.fromName + '", "to":"' + toNodeId + '", "GuardText":"'+GuardText+'", "GuardVal":"'+GuardVal+'","GuardCond":"'+GuardCond+'", "text":"' + transition + '", "Function":"' + value.func + '", "Trigger":"' + value.trigger + '", "isPass":"PASS"}' + "\n";

                idx++;
            }
        };
    }
    jsonData += "\n";
    if (showFAIL|| showALL) {
        //idx = 0;
        var listEntry2 = Object.entries(mapState2FAIL);
        for (let [key, value] of listEntry2) {
            idx++;
            if (typeof(mapState[value.toNodeName]) == "undefined") {
                console.log("Undefine value.toNodeName=" + value.toNodeName);
            } else {
                var toNodeId = mapState[value.toNodeName].Name;
                var Function = mapState[value.toNodeName].Function;
                var Trigger = mapState[value.toNodeName].Trigger;
                if (Trigger == "*") {
                    Trigger = "* " + Function;
                }
                var GuardCond = value.GuardCond;
                var GuardVal = value.GuardVal;
                var GuardText = MachineStateModel.getGuardText(value.GuardCond, value.GuardVal);
                var transition = MachineStateModel.getTransition(value.trigger, value.func);
                jsonData += ',{"from":"' + value.fromName + '", "to":"' + toNodeId + '","GuardText":"'+GuardText+'","GuardVal":"'+GuardVal+'","GuardCond":"'+GuardCond+'", "text":"' + transition + '", "Function":"' + value.func + '", "Trigger":"' + value.trigger + '", "isPass":"FAIL"}' + "\n";

            }
        };
    }
    // End build 
    jsonData += ']}';
    //console.log(jsonData);
    return jsonData;
    // END BUILD
  }
  static getGuardText(GuardCond: any, GuardVal: any) {
    var GuardText = GuardCond + " "+GuardVal;
    if( GuardText.length>20){
      GuardText = GuardCond + "...";
    }
    return GuardText.trim();
  }
  
  static getTransition(Trigger:any, Function:any) {
    var Transition = "";
    if (Trigger.toUpperCase().indexOf("T_") > -1)
        Transition = "" + Trigger + "\\n" + Function;
    else
        Transition =  "" + Trigger;
    return Transition.trim();
  }
  
}