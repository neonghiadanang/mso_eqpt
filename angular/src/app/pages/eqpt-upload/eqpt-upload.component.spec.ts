import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NexgenecEqptUploadSettingComponent } from './eqpt-upload.component';

describe('NexgenecEqptUploadSettingComponent', () => {
  let component: NexgenecEqptUploadSettingComponent;
  let fixture: ComponentFixture<NexgenecEqptUploadSettingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NexgenecEqptUploadSettingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NexgenecEqptUploadSettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
