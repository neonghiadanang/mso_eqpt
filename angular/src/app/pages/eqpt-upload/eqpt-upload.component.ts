import { Component, ViewChild, OnInit, OnDestroy,Inject } from '@angular/core';
import { DataSource} from '@angular/cdk/collections';
import { MatPaginator } from '@angular/material/paginator';
import { BehaviorSubject, Observable, merge } from 'rxjs';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { map } from 'rxjs/operators';
import { BasePageComponent } from '../base-page/base-page.component';
import { Store } from '@ngrx/store';
import { AppState } from '../../interfaces/app-state';

import { MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatDialogModule } from '@angular/material/dialog';
import { NexgenecEqptUploadSetting, NexgenecEqptUploadSettingService } from './eqpt-upload.models';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';

import { NexgenecSourceCode, NexgenecSourceCodeService } from './eqpt-upload.models';

import { Configuration } from '../../../app/configuration';

import { ConfirmationDialogComponent } from '../../ui/components/confirmation-dialog/confirmation-dialog.component';
import { HttpClient, HttpInterceptor, HttpRequest, HttpErrorResponse, HttpHandler, HttpEvent, HttpHeaders } from '@angular/common/http';
import { tap, shareReplay, catchError } from 'rxjs/operators';

export interface DialogData {
  animal: string;
  name: string;
}

@Component({
  selector: 'edit-dialog',
  templateUrl: 'edit.dialog.html',
})
export class DialogNexgenecEqptUploadSettingComponent implements OnInit{
  action:string;
  local_data:any;
  public ownerForm: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<DialogNexgenecEqptUploadSettingComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {
    //console.log(data);
    this.local_data = {...data};
    this.action = this.local_data.action;
  }

  doAction(){
    //this.dialogRef.close({data:this.local_data});
    this.dialogRef.close({data:this.ownerForm.value});
  }

  closeDialog(){
    this.dialogRef.close({event:'Cancel'});
  }

  ngOnInit() {
    this.ownerForm = new FormGroup({
      eqpt_id: new FormControl(this.local_data.eqpt_id, []),
      conn_type: new FormControl(this.local_data.conn_type, [Validators.required, Validators.maxLength(20)]),
      conn_ip: new FormControl(this.local_data.conn_ip, [Validators.required, Validators.maxLength(20)]),
      conn_port: new FormControl(this.local_data.conn_port, [Validators.required, Validators.maxLength(20)]),
      conn_folder: new FormControl(this.local_data.conn_folder, [Validators.required, Validators.maxLength(20)]),
      conn_username: new FormControl(this.local_data.conn_username, [ Validators.maxLength(50)]),
      conn_password: new FormControl(this.local_data.conn_password, [Validators.maxLength(60)]),
      updated_by: new FormControl(this.local_data.updated_by, []),
      updated_time: new FormControl(this.local_data.updated_time, []),
    });
  }

  public hasError = (controlName: string, errorName: string) =>{
    return this.ownerForm.controls[controlName].hasError(errorName);
  }

}


@Component({
  selector: 'nexgenecsourcecode.editdialog',
  templateUrl: 'nexgenecsourcecode.editdialog.html',
})
export class DialogNexgenecSourceCodeComponent implements OnInit{
  action:string;
  local_data:any;
  public ownerForm: FormGroup;
  fileToUpload: File = null;

  constructor(
    public dialogRef: MatDialogRef<DialogNexgenecSourceCodeComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {
    //console.log(data);
    this.local_data = {...data};
    this.action = this.local_data.action;
  }
  doAction(){
    this.dialogRef.close({data:this.ownerForm.value, file: this.fileToUpload});
  }
  handleFileInput(files: FileList) {
      this.fileToUpload = files.item(0);
  }
  closeDialog(){
    this.dialogRef.close({event:'Cancel'});
  }

  ngOnInit() {
    this.ownerForm = new FormGroup({
      seq_id: new FormControl(this.local_data.seq_id, []),
      eqpt_id: new FormControl(this.local_data.eqpt_id, []),
      src_desc: new FormControl(this.local_data.src_desc, []),
      src_version: new FormControl(this.local_data.src_version, []),
      src_comment: new FormControl(this.local_data.src_comment, [Validators.required, Validators.maxLength(200)]),
      uploaded_time: new FormControl(this.local_data.uploaded_time, []),
      uploaded_by: new FormControl(this.local_data.uploaded_by, []),
      approved_time: new FormControl(this.local_data.approved_time, []),
      approved_by: new FormControl(this.local_data.approved_by, []),
    });
  }

  public hasError = (controlName: string, errorName: string) =>{
    return this.ownerForm.controls[controlName].hasError(errorName);
  }

}
@Component({
  selector: 'app-nexgeneceqptuploadsetting',
  templateUrl: './eqpt-upload.component.html',
  styleUrls: ['./eqpt-upload.component.scss']
})

export class NexgenecEqptUploadSettingComponent extends BasePageComponent implements OnInit, OnDestroy {
  displayedColumns: string[];
  database: NexgenecEqptUploadSettingDatabase;
  dataSource: NexgenecEqptUploadSettingDataSource | null;

  displayedColumns2: string[];
  database2: NexgenecSourceCodeDatabase;
  dataSource2: NexgenecSourceCodeDataSource | null;

  eqpt_id = "";

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatPaginator, { static: true }) paginator2: MatPaginator;

  constructor(store: Store<AppState>,
    private eqpSv: NexgenecEqptUploadSettingService,
    private eqpSv2: NexgenecSourceCodeService,
    private http: HttpClient, 
    public dialog: MatDialog,
    private actRoute: ActivatedRoute,
    private router: Router
  ) {
    super(store);

    this.pageData = {
      title: 'Upload Setting',
      loaded: true,
      breadcrumbs: [
      ]
    };
    
    this.displayedColumns = [
      'conn_type',
      'conn_ip',
      'conn_port',
      'conn_folder',

      'conn_username',
      'conn_password',
      'eqpt_id'
    ];
    this.database = new NexgenecEqptUploadSettingDatabase(eqpSv);
    

    this.displayedColumns2 = [
      //'eqpt_id',
      'src_desc',
      'src_version',
      //'src_zip',
      'src_comment',
      'uploaded_time',
      'uploaded_by',
      'approved_time',
      'approved_by',
      'seq_id'
    ];
    this.database2 = new NexgenecSourceCodeDatabase(eqpSv2);

    this.actRoute.paramMap.subscribe(params => {
      this.eqpt_id = params.get('id');
      this.database.search(this.eqpt_id,'')
      this.database2.search(this.eqpt_id,'')
    });
  }

  ngOnInit() {
    super.ngOnInit();
    this.dataSource = new NexgenecEqptUploadSettingDataSource(this.database, this.paginator);
    this.dataSource2 = new NexgenecSourceCodeDataSource(this.database2, this.paginator2);
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }
  deployment(item){
    let dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '500px',  
      data:"Please confirm deployment to host?"
    });
    dialogRef.afterClosed().subscribe(result => {
      // NOTE: The result can also be nothing if the user presses the `esc` key or clicks outside the dialog
      //console.log(result.result);
      if (result.result == true) {
          this.eqpSv.findByQuery({
              "action":"deploy",
              "eqpt_id":item.eqpt_id
            })
            .subscribe(
              response => {
                console.log(response);
                alert(response);
              }
          );
      }
    })
  }
  approve(item){
    var query = "/eqptupload/approve/"+item.seq_id+"/";
    console.log("Send approve request " + query);
    let dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '500px',  
      data:"Do you confirm to approve this source?"
    });
    dialogRef.afterClosed().subscribe(result => {
      // NOTE: The result can also be nothing if the user presses the `esc` key or clicks outside the dialog
      console.log(result.result);
      if (result.result == true) {
          this.eqpSv.get(query)
            .subscribe(
              response => {
                console.log(response)
                this.searchSource('','')
              }
          );
      }
    })
  }
  private handleError(error: any) {
    console.log(error);
  }
  download(item){
    var url = Configuration.server+"/eqptupload/download/"+item.seq_id+"/";
    console.log(url)
    const link = document.createElement('a');
    link.setAttribute('target', '_blank');
    link.setAttribute('href', url);
    // link.setAttribute('download', `products.csv`);
    document.body.appendChild(link);
    link.click();
    link.remove();
  }
  search(id,name){
    this.database.search(this.eqpt_id,name);
  }
  openDialog(event, item) {
    console.log("openDialog "+event )
    console.log(item)
    
    if(event=="Delete"){
      let dialogRef = this.dialog.open(ConfirmationDialogComponent, {
        width: '500px',  
        data:"Do you confirm the deletion of this data?"
      });
      dialogRef.afterClosed().subscribe(result => {
        // NOTE: The result can also be nothing if the user presses the `esc` key or clicks outside the dialog
        console.log(result.result);
        if (result.result == true) {
            this.eqpSv.delete(item.seq_id)
              .subscribe(
                response => {
                  console.log(response)
                  this.search('','')
                }
            );
        }
      })
    }
    else{
    const dialogRef = this.dialog.open(DialogNexgenecEqptUploadSettingComponent, {
      width: '500px',
      data:item
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result==''){
      }
      else{
        if(event=="Update"){
          this.eqpSv.save(result)
            .subscribe(
                response => {
                  console.log(response);
                  this.search('','');
                }
            );
        }
        else if(event=="New"){
          this.eqpSv.update(result.data.seq_id, result)
            .subscribe(
                response => {
                  console.log(response);
                  this.search('','');
                }
            );
        }
      }
    });
    }
  }

  searchSource(id,name){
    this.database2.search(this.eqpt_id,id);
  }
  openDialogSource(event, item) {
    console.log("openDialog "+event )
    console.log(item)
    
    if(event=="Delete"){
      let dialogRef = this.dialog.open(ConfirmationDialogComponent, {
        width: '500px',  
        data:"Do you confirm the deletion of this data?"
      });
      dialogRef.afterClosed().subscribe(result => {
        // NOTE: The result can also be nothing if the user presses the `esc` key or clicks outside the dialog
        console.log(result.result);
        if (result.result == true) {
            this.eqpSv2.delete(item.seq_id)
              .subscribe(
                response => {
                  console.log(response)
                  this.searchSource('','')
                }
            );
        }
      })
    }
    else{
    const dialogRef = this.dialog.open(DialogNexgenecSourceCodeComponent, {
      width: '500px',
      data:item
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result)
      if(result==''){
      }
      else{
        if(event=="New"){
          if(result.data.eqpt_id==null){
            result.data.eqpt_id=this.eqpt_id;
          }
          this.eqpSv2.saveFiles(result.data, result.file )
            .subscribe(
                response => {
                  console.log(response);
                  this.searchSource('','');
                }
            );
        }
        else if(event=="Update"){
          this.eqpSv2.updateFiles(result.data.seq_id, result.data, result.file)
            .subscribe(
                response => {
                  console.log(response);
                  this.searchSource('','');
                }
            );
        }
      }
    });
    }
  }
}

/** An example database that the data source uses to retrieve data for the table. */
export class NexgenecEqptUploadSettingDatabase {
  
  /** Stream that emits whenever the data has been modified. */
  dataChange: BehaviorSubject<NexgenecEqptUploadSetting[]> = new BehaviorSubject<NexgenecEqptUploadSetting[]>([]);

  get data(): NexgenecEqptUploadSetting[] {
    return this.dataChange.value; 
  }
  
  constructor(private eqpSer: NexgenecEqptUploadSettingService) {
    this.eqpSer = eqpSer;
    // this.search('','');
  }

  search(eqpt_id: any, fullname: any) {
    this.eqpSer.findByQuery(
      { 
        "eqpt_id":eqpt_id
      }).subscribe(
      data => {
        var arr = <Array<any>>data;
        const copiedData = [];
        arr.forEach(function (value) {
            copiedData.push( value);
        });
        this.dataChange.next(copiedData);
      },
      err => {
        console.log(err);
      },
      () => {
        //(callbackFnName && typeof this[callbackFnName] === 'function') ? this[callbackFnName](this[dataName]) : null;
      }
    );
  }

}

/**
 * Data source to provide what data should be rendered in the table. Note that the data source
 * can retrieve its data in any way. In this case, the data source is provided a reference
 * to a common data base, NexgenecEqptUploadSettingDatabase. It is not the data source's responsibility to manage
 * the underlying data. Instead, it only needs to take the data and send the table exactly what
 * should be rendered.
 */
export class NexgenecEqptUploadSettingDataSource extends DataSource<any> {
  constructor(private _database: NexgenecEqptUploadSettingDatabase, private _paginator: MatPaginator) {
    super();
  }

  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<NexgenecEqptUploadSetting[]> {


    const displayDataChanges = [
      this._database.dataChange,
      this._paginator.page,
    ];

    return merge(...displayDataChanges).pipe(
            map(() => {
                const data = this._database.data.slice();

                // Grab the page's slice of data.
                const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
                return data.splice(startIndex, this._paginator.pageSize);
            })
        );
  }

  disconnect() {}
}

/** An example database that the data source uses to retrieve data for the table. */
export class NexgenecSourceCodeDatabase {
  
  /** Stream that emits whenever the data has been modified. */
  dataChange: BehaviorSubject<NexgenecSourceCode[]> = new BehaviorSubject<NexgenecSourceCode[]>([]);

  get data(): NexgenecSourceCode[] {
    return this.dataChange.value; 
  }
  
  constructor(private eqpSer: NexgenecSourceCodeService) {
    this.eqpSer = eqpSer;
    this.search('','');
  }

  search(eqpt_id: any, fullname: any) {
    if(eqpt_id.length==0){
      return;
    }
    this.eqpSer.findByQuery(
      { 
        "eqpt_id":eqpt_id
      }).subscribe(
      data => {
        var arr = <Array<any>>data;
        const copiedData = [];
        arr.forEach(function (value) {
            copiedData.push( value);
            console.log("data---" + value)
            console.log(value)
        });
        this.dataChange.next(copiedData);
      },
      err => {
        console.log(err);
      },
      () => {
        //(callbackFnName && typeof this[callbackFnName] === 'function') ? this[callbackFnName](this[dataName]) : null;
      }
    );
  }

}

/**
 * Data source to provide what data should be rendered in the table. Note that the data source
 * can retrieve its data in any way. In this case, the data source is provided a reference
 * to a common data base, NexgenecSourceCodeDatabase. It is not the data source's responsibility to manage
 * the underlying data. Instead, it only needs to take the data and send the table exactly what
 * should be rendered.
 */
export class NexgenecSourceCodeDataSource extends DataSource<any> {
  constructor(private _database: NexgenecSourceCodeDatabase, private _paginator: MatPaginator) {
    super();
  }

  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<NexgenecSourceCode[]> {


    const displayDataChanges = [
      this._database.dataChange,
      this._paginator.page,
    ];

    return merge(...displayDataChanges).pipe(
            map(() => {
                const data = this._database.data.slice();

                // Grab the page's slice of data.
                const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
                return data.splice(startIndex, this._paginator.pageSize);
            })
        );
  }

  disconnect() {}
}