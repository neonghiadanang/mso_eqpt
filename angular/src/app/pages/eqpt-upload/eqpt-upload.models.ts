import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CrudService } from '../../services/crud.service';
import { HttpResponse ,HttpHeaders} from '@angular/common/http';

export class NexgenecEqptUploadSetting {
    eqpt_id : any;
    conn_type : any;
    conn_ip : any;
    conn_port : any;
    conn_folder : any;
    conn_username : any;
    conn_password : any;
    updated_by : any;
    updated_time : any;
}

@Injectable({
    providedIn: 'root',
  })
  export class NexgenecEqptUploadSettingService extends CrudService<NexgenecEqptUploadSetting, number> {
    constructor(protected _http: HttpClient) {
      super(_http, "eqptupload/conn");
    }
}

// approved_by: ""
// approved_time: null
// eqpt_id: "LeadsCompDispenser01"
// seq_id: 3
// src_comment: "3"
// src_desc: "ShareMe.zip"
// src_version: 1
// uploaded_by: "TINHL2"
// uploaded_time: "2020-09-23T02:00:44.512Z"
export class NexgenecSourceCode {
  seq_id : any;
  eqpt_id : any;
  src_desc : any;
  src_version : any;
  src_comment : any;
  uploaded_time : any;
  uploaded_by : any;
  approved_time : any;
  approved_by : any;
}

@Injectable({
  providedIn: 'root',
})
export class NexgenecSourceCodeService extends CrudService<NexgenecSourceCode, number> {
  constructor(protected _http: HttpClient) {
    super(_http, "eqptupload/code");
  }
}