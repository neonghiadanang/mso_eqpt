import { Component, OnDestroy, OnInit } from '@angular/core';
import { BasePageComponent } from '../../base-page/base-page.component';
import { Store } from '@ngrx/store';
import { AppState } from '../../../interfaces/app-state';
import {AuthService }  from '../../../services/auth/auth.service';
import { Router } from '@angular/router';
import { AppSettings } from '../../../interfaces/settings';
import * as SettingsActions from '../../../store/actions/app-settings.actions';

@Component({
  selector: 'page-sign-in-1',
  templateUrl: './sign-in-1.component.html',
  styleUrls: ['./sign-in-1.component.scss']
})

export class PageSignIn1Component extends BasePageComponent implements OnInit, OnDestroy {
  appSettings: AppSettings;
  submitted = false;
  error: any;
  mensagem : string;
  username: string;
  password: string;
  hide = true;

  constructor(store: Store<AppState>, 
    private authService: AuthService,
    private router: Router
    ) {
    super(store);
    this.hide = true;
    this.pageData = {
      title: '',
      loaded: true,
      breadcrumbs: []
    };
  }

  ngOnInit() {
    super.ngOnInit();
    this.store.select('appSettings').subscribe(settings => {
      console.log("ActingOnInit logon")
      console.log(settings)
      this.appSettings = settings;
    });
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }
  login(username: string, password: string) {
    // console.log("LOGIN " + username+" PASSWORD="+password)
    this.mensagem = "Begin login...";
    this.authService.login(username, btoa(password)).subscribe(
      success => {
        var appSettings = {
          loginId:username,
          operationId:"TEST",
          siteId:"MSO"
        }
        this.store.dispatch(new SettingsActions.Update(appSettings));
        this.router.navigate(['']);
      },
      error => {
        console.log(error);
        this.mensagem = error.detail;
        console.log(this.mensagem);
        this.submitted = true;
      }
    );
  }
}
