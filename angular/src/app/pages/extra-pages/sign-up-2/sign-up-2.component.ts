import { Component, OnDestroy, OnInit } from '@angular/core';
import { BasePageComponent } from '../../base-page/base-page.component';
import { Store } from '@ngrx/store';
import { AppState } from '../../../interfaces/app-state';

@Component({
  selector: 'page-sign-up-2',
  templateUrl: './sign-up-2.component.html',
  styleUrls: ['./sign-up-2.component.scss']
})
export class PageSignUp2Component extends BasePageComponent implements OnInit, OnDestroy {
  constructor(store: Store<AppState>) {
    super(store);

    this.pageData = {
      title: 'Sign Up',
      loaded: true,
      breadcrumbs: [
        {
          title: 'Pages',
          route: './dashboard'
        },
        {
          title: 'Extra pages',
          route: './dashboard'
        },
        {
          title: 'Sign Up'
        }
      ]
    };
  }

  ngOnInit() {
    super.ngOnInit();
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }
}
