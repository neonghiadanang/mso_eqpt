import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageFirebaseComponent } from './firebase.component';

describe('PageFirebaseComponent', () => {
  let component: PageFirebaseComponent;
  let fixture: ComponentFixture<PageFirebaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageFirebaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageFirebaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
