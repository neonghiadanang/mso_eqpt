import { Component, OnDestroy, OnInit } from '@angular/core';
import { BasePageComponent } from '../base-page/base-page.component';
import { Store } from '@ngrx/store';
import { AppState } from '../../interfaces/app-state';
import { Sale } from '../dashboards/dashboard-4/dashboard-4.component';
import { FirebaseService } from '../../services/firebase/firebase.service';
import echarts, { EChartOption } from 'echarts';

@Component({
  selector: 'page-firebase',
  templateUrl: './firebase.component.html',
  styleUrls: ['./firebase.component.scss']
})
export class PageFirebaseComponent extends BasePageComponent implements OnInit, OnDestroy {
  salesColumns: string[];
  sales: Sale[];
  chartData: any[];
  browsersOptions: EChartOption;

  constructor(
    store: Store<AppState>,
    private firebaseSv: FirebaseService
  ) {
    super(store);

    this.pageData = {
      title: 'Firebase',
      loaded: false,
      breadcrumbs: [
        {
          title: 'Main',
          route: './dashboard'
        },
        {
          title: 'Firebase'
        }
      ]
    };
    this.salesColumns = ['no', 'product', 'quantity', 'status', 'amount', 'actions'];
    this.sales = [];
  }

  ngOnInit() {
    super.ngOnInit();

    this.getSales();
    this.getBrowsersData();
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }

  getSales() {
    this.firebaseSv.getItems('/table').subscribe(
      data => {
        this.sales = data;

        // set this.loaded = true
        this.setLoaded(400);
      },
      err => {
        this.sales = [];

        console.log(err);
      }
    );
  }

  getBrowsersData() {
    this.firebaseSv.getItems('/chart').subscribe(
      data => {
        this.chartData = data;
        this.setBrowsersChart(data);
      },
      err => {
        this.chartData = [];

        console.log(err);
      }
    );
  }

  setBrowsersChart(data: any[]) {
    const chartData = data.map(item => {
      return {
        value: item.value,
        name: item.name,
        itemStyle: {
          normal: {
            color: item.color,
            borderColor: item.bgColor
          }
        }
      }
    });

    this.browsersOptions = {
      grid: {
        left: 0,
        right: 0,
        top: 0,
        bottom: 0
      },
      tooltip : {
        trigger: 'item',
        formatter: '{b}<br>{c} ({d}%)'
      },
      series: [{
        name: 'pie',
        type: 'pie',
        selectedMode: 'single',
        selectedOffset: 30,
        clockwise: true,
        radius: [0, '90%'],
        data: chartData,
        itemStyle: {
          normal: {
            label: {
              color: '#000',
              position: 'inner'
            },
            labelLine: {
              show: false
            }
          }
        }
      }]
    };
  }
}
