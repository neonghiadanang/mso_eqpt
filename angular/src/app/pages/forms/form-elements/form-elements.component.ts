import { Component, OnDestroy, OnInit } from '@angular/core';

import { BasePageComponent } from '../../base-page/base-page.component';
import { Store } from '@ngrx/store';
import { AppState } from '../../../interfaces/app-state';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

interface State {
  flag: string;
  name: string;
  population: string;
}

@Component({
  selector: 'page-form-elements',
  templateUrl: './form-elements.component.html',
  styleUrls: ['./form-elements.component.scss']
})
export class PageFormElementsComponent extends BasePageComponent implements OnInit, OnDestroy {
  control1: FormControl;
  control2: FormControl;
  filteredStates: Observable<State[]>;
  filteredStates2: Observable<State[]>;
  states: State[];
  states2: State[];
  date: FormControl;

  constructor(store: Store<AppState>) {
    super(store);

    this.pageData = {
      title: 'Form elements',
      loaded: true,
      breadcrumbs: [
        {
          title: 'UI elements',
          route: './dashboard'
        },
        {
          title: 'Forms',
          route: './dashboard'
        },
        {
          title: 'Form elements'
        }
      ]
    };
    this.control1 = new FormControl();
    this.control2 = new FormControl('California');
    this.states = [
      {
        name: 'Arkansas',
        population: '2.978M',
        flag: 'assets/content/avatar-1.jpg'
      },
      {
        name: 'California',
        population: '39.14M',
        flag: 'assets/content/avatar-2.jpg'
      },
      {
        name: 'Florida',
        population: '20.27M',
        flag: 'assets/content/avatar-3.jpg'
      },
      {
        name: 'Texas',
        population: '27.47M',
        flag: 'assets/content/avatar-4.jpg'
      }
    ];
    this.states2 = [
      {
        name: 'Arkansas',
        population: '2.978M',
        flag: 'assets/content/avatar-1.jpg'
      },
      {
        name: 'California',
        population: '39.14M',
        flag: 'assets/content/avatar-2.jpg'
      },
      {
        name: 'Florida',
        population: '20.27M',
        flag: 'assets/content/avatar-3.jpg'
      },
      {
        name: 'Texas',
        population: '27.47M',
        flag: 'assets/content/avatar-4.jpg'
      }
    ];
    this.date = new FormControl(new Date());
  }

  ngOnInit() {
    super.ngOnInit();

    this.filteredStates = this.control1.valueChanges.pipe(
      startWith(''),
      map(state => state ? this.filterStates(state) : this.states.slice())
    );
    this.filteredStates2 = this.control2.valueChanges.pipe(
      startWith(''),
      map(state => state ? this.filterStates2(state) : this.states2.slice())
    );
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }

  filterStates(value: string): State[] {
    const filterValue = value.toLowerCase();

    return this.states.filter(state => state.name.toLowerCase().indexOf(filterValue) === 0);
  }

  filterStates2(value: string): State[] {
    const filterValue = value.toLowerCase();

    return this.states2.filter(state => state.name.toLowerCase().indexOf(filterValue) === 0);
  }
}
