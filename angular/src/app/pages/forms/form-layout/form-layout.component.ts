import { Component, OnDestroy, OnInit } from '@angular/core';
import { BasePageComponent } from '../../base-page/base-page.component';
import { Store } from '@ngrx/store';
import { AppState } from '../../../interfaces/app-state';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'page-form-layout',
  templateUrl: './form-layout.component.html',
  styleUrls: ['./form-layout.component.scss']
})
export class PageFormLayoutComponent extends BasePageComponent implements OnInit, OnDestroy {
  form: FormGroup;

  constructor(
    store: Store<AppState>,
    private formBuilder: FormBuilder
  ) {
    super(store);

    this.pageData = {
      title: 'Form layout',
      loaded: true,
      breadcrumbs: [
        {
          title: 'UI elements',
          route: './dashboard'
        },
        {
          title: 'Forms',
          route: './dashboard'
        },
        {
          title: 'Form layout'
        }
      ]
    };
  }

  ngOnInit() {
    super.ngOnInit();

    this.initForm();
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }

  initForm() {
    this.form = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required]
    });
  }
}
