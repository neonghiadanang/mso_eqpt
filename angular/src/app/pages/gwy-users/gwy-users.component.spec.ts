import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GwyUsersComponent } from './gwy-users.component';

describe('GwyUsersComponent', () => {
  let component: GwyUsersComponent;
  let fixture: ComponentFixture<GwyUsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GwyUsersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GwyUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
