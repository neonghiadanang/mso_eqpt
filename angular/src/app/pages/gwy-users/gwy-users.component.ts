import { Component, ViewChild, OnInit, OnDestroy,Inject } from '@angular/core';
import { DataSource} from '@angular/cdk/collections';
import { MatPaginator } from '@angular/material/paginator';
import { BehaviorSubject, Observable, merge } from 'rxjs';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { map } from 'rxjs/operators';
import { BasePageComponent } from '../base-page/base-page.component';
import { Store } from '@ngrx/store';
import { AppState } from '../../interfaces/app-state';

import { MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatDialogModule } from '@angular/material/dialog';
import { GwyUsers, GwyUsersService } from './gwy-users.model';
import { ActivatedRoute } from '@angular/router';


export interface DialogData {
  animal: string;
  name: string;
}

@Component({
  selector: 'edit-dialog',
  templateUrl: 'edit.dialog.html',
})
export class DialogGwyUsersComponent implements OnInit{
  action:string;
  local_data:any;
  public ownerForm: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<DialogGwyUsersComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {
    //console.log(data);
    this.local_data = {...data};
    this.action = this.local_data.action;
  }

  doAction(){
    this.dialogRef.close({data:this.local_data});
  }

  closeDialog(){
    this.dialogRef.close({event:'Cancel'});
  }

  ngOnInit() {
    this.ownerForm = new FormGroup({
      id: new FormControl(this.local_data.id, [Validators.required, Validators.maxLength(0)]),
      username: new FormControl(this.local_data.username, [Validators.required, Validators.maxLength(50)]),
      phone: new FormControl(this.local_data.phone, [Validators.required, Validators.maxLength(50)]),
      email: new FormControl(this.local_data.email, [Validators.required, Validators.maxLength(50)]),
      fullname: new FormControl(this.local_data.fullname, [Validators.required, Validators.maxLength(100)]),
      supervisor: new FormControl(this.local_data.supervisor, [Validators.required, Validators.maxLength(100)]),
      is_admin: new FormControl(this.local_data.is_admin, [Validators.required, Validators.maxLength(1)]),
      del_flag: new FormControl(this.local_data.del_flag, [Validators.required, Validators.maxLength(1)]),
      supervisor_email: new FormControl(this.local_data.supervisor_email, [Validators.required, Validators.maxLength(128)]),
      site_cn: new FormControl(this.local_data.site_cn, [Validators.required, Validators.maxLength(50)]),
    });
  }

  public hasError = (controlName: string, errorName: string) =>{
    return this.ownerForm.controls[controlName].hasError(errorName);
  }

}

@Component({
  selector: 'app-gwyusers',
  templateUrl: './gwy-users.component.html',
  styleUrls: ['./gwy-users.component.scss']
})

export class GwyUsersComponent extends BasePageComponent implements OnInit, OnDestroy {
  displayedColumns: string[];
  database: GwyUsersDatabase;
  dataSource: GwyUsersDataSource | null;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(store: Store<AppState>,
    private eqpSv: GwyUsersService,
    public dialog: MatDialog,
    private actRoute: ActivatedRoute
  ) {
    super(store);

    this.pageData = {
      title: 'GwyUsers 1',
      loaded: true,
      breadcrumbs: [
        {
          title: 'GwyUserss',
          route: './dashboard'
        },
        {
          title: 'GwyUsers 1'
        }
      ]
    };
    
    this.displayedColumns = [
      'username',
      'phone',
      'email',
      'fullname',
      'supervisor',
      'is_admin',
      'del_flag',
      'supervisor_email',
      'site_cn',
      'id'
    ];
    this.database = new GwyUsersDatabase(eqpSv);
  }

  ngOnInit() {
    super.ngOnInit();
    this.actRoute.paramMap.subscribe(params => {
      console.log( "EQPT_ID=" + params.get('id') )
    });
    this.dataSource = new GwyUsersDataSource(this.database, this.paginator);
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }
 
  search(id,name){
    //console.log(id + " "+name)
    this.database.search(id,name);
  }
  openDialog(event, item) {
    const dialogRef = this.dialog.open(DialogGwyUsersComponent, {
      width: '500px',
      data:item
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result==''){
      }
      else{
        if(event=="Update"){
          this.eqpSv.save(result)
            .subscribe(
                response => console.log(response)
            );
        }
        else if(event=="New"){
          this.eqpSv.save(result)
            .subscribe(
                response => console.log(response)
            );
        }
        else if(event=="Delete"){
          this.eqpSv.delete(result.data.ID)
            .subscribe(
                response => console.log(response)
            );
        }
      }
    });
  }
}

/** An example database that the data source uses to retrieve data for the table. */
export class GwyUsersDatabase {
  
  /** Stream that emits whenever the data has been modified. */
  dataChange: BehaviorSubject<GwyUsers[]> = new BehaviorSubject<GwyUsers[]>([]);

  get data(): GwyUsers[] {
    return this.dataChange.value; 
  }
  
  constructor(private eqpSer: GwyUsersService) {
    this.eqpSer = eqpSer;
    this.search('','');
  }

  search(username: any, fullname: any) {
    this.eqpSer.findByQuery({"username":username}).subscribe(
      data => {
        console.log(data);
        // console.log(data.length);
        // const copiedData = [];
        // for(let i=0; i<data.length; i++){
        //   copiedData.push( data[i]);
        // }
        // this.dataChange.next(copiedData);
      },
      err => {
        console.log(err);
      },
      () => {
        //(callbackFnName && typeof this[callbackFnName] === 'function') ? this[callbackFnName](this[dataName]) : null;
      }
    );
  }

}

/**
 * Data source to provide what data should be rendered in the table. Note that the data source
 * can retrieve its data in any way. In this case, the data source is provided a reference
 * to a common data base, GwyUsersDatabase. It is not the data source's responsibility to manage
 * the underlying data. Instead, it only needs to take the data and send the table exactly what
 * should be rendered.
 */
export class GwyUsersDataSource extends DataSource<any> {
  constructor(private _database: GwyUsersDatabase, private _paginator: MatPaginator) {
    super();
  }

  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<GwyUsers[]> {


    const displayDataChanges = [
      this._database.dataChange,
      this._paginator.page,
    ];

    return merge(...displayDataChanges).pipe(
            map(() => {
                const data = this._database.data.slice();

                // Grab the page's slice of data.
                const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
                return data.splice(startIndex, this._paginator.pageSize);
            })
        );
  }

  disconnect() {}
}