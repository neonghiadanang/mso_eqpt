import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CrudService } from '../../services/crud.service';
import { HttpResponse, HttpHeaders} from '@angular/common/http';

export class GwyUsers {
    id : any;
    username : any;
    phone : any;
    email : any;
    fullname : any;
    supervisor : any;
    is_admin : any;
    del_flag : any;
    supervisor_email : any;
    site_cn : any;
}

@Injectable({
    providedIn: 'root',
  })
  export class GwyUsersService extends CrudService<GwyUsers, number> {
    constructor(protected _http: HttpClient) {
      super(  _http, 
              "http://localhost:8000/listeqpdetail"
              );
    }
}