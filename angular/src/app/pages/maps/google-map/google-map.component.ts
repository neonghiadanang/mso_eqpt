import { Component, OnDestroy, OnInit } from '@angular/core';

import { Store } from '@ngrx/store';

import { BasePageComponent } from '../../base-page/base-page.component';
import { AppState } from '../../../interfaces/app-state';

@Component({
  selector: 'page-google-map',
  templateUrl: './google-map.component.html',
  styleUrls: ['./google-map.component.scss']
})
export class PageGoogleMapComponent extends BasePageComponent implements OnInit, OnDestroy {
  lat: number;
  lng: number;

  constructor(store: Store<AppState>) {
    super(store);

    this.pageData = {
      title: 'Google map',
      loaded: true,
      breadcrumbs: []
    };
    this.lat = 50.4664212;
    this.lng = 30.6;
  }

  ngOnInit() {
    super.ngOnInit();
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }
}
