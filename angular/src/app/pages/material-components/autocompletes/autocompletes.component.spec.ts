import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageMAutocompletesComponent } from './autocompletes.component';

describe('PageMAutocompletesComponent', () => {
  let component: PageMAutocompletesComponent;
  let fixture: ComponentFixture<PageMAutocompletesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageMAutocompletesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageMAutocompletesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
