import { Component, OnDestroy, OnInit } from '@angular/core';
import { BasePageComponent } from '../../base-page/base-page.component';
import { Store } from '@ngrx/store';
import { AppState } from '../../../interfaces/app-state';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

interface State {
  flag: string;
  name: string;
  population: string;
}
interface User {
  name: string;
}
interface StateGroup {
  letter: string;
  names: string[];
}
const _filter = (opt: string[], value: string): string[] => {
  const filterValue = value.toLowerCase();

  return opt.filter(item => item.toLowerCase().indexOf(filterValue) === 0);
};

@Component({
  selector: 'page-m-autocompletes',
  templateUrl: './autocompletes.component.html',
  styleUrls: ['./autocompletes.component.scss']
})
export class PageMAutocompletesComponent extends BasePageComponent implements OnInit, OnDestroy {
  control1: FormControl;
  control2: FormControl;
  control3: FormControl;
  control4: FormControl;
  control5: FormControl;
  filteredStates: Observable<State[]>;
  states: State[];
  options: string[];
  options2: User[];
  filteredOptions: Observable<User[]>;
  filteredOptions2: Observable<string[]>;
  filteredOptions3: Observable<string[]>;
  stateForm: FormGroup;
  stateGroups: StateGroup[];
  stateGroupOptions: Observable<StateGroup[]>;

  constructor(
    store: Store<AppState>,
    private formBuilder: FormBuilder
  ) {
    super(store);

    this.pageData = {
      title: 'Autocompletes',
      loaded: true,
      breadcrumbs: [
        {
          title: 'UI elements',
          route: './dashboard'
        },
        {
          title: 'Material components',
          route: './dashboard'
        },
        {
          title: 'Autocompletes'
        }
      ]
    };
    this.control1 = new FormControl();
    this.control2 = new FormControl();
    this.control3 = new FormControl();
    this.control4 = new FormControl();
    this.control5 = new FormControl();
    this.states = [
      {
        name: 'Arkansas',
        population: '2.978M',
        flag: 'assets/content/avatar-1.jpg'
      },
      {
        name: 'California',
        population: '39.14M',
        flag: 'assets/content/avatar-2.jpg'
      },
      {
        name: 'Florida',
        population: '20.27M',
        flag: 'assets/content/avatar-3.jpg'
      },
      {
        name: 'Texas',
        population: '27.47M',
        flag: 'assets/content/avatar-4.jpg'
      }
    ];
    this.options = ['One', 'Two', 'Three'];
    this.options2 = [
      { name: 'Mary' },
      { name: 'Shelley' },
      { name: 'Igor' }
    ];
    this.stateForm = this.formBuilder.group({
      stateGroup: '',
    });
    this.stateGroups = [{
      letter: 'A',
      names: ['Alabama', 'Alaska', 'Arizona', 'Arkansas']
    }, {
      letter: 'C',
      names: ['California', 'Colorado', 'Connecticut']
    }, {
      letter: 'D',
      names: ['Delaware']
    }, {
      letter: 'F',
      names: ['Florida']
    }, {
      letter: 'G',
      names: ['Georgia']
    }, {
      letter: 'H',
      names: ['Hawaii']
    }, {
      letter: 'I',
      names: ['Idaho', 'Illinois', 'Indiana', 'Iowa']
    }, {
      letter: 'K',
      names: ['Kansas', 'Kentucky']
    }, {
      letter: 'L',
      names: ['Louisiana']
    }, {
      letter: 'M',
      names: ['Maine', 'Maryland', 'Massachusetts', 'Michigan',
        'Minnesota', 'Mississippi', 'Missouri', 'Montana']
    }, {
      letter: 'N',
      names: ['Nebraska', 'Nevada', 'New Hampshire', 'New Jersey',
        'New Mexico', 'New York', 'North Carolina', 'North Dakota']
    }, {
      letter: 'O',
      names: ['Ohio', 'Oklahoma', 'Oregon']
    }, {
      letter: 'P',
      names: ['Pennsylvania']
    }, {
      letter: 'R',
      names: ['Rhode Island']
    }, {
      letter: 'S',
      names: ['South Carolina', 'South Dakota']
    }, {
      letter: 'T',
      names: ['Tennessee', 'Texas']
    }, {
      letter: 'U',
      names: ['Utah']
    }, {
      letter: 'V',
      names: ['Vermont', 'Virginia']
    }, {
      letter: 'W',
      names: ['Washington', 'West Virginia', 'Wisconsin', 'Wyoming']
    }];
  }

  ngOnInit() {
    super.ngOnInit();

    this.filteredStates = this.control1.valueChanges.pipe(
      startWith(''),
      map(state => state ? this.filterStates(state) : this.states.slice())
    );
    this.filteredOptions = this.control3.valueChanges.pipe(
      startWith(''),
      map(value => typeof value === 'string' ? value : value.name),
      map(name => name ? this.filter(name) : this.options2.slice())
    );
    this.filteredOptions2 = this.control4.valueChanges.pipe(
      startWith(''),
      map(value => this.filter2(value))
    );
    this.stateGroupOptions = this.stateForm.get('stateGroup').valueChanges.pipe(
      startWith(''),
      map(value => this.filterGroup(value))
    );
    this.filteredOptions3 = this.control5.valueChanges.pipe(
      startWith(''),
      map(value => this.filter3(value))
    );
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }

  filterStates(value: string): State[] {
    const filterValue = value.toLowerCase();

    return this.states.filter(state => state.name.toLowerCase().indexOf(filterValue) === 0);
  }

  displayFn(user?: User): string | undefined {
    return user ? user.name : undefined;
  }

  filter(name: string): User[] {
    const filterValue = name.toLowerCase();

    return this.options2.filter(option => option.name.toLowerCase().indexOf(filterValue) === 0);
  }

  filter2(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.options.filter(option => option.toLowerCase().includes(filterValue));
  }

  filterGroup(value: string): StateGroup[] {
    if (value) {
      return this.stateGroups
        .map(group => ({letter: group.letter, names: _filter(group.names, value)}))
        .filter(group => group.names.length > 0);
    }

    return this.stateGroups;
  }

  filter3(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.options.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
  }
}
