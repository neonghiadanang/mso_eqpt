import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageMBadgesComponent } from './badges.component';

describe('PageMBadgesComponent', () => {
  let component: PageMBadgesComponent;
  let fixture: ComponentFixture<PageMBadgesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageMBadgesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageMBadgesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
