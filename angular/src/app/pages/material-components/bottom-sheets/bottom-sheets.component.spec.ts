import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageMBottomSheetsComponent } from './bottom-sheets.component';

describe('PageMBottomSheetsComponent', () => {
  let component: PageMBottomSheetsComponent;
  let fixture: ComponentFixture<PageMBottomSheetsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageMBottomSheetsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageMBottomSheetsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
