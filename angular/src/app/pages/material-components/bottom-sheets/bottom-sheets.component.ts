import { Component, OnDestroy, OnInit } from '@angular/core';
import { BasePageComponent } from '../../base-page/base-page.component';
import { Store } from '@ngrx/store';
import { AppState } from '../../../interfaces/app-state';
import { MatBottomSheet, MatBottomSheetRef } from '@angular/material/bottom-sheet';

@Component({
  selector: 'page-m-bottom-sheets',
  templateUrl: './bottom-sheets.component.html',
  styleUrls: ['./bottom-sheets.component.scss']
})
export class PageMBottomSheetsComponent extends BasePageComponent implements OnInit, OnDestroy {
  constructor(
    store: Store<AppState>,
    private _bottomSheet: MatBottomSheet
  ) {
    super(store);

    this.pageData = {
      title: 'Bottom sheets',
      loaded: true,
      breadcrumbs: [
        {
          title: 'UI elements',
          route: './dashboard'
        },
        {
          title: 'Material components',
          route: './dashboard'
        },
        {
          title: 'Bottom sheets'
        }
      ]
    };
  }

  ngOnInit() {
    super.ngOnInit();
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }

  openBottomSheet(): void {
    this._bottomSheet.open(BottomSheetOverviewComponent);
  }
}

@Component({
  selector: 'bottom-sheet-overview',
  templateUrl: 'bottom-sheet-overview.html',
})
export class BottomSheetOverviewComponent {
  constructor(private _bottomSheetRef: MatBottomSheetRef<BottomSheetOverviewComponent>) {}

  openLink(event: MouseEvent): void {
    event.preventDefault();

    this._bottomSheetRef.dismiss();
  }
}
