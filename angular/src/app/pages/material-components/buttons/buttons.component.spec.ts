import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageMButtonsComponent } from './buttons.component';

describe('PageMButtonsComponent', () => {
  let component: PageMButtonsComponent;
  let fixture: ComponentFixture<PageMButtonsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageMButtonsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageMButtonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
