import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageMCardsComponent } from './cards.component';

describe('PageMCardsComponent', () => {
  let component: PageMCardsComponent;
  let fixture: ComponentFixture<PageMCardsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageMCardsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageMCardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
