import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageMCheckboxesComponent } from './checkboxes.component';

describe('PageMCheckboxesComponent', () => {
  let component: PageMCheckboxesComponent;
  let fixture: ComponentFixture<PageMCheckboxesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageMCheckboxesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageMCheckboxesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
