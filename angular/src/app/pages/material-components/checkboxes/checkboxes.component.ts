import { Component, OnDestroy, OnInit } from '@angular/core';
import { BasePageComponent } from '../../base-page/base-page.component';
import { Store } from '@ngrx/store';
import { AppState } from '../../../interfaces/app-state';

@Component({
  selector: 'page-m-checkboxes',
  templateUrl: './checkboxes.component.html',
  styleUrls: ['./checkboxes.component.scss']
})
export class PageMCheckboxesComponent extends BasePageComponent implements OnInit, OnDestroy {
  checked: boolean;
  indeterminate: boolean;
  labelPosition: string;
  disabled: boolean;

  constructor(store: Store<AppState>) {
    super(store);

    this.pageData = {
      title: 'Checkboxes',
      loaded: true,
      breadcrumbs: [
        {
          title: 'UI elements',
          route: './dashboard'
        },
        {
          title: 'Material components',
          route: './dashboard'
        },
        {
          title: 'Checkboxes'
        }
      ]
    };
    this.checked = false;
    this.indeterminate = false;
    this.labelPosition = 'after';
    this.disabled = false;
  }

  ngOnInit() {
    super.ngOnInit();
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }
}
