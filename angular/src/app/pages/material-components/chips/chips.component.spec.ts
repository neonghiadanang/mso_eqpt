import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageMChipsComponent } from './chips.component';

describe('PageMChipsComponent', () => {
  let component: PageMChipsComponent;
  let fixture: ComponentFixture<PageMChipsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageMChipsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageMChipsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
