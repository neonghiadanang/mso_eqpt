import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { BasePageComponent } from '../../base-page/base-page.component';
import { Store } from '@ngrx/store';
import { AppState } from '../../../interfaces/app-state';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatAutocomplete, MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatChipInputEvent } from '@angular/material/chips';
import { map, startWith } from 'rxjs/operators';
import { ThemePalette } from '@angular/material/core';

export interface ChipColor {
  name: string;
  color: ThemePalette;
}
export interface Fruit {
  name: string;
}

@Component({
  selector: 'page-m-chips',
  templateUrl: './chips.component.html',
  styleUrls: ['./chips.component.scss']
})
export class PageMChipsComponent extends BasePageComponent implements OnInit, OnDestroy {
  visible: boolean;
  selectable: boolean;
  removable: boolean;
  addOnBlur: boolean;
  separatorKeysCodes: number[];
  fruitCtrl: FormControl;
  filteredFruits: Observable<string[]>;
  fruits: string[];
  fruits2: Fruit[];
  allFruits: string[];
  availableColors: ChipColor[];

  @ViewChild('fruitInput', {static: false}) fruitInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto', {static: false}) matAutocomplete: MatAutocomplete;

  constructor(store: Store<AppState>) {
    super(store);

    this.pageData = {
      title: 'Chips',
      loaded: true,
      breadcrumbs: [
        {
          title: 'UI elements',
          route: './dashboard'
        },
        {
          title: 'Material components',
          route: './dashboard'
        },
        {
          title: 'Chips'
        }
      ]
    };
    this.visible = true;
    this.selectable = true;
    this.removable = true;
    this.addOnBlur = true;
    this.separatorKeysCodes = [ENTER, COMMA];
    this.fruitCtrl = new FormControl();
    this.fruits = ['Lemon'];
    this.fruits2 = [
      { name: 'Lemon' },
      { name: 'Lime' },
      { name: 'Apple' },
    ];
    this.allFruits = ['Apple', 'Lemon', 'Lime', 'Orange', 'Strawberry'];
    this.availableColors = [
      { name: 'none', color: undefined },
      { name: 'Primary', color: 'primary' },
      { name: 'Accent', color: 'accent' },
      { name: 'Warn', color: 'warn' }
    ];

    this.filteredFruits = this.fruitCtrl.valueChanges.pipe(
      startWith(null),
      map((fruit: string | null) => fruit ? this._filter(fruit) : this.allFruits.slice())
    );
  }

  ngOnInit() {
    super.ngOnInit();
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }

  add(event: MatChipInputEvent, arr: any, second: boolean = false): void {
    // Add fruit only when MatAutocomplete is not open
    // To make sure this does not conflict with OptionSelected Event
    if (!this.matAutocomplete.isOpen) {
      const input = event.input;
      const value = event.value;

      // Add our fruit
      if ((value || '').trim()) {
        second ? arr.push({ name: value.trim() }) : arr.push(value.trim());
      }

      // Reset the input value
      if (input) {
        input.value = '';
      }

      this.fruitCtrl.setValue(null);
    }
  }

  remove(fruit: string, arr: any): void {
    const index = arr.indexOf(fruit);

    if (index >= 0) {
      arr.splice(index, 1);
    }
  }

  selected(event: MatAutocompleteSelectedEvent, arr: any): void {
    arr.push(event.option.viewValue);
    this.fruitInput.nativeElement.value = '';
    this.fruitCtrl.setValue(null);
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.allFruits.filter(fruit => fruit.toLowerCase().indexOf(filterValue) === 0);
  }
}
