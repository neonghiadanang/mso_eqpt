import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageMDatepickersComponent } from './datepickers.component';

describe('PageMDatepickersComponent', () => {
  let component: PageMDatepickersComponent;
  let fixture: ComponentFixture<PageMDatepickersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageMDatepickersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageMDatepickersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
