import { Component, OnDestroy, OnInit } from '@angular/core';
import { BasePageComponent } from '../../base-page/base-page.component';
import { Store } from '@ngrx/store';
import { AppState } from '../../../interfaces/app-state';
import { FormControl } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';

@Component({
  selector: 'page-m-datepickers',
  templateUrl: './datepickers.component.html',
  styleUrls: ['./datepickers.component.scss'],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'en-GB' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
  ],
})
export class PageMDatepickersComponent extends BasePageComponent implements OnInit, OnDestroy {
  startDate: Date;
  date: FormControl;
  serializedDate: FormControl;
  minDate: Date;
  maxDate: Date;
  events: string[];

  constructor(
    store: Store<AppState>,
    private adapter: DateAdapter<any>
  ) {
    super(store);

    this.pageData = {
      title: 'Datepickers',
      loaded: true,
      breadcrumbs: [
        {
          title: 'UI elements',
          route: './dashboard'
        },
        {
          title: 'Material components',
          route: './dashboard'
        },
        {
          title: 'Datepickers'
        }
      ]
    };
    this.startDate = new Date(1990, 0, 1);
    this.date = new FormControl(new Date());
    this.serializedDate = new FormControl((new Date()).toISOString());
    this.minDate = new Date(2000, 0, 1);
    this.maxDate = new Date(2020, 0, 1);
    this.events = [];
  }

  ngOnInit() {
    super.ngOnInit();
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }

  filter = (d: Date): boolean => {
    const day = d.getDay();
    // Prevent Saturday and Sunday from being selected.
    return day !== 0 && day !== 6;
  }

  addEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    this.events.push(`${type}: ${event.value}`);
  }

  french() {
    this.adapter.setLocale('fr');
  }
}
