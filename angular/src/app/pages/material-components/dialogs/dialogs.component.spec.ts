import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageMDialogsComponent } from './dialogs.component';

describe('PageMDialogsComponent', () => {
  let component: PageMDialogsComponent;
  let fixture: ComponentFixture<PageMDialogsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageMDialogsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageMDialogsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
