import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageMIconsComponent } from './icons.component';

describe('PageMIconsComponent', () => {
  let component: PageMIconsComponent;
  let fixture: ComponentFixture<PageMIconsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageMIconsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageMIconsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
