import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageMInputsComponent } from './inputs.component';

describe('PageMInputsComponent', () => {
  let component: PageMInputsComponent;
  let fixture: ComponentFixture<PageMInputsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageMInputsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageMInputsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
