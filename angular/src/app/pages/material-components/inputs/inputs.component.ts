import { Component, NgZone, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { BasePageComponent } from '../../base-page/base-page.component';
import { Store } from '@ngrx/store';
import { AppState } from '../../../interfaces/app-state';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { CdkTextareaAutosize } from '@angular/cdk/text-field';
import { take } from 'rxjs/operators';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'page-m-inputs',
  templateUrl: './inputs.component.html',
  styleUrls: ['./inputs.component.scss']
})
export class PageMInputsComponent extends BasePageComponent implements OnInit, OnDestroy {
  emailFormControl: FormControl;
  emailFormControl2: FormControl;
  matcher: any;
  clearValue: string;

  @ViewChild('autosize', {static: false}) autosize: CdkTextareaAutosize;

  constructor(
    store: Store<AppState>,
    private ngZone: NgZone
  ) {
    super(store);

    this.pageData = {
      title: 'Inputs',
      loaded: true,
      breadcrumbs: [
        {
          title: 'UI elements',
          route: './dashboard'
        },
        {
          title: 'Material components',
          route: './dashboard'
        },
        {
          title: 'Inputs'
        }
      ]
    };
    this.emailFormControl = new FormControl('', [
      Validators.required,
      Validators.email
    ]);
    this.emailFormControl2 = new FormControl('', [
      Validators.required,
      Validators.email
    ]);
    this.matcher = new MyErrorStateMatcher();
    this.clearValue = 'Clear me';
  }

  ngOnInit() {
    super.ngOnInit();
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }

  triggerResize() {
    this.ngZone.onStable.pipe(take(1)).subscribe(
      () => this.autosize.resizeToFitContent(true)
    );
  }
}
