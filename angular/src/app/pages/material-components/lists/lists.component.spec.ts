import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageMListsComponent } from './lists.component';

describe('PageMListsComponent', () => {
  let component: PageMListsComponent;
  let fixture: ComponentFixture<PageMListsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageMListsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageMListsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
