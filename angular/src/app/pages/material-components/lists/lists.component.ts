import { Component, OnDestroy, OnInit } from '@angular/core';
import { BasePageComponent } from '../../base-page/base-page.component';
import { Store } from '@ngrx/store';
import { AppState } from '../../../interfaces/app-state';

const links: string[] = ['Link 1', 'Link 2', 'Link 3'];
const typesOfShoes: string[] = ['Boots', 'Clogs', 'Loafers', 'Moccasins', 'Sneakers'];
const messages: any[] = [
  {
    from: 'Nancy',
    subject: 'HTML',
    content: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
    avatar: 'assets/content/avatar-1.jpg'
  },
  {
    from: 'Mary',
    subject: 'Css',
    content: 'Lorem Ipsum has been the industrys standard',
    avatar: 'assets/content/avatar-2.jpg'
  },
  {
    from: 'Bobby',
    subject: 'Angular 2',
    content: 'It is a long established fact that a reader will be distracted by the readable content',
    avatar: 'assets/content/avatar-3.jpg'
  },
  {
    from: 'Roma',
    subject: 'Type Script',
    content: 'There are many variations of passages of',
    avatar: 'assets/content/avatar-4.jpg'
  }
];
const folders: any[] = [
  {
    name: 'Nancy',
    updated: 'Jan 21, 2017'
  },
  {
    name: 'Mary',
    updated: 'Jan 19, 2017'
  }
];
const notes = [
  {
    name: 'Bobby',
    updated: 'Jan 18, 2017'
  },
  {
    name: 'Roma',
    updated: 'Jan 17, 2017'
  }
];

@Component({
  selector: 'page-m-lists',
  templateUrl: './lists.component.html',
  styleUrls: ['./lists.component.scss']
})
export class PageMListsComponent extends BasePageComponent implements OnInit, OnDestroy {
  links: string[];
  typesOfShoes: string[];
  messages: any[];
  folders: any[];
  notes: any[];

  constructor(store: Store<AppState>) {
    super(store);

    this.pageData = {
      title: 'Lists',
      loaded: true,
      breadcrumbs: [
        {
          title: 'UI elements',
          route: './dashboard'
        },
        {
          title: 'Material components',
          route: './dashboard'
        },
        {
          title: 'Lists'
        }
      ]
    };
    this.links = links;
    this.typesOfShoes = typesOfShoes;
    this.messages = messages;
    this.folders = folders;
    this.notes = notes;
  }

  ngOnInit() {
    super.ngOnInit();
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }
}
