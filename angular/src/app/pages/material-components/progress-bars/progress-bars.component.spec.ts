import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageMProgressBarsComponent } from './progress-bars.component';

describe('PageMProgressBarsComponent', () => {
  let component: PageMProgressBarsComponent;
  let fixture: ComponentFixture<PageMProgressBarsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageMProgressBarsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageMProgressBarsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
