import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageMProgressSpinnersComponent } from './progress-spinners.component';

describe('PageMProgressSpinnersComponent', () => {
  let component: PageMProgressSpinnersComponent;
  let fixture: ComponentFixture<PageMProgressSpinnersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageMProgressSpinnersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageMProgressSpinnersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
