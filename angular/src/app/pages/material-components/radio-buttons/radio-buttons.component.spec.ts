import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageMRadioButtonsComponent } from './radio-buttons.component';

describe('PageMRadioButtonsComponent', () => {
  let component: PageMRadioButtonsComponent;
  let fixture: ComponentFixture<PageMRadioButtonsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageMRadioButtonsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageMRadioButtonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
