import { Component, OnDestroy, OnInit } from '@angular/core';
import { BasePageComponent } from '../../base-page/base-page.component';
import { Store } from '@ngrx/store';
import { AppState } from '../../../interfaces/app-state';

@Component({
  selector: 'page-m-radio-buttons',
  templateUrl: './radio-buttons.component.html',
  styleUrls: ['./radio-buttons.component.scss']
})
export class PageMRadioButtonsComponent extends BasePageComponent implements OnInit, OnDestroy {
  favoriteSeason: string;
  seasons: string[];

  constructor(store: Store<AppState>) {
    super(store);

    this.pageData = {
      title: 'Radio buttons',
      loaded: true,
      breadcrumbs: [
        {
          title: 'UI elements',
          route: './dashboard'
        },
        {
          title: 'Material components',
          route: './dashboard'
        },
        {
          title: 'Radio buttons'
        }
      ]
    };
    this.seasons = ['Winter', 'Spring', 'Summer', 'Autumn'];
  }

  ngOnInit() {
    super.ngOnInit();
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }
}
