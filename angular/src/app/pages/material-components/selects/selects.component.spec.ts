import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageMSelectsComponent } from './selects.component';

describe('PageMSelectsComponent', () => {
  let component: PageMSelectsComponent;
  let fixture: ComponentFixture<PageMSelectsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageMSelectsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageMSelectsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
