import { Component, OnDestroy, OnInit } from '@angular/core';
import { BasePageComponent } from '../../base-page/base-page.component';
import { Store } from '@ngrx/store';
import { AppState } from '../../../interfaces/app-state';
import { FormControl, Validators } from '@angular/forms';

export interface Food {
  value: string;
  viewValue: string;
}
export interface Car {
  value: string;
  viewValue: string;
}
export interface Animal {
  name: string;
  sound: string;
}
export interface PokemonGroup {
  name: string;
  pokemon: Pokemon[];
  disabled?: boolean;
}
export interface Pokemon {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'page-m-selects',
  templateUrl: './selects.component.html',
  styleUrls: ['./selects.component.scss']
})
export class PageMSelectsComponent extends BasePageComponent implements OnInit, OnDestroy {
  foods: Food[];
  cars: Car[];
  selected: string;
  selectedFood: string;
  selectedCar: string;
  animalControl: FormControl;
  selectFormControl: FormControl;
  animals: Animal[];
  disableSelect: FormControl;
  states: string[];
  pokemonControl: FormControl;
  pokemonGroups: PokemonGroup[];
  toppings: FormControl;
  toppings2: FormControl;
  toppingList: string[];
  panelColor: FormControl;

  constructor(store: Store<AppState>) {
    super(store);

    this.pageData = {
      title: 'Selects',
      loaded: true,
      breadcrumbs: [
        {
          title: 'UI elements',
          route: './dashboard'
        },
        {
          title: 'Material components',
          route: './dashboard'
        },
        {
          title: 'Selects'
        }
      ]
    };
    this.foods = [
      { value: 'steak-0', viewValue: 'Steak' },
      { value: 'pizza-1', viewValue: 'Pizza' },
      { value: 'tacos-2', viewValue: 'Tacos' }
    ];
    this.cars = [
      { value: 'volvo', viewValue: 'Volvo' },
      { value: 'saab', viewValue: 'Saab' },
      { value: 'mercedes', viewValue: 'Mercedes' }
    ];
    this.selected = 'option2';
    this.animalControl = new FormControl('', [Validators.required]);
    this.selectFormControl = new FormControl('', Validators.required);
    this.animals = [
      { name: 'Dog', sound: 'Woof!' },
      { name: 'Cat', sound: 'Meow!' },
      { name: 'Cow', sound: 'Moo!' },
      { name: 'Fox', sound: 'Wa-pa-pa-pa-pa-pa-pow!' },
    ];
    this.disableSelect = new FormControl(false);
    this.states = [
      'Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware',
      'Florida', 'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky',
      'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi',
      'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico',
      'New York', 'North Carolina', 'North Dakota', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania',
      'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont',
      'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'
    ];
    this.pokemonControl = new FormControl();
    this.pokemonGroups = [
      {
        name: 'Grass',
        pokemon: [
          { value: 'bulbasaur-0', viewValue: 'Bulbasaur' },
          { value: 'oddish-1', viewValue: 'Oddish' },
          { value: 'bellsprout-2', viewValue: 'Bellsprout' }
        ]
      },
      {
        name: 'Water',
        pokemon: [
          { value: 'squirtle-3', viewValue: 'Squirtle' },
          { value: 'psyduck-4', viewValue: 'Psyduck' },
          { value: 'horsea-5', viewValue: 'Horsea' }
        ]
      },
      {
        name: 'Fire',
        disabled: true,
        pokemon: [
          { value: 'charmander-6', viewValue: 'Charmander' },
          { value: 'vulpix-7', viewValue: 'Vulpix' },
          { value: 'flareon-8', viewValue: 'Flareon' }
        ]
      },
      {
        name: 'Psychic',
        pokemon: [
          { value: 'mew-9', viewValue: 'Mew' },
          { value: 'mewtwo-10', viewValue: 'Mewtwo' },
        ]
      }
    ];
    this.toppings = new FormControl();
    this.toppings2 = new FormControl();
    this.toppingList = ['Extra cheese', 'Mushroom', 'Onion', 'Pepperoni', 'Sausage', 'Tomato'];
    this.panelColor = new FormControl('red');
  }

  ngOnInit() {
    super.ngOnInit();
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }
}
