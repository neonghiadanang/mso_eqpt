import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageMSlideTogglesComponent } from './slide-toggles.component';

describe('PageMSlideTogglesComponent', () => {
  let component: PageMSlideTogglesComponent;
  let fixture: ComponentFixture<PageMSlideTogglesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageMSlideTogglesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageMSlideTogglesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
