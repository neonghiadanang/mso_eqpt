import { Component, OnDestroy, OnInit } from '@angular/core';
import { BasePageComponent } from '../../base-page/base-page.component';
import { Store } from '@ngrx/store';
import { AppState } from '../../../interfaces/app-state';

@Component({
  selector: 'page-m-slide-toggles',
  templateUrl: './slide-toggles.component.html',
  styleUrls: ['./slide-toggles.component.scss']
})
export class PageMSlideTogglesComponent extends BasePageComponent implements OnInit, OnDestroy {
  color: string;
  checked: boolean;
  disabled: boolean;

  constructor(store: Store<AppState>) {
    super(store);

    this.pageData = {
      title: 'Slide toggles',
      loaded: true,
      breadcrumbs: [
        {
          title: 'UI elements',
          route: './dashboard'
        },
        {
          title: 'Material components',
          route: './dashboard'
        },
        {
          title: 'Slide toggles'
        }
      ]
    };
    this.color = 'accent';
    this.checked = false;
    this.disabled = false;
  }

  ngOnInit() {
    super.ngOnInit();
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }
}
