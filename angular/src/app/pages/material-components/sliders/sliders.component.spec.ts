import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageMSlidersComponent } from './sliders.component';

describe('PageMSlidersComponent', () => {
  let component: PageMSlidersComponent;
  let fixture: ComponentFixture<PageMSlidersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageMSlidersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageMSlidersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
