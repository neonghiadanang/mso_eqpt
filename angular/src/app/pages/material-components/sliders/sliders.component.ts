import { Component, OnDestroy, OnInit } from '@angular/core';
import { coerceNumberProperty } from '@angular/cdk/coercion';

import { BasePageComponent } from '../../base-page/base-page.component';
import { Store } from '@ngrx/store';
import { AppState } from '../../../interfaces/app-state';

@Component({
  selector: 'page-m-sliders',
  templateUrl: './sliders.component.html',
  styleUrls: ['./sliders.component.scss']
})
export class PageMSlidersComponent extends BasePageComponent implements OnInit, OnDestroy {
  autoTicks: boolean;
  disabled: boolean;
  invert: boolean;
  max: number;
  min: number;
  showTicks: boolean;
  step: number;
  thumbLabel: boolean;
  value: number;
  vertical: boolean;
  _tickInterval: number;

  constructor(store: Store<AppState>) {
    super(store);

    this.pageData = {
      title: 'Sliders',
      loaded: true,
      breadcrumbs: [
        {
          title: 'UI elements',
          route: './dashboard'
        },
        {
          title: 'Material components',
          route: './dashboard'
        },
        {
          title: 'Sliders'
        }
      ]
    };
    this.autoTicks = false;
    this.disabled = false;
    this.invert = false;
    this.max = 100;
    this.min = 0;
    this.showTicks = false;
    this.step = 1;
    this.thumbLabel = false;
    this.value = 0;
    this.vertical = false;
    this._tickInterval = 1;
  }

  ngOnInit() {
    super.ngOnInit();
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }

  formatLabel(value: number | null) {
    if (!value) {
      return 0;
    }

    if (value >= 1000) {
      return Math.round(value / 1000) + 'k';
    }

    return value;
  }

  get tickInterval(): number | 'auto' {
    return this.showTicks ? (this.autoTicks ? 'auto' : this._tickInterval) : 0;
  }
  set tickInterval(value) {
    this._tickInterval = coerceNumberProperty(value);
  }
}
