import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageMSnackbarsComponent } from './snackbars.component';

describe('PageMSnackbarsComponent', () => {
  let component: PageMSnackbarsComponent;
  let fixture: ComponentFixture<PageMSnackbarsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageMSnackbarsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageMSnackbarsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
