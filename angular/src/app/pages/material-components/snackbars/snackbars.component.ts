import { Component, OnDestroy, OnInit } from '@angular/core';
import { BasePageComponent } from '../../base-page/base-page.component';
import { Store } from '@ngrx/store';
import { AppState } from '../../../interfaces/app-state';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';

@Component({
  selector: 'page-m-snackbars',
  templateUrl: './snackbars.component.html',
  styleUrls: ['./snackbars.component.scss']
})
export class PageMSnackbarsComponent extends BasePageComponent implements OnInit, OnDestroy {
  constructor(
    store: Store<AppState>,
    private _snackBar: MatSnackBar
    ) {
    super(store);

    this.pageData = {
      title: 'Snackbars',
      loaded: true,
      breadcrumbs: [
        {
          title: 'UI elements',
          route: './dashboard'
        },
        {
          title: 'Material components',
          route: './dashboard'
        },
        {
          title: 'Snackbars'
        }
      ]
    };
  }

  ngOnInit() {
    super.ngOnInit();
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }

  openSnackBarPosition(message: string, hPos: MatSnackBarHorizontalPosition, vPos: MatSnackBarVerticalPosition) {
    this._snackBar.open(`${message} : ${hPos} - ${vPos}`, null, {
      duration: 2000,
      horizontalPosition: hPos,
      verticalPosition: vPos
    });
  }
}
