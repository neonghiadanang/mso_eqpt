import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageMSteppersComponent } from './steppers.component';

describe('PageMSteppersComponent', () => {
  let component: PageMSteppersComponent;
  let fixture: ComponentFixture<PageMSteppersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageMSteppersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageMSteppersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
