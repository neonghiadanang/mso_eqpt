import { Component, OnDestroy, OnInit } from '@angular/core';
import { BasePageComponent } from '../../base-page/base-page.component';
import { Store } from '@ngrx/store';
import { AppState } from '../../../interfaces/app-state';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'page-m-steppers',
  templateUrl: './steppers.component.html',
  styleUrls: ['./steppers.component.scss']
})
export class PageMSteppersComponent extends BasePageComponent implements OnInit, OnDestroy {
  isLinear: boolean;
  isEditable:  boolean;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;

  constructor(
    store: Store<AppState>,
    private formBuilder: FormBuilder
  ) {
    super(store);

    this.pageData = {
      title: 'Steppers',
      loaded: true,
      breadcrumbs: [
        {
          title: 'UI elements',
          route: './dashboard'
        },
        {
          title: 'Material components',
          route: './dashboard'
        },
        {
          title: 'Steppers'
        }
      ]
    };
    this.isLinear = false;
    this.isEditable = false;
  }

  ngOnInit() {
    super.ngOnInit();

    this.firstFormGroup = this.formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this.formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }
}
