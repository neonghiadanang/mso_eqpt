import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageMTabsComponent } from './tabs.component';

describe('PageMTabsComponent', () => {
  let component: PageMTabsComponent;
  let fixture: ComponentFixture<PageMTabsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageMTabsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageMTabsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
