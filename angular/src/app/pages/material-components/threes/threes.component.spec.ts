import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageMThreesComponent } from './threes.component';

describe('PageMThreesComponent', () => {
  let component: PageMThreesComponent;
  let fixture: ComponentFixture<PageMThreesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageMThreesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageMThreesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
