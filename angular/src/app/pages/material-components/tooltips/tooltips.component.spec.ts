import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageMTooltipsComponent } from './tooltips.component';

describe('PageMTooltipsComponent', () => {
  let component: PageMTooltipsComponent;
  let fixture: ComponentFixture<PageMTooltipsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageMTooltipsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageMTooltipsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
