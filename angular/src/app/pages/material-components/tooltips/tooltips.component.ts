import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { BasePageComponent } from '../../base-page/base-page.component';
import { Store } from '@ngrx/store';
import { AppState } from '../../../interfaces/app-state';
import { FormControl } from '@angular/forms';
import { TooltipPosition } from '@angular/material/tooltip';

@Component({
  selector: 'page-m-tooltips',
  templateUrl: './tooltips.component.html',
  styleUrls: ['./tooltips.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class PageMTooltipsComponent extends BasePageComponent implements OnInit, OnDestroy {
  showDelay: FormControl;
  hideDelay: FormControl;
  positionOptions: TooltipPosition[];
  position: FormControl;
  disabled: FormControl;

  constructor(store: Store<AppState>) {
    super(store);

    this.pageData = {
      title: 'Tooltips',
      loaded: true,
      breadcrumbs: [
        {
          title: 'UI elements',
          route: './dashboard'
        },
        {
          title: 'Material components',
          route: './dashboard'
        },
        {
          title: 'Tooltips'
        }
      ]
    };
    this.showDelay = new FormControl(1000);
    this.hideDelay = new FormControl(2000);
    this.positionOptions = ['after', 'before', 'above', 'below', 'left', 'right'];
    this.position = new FormControl(this.positionOptions[0]);
    this.disabled = new FormControl(false);
  }

  ngOnInit() {
    super.ngOnInit();
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }
}
