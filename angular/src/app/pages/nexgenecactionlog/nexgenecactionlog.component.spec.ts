import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NexgenecactionlogComponent } from './nexgenecactionlog.component';

describe('NexgenecactionlogComponent', () => {
  let component: NexgenecactionlogComponent;
  let fixture: ComponentFixture<NexgenecactionlogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NexgenecactionlogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NexgenecactionlogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
