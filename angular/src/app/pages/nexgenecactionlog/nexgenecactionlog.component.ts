import { Component, ViewChild, OnInit, OnDestroy,Inject } from '@angular/core';
import { DataSource} from '@angular/cdk/collections';
import { MatPaginator } from '@angular/material/paginator';
import { BehaviorSubject, Observable, merge } from 'rxjs';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { map } from 'rxjs/operators';
import { BasePageComponent } from '../base-page/base-page.component';
import { Store } from '@ngrx/store';
import { AppState } from '../../interfaces/app-state';

import { MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatDialogModule } from '@angular/material/dialog';
import { NexgenecActionLog, NexgenecActionLogService } from './nexgenecactionlog.model';

import { ConfirmationDialogComponent } from '../../ui/components/confirmation-dialog/confirmation-dialog.component';

@Component({
  selector: 'app-nexgenecactionlog',
  templateUrl: './nexgenecactionlog.component.html',
  styleUrls: ['./nexgenecactionlog.component.scss']
})

export class NexgenecActionLogComponent extends BasePageComponent implements OnInit, OnDestroy {
  displayedColumns: string[];
  database: NexgenecActionLogDatabase;
  dataSource: NexgenecActionLogDataSource | null;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(store: Store<AppState>,
    private eqpSv: NexgenecActionLogService,
    public dialog: MatDialog
  ) {
    super(store);

    this.pageData = {
      title: 'Action Log',
      loaded: true,
      breadcrumbs: [
      ]
    };
    
    this.displayedColumns = [
      'at_time',
      // 'username',
      // 'action_id',
      'action_desc',
      // 'field_id',
      // 'table_name',
      //'from_ip',
      // 'from_user_agent',
    ];
    this.database = new NexgenecActionLogDatabase(eqpSv);
  }

  ngOnInit() {
    super.ngOnInit();
    this.dataSource = new NexgenecActionLogDataSource(this.database, this.paginator);
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }
 
  search(id,name){
    this.database.search(id,name);
  }
  
}

/** An example database that the data source uses to retrieve data for the table. */
export class NexgenecActionLogDatabase {
  
  /** Stream that emits whenever the data has been modified. */
  dataChange: BehaviorSubject<NexgenecActionLog[]> = new BehaviorSubject<NexgenecActionLog[]>([]);

  get data(): NexgenecActionLog[] {
    return this.dataChange.value; 
  }
  
  constructor(private eqpSer: NexgenecActionLogService) {
    this.eqpSer = eqpSer;
    this.search('','');
  }

  search(username: any, search_text: any) {
    this.eqpSer.findByQuery(
      { 
        "username":username.toUpperCase(),
        "search_text":search_text
      }).subscribe(
      data => {
        var arr = <Array<any>>data;
        const copiedData = [];
        arr.forEach(function (value) {
            copiedData.push( value);
        });
        this.dataChange.next(copiedData);
      },
      err => {
        console.log(err);
      },
      () => {
        //(callbackFnName && typeof this[callbackFnName] === 'function') ? this[callbackFnName](this[dataName]) : null;
      }
    );
  }

}

/**
 * Data source to provide what data should be rendered in the table. Note that the data source
 * can retrieve its data in any way. In this case, the data source is provided a reference
 * to a common data base, NexgenecActionLogDatabase. It is not the data source's responsibility to manage
 * the underlying data. Instead, it only needs to take the data and send the table exactly what
 * should be rendered.
 */
export class NexgenecActionLogDataSource extends DataSource<any> {
  constructor(private _database: NexgenecActionLogDatabase, private _paginator: MatPaginator) {
    super();
  }

  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<NexgenecActionLog[]> {


    const displayDataChanges = [
      this._database.dataChange,
      this._paginator.page,
    ];

    return merge(...displayDataChanges).pipe(
            map(() => {
                const data = this._database.data.slice();

                // Grab the page's slice of data.
                const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
                return data.splice(startIndex, this._paginator.pageSize);
            })
        );
  }

  disconnect() {}
}