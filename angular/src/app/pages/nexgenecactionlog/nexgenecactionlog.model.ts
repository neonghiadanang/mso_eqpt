import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CrudService } from '../../services/crud.service';
import { HttpResponse ,HttpHeaders} from '@angular/common/http';

export class NexgenecActionLog {
    id : any;
    username : any;
    from_ip : any;
    from_user_agent : any;
    at_time : any;
    site_id : any;
    operation_id : any;
    field_id : any;
    table_name : any;
    action_id : any;
    action_desc : any;
}

@Injectable({
    providedIn: 'root',
  })
  export class NexgenecActionLogService extends CrudService<NexgenecActionLog, number> {
    constructor(protected _http: HttpClient) {
      super(_http, "nexgenecactionlog");
    }
}