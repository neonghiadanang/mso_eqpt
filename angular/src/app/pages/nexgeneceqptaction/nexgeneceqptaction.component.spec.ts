import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NexgenecEqptActionComponent } from './nexgeneceqptaction.component';

describe('NexgeneceqptactionComponent', () => {
  let component: NexgenecEqptActionComponent;
  let fixture: ComponentFixture<NexgenecEqptActionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NexgenecEqptActionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NexgenecEqptActionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
