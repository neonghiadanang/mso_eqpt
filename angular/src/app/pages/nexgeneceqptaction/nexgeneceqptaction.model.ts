import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CrudService } from '../../services/crud.service';
import { HttpResponse ,HttpHeaders} from '@angular/common/http';

export class NexgenecEqptAction {
    seq_id : any;
    action_name : any;
    action_desc : any;
    updated_by : any;
    timestamp : any;
}

@Injectable({
    providedIn: 'root',
  })
  export class NexgenecEqptActionService extends CrudService<NexgenecEqptAction, number> {
    constructor(protected _http: HttpClient) {
      super(_http, "nexgeneceqptaction");
    }
}