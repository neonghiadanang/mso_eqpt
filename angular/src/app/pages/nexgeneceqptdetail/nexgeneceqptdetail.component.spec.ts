import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NexgenecEqptDetailComponent } from './nexgeneceqptdetail.component';

describe('NexgenecEqptDetailComponent', () => {
  let component: NexgenecEqptDetailComponent;
  let fixture: ComponentFixture<NexgenecEqptDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NexgenecEqptDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NexgenecEqptDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
