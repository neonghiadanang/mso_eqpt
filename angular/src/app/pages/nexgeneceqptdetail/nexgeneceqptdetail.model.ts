import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CrudService } from '../../services/crud.service';
import { HttpResponse ,HttpHeaders} from '@angular/common/http';
// import {Configuration} from '../../app.module';

export class NexgenecEqptDetail {
    seq_id : any;
    eqpt_id : any;
    eqpt_type : any;
    updated_by : any;
    timestamp : any;
}

@Injectable({
    providedIn: 'root',
  })
  export class NexgenecEqptDetailService extends CrudService<NexgenecEqptDetail, number> {
    constructor(protected _http: HttpClient) {
      super(_http, "nexgeneceqptdetail");
    }
}