import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NexgeneceqptsettingComponent } from './nexgeneceqptsetting.component';

describe('NexgeneceqptsettingComponent', () => {
  let component: NexgeneceqptsettingComponent;
  let fixture: ComponentFixture<NexgeneceqptsettingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NexgeneceqptsettingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NexgeneceqptsettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
