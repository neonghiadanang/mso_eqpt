import { Component, ViewChild, OnInit, OnDestroy,Inject } from '@angular/core';
import { DataSource} from '@angular/cdk/collections';
import { MatPaginator } from '@angular/material/paginator';
import { BehaviorSubject, Observable, merge } from 'rxjs';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { map } from 'rxjs/operators';
import { BasePageComponent } from '../base-page/base-page.component';
import { Store } from '@ngrx/store';
import { AppState } from '../../interfaces/app-state';

import { MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatDialogModule } from '@angular/material/dialog';
import { NexgenecEqptSetting, NexgenecEqptSettingService } from './nexgeneceqptsetting.model';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';

import { ConfirmationDialogComponent } from '../../ui/components/confirmation-dialog/confirmation-dialog.component';

export interface DialogData {
  animal: string;
  name: string;
}

@Component({
  selector: 'edit-dialog',
  templateUrl: 'edit.dialog.html',
})
export class DialogNexgenecEqptSettingComponent implements OnInit{
  action:string;
  local_data:any;
  public ownerForm: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<DialogNexgenecEqptSettingComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {
    this.local_data = {...data};
    this.action = this.local_data.action;
  }

  doAction(){
    // this.dialogRef.close({data:this.local_data});
    this.dialogRef.close({data:this.ownerForm.value});
  }

  closeDialog(){
    this.dialogRef.close({event:'Cancel'});
  }
  
  ngOnInit() {
    this.ownerForm = new FormGroup({
      seq_id: new FormControl(this.local_data.seq_id, []),
      eqpt_id: new FormControl(this.local_data.eqpt_id, []),
      key: new FormControl(this.local_data.key, [Validators.required, Validators.maxLength(50)]),
      value: new FormControl(this.local_data.value, [Validators.required, Validators.maxLength(100)]),
      description: new FormControl(this.local_data.description, [Validators.maxLength(255)]),
      site_id: new FormControl(this.local_data.site_id, []),
      operation_id: new FormControl(this.local_data.operation_id, []),
    });
  }

  public hasError = (controlName: string, errorName: string) =>{
    const error = this.ownerForm.controls[controlName].hasError(errorName);
    return error;
  }

}

@Component({
  selector: 'app-nexgeneceqptsetting',
  templateUrl: './NexgenecEqptSetting.component.html',
  styleUrls: ['./NexgenecEqptSetting.component.scss']
})
export class NexgenecEqptSettingComponent extends BasePageComponent implements OnInit, OnDestroy {
  displayedColumns: string[];
  database: NexgenecEqptSettingDatabase;
  dataSource: NexgenecEqptSettingDataSource | null;
  eqpt_id = "";

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(store: Store<AppState>,
    private eqpSv: NexgenecEqptSettingService,
    public dialog: MatDialog,
    private actRoute: ActivatedRoute,
    private router: Router
  ) {
    super(store);

    this.pageData = {
      title: 'Equipment Setting',
      loaded: true,
      breadcrumbs: [
        
      ]
    };
    
    this.displayedColumns = [
      'key',
      'value',
      'description',
      'seq_id',
      // 'site_id',
      // 'operation_id',
    ];
    this.database = new NexgenecEqptSettingDatabase(eqpSv);
    this.actRoute.paramMap.subscribe(params => {
      this.eqpt_id = params.get('id');
      this.database.search(this.eqpt_id,'')
    });
  }

  ngOnInit() {
    super.ngOnInit();
    this.dataSource = new NexgenecEqptSettingDataSource(this.database, this.paginator);
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }
  gotoEquipmentStateMachine(){
    console.log("gotoEquipmentStateMachine")
    this.router.navigate(["vertical", "eqptmachinestate", this.eqpt_id]);
  }
  gotoEquipmentSetting(){
    console.log("gotoEquipmentSetting")
  }
  gotoEquipmentConnect(){
    console.log("gotoEquipmentConnect")
  }
  search(id,name){
    this.database.search(this.eqpt_id,name);
  }
  openDialog(event, item) {
    console.log("openDialog "+event )
    console.log(item)
    
    if(event=="Delete"){
      let dialogRef = this.dialog.open(ConfirmationDialogComponent, {
        width: '500px',  
        data:"Do you confirm the deletion of this data?"
      });
      dialogRef.afterClosed().subscribe(result => {
        // NOTE: The result can also be nothing if the user presses the `esc` key or clicks outside the dialog
        console.log(result.result);
        if (result.result == true) {
            this.eqpSv.delete(item.seq_id)
              .subscribe(
                response => {
                  console.log(response)
                  this.search('','')
                }
            );
        }
      })
    }
    else{
      const dialogRef = this.dialog.open(DialogNexgenecEqptSettingComponent, {
        width: '500px',
        data:item
      });
  
      dialogRef.afterClosed().subscribe(result => {
        if(result==''){
        }
        else{
          if(event=="Update"){
            this.eqpSv.update(result.data.seq_id, result)
              .subscribe(
                response => {
                  console.log(response)
                  this.search('','')
                }
            );
          }
          else if(event=="New"){
            if(result.data.eqpt_id==null){
              result.data.eqpt_id = this.eqpt_id;
            }
            console.log(result)
            this.eqpSv.save(result)
              .subscribe(
                  response => {
                    console.log(response)
                    this.search('','')
                  }
              );
          }
        }
      });
        
    }

  }
}

/** An example database that the data source uses to retrieve data for the table. */
export class NexgenecEqptSettingDatabase {
  
  /** Stream that emits whenever the data has been modified. */
  dataChange: BehaviorSubject<NexgenecEqptSetting[]> = new BehaviorSubject<NexgenecEqptSetting[]>([]);

  get data(): NexgenecEqptSetting[] {
    return this.dataChange.value; 
  }
  
  constructor(private eqpSer: NexgenecEqptSettingService) {
    this.eqpSer = eqpSer;
    this.search('','');
  }

  search(eqpt_id: any, name: any) {
    console.log("search eqpt_id="+eqpt_id)
    this.eqpSer.findByQuery(
      { "eqpt_id":eqpt_id,
        "key":name
      }
      ).subscribe(
      data => {
        // console.log(data);
        var arr = <Array<any>>data;
        const copiedData = [];
        arr.forEach(function (value) {
            // console.log(value);
            copiedData.push( value);
        });
        this.dataChange.next(copiedData);
      },
      err => {
        console.log(err);
      },
      () => {
        //(callbackFnName && typeof this[callbackFnName] === 'function') ? this[callbackFnName](this[dataName]) : null;
      }
    );
  }

}

/**
 * Data source to provide what data should be rendered in the table. Note that the data source
 * can retrieve its data in any way. In this case, the data source is provided a reference
 * to a common data base, NexgenecEqptSettingDatabase. It is not the data source's responsibility to manage
 * the underlying data. Instead, it only needs to take the data and send the table exactly what
 * should be rendered.
 */
export class NexgenecEqptSettingDataSource extends DataSource<any> {
  constructor(private _database: NexgenecEqptSettingDatabase, private _paginator: MatPaginator) {
    super();
  }

  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<NexgenecEqptSetting[]> {


    const displayDataChanges = [
      this._database.dataChange,
      this._paginator.page,
    ];

    return merge(...displayDataChanges).pipe(
            map(() => {
                const data = this._database.data.slice();

                // Grab the page's slice of data.
                const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
                return data.splice(startIndex, this._paginator.pageSize);
            })
        );
  }

  disconnect() {}
}
