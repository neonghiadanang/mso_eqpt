import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CrudService } from '../../services/crud.service';
import { HttpResponse ,HttpHeaders} from '@angular/common/http';

export class NexgenecEqptSetting {
    seq_id : any;
    eqpt_id : any;
    key : any;
    value : any;
    description : any;
    site_id : any;
    operation_id : any;
}

@Injectable({
    providedIn: 'root',
  })
  export class NexgenecEqptSettingService extends CrudService<NexgenecEqptSetting, number> {
    constructor(protected _http: HttpClient) {
      super(_http, "nexgeneceqptsetting");
    }
}