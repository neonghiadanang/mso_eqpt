import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NexgenecEqptStateComponent } from './nexgeneceqptstate.component';

describe('NexgenecEqptStateComponent', () => {
  let component: NexgenecEqptStateComponent;
  let fixture: ComponentFixture<NexgenecEqptStateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NexgenecEqptStateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NexgenecEqptStateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
