import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CrudService } from '../../services/crud.service';
import { HttpResponse ,HttpHeaders} from '@angular/common/http';

export class NexgenecEqptState {
    seq_id : any;
    state_name : any;
    state_desc : any;
    updated_by : any;
    timestamp : any;
}

@Injectable({
    providedIn: 'root',
  })
  export class NexgenecEqptStateService extends CrudService<NexgenecEqptState, number> {
    constructor(protected _http: HttpClient) {
      super(_http, "nexgeneceqptstate");
    }
}