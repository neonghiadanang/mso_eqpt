import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NexgenecUsergroupComponent } from './nexgenecgroup.component';

describe('NexgenecUsergroupComponent', () => {
  let component: NexgenecUsergroupComponent;
  let fixture: ComponentFixture<NexgenecUsergroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NexgenecUsergroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NexgenecUsergroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
