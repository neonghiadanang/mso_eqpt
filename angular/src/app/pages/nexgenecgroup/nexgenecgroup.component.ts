import { Component, ViewChild, OnInit, OnDestroy,Inject } from '@angular/core';
import { DataSource} from '@angular/cdk/collections';
import { MatPaginator } from '@angular/material/paginator';
import { BehaviorSubject, Observable, merge } from 'rxjs';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { map } from 'rxjs/operators';
import { BasePageComponent } from '../base-page/base-page.component';
import { Store } from '@ngrx/store';
import { AppState } from '../../interfaces/app-state';

import { MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatDialogModule } from '@angular/material/dialog';
import { NexgenecUsergroup, NexgenecUsergroupService } from './nexgenecusergroup.model';

import { ConfirmationDialogComponent } from '../../ui/components/confirmation-dialog/confirmation-dialog.component';

export interface DialogData {
  animal: string;
  name: string;
}

@Component({
  selector: 'edit-dialog',
  templateUrl: 'edit.dialog.html',
})
export class DialogNexgenecUsergroupComponent implements OnInit{
  action:string;
  local_data:any;
  public ownerForm: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<DialogNexgenecUsergroupComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {
    //console.log(data);
    this.local_data = {...data};
    this.action = this.local_data.action;
  }

  doAction(){
    //this.dialogRef.close({data:this.local_data});
    this.dialogRef.close({data:this.ownerForm.value});
  }

  closeDialog(){
    this.dialogRef.close({event:'Cancel'});
  }

  ngOnInit() {
    this.ownerForm = new FormGroup({
      seq_id: new FormControl(this.local_data.seq_id, []),
      group_name: new FormControl(this.local_data.group_name, [Validators.required, Validators.maxLength(50)]),
      group_desc: new FormControl(this.local_data.group_desc, [Validators.maxLength(60)]),
      updated_by: new FormControl(this.local_data.updated_by, []),
      timestamp: new FormControl(this.local_data.timestamp, []),
    });
  }

  public hasError = (controlName: string, errorName: string) =>{
    return this.ownerForm.controls[controlName].hasError(errorName);
  }

}

@Component({
  selector: 'app-nexgenecusergroup',
  templateUrl: './nexgenecgroup.component.html',
  styleUrls: ['./nexgenecgroup.component.scss']
})

export class NexgenecUsergroupComponent extends BasePageComponent implements OnInit, OnDestroy {
  displayedColumns: string[];
  database: NexgenecUsergroupDatabase;
  dataSource: NexgenecUsergroupDataSource | null;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(store: Store<AppState>,
    private eqpSv: NexgenecUsergroupService,
    public dialog: MatDialog
  ) {
    super(store);

    this.pageData = {
      title: 'Group',
      loaded: true,
      breadcrumbs: [
      ]
    };
    
    this.displayedColumns = [
      'group_name',
      // 'group_desc',
      // 'updated_by',
      // 'timestamp',
      'seq_id',
    ];
    this.database = new NexgenecUsergroupDatabase(eqpSv);
  }

  ngOnInit() {
    super.ngOnInit();
    this.dataSource = new NexgenecUsergroupDataSource(this.database, this.paginator);
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }
 
  search(id,name){
    //console.log(id + " "+name)
    this.database.search(id,name);
  }
  openDialog(event, item) {
    console.log("openDialog "+event )
    console.log(item)
    
    if(event=="Delete"){
      let dialogRef = this.dialog.open(ConfirmationDialogComponent, {
        width: '500px',  
        data:"Do you confirm the deletion of this data?"
      });
      dialogRef.afterClosed().subscribe(result => {
        // NOTE: The result can also be nothing if the user presses the `esc` key or clicks outside the dialog
        console.log(result.result);
        if (result.result == true) {
            this.eqpSv.delete(item.seq_id)
              .subscribe(
                response => {
                  console.log(response)
                  this.search('','')
                }
            );
        }
      })
    }
    else{
    const dialogRef = this.dialog.open(DialogNexgenecUsergroupComponent, {
      width: '500px',
      data:item
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result)
      if(result==''){
      }
      else{
        if(event=="New"){
          this.eqpSv.save(result)
            .subscribe(
                response => {
                  console.log(response);
                  this.search('','');
                }
            );
        }
        else if(event=="Update"){
          this.eqpSv.update(result.data.seq_id, result)
            .subscribe(
                response => {
                  console.log(response);
                  this.search('','');
                }
            );
        }
      }
    });
    }
  }
}

/** An example database that the data source uses to retrieve data for the table. */
export class NexgenecUsergroupDatabase {
  
  /** Stream that emits whenever the data has been modified. */
  dataChange: BehaviorSubject<NexgenecUsergroup[]> = new BehaviorSubject<NexgenecUsergroup[]>([]);

  get data(): NexgenecUsergroup[] {
    return this.dataChange.value; 
  }
  
  constructor(private eqpSer: NexgenecUsergroupService) {
    this.eqpSer = eqpSer;
    this.search('','');
  }

  search(username: any, fullname: any) {
    this.eqpSer.findByQuery(
      { 
        "username":username
      }).subscribe(
      data => {
        var arr = <Array<any>>data;
        const copiedData = [];
        arr.forEach(function (value) {
            copiedData.push( value);
        });
        this.dataChange.next(copiedData);
      },
      err => {
        console.log(err);
      },
      () => {
        //(callbackFnName && typeof this[callbackFnName] === 'function') ? this[callbackFnName](this[dataName]) : null;
      }
    );
  }

}

/**
 * Data source to provide what data should be rendered in the table. Note that the data source
 * can retrieve its data in any way. In this case, the data source is provided a reference
 * to a common data base, NexgenecUsergroupDatabase. It is not the data source's responsibility to manage
 * the underlying data. Instead, it only needs to take the data and send the table exactly what
 * should be rendered.
 */
export class NexgenecUsergroupDataSource extends DataSource<any> {
  constructor(private _database: NexgenecUsergroupDatabase, private _paginator: MatPaginator) {
    super();
  }

  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<NexgenecUsergroup[]> {


    const displayDataChanges = [
      this._database.dataChange,
      this._paginator.page,
    ];

    return merge(...displayDataChanges).pipe(
            map(() => {
                const data = this._database.data.slice();

                // Grab the page's slice of data.
                const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
                return data.splice(startIndex, this._paginator.pageSize);
            })
        );
  }

  disconnect() {}
}