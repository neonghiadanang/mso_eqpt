import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CrudService } from '../../services/crud.service';
import { HttpResponse ,HttpHeaders} from '@angular/common/http';

export class NexgenecUsergroup {
    seq_id : any;
    group_name : any;
    group_desc : any;
    updated_by : any;
    timestamp : any;
}

@Injectable({
    providedIn: 'root',
  })
  export class NexgenecUsergroupService extends CrudService<NexgenecUsergroup, number> {
    constructor(protected _http: HttpClient) {
      super(_http, "groups");
    }
}