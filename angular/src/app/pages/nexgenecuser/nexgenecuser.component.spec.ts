import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NexgenecuserComponent } from './nexgenecuser.component';

describe('NexgenecuserComponent', () => {
  let component: NexgenecuserComponent;
  let fixture: ComponentFixture<NexgenecuserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NexgenecuserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NexgenecuserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
