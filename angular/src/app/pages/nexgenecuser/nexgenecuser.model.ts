import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CrudService } from '../../services/crud.service';
import { HttpResponse ,HttpHeaders} from '@angular/common/http';

export class NexgenecUser {
    seq_id : any;
    user_name : any;
    full_name : any;
    updated_by : any;
    timestamp : any;
    is_staff : any;
    is_superuser : any;
    is_active : any;
    last_login : any;
    date_joined : any;
    password : any;
}

@Injectable({
    providedIn: 'root',
  })
  export class NexgenecUserService extends CrudService<NexgenecUser, number> {
    constructor(protected _http: HttpClient) {
      super(_http, "user");
    }
 
}