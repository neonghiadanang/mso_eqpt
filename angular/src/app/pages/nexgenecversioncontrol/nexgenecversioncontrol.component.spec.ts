import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogNexgenecVersionControlComponent } from './nexgenecversioncontrol.component';

describe('DialogNexgenecVersionControlComponent', () => {
  let component: DialogNexgenecVersionControlComponent;
  let fixture: ComponentFixture<DialogNexgenecVersionControlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogNexgenecVersionControlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogNexgenecVersionControlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
