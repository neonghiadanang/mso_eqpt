import { Component, ViewChild, OnInit, OnDestroy,Inject } from '@angular/core';
import { DataSource} from '@angular/cdk/collections';
import { MatPaginator } from '@angular/material/paginator';
import { BehaviorSubject, Observable, merge } from 'rxjs';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { map } from 'rxjs/operators';
import { BasePageComponent } from '../base-page/base-page.component';
import { Store } from '@ngrx/store';
import { AppState } from '../../interfaces/app-state';

import { MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatDialogModule } from '@angular/material/dialog';
import { NexgenecVersionControl, NexgenecVersionControlService } from './nexgenecversioncontrol.model';
import { NexgenecEqptConfig } from '../eqpt-config/eqpt-config.models';

import { ConfirmationDialogComponent } from '../../ui/components/confirmation-dialog/confirmation-dialog.component';
import { MatTableDataSource } from '@angular/material/table';

export interface DialogData {
  animal: string;
  name: string;
}

@Component({
  selector: 'edit-dialog',
  templateUrl: 'edit.dialog.html',
})
export class DialogNexgenecVersionControlComponent implements OnInit{
  action:string;
  local_data:any;
  public ownerForm: FormGroup;
  displayedColumns:any;
  dataSource:any;
  service: NexgenecVersionControlService;

  lista: NexgenecEqptConfig[];
  isData:boolean;
  constructor(
    public dialogRef: MatDialogRef<DialogNexgenecVersionControlComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {
    this.local_data = {...data[0]};
    this.action = this.local_data.action;

    this.displayedColumns = ['section_id', 'key', 'value', 'old_value'];
    //this.dataSource = new MatTableDataSource(NexgenecEqptConfig[]);
    this.service = data[1];
    console.log(this.local_data);
    this.isData = false;
    this.loadData();
  }

  loadData(){
    //http://localhost:8000/eqp/config/?site_id=MSO&operation_id=TEST&eqpt_id=EQ_LASER-01&section=&version=1
    var query = "/eqp/config/?"
    query += "site_id="+this.local_data.site_id;
    query += "&operation_id="+this.local_data.operation_id;
    query += "&version="+this.local_data.version;
    query += "&section=";
    query += "&eqpt_id="+this.local_data.eqpt_id;
    query += "&data_type="+this.local_data.data_type;
    this.service.get(query
      ).subscribe(
      data => {
        var arr = <Array<any>>data;
        var lista = []
        arr.forEach(function (value) {
          lista.push( value);
        });
        this.lista = lista;
        if(lista.length>0){
          this.isData = true;
        }
        this.dataSource = new MatTableDataSource(this.lista);
      },
      err => {
        console.log(err);
      },
      () => {
        //(callbackFnName && typeof this[callbackFnName] === 'function') ? this[callbackFnName](this[dataName]) : null;
      }
    );
  }
  doAction(){
    this.dialogRef.close({result:true, data:this.ownerForm.value});
  }

  closeDialog(){
    this.dialogRef.close({event:'Cancel'});
  }

  ngOnInit() {
    this.ownerForm = new FormGroup({
      seq_id: new FormControl(this.local_data.seq_id, []),
      site_id: new FormControl(this.local_data.site_id, []),
      operation_id: new FormControl(this.local_data.operation_id, []),
      eqpt_id: new FormControl(this.local_data.eqpt_id, [Validators.required, Validators.maxLength(20)]),
      state: new FormControl(this.local_data.state, [Validators.required, Validators.maxLength(20)]),
      data_type: new FormControl(this.local_data.data_type, [Validators.required, Validators.maxLength(20)]),
      version: new FormControl(this.local_data.version, [Validators.required, Validators.maxLength(0)]),
      requested_by: new FormControl(this.local_data.requested_by, []),
      requested_time: new FormControl(this.local_data.requested_time, []),
      requested_comment: new FormControl(this.local_data.requested_comment, []),
      updated_by: new FormControl(this.local_data.updated_by, []),
      updated_time: new FormControl(this.local_data.updated_time, []),
      updated_comment: new FormControl(this.local_data.updated_comment, [Validators.required, Validators.maxLength(256)]),
    });
  }

  public hasError = (controlName: string, errorName: string) =>{
    return this.ownerForm.controls[controlName].hasError(errorName);
  }

}

@Component({
  selector: 'app-nexgenecversioncontrol',
  templateUrl: './nexgenecversioncontrol.component.html',
  styleUrls: ['./nexgenecversioncontrol.component.scss']
})

export class NexgenecVersionControlComponent extends BasePageComponent implements OnInit, OnDestroy {
  displayedColumns: string[];
  database: NexgenecVersionControlDatabase;
  dataSource: NexgenecVersionControlDataSource | null;

  listState = [
    { value:"", text: "All" },
    //{ value:"Editting", text: "Editting" },
    { value:"Requesting", text: "Requesting" },
    { value:"Cancelled", text: "Cancelled" },
    { value:"Rejected", text: "Rejected" },
    { value:"Approved", text: "Approved" },
  ]
  data = {
    state:""
  }
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(store: Store<AppState>,
    private eqpSv: NexgenecVersionControlService,
    public dialog: MatDialog
  ) {
    super(store);

    this.pageData = {
      title: 'Version Control',
      loaded: true,
      breadcrumbs: [
      ]
    };
    
    this.displayedColumns = [
      // 'site_id',
      // 'operation_id',
      // 'data_type',
      'version',
      'state',
      'eqpt_id',
      'requested_by',
      'requested_time',
      'requested_comment',
      'updated_by',
      'updated_time',
      'updated_comment',
      'seq_id'
    ];
    this.database = new NexgenecVersionControlDatabase(eqpSv);
  }

  ngOnInit() {
    super.ngOnInit();
    this.dataSource = new NexgenecVersionControlDataSource(this.database, this.paginator);
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }
 
  search(){
    this.database.search(this.data);
  }
  openDialog(event, item) {
    console.log("openDialog "+event )
    console.log(item)
    
    
    const dialogRef = this.dialog.open(DialogNexgenecVersionControlComponent, {
        width: '500px',
        data:[item,this.eqpSv]
      }, 
    );

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      if (result.result == true) {
        this.eqpSv.send(
          {
            action: 'Approved',
            seq_id: result.data.seq_id,
            version: result.data.version,
            eqpt_id: result.data.eqpt_id,
            commented: result.data.updated_comment,
          }
        )
          .subscribe(
            response => {
              console.log(response)
              this.search()
            }
        );
      }
    });
  }
}

/** An example database that the data source uses to retrieve data for the table. */
export class NexgenecVersionControlDatabase {
  
  /** Stream that emits whenever the data has been modified. */
  dataChange: BehaviorSubject<NexgenecVersionControl[]> = new BehaviorSubject<NexgenecVersionControl[]>([]);

  get data(): NexgenecVersionControl[] {
    return this.dataChange.value; 
  }
  
  constructor(private eqpSer: NexgenecVersionControlService) {
    this.eqpSer = eqpSer;
    this.search(null);
  }

  search(data: any) {
    this.eqpSer.findByQuery(
        data
      ).subscribe(
      data => {
        var arr = <Array<any>>data;
        const copiedData = [];
        arr.forEach(function (value) {
            copiedData.push( value);
        });
        this.dataChange.next(copiedData);
      },
      err => {
        console.log(err);
      },
      () => {
        //(callbackFnName && typeof this[callbackFnName] === 'function') ? this[callbackFnName](this[dataName]) : null;
      }
    );
  }

}

/**
 * Data source to provide what data should be rendered in the table. Note that the data source
 * can retrieve its data in any way. In this case, the data source is provided a reference
 * to a common data base, NexgenecVersionControlDatabase. It is not the data source's responsibility to manage
 * the underlying data. Instead, it only needs to take the data and send the table exactly what
 * should be rendered.
 */
export class NexgenecVersionControlDataSource extends DataSource<any> {
  constructor(private _database: NexgenecVersionControlDatabase, private _paginator: MatPaginator) {
    super();
  }

  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<NexgenecVersionControl[]> {


    const displayDataChanges = [
      this._database.dataChange,
      this._paginator.page,
    ];

    return merge(...displayDataChanges).pipe(
            map(() => {
                const data = this._database.data.slice();

                // Grab the page's slice of data.
                const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
                return data.splice(startIndex, this._paginator.pageSize);
            })
        );
  }

  disconnect() {}
}