import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CrudService } from '../../services/crud.service';
import { HttpResponse ,HttpHeaders} from '@angular/common/http';

export class NexgenecVersionControl {
    seq_id : any;
    site_id : any;
    operation_id : any;
    eqpt_id : any;
    version : any;
    updated_by : any;
    updated_time : any;
    requested_by : any;
    requested_time : any;
    requested_comment : any;
    state : any;
    data_type : any;
    updated_comment : any;
}

@Injectable({
    providedIn: 'root',
  })
  export class NexgenecVersionControlService extends CrudService<NexgenecVersionControl, number> {
    constructor(protected _http: HttpClient) {
      super(_http, "version");
    }
}