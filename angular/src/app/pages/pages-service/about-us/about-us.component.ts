import { Component, OnDestroy, OnInit } from '@angular/core';
import { BasePageComponent } from '../../base-page/base-page.component';
import { Store } from '@ngrx/store';
import { AppState } from '../../../interfaces/app-state';

@Component({
  selector: 'page-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.scss']
})
export class PageAboutUsComponent extends BasePageComponent implements OnInit, OnDestroy {
  lineChartData: any[];
  lineChartLabels: any[];
  lineChartOptions: any;
  lineChartColors: any[];
  lineChartLegend: boolean;
  lineChartType: string;

  constructor(store: Store<AppState>) {
    super(store);

    this.pageData = {
      title: 'About us',
      loaded: true,
      breadcrumbs: [
        {
          title: 'Pages',
          route: './dashboard'
        },
        {
          title: 'Pages service',
          route: './dashboard'
        },
        {
          title: 'About us'
        }
      ]
    };

    // lineChart
    this.lineChartData = [
      {
        data: [30, 42, 46, 51, 65, 73, 80],
        label: 'Users',
        borderWidth: 1,
        pointRadius: 1
      },
      {
        data: [42, 43, 52, 47, 65, 70, 79],
        label: 'Pages',
        borderWidth: 1,
        pointRadius: 1
      },
      {
        data: [51, 48, 45, 56, 61, 69, 67],
        label: 'Visits',
        borderWidth: 1,
        pointRadius: 1
      }
    ];
    this.lineChartLabels = [
      'Mon.',
      'Tue.',
      'Wed.',
      'Thu.',
      'Fri.',
      'Sat.',
      'Sun.'
    ];
    this.lineChartOptions = {
      responsiveAnimationDuration: 500,
      responsive: true,
      scales: {
        xAxes: [{
          gridLines: {
            display: false
          }
        }],
        yAxes: [{
          gridLines: {
            display: true
          },
          ticks: {
            beginAtZero: true
          }
        }],
      }
    };
    this.lineChartColors = [
      {
        backgroundColor: 'rgba(93,173,224,0.2)',
        borderColor: '#5dade0',
        pointBackgroundColor: '#5dade0',
        pointBorderColor: '#0e7cc5',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: '#000'
      },
      {
        backgroundColor: 'rgba(255,140,0,0.2)',
        borderColor: '#ff8c00',
        pointBackgroundColor: '#ff8c00',
        pointBorderColor: '#FF630B',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: '#000'
      },
      {
        backgroundColor: 'rgba(220,20,60,0.2)',
        borderColor: '#dc143c',
        pointBackgroundColor: '#dc143c',
        pointBorderColor: '#7E2303',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: '#000'
      }
    ];
    this.lineChartLegend = true;
    this.lineChartType = 'line';
  }

  ngOnInit() {
    super.ngOnInit();
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }
}
