import { Component, OnDestroy, OnInit } from '@angular/core';

import { Store } from '@ngrx/store';

import { BasePageComponent } from '../../base-page/base-page.component';
import { AppState } from '../../../interfaces/app-state';

@Component({
  selector: 'page-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss']
})
export class PageFaqComponent extends BasePageComponent implements OnInit, OnDestroy {
  constructor(store: Store<AppState>) {
    super(store);

    this.pageData = {
      title: 'FAQ',
      loaded: true,
      breadcrumbs: [
        {
          title: 'Pages',
          route: './dashboard'
        },
        {
          title: 'Pages service',
          route: './dashboard'
        },
        {
          title: 'FAQ'
        }
      ]
    };
  }

  ngOnInit() {
    super.ngOnInit();
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }
}
