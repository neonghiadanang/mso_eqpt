import { NgModule, CUSTOM_ELEMENTS_SCHEMA}	from '@angular/core';
import { CommonModule }	from '@angular/common';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';

import { UIModule } from '../ui/ui.module';

import { MAT_DATE_LOCALE, MatNativeDateModule, MatRippleModule } from '@angular/material/core';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatBadgeModule } from '@angular/material/badge';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatChipsModule } from '@angular/material/chips';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSliderModule } from '@angular/material/slider';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatStepperModule } from '@angular/material/stepper';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatTreeModule } from '@angular/material/tree';

import { ChartsModule }	from 'ng2-charts';
import { NgxChartsModule }	from '@swimlane/ngx-charts';
import { AgmCoreModule }	from '@agm/core';
import { AmChartsModule }	from '@amcharts/amcharts3-angular';
import { NgxEchartsModule } from 'ngx-echarts';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';

import { BasePageComponent } from './base-page/base-page.component';

import { PageDashboardComponent }	from './dashboards/dashboard-1/dashboard.component';
import { PageDashboard2Component }	from './dashboards/dashboard-2/dashboard-2.component';
import { PageDashboard3Component }	from './dashboards/dashboard-3/dashboard-3.component';
import { PageDashboard4Component } from './dashboards/dashboard-4/dashboard-4.component';
import { PageTypographyComponent }	from './typography/typography.component';
import { PageNotFoundComponent }	from './not-found/not-found.component';
import { PageAboutUsComponent }	from './pages-service/about-us/about-us.component';
import { PageFaqComponent }	from './pages-service/faq/faq.component';
import { PageTimelineComponent }	from './pages-service/timeline/timeline.component';
import { PageInvoiceComponent }	from './pages-service/invoice/invoice.component';
import { PageNg2ChartsComponent }	from './charts/ng2-charts/ng2-charts.component';
import { PageNgxChartsComponent }	from './charts/ngx-charts/ngx-charts.component';
import { PageAmchartsComponent }	from './charts/amcharts/amcharts.component';
import { PageCalendarComponent }	from './calendar/calendar.component';
import { CalendarDialogComponent }	from './calendar/calendar.component';
import { PageSimpleTableComponent }	from './tables/simple-table/simple-table.component';
import { PageBootstrapTablesComponent }	from './tables/bootstrap-tables/bootstrap-tables.component';
import { PageSortingTableComponent }	from './tables/sorting-table/sorting-table.component';
import { PageFilteringTableComponent }	from './tables/filtering-table/filtering-table.component';
import { PagePaginationTableComponent }	from './tables/pagination-table/pagination-table.component';
import { PageFormElementsComponent }	from './forms/form-elements/form-elements.component';
import { PageFormLayoutComponent }	from './forms/form-layout/form-layout.component';
import { PageFormValidationComponent }	from './forms/form-validation/form-validation.component';
import { PageGoogleMapComponent }	from './maps/google-map/google-map.component';
import { PageLeafletMapComponent }	from './maps/leaflet-map/leaflet-map.component';
import { PageWidgetsComponent }	from './widgets/widgets.component';
import { PageSignIn1Component }	from './extra-pages/sign-in-1/sign-in-1.component';
import { PageSignIn2Component }	from './extra-pages/sign-in-2/sign-in-2.component';
import { PageSignIn3Component }	from './extra-pages/sign-in-3/sign-in-3.component';
import { PageSignUp1Component }	from './extra-pages/sign-up-1/sign-up-1.component';
import { PageSignUp2Component }	from './extra-pages/sign-up-2/sign-up-2.component';
import { PageForgotComponent }	from './extra-pages/forgot/forgot.component';
import { PageConfirmComponent }	from './extra-pages/confirm/confirm.component';
import { Page404Component }	from './extra-pages/page-404/page-404.component';
import { Page500Component }	from './extra-pages/page-500/page-500.component';
import { environment } from '../../environments/environment';
import { PageTAlertsComponent } from './theme-components/alerts/alerts.component';
import { PageTBreadcrumbsComponent } from './theme-components/breadcrumbs/breadcrumbs.component';
import { PageTAvatarsComponent } from './theme-components/avatars/avatars.component';
import { PageTBadgesComponent } from './theme-components/badges/badges.component';
import { PageTButtonsComponent } from './theme-components/buttons/buttons.component';
import { PageTCardsComponent } from './theme-components/cards/cards.component';
import { PageTDropdownsComponent } from './theme-components/dropdowns/dropdowns.component';
import { PageTIconsComponent } from './theme-components/icons/icons.component';
import { PageTRatingsComponent } from './theme-components/ratings/ratings.component';
import { PageTFilesComponent } from './theme-components/files/files.component';
import { PageTChatsComponent } from './theme-components/chats/chats.component';
import { PageTVTimelinesComponent } from './theme-components/v-timelines/v-timelines.component';
import { PageNgxEchartsComponent } from './charts/ngx-echarts/ngx-echarts.component';
import { PageMAutocompletesComponent } from './material-components/autocompletes/autocompletes.component';
import { PageMCheckboxesComponent } from './material-components/checkboxes/checkboxes.component';
import { PageMDatepickersComponent } from './material-components/datepickers/datepickers.component';
import { PageMInputsComponent } from './material-components/inputs/inputs.component';
import { PageMRadioButtonsComponent } from './material-components/radio-buttons/radio-buttons.component';
import { PageMSelectsComponent } from './material-components/selects/selects.component';
import { PageMSlidersComponent } from './material-components/sliders/sliders.component';
import { PageMSlideTogglesComponent } from './material-components/slide-toggles/slide-toggles.component';
import { PageMCardsComponent } from './material-components/cards/cards.component';
import { PageMListsComponent } from './material-components/lists/lists.component';
import { PageMSteppersComponent } from './material-components/steppers/steppers.component';
import { PageMTabsComponent } from './material-components/tabs/tabs.component';
import { PageMThreesComponent } from './material-components/threes/threes.component';
import { PageMButtonsComponent } from './material-components/buttons/buttons.component';
import { PageMBadgesComponent } from './material-components/badges/badges.component';
import { PageMProgressBarsComponent } from './material-components/progress-bars/progress-bars.component';
import { PageMIconsComponent } from './material-components/icons/icons.component';
import { PageMChipsComponent } from './material-components/chips/chips.component';
import { PageMProgressSpinnersComponent } from './material-components/progress-spinners/progress-spinners.component';
import { BottomSheetOverviewComponent, PageMBottomSheetsComponent } from './material-components/bottom-sheets/bottom-sheets.component';
import { DialogOverview2Component, DialogOverviewComponent, PageMDialogsComponent } from './material-components/dialogs/dialogs.component';

import { PageMSnackbarsComponent } from './material-components/snackbars/snackbars.component';
import { PageMTooltipsComponent } from './material-components/tooltips/tooltips.component';
import { PageFirebaseComponent } from './firebase/firebase.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { RouterModule } from '@angular/router';

import { GwyUsersComponent } from './gwy-users/gwy-users.component';

import { DialogGwyUsersComponent } from './gwy-users/gwy-users.component';
import { GojsAngularModule } from 'gojs-angular';
import { EqptMachineStateComponent, DialogEditNodeComponent, DialogEditLinkComponent } from './eqpt-machine-state/eqpt-machine-state.component';
import { NexgenecEqptSettingComponent, DialogNexgenecEqptSettingComponent } from './nexgeneceqptsetting/nexgeneceqptsetting.component';
import { ConfirmationDialogComponent } from '../ui/components/confirmation-dialog/confirmation-dialog.component';
import { DialogNexgenecEqptDetailComponent, NexgenecEqptDetailComponent } from './nexgeneceqptdetail/nexgeneceqptdetail.component';
import { StatemachinelistComponent } from './statemachinelist/statemachinelist.component';
import { DialogNexgenecUserComponent, NexgenecUserComponent } from './nexgenecuser/nexgenecuser.component';
import { NexgenecEqptConfigComponent, DialogRequestApprove,DialogNexgenecEqptConfigComponent,DialogNexgenecMesSettingComponent,DialogNexgenecEqptConnectionComponent } from './eqpt-config/eqpt-config.component';
import { NexgenecEqptUploadSettingComponent, DialogNexgenecEqptUploadSettingComponent, DialogNexgenecSourceCodeComponent } from './eqpt-upload/eqpt-upload.component';
import { NexgenecEqptLoggerComponent, DialogNexgenecEqptLoggerComponent} from './eqpt-logger/eqpt-logger.component';
import { NexgenecActionLogComponent } from './nexgenecactionlog/nexgenecactionlog.component';
import { NexgenecEqptActionComponent, DialogNexgenecEqptActionComponent } from './nexgeneceqptaction/nexgeneceqptaction.component';
import { NexgenecEqptStateComponent, DialogNexgenecEqptStateComponent } from './nexgeneceqptstate/nexgeneceqptstate.component';
import { NexgenecUsergroupComponent, DialogNexgenecUsergroupComponent } from './nexgenecgroup/nexgenecgroup.component';
import { NexgenecVersionControlComponent, DialogNexgenecVersionControlComponent } from './nexgenecversioncontrol/nexgenecversioncontrol.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,

    UIModule,

    MatAutocompleteModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatStepperModule,
    MatTreeModule,
    MatBadgeModule,
    MatBottomSheetModule,

    ChartsModule,
    NgxChartsModule,
    AmChartsModule,
    NgxEchartsModule,
    AgmCoreModule.forRoot({
     apiKey: environment.googleMapApiKey
    }),
    LeafletModule.forRoot(),
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
    AngularFireModule.initializeApp(environment.firebase, 'assist'),
    AngularFirestoreModule,
    AngularFireStorageModule,
    AngularFireDatabaseModule,
    AngularFireDatabaseModule,
    GojsAngularModule
  ],
  declarations: [
    PageDashboardComponent,
    PageDashboard2Component,
    PageDashboard3Component,
    PageDashboard4Component,
    PageTypographyComponent,
    PageSignIn1Component,
    PageSignIn2Component,
    PageSignIn3Component,
    PageSignUp1Component,
    PageSignUp2Component,
    PageForgotComponent,
    PageConfirmComponent,
    Page404Component,
    Page500Component,
    PageAboutUsComponent,
    PageFaqComponent,
    PageTimelineComponent,
    PageInvoiceComponent,
    PageCalendarComponent,
    CalendarDialogComponent,
    PageSimpleTableComponent,
    PageBootstrapTablesComponent,
    PageSortingTableComponent,
    PageFilteringTableComponent,
    PagePaginationTableComponent,
    PageFormElementsComponent,
    PageFormLayoutComponent,
    PageFormValidationComponent,
    PageGoogleMapComponent,
    PageLeafletMapComponent,
    PageWidgetsComponent,
    PageNg2ChartsComponent,
    PageNgxChartsComponent,
    PageAmchartsComponent,
    PageNgxEchartsComponent,
    BasePageComponent,
    PageNotFoundComponent,
    PageTAlertsComponent,
    PageTBreadcrumbsComponent,
    PageTAvatarsComponent,
    PageTBadgesComponent,
    PageTButtonsComponent,
    PageTCardsComponent,
    PageTDropdownsComponent,
    PageTIconsComponent,
    PageTRatingsComponent,
    PageTFilesComponent,
    PageTChatsComponent,
    PageTVTimelinesComponent,
    PageMAutocompletesComponent,
    PageMCheckboxesComponent,
    PageMDatepickersComponent,
    PageMInputsComponent,
    PageMRadioButtonsComponent,
    PageMSelectsComponent,
    PageMSlidersComponent,
    PageMSlideTogglesComponent,
    PageMCardsComponent,
    PageMListsComponent,
    PageMSteppersComponent,
    PageMTabsComponent,
    PageMThreesComponent,
    PageMButtonsComponent,
    PageMBadgesComponent,
    PageMChipsComponent,
    PageMIconsComponent,
    PageMProgressSpinnersComponent,
    PageMProgressBarsComponent,
    PageMBottomSheetsComponent,
    BottomSheetOverviewComponent,
    PageMDialogsComponent,
    DialogOverviewComponent,
    DialogOverview2Component,
    PageMSnackbarsComponent,
    PageMTooltipsComponent,
    PageFirebaseComponent,
    GwyUsersComponent,
    DialogGwyUsersComponent,
    DialogNexgenecEqptSettingComponent,
    EqptMachineStateComponent,
    NexgenecEqptSettingComponent,
    NexgenecEqptDetailComponent,
    DialogNexgenecEqptDetailComponent,
    StatemachinelistComponent,
    DialogNexgenecUserComponent,
    NexgenecUserComponent,
    NexgenecEqptConfigComponent,
    DialogNexgenecEqptConfigComponent,
    DialogNexgenecMesSettingComponent,
    DialogNexgenecEqptConnectionComponent,
    NexgenecEqptUploadSettingComponent,
    DialogNexgenecEqptUploadSettingComponent,
    DialogNexgenecSourceCodeComponent,
    NexgenecEqptLoggerComponent,
    DialogNexgenecEqptLoggerComponent,
    NexgenecActionLogComponent,
    NexgenecEqptActionComponent,
    DialogNexgenecEqptActionComponent,
    NexgenecEqptStateComponent,
    DialogNexgenecEqptStateComponent,
    DialogEditNodeComponent,
    DialogEditLinkComponent,
    NexgenecUsergroupComponent, 
    DialogNexgenecUsergroupComponent, 
    DialogNexgenecVersionControlComponent,
    NexgenecVersionControlComponent,
    DialogRequestApprove,
  ],
  exports: [],
  entryComponents: [
    CalendarDialogComponent,
    BottomSheetOverviewComponent,
    DialogOverviewComponent,
    DialogOverview2Component,
    DialogGwyUsersComponent,
  ],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'en-GB' }
  ],
  // schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class PagesModule {}
