import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatemachinelistComponent } from './statemachinelist.component';

describe('StatemachinelistComponent', () => {
  let component: StatemachinelistComponent;
  let fixture: ComponentFixture<StatemachinelistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatemachinelistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatemachinelistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
