import { Component, ViewChild, OnInit, OnDestroy,Inject } from '@angular/core';
import { DataSource} from '@angular/cdk/collections';
import { MatPaginator } from '@angular/material/paginator';
import { BehaviorSubject, Observable, merge } from 'rxjs';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { map } from 'rxjs/operators';
import { BasePageComponent } from '../base-page/base-page.component';
import { Store } from '@ngrx/store';
import { AppState } from '../../interfaces/app-state';

import { MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatDialogModule } from '@angular/material/dialog';
import { StateMachineList, StateMachineListService } from './statemachinelist.model';
import { ActivatedRoute } from '@angular/router';

import { ConfirmationDialogComponent } from '../../ui/components/confirmation-dialog/confirmation-dialog.component';

export interface DialogData {
  animal: string;
  name: string;
}

@Component({
  selector: 'app-statemachinelist',
  templateUrl: './statemachinelist.component.html',
  styleUrls: ['./statemachinelist.component.scss']
})
export class StatemachinelistComponent extends BasePageComponent implements OnInit, OnDestroy {
  database: StatemachineListDatabase;
  eqpt_id = "";

  constructor(store: Store<AppState>,
    private eqpSv: StateMachineListService,
    private actRoute: ActivatedRoute
  ) {
    super(store);

    this.pageData = {
      title: 'Equipment State Machine',
      loaded: true,
      breadcrumbs: [
        
      ]
    };
    this.database = new StatemachineListDatabase(eqpSv);
    this.actRoute.paramMap.subscribe(params => {
      this.eqpt_id = params.get('id');
      this.database.search(this.eqpt_id,'')
    });
  }

  ngOnInit(): void {
  }

}



/** An example database that the data source uses to retrieve data for the table. */
export class StatemachineListDatabase {
  
  /** Stream that emits whenever the data has been modified. */
  dataChange: BehaviorSubject<StateMachineList[]> = new BehaviorSubject<StateMachineList[]>([]);

  get data(): StateMachineList[] {
    return this.dataChange.value; 
  }
  
  constructor(private eqpSer: StateMachineListService) {
    this.eqpSer = eqpSer;
    this.search('','');
  }

  search(eqpt_id: any, name: any) {
    this.eqpSer.findByQuery(
      { "eqpt_id":eqpt_id,
        "key":name
      }
      ).subscribe(
      data => {
        // console.log(data);
        var arr = <Array<any>>data;
        const copiedData = [];
        arr.forEach(function (value) {
            // console.log(value);
            copiedData.push( value);
        });
        this.dataChange.next(copiedData);
      },
      err => {
        console.log(err);
      },
      () => {
        //(callbackFnName && typeof this[callbackFnName] === 'function') ? this[callbackFnName](this[dataName]) : null;
      }
    );
  }
}
