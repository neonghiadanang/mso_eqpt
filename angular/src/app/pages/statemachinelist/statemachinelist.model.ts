import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CrudService } from '../../services/crud.service';
import { HttpResponse ,HttpHeaders} from '@angular/common/http';

export class StateMachineList {
    eqpt_id : any;
    statemachine_id : any;
    site_id : any;
    operation_id : any;
}

@Injectable({
    providedIn: 'root',
  })
  export class StateMachineListService extends CrudService<StateMachineList, number> {
    constructor(protected _http: HttpClient) {
      super(_http, "statemachinelist");
    }
}