import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageTAlertsComponent } from './alerts.component';

describe('PageTAlertsComponent', () => {
  let component: PageTAlertsComponent;
  let fixture: ComponentFixture<PageTAlertsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageTAlertsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageTAlertsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
