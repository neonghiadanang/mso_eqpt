import { Component, OnDestroy, OnInit } from '@angular/core';

import { Store } from '@ngrx/store';

import { AppState } from '../../../interfaces/app-state';
import { BasePageComponent } from '../../base-page/base-page.component';

@Component({
  selector: 'page-t-alerts',
  templateUrl: './alerts.component.html',
  styleUrls: ['./alerts.component.scss']
})
export class PageTAlertsComponent extends BasePageComponent implements OnInit, OnDestroy {
  constructor(store: Store<AppState>) {
    super(store);

    this.pageData = {
      title: 'Alerts',
      loaded: true,
      breadcrumbs: [
        {
          title: 'UI elements',
          route: './dashboard'
        },
        {
          title: 'Theme components',
          route: './dashboard'
        },
        {
          title: 'Alerts'
        }
      ]
    };
  }

  ngOnInit() {
    super.ngOnInit();
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }
}
