import { Component, OnDestroy, OnInit } from '@angular/core';

import { Store } from '@ngrx/store';

import { AppState } from '../../../interfaces/app-state';
import { BasePageComponent } from '../../base-page/base-page.component';
import { Breadcrumb } from '../../../ui/interfaces/breadcrumbs';

const breadcrumbs: Breadcrumb[] = [
  {
    title: 'Home',
    route: '#',
    icon: ''
  },
  {
    title: 'UI Elements',
    route: '#',
    icon: ''
  },
  {
    title: 'Components',
    route: '#',
    icon: ''
  },
  {
    title: 'Breadcrumb',
    route: '',
    icon: ''
  }
];
const breadcrumbsIcons: Breadcrumb[] = [
  {
    title: 'Home',
    route: '#',
    icon: 'fa fa-home'
  },
  {
    title: 'UI Elements',
    route: '#',
    icon: 'fa fa-paper-plane'
  },
  {
    title: 'Components',
    route: '#',
    icon: 'fa fa-shopping-bag'
  },
  {
    title: 'Breadcrumb',
    route: '',
    icon: 'fa fa-diamond'
  }
];

@Component({
  selector: 'page-t-breadcrumbs',
  templateUrl: './breadcrumbs.component.html',
  styleUrls: ['./breadcrumbs.component.scss']
})
export class PageTBreadcrumbsComponent extends BasePageComponent implements OnInit, OnDestroy {
  breadcrumbs: Breadcrumb[];
  breadcrumbsIcons: Breadcrumb[];

  constructor(store: Store<AppState>) {
    super(store);

    this.pageData = {
      title: 'Breadcrumbs',
      loaded: true,
      breadcrumbs: [
        {
          title: 'UI elements',
          route: './dashboard'
        },
        {
          title: 'Theme components',
          route: './dashboard'
        },
        {
          title: 'Breadcrumbs'
        }
      ]
    };
    this.breadcrumbs = breadcrumbs;
    this.breadcrumbsIcons = breadcrumbsIcons;
  }

  ngOnInit() {
    super.ngOnInit();
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }
}
