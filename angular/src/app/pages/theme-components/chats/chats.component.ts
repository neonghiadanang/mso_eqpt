import { Component, OnDestroy, OnInit } from '@angular/core';

import { Store } from '@ngrx/store';

import { AppState } from '../../../interfaces/app-state';
import { BasePageComponent } from '../../base-page/base-page.component';
import { User } from '../../../ui/interfaces/user';
import { Message } from '../../../ui/interfaces/message';

@Component({
  selector: 'page-t-chats',
  templateUrl: './chats.component.html',
  styleUrls: ['./chats.component.scss']
})
export class PageTChatsComponent extends BasePageComponent implements OnInit, OnDestroy {
  activeUser: User;
  activeUser2: User;
  messages: Message[];
  messages2: Message[];

  constructor(store: Store<AppState>) {
    super(store);

    this.pageData = {
      title: 'Chats',
      loaded: true,
      breadcrumbs: [
        {
          title: 'UI elements',
          route: './dashboard'
        },
        {
          title: 'Theme components',
          route: './dashboard'
        },
        {
          title: 'Chats'
        }
      ]
    };
    this.activeUser = {
      name: 'Dennis',
      lastSeen: 'online',
      avatar: ''
    };
    this.activeUser2 = {
      name: 'Amanda Li',
      lastSeen: 'last seen 10 minutes ago',
      avatar: 'assets/content/avatar-4.jpg'
    };
    this.messages = [
      {
        'date': '7 : 21',
        'content': `Aenean lacinia bibendum nulla sed consectetur. Nullam id dolor id nibh ultricies vehicula ut id elit.`,
        'my': false
      },
      {
        'date': '7 : 25',
        'content': `Aenean lacinia bibendum nulla sed consectetur.`,
        'my': true
      },
      {
        'date': '7 : 29',
        'content': `Aenean lacinia bibendum nulla.`,
        'my': false
      },
      {
        'date': '8 : 15',
        'content': `Lorem ipsum dolor sit.`,
        'my': false
      }
    ];
    this.messages2 = [
      {
        'date': '8 hours ago',
        'content': `Aenean lacinia bibendum nulla sed consectetur. Nullam id dolor id nibh ultricies vehicula ut id elit.`,
        'my': false
      },
      {
        'date': '7 hours ago',
        'content': `Aenean lacinia bibendum nulla sed consectetur.`,
        'my': true
      },
      {
        'date': '2 hours ago',
        'content': `Contrary to popular belief,`,
        'my': false
      },
      {
        'date': '15 minutes ago',
        'content': `Lorem ipsum dolor sit.`,
        'my': true
      },
      {
        'date': '14 minutes ago',
        'content': `Aenean lacinia bibendum nulla sed consectetur. Nullam id dolor id nibh ultricies vehicula ut id elit.`,
        'my': false
      },
      {
        'date': '12 minutes ago',
        'content': `Aenean lacinia bibendum nulla sed consectetur.`,
        'my': true
      },
    ];
  }

  ngOnInit() {
    super.ngOnInit();
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }
}
