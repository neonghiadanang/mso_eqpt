import { Component, OnDestroy, OnInit } from '@angular/core';

import { Store } from '@ngrx/store';

import { AppState } from '../../../interfaces/app-state';
import { BasePageComponent } from '../../base-page/base-page.component';
import { HttpService } from '../../../services/http/http.service';

@Component({
  selector: 'page-t-icons',
  templateUrl: './icons.component.html',
  styleUrls: ['./icons.component.scss']
})
export class PageTIconsComponent extends BasePageComponent implements OnInit, OnDestroy {
  sliIcons: string[];

  constructor(
    store: Store<AppState>,
    private httpSv: HttpService
  ) {
    super(store);

    this.pageData = {
      title: 'Icons',
      loaded: false,
      breadcrumbs: [
        {
          title: 'UI elements',
          route: './dashboard'
        },
        {
          title: 'Theme components',
          route: './dashboard'
        },
        {
          title: 'Icons'
        }
      ]
    };
    this.sliIcons = [];
  }

  ngOnInit() {
    super.ngOnInit();

    this.getData('assets/data/icons-simpleline.json', 'sliIcons', 'setLoaded');
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }

  getData(url: string, dataName: string, callbackFnName?: string) {
    this.httpSv.getData(url).subscribe(
      data => {
        this[dataName] = data;
      },
      err => {
        console.log(err);
      },
      () => {
        if ((callbackFnName && typeof this[callbackFnName] === 'function')) {
          this[callbackFnName](this[dataName]);
        }
      }
    );
  }
}
