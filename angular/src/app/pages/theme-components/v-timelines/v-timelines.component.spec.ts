import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VTimelinesComponent } from './v-timelines.component';

describe('VTimelinesComponent', () => {
  let component: VTimelinesComponent;
  let fixture: ComponentFixture<VTimelinesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VTimelinesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VTimelinesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
