import { Component, OnDestroy, OnInit } from '@angular/core';

import { Store } from '@ngrx/store';

import { AppState } from '../../../interfaces/app-state';
import { BasePageComponent } from '../../base-page/base-page.component';

const timelineData: any[] = [
  {
    'label': '2017',
    'timeline': [
      {
        'date': '2 hours ago',
        'content': `Aenean lacinia bibendum nulla sed consectetur. Nullam id dolor id nibh ultricies vehicula ut id elit.`,
        'pointColor': '#ea8080'
      },
      {
        'date': 'July 10, 2017',
        'content': `Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptate.`,
        'pointColor': '#B2DFDB'
      }
    ]
  },
  {
    'label': '2016',
    'timeline': [
      {
        'date': 'December 27, 2016',
        'content': `Lorem ipsum dolor sit.`,
        'pointColor': '#FFC6E6'
      },
      {
        'date': 'December 20, 2016',
        'content': `Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur nam nisi veniam.`,
        'pointColor': '#FFA78D'
      }
    ]
  }
];

@Component({
  selector: 'page-t-v-timelines',
  templateUrl: './v-timelines.component.html',
  styleUrls: ['./v-timelines.component.scss']
})
export class PageTVTimelinesComponent extends BasePageComponent implements OnInit, OnDestroy {
  timelineData: any[];

  constructor(store: Store<AppState>) {
    super(store);

    this.pageData = {
      title: 'Vertical timelines',
      loaded: true,
      breadcrumbs: [
        {
          title: 'UI elements',
          route: './dashboard'
        },
        {
          title: 'Theme components',
          route: './dashboard'
        },
        {
          title: 'Vertical timelines'
        }
      ]
    };
    this.timelineData = timelineData;
  }

  ngOnInit() {
    super.ngOnInit();
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }
}
