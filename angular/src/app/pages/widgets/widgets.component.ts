import { Component, OnDestroy, OnInit } from '@angular/core';
import { BasePageComponent } from '../base-page/base-page.component';
import { Store } from '@ngrx/store';
import { AppState } from '../../interfaces/app-state';

const messages: any[] = [
  {
    from: 'Nancy',
    subject: 'HTML',
    content: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
    avatar: 'assets/content/avatar-4.jpg'
  },
  {
    from: 'Mary',
    subject: 'Css',
    content: 'Lorem Ipsum has been the industrys standard',
    avatar: 'assets/content/avatar-3.jpg'
  },
  {
    from: 'Bobby',
    subject: 'Angular 2',
    content: 'It is a long established fact that a reader will be distracted by the readable content',
    avatar: 'assets/content/avatar-2.jpg'
  },
  {
    from: 'Roma',
    subject: 'Type Script',
    content: 'There are many variations of passages of',
    avatar: 'assets/content/avatar-1.jpg'
  },
  {
    from: 'Amanda',
    subject: 'PHP',
    content: 'Lorem Ipsum has been the industrys standard',
    avatar: 'assets/content/avatar-5.jpg'
  },
  {
    from: 'Tom',
    subject: 'Sql',
    content: 'There are many variations of passages of',
    avatar: 'assets/content/avatar-6.jpg'
  }
];
const folders: any[] = [
  {
    icon: 'android',
    badge: false,
    name: 'Android app',
    updated: 'March 21, 2017'
  },
  {
    icon: 'update',
    badge: false,
    name: 'Update plugins',
    updated: 'March 19, 2017'
  },
  {
    icon: 'bug_report',
    badge: false,
    name: 'Fix bugs',
    updated: 'March 22, 2017'
  },
  {
    icon: 'unarchive',
    badge: false,
    name: 'Create app design',
    updated: 'March 25, 2017'
  },
  {
    icon: 'content_copy',
    badge: 8,
    name: 'Create widgets',
    updated: 'March 16, 2017'
  },
  {
    icon: 'folder_open',
    badge: false,
    name: 'Documentation',
    updated: 'March 28, 2017'
  }
];

@Component({
  selector: 'page-widgets',
  templateUrl: './widgets.component.html',
  styleUrls: ['./widgets.component.scss']
})
export class PageWidgetsComponent extends BasePageComponent implements OnInit, OnDestroy {
  lat: number;
  lng: number;
  messages: any[];
  folders: any[];
  barChartOptions: any;
  barChartLabels: string[];
  barChartType: string;
  barChartLegend: boolean;
  barChartData: any[];
  pieChartLabels: string[];
  pieChartData: any[];
  pieChartColors: any[];
  pieChartType: string;
  pieChartOptions: any;
  doughnutChartLabels: string[];
  doughnutChartData: number[];
  doughnutChartColors: any[];
  doughnutChartType: string;
  doughnutChartOptions: any;
  polarAreaChartLabels: string[];
  polarAreaChartData: number[];
  polarAreaChartColors: any[];
  polarAreaChartType: string;
  polarAreaChartOptions: any;

  constructor(store: Store<AppState>) {
    super(store);

    this.pageData = {
      title: 'Widgets',
      loaded: true,
      breadcrumbs: [
        {
          title: 'Main',
          route: './dashboard'
        },
        {
          title: 'Widgets'
        }
      ]
    };
    this.lat = 50.4664212;
    this.lng = 30.6;
    this.messages = messages;
    this.folders = folders;

    // barChart
    this.barChartOptions = {
      scaleShowVerticalLines: false,
      responsive: true,
      responsiveAnimationDuration: 500
    };
    this.barChartLabels = [
      '2012',
      '2013',
      '2014',
      '2015',
      '2016',
      '2017'
    ];
    this.barChartType = 'bar';
    this.barChartLegend = true;
    this.barChartData = [
      {
        data: [59, 80, 81, 56, 55, 40],
        label: 'Angular JS',
        borderWidth: 1,
        pointRadius: 1
      },
      {
        data: [48, 40, 19, 86, 27, 90],
        label: 'React JS',
        borderWidth: 1,
        pointRadius: 1
      }
    ];

    // Pie
    this.pieChartLabels = [
      'Angular',
      'PHP',
      'HTML'
    ];
    this.pieChartData = [
      300,
      500,
      100
    ];
    this.pieChartColors = [
      {
        backgroundColor: [
          '#778391',
          '#5dade0',
          '#3c4e62'
        ],
      }
    ];
    this.pieChartType = 'pie';
    this.pieChartOptions = {
      elements: {
        arc : {
          borderWidth: 0
        }
      },
      tooltips: false
    };

    // Doughnut
    this.doughnutChartLabels = [
      'Angular',
      'PHP',
      'HTML'
    ];
    this.doughnutChartData = [
      350,
      450,
      100
    ];
    this.doughnutChartColors = [
      {
        backgroundColor: [
          '#778391',
          '#ff8c00',
          '#3c4e62'
        ],
      }
    ];
    this.doughnutChartType = 'doughnut';
    this.doughnutChartOptions = {
      elements: {
        arc : {
          borderWidth: 0
        }
      },
      tooltips: false
    };

    // PolarArea
    this.polarAreaChartLabels = [
      'Angular',
      'PHP',
      'HTML'
    ];
    this.polarAreaChartData = [
      300,
      400,
      500
    ];
    this.polarAreaChartColors = [
      {
        backgroundColor: [
          '#778391',
          '#dc143c',
          '#3c4e62'
        ],
      }
    ];
    this.polarAreaChartType = 'polarArea';
    this.polarAreaChartOptions = {
      elements: {
        arc : {
          borderWidth: 0
        }
      },
      tooltips: false
    };
  }

  ngOnInit() {
    super.ngOnInit();
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }
}
