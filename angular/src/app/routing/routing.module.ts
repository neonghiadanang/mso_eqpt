import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { Routes, RouterModule} from '@angular/router';
import { 
  AuthGuardService as AuthGuard 
} from '../services/auth/auth-guard.service';
import { JwtHelperService, JWT_OPTIONS  } from '@auth0/angular-jwt';
import { VerticalLayoutComponent } from '../layouts/vertical/vertical.component';
import { ExtraLayoutComponent } from '../layouts/extra/extra.component';
import { PageSignIn2Component } from '../pages/extra-pages/sign-in-2/sign-in-2.component';
import { PageNotFoundComponent } from '../pages/not-found/not-found.component';
import { PageSignIn1Component } from '../pages/extra-pages/sign-in-1/sign-in-1.component';
import { Page404Component } from '../pages/extra-pages/page-404/page-404.component';
import { Page500Component } from '../pages/extra-pages/page-500/page-500.component';
import { EqptMachineStateComponent } from '../pages/eqpt-machine-state/eqpt-machine-state.component';
import { NexgenecEqptSettingComponent } from '../pages/nexgeneceqptsetting/nexgeneceqptsetting.component';
import { NexgenecEqptDetailComponent } from '../pages/nexgeneceqptdetail/nexgeneceqptdetail.component';
import { NexgenecUserComponent } from '../pages/nexgenecuser/nexgenecuser.component';
import { NexgenecEqptConfigComponent } from '../pages/eqpt-config/eqpt-config.component';
import { NexgenecEqptLoggerComponent } from '../pages/eqpt-logger/eqpt-logger.component';
import { NexgenecEqptUploadSettingComponent } from '../pages/eqpt-upload/eqpt-upload.component';
import { NexgenecActionLogComponent } from '../pages/nexgenecactionlog/nexgenecactionlog.component';
import { NexgenecEqptActionComponent } from '../pages/nexgeneceqptaction/nexgeneceqptaction.component';
import { NexgenecEqptStateComponent } from '../pages/nexgeneceqptstate/nexgeneceqptstate.component';
import { NexgenecUsergroupComponent } from '../pages/nexgenecgroup/nexgenecgroup.component';
import { NexgenecVersionControlComponent } from '../pages/nexgenecversioncontrol/nexgenecversioncontrol.component';

const mainRoutes: Routes = [
  { path: 'equipmentlist', 
    component: NexgenecEqptDetailComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'equipmentsetting/:id',
    component:NexgenecEqptSettingComponent,
  },
  {
    path: 'users',
    component:NexgenecUserComponent,
  },
  {
    path: 'groups',
    component:NexgenecUsergroupComponent,
  },
  {
    path: 'version',
    component:NexgenecVersionControlComponent,
  },
  {
    path: 'eqptmachinestate/:id',
    component:EqptMachineStateComponent,
  },
  {
    path: 'eqptconfig/:id',
    component:NexgenecEqptConfigComponent,
  },
  {
    path: 'eqptlogger/:id',
    component:NexgenecEqptLoggerComponent,
  },
  {
    path: 'eqptupload/:id',
    component:NexgenecEqptUploadSettingComponent,
  },
  {
    path: 'actionlog',
    component:NexgenecActionLogComponent,
  },
  {
    path: 'machinastatelist',
    component:NexgenecEqptStateComponent,
  },
  {
    path: 'machinaactionlist',
    component:NexgenecEqptActionComponent,
  },
  { path: '**', component: PageNotFoundComponent },
];

const extraRoutes: Routes = [
  { path: 'sign-in', component: PageSignIn1Component },
  { path: 'page-404', component: Page404Component },
  { path: 'page-500', component: Page500Component },
];

const layoutRoutes: Routes = [
  {
    path: '',
    redirectTo: '/vertical/equipmentlist',
    pathMatch: 'full',
    canActivate: [AuthGuard] 
  },
  {
    path: 'login',
    component: PageSignIn2Component,
    children: extraRoutes
  },
  {
    path: 'vertical',
    component: VerticalLayoutComponent,
    children: mainRoutes
  },
  {
    path: 'extra',
    component: ExtraLayoutComponent,
    children: extraRoutes
  },
  {
    path: '**',
    component: VerticalLayoutComponent,
    children: mainRoutes
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(layoutRoutes, { useHash: true }),
  ],
  providers: [
    AuthGuard,
    { provide: JWT_OPTIONS, useValue: JWT_OPTIONS },
    JwtHelperService
  ],
  exports: [RouterModule],
})
export class RoutingModule { }
