import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable()
export class AuthGuardService implements CanActivate {
  constructor(public auth: AuthService, public router: Router) {}
  canActivate(): boolean {
    // console.log( "this.auth.isAuthenticated()="+this.auth.isAuthenticated())
    if (!this.auth.isAuthenticated()) {
      // console.log( "navigator to sign in")
      this.router.navigate(['/extra/sign-in']);
      return false;
    }
    //console.log( "navigator return true")
    return true;
  }
}