import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { HttpClient, HttpInterceptor, HttpRequest, HttpErrorResponse, HttpHandler, HttpEvent, HttpHeaders } from '@angular/common/http';
import { CanActivate, Router } from '@angular/router';
import { throwError as observableThrowError } from 'rxjs';

import { Observable } from 'rxjs';
import { tap, shareReplay, catchError } from 'rxjs/operators';

import { Store } from '@ngrx/store';
import { AppSettings } from '../../interfaces/settings';
import * as SettingsActions from '../../store/actions/app-settings.actions';
import { AppState } from '../../interfaces/app-state';

import * as moment from 'moment';
import * as jwtDecode from 'jwt-decode';
import { Configuration } from '../../../app/configuration';

@Injectable({providedIn: "root"})

export class AuthService {
  appSettings: AppSettings;

  constructor(
    public jwtHelper: JwtHelperService, 
    private http: HttpClient, 
    public router: Router,
    store: Store<AppState>, 
  ) {
    const token = localStorage.getItem('token');
    if( this.isLoggedIn()==true ){
      console.log("AuthService is login true");
      var appSettings = {
        fullName: localStorage.getItem('full_name'),
        loginId: localStorage.getItem('loginId'),
        operationId:localStorage.getItem('operation_id'),
        siteId:localStorage.getItem('site_id')
      }
      console.log(appSettings);
      store.dispatch(new SettingsActions.Update(appSettings));
      this.router.navigate(['']);
    }
  }

  // private url = 'http://10.140.104.28:8000'
  private url = Configuration.server;
  private apiRoot = this.url + '/';
  // ...
  public isAuthenticated(): boolean {
    const token = localStorage.getItem('token');
    // Check whether the token is expired and return
    // true or false
    // console.log("Call isAuthenticated token="+token)
    return !this.jwtHelper.isTokenExpired(token);
  }

  private setSession(authResult) {
    console.log(authResult )
    const token = authResult.token;
    const payload = <JWTPayload> jwtDecode(token);
    const expiresAt = moment.unix(payload.exp);
    localStorage.clear();
    console.log(payload )
    localStorage.setItem('full_name', authResult.fullname);
    localStorage.setItem('loginId', payload.username);
    localStorage.setItem('site_id', authResult.site_id);
    localStorage.setItem('operation_id', authResult.operation_id);
    //
    localStorage.setItem('payload', payload.user_id);
    localStorage.setItem('token', authResult.token);
    localStorage.setItem('expires_at', JSON.stringify(expiresAt.valueOf()));
  }

  get token(): string {
    return localStorage.getItem('token');
  }

  login(username: string, password: string) {
    // console.log("Call service login");
    // var headers_object = new HttpHeaders();
    // headers_object.set("site_id", "NEO");
    // headers_object.set("operation_id", "TEST");
    // var httpOptions = {
    //   headers: headers_object
    // };
    localStorage.clear();
    return this.http.post(
      this.apiRoot.concat('api-token-auth/'),
      { username, password },
      // httpOptions
    ).pipe(
      tap(response => this.setSession(response)),
      shareReplay(),
      catchError(this.handleError)
    );
  }

  logout() {
    console.log("logout call")
    localStorage.removeItem('token');
    localStorage.removeItem('expires_at');
    localStorage.clear();
  }

  refreshToken() {
    if (moment().isBetween(this.getExpiration().subtract(1, 'days'), this.getExpiration())) {
      return this.http.post(
        this.apiRoot.concat('refresh-token/'),
        { token: this.token }
      ).pipe(
        tap(response => this.setSession(response)),
        shareReplay(),
      ).subscribe();
    }
  }
  private handleError(error: any) {
    console.log(error);
    return observableThrowError(error.error || 'Server error')
  }
  private handleErrorTest(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {

      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {

      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(`Backend returned code ${error.status}, ` + `body was: ${error.error}`);
    }

    // return an observable with a user-facing error message
    // this.errorData = {
    //   errorTitle: 'Oops! Request for document failed',
    //   errorDesc: 'Something bad happened. Please try again later.'
    // };
    // return throwError(this.errorData);
  }
  getExpiration() {
    const expiration = localStorage.getItem('expires_at');
    const expiresAt = JSON.parse(expiration);

    return moment(expiresAt);
  }

  isLoggedIn() {
    const isExpired =  moment().isBefore(this.getExpiration());
    console.log( "isExpired="+isExpired);
    return isExpired;
  }
}

interface JWTPayload {
  user_id: string;
  username: string;
  email: string;
  exp: number;
}
