import { HttpEvent } from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { Observable } from 'rxjs';

export interface CrudOperations<T, ID> {
	saveFiles(t: T, files:any): Observable<HttpEvent<T>>;
	save(t: T): Observable<HttpEvent<T>>;
	updateFiles(id: ID, t: T, files:any): Observable<HttpEvent<T>>;
	update(id: ID, t: T): Observable<HttpEvent<T>>;
	findOne(id: ID): Observable<HttpEvent<T>>;
	findAll(): Observable<HttpEvent<T[]>>;
	delete(id: ID): Observable<HttpEvent<T>>;
	findByQuery(arrQuery:any): Observable<Object>;
	get(url:any): Observable<Object>;
	send(arrQuery:any): Observable<Object>;
}
