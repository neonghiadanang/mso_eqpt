
import { Observable } from 'rxjs';
import { HttpClient, HttpEvent, HttpHeaders } from '@angular/common/http';
import { CrudOperations } from './crud-operations.interface';
import { Configuration } from '../../app/configuration';

export abstract class CrudService<T, ID> implements CrudOperations<T, ID> {
 
  constructor(
    protected _http: HttpClient,
    protected _base: string
  ) {
    var url = Configuration.server;
    this._base = url + "/" + this._base;
    console.log("Construction class CrudService _base="+this._base)

    var token = localStorage.getItem('token');
    var headers_object = new HttpHeaders()
        .set("Authorization", "Token " + token)
    this.httpOptions = {
      headers: headers_object
    };
    console.log(this.httpOptions);
    console.log(this._base);
  }

  httpOptions:any;
  
  saveFiles(t: T, files: any): Observable<HttpEvent<T>> {
    var site_id = localStorage.getItem('site_id');
    var operation_id = localStorage.getItem('operation_id');
    var query = "?site_id="+site_id+"&operation_id="+operation_id;
    if(files==null){
      return this._http.post<T>(this._base+"/"+query, t, this.httpOptions);
    }
    else{
      const formData: FormData = new FormData();
      console.log(files);
      if(Array.isArray(files)){
        for ( let i = 0; i < files.length; i++ ) {
          formData.append( "file", files[i], files[i]['name'] );
        }
      }
      else{
        formData.append( "file", files, files['name'] );
      }
      formData.append( "data", JSON.stringify(t) );
      return this._http.post<T>(this._base + "/", formData, this.httpOptions);
    }
  }

  save(t: T): Observable<HttpEvent<T>> {
    return this.saveFiles(t, null);
    // var site_id = localStorage.getItem('site_id');
    // var operation_id = localStorage.getItem('operation_id');
    // var query = "?site_id="+site_id+"&operation_id="+operation_id;
    // return this._http.post<T>(this._base+"/"+query, t, this.httpOptions);
  }
  updateFiles(id: ID, t: T, files: any): Observable<HttpEvent<T>> {
    var site_id = localStorage.getItem('site_id');
    var operation_id = localStorage.getItem('operation_id');
    var query = "?site_id="+site_id+"&operation_id="+operation_id;
    if(files==null){
      return this._http.post<T>(this._base + "/" + id+"/"+query, t, this.httpOptions);
    }
    else{
      const formData: FormData = new FormData();
      if(Array.isArray(files)){
        for ( let i = 0; i < files.length; i++ ) {
          formData.append( "file", files[i], files[i]['name'] );
        }
      }
      else{
        formData.append( "file", files, files['name'] );
      }
      formData.append( "data", JSON.stringify(t) );
      return this._http.post<T>(this._base + "/" + id+"/"+query, formData, this.httpOptions);
    }
  }
  update(id: ID, t: T): Observable<HttpEvent<T>> {
    return this.updateFiles(id, t, null);
  }

  findOne(id: ID): Observable<HttpEvent<T>> {
    var site_id = localStorage.getItem('site_id');
    var operation_id = localStorage.getItem('operation_id');
    var query = "?site_id="+site_id+"&operation_id="+operation_id;
    return this._http.get<T>(this._base + "/" + id+"/"+query, this.httpOptions);
  }

  findAll(): Observable<HttpEvent<T[]>> {
    var site_id = localStorage.getItem('site_id');
    var operation_id = localStorage.getItem('operation_id');
    var query = "?site_id="+site_id+"&operation_id="+operation_id;
    return this._http.get<T[]>(this._base+"/"+query, this.httpOptions)
  }

  delete(id: ID): Observable<HttpEvent<T>> {
    var site_id = localStorage.getItem('site_id');
    var operation_id = localStorage.getItem('operation_id');
    var query = "?site_id="+site_id+"&operation_id="+operation_id;
    return this._http.delete<T>(this._base + '/' + id+"/"+query, this.httpOptions);
	}

  findByQuery(arrQuery:any): Observable<Object> {
    var site_id = localStorage.getItem('site_id');
    var operation_id = localStorage.getItem('operation_id');
    var query = "?site_id="+site_id+"&operation_id="+operation_id;
    console.log(arrQuery )
    for (let key in arrQuery) {
      console.log(key )
      query += "&"+key+"="+arrQuery[key]
    }
    console.log(query)
    return this._http.get(this._base + "/"+query, this.httpOptions)
  }

  get(query:any): Observable<Object> {
    var site_id = localStorage.getItem('site_id');
    var operation_id = localStorage.getItem('operation_id');
    query = query+"&site_id="+site_id+"&operation_id="+operation_id
    console.log(query)
    var url = Configuration.server;
    return this._http.get(url + query, this.httpOptions)
  }

  send(arrQuery:any): Observable<Object> {
    var site_id = localStorage.getItem('site_id');
    var operation_id = localStorage.getItem('operation_id');
    var query = "?site_id="+site_id+"&operation_id="+operation_id;
    console.log(arrQuery )
    for (let key in arrQuery) {
      console.log(key )
      query += "&"+key+"="+arrQuery[key]
    }
    console.log(query)
    return this._http.get(this._base + "/"+query, this.httpOptions)
  }
}
