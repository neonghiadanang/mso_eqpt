import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { throwError as observableThrowError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class HttpService {
  constructor(private http: HttpClient) {
    
  }

  getData(source: string) {
    var token = localStorage.getItem('token');
    var headers_object = new HttpHeaders().set("Authorization", "Token " + token);
    const httpOptions = {
      headers: headers_object
    };
    return this.http.get(source, httpOptions).pipe(
      tap((res: any) => res),
      catchError(this.handleError)
    );
  }

  postData(source: string, body: any) {
    var token = localStorage.getItem('token');
    var headers_object = new HttpHeaders().set("Authorization", "Token " + token);
    const httpOptions = {
      headers: headers_object
    };
    return this.http.post(source, body, httpOptions).pipe(
      tap((res: any) => res),
      catchError(this.handleError)
    );
  }


  private handleError(error: any) {
    return observableThrowError(error.error || 'Server error');
  }
}
