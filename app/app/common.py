def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    obj = [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]
    return obj

def isNum(data):
    try:
        int(data)
        return True
    except ValueError:
        return False

def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip