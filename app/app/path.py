from django.utils.deprecation import MiddlewareMixin
from django_requestlogging.middleware import LogSetupMiddleware as Original

class LogSetupMiddleware(MiddlewareMixin, Original):
    pass