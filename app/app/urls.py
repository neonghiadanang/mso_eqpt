"""app URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path
from django.conf.urls import url, include
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token
from . import views
# from .server.nexgenec_eqpt_config.views import listDataConfig, listDataConnection, listDataMes

# from .common import *

urlpatterns = [
    path('auth/login/', obtain_jwt_token),
    path('auth/refresh-token/', refresh_jwt_token),
    path('admin/', admin.site.urls),
    path(r'api-token-auth/', views.MyView.as_view()),
    path(r'api-token-refresh/', refresh_jwt_token),
    path('listeqp/', include('eqp.urls')),
    path('nexgenecactionlog/', include('server.nexgenec_action_log.urls')),
    path('nexgeneceqptdetail/', include('server.eqpt_detail.urls')),
    path('nexgeneceqptsetting/', include('server.eqpt_setting.urls')),
    path('eqpt/', include('server.nexgenec_eqpt_statemachine.urls')),
    path('user/', include('server.user.urls')),
    path('groups/', include('server.user.urls_groups')),
    path('eqp/', include('server.nexgenec_eqpt_config.urls')),
    path('eqptlogger/', include('server.nexgenec_eqpt_logger.urls')),
    path('eqptupload/', include('server.nexgenec_eqpt_upload_setting.urls')),
    path('nexgeneceqptaction/', include('server.nexgenec_eqpt_actionlist.urls')),
    path('nexgeneceqptstate/', include('server.nexgenec_eqpt_statelist.urls')),
    path('version/', include('server.nexgenec_version_control.urls')),
]
