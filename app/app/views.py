from django.contrib.auth.models import User
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.views import APIView    
from rest_framework.permissions import IsAuthenticated
from rest_framework import exceptions
from rest_framework import authentication
from django.contrib.auth import authenticate, get_user_model
from rest_framework.authentication import BasicAuthentication, SessionAuthentication
from rest_framework.authtoken.models import Token

from rest_framework_jwt.settings import api_settings
from rest_framework_jwt.compat import get_username_field, PasswordField
from rest_framework_jwt.authentication import BaseJSONWebTokenAuthentication
from django.forms.models import model_to_dict

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
jwt_decode_handler = api_settings.JWT_DECODE_HANDLER
jwt_get_username_from_payload = api_settings.JWT_PAYLOAD_GET_USERNAME_HANDLER

import base64
from django.core.exceptions import ObjectDoesNotExist

from .ldap_utils import check_Ldap
import logging
logger = logging.getLogger('logit')

#from ..server.nexgenec_action_log.views import loggerDb
# from ..server.user.models import NexgenecUser

class MyAppAuthentication(authentication.BaseAuthentication):
    def authenticate(self, request):
        logger.info('CALL LOGIN authenticate')
        # Get the username and password
        username = request.data.get('username', None)
        password = request.data.get('password', None)
        if not username or not password:
            raise exceptions.AuthenticationFailed('No credentials provided.')
        # print("=================== "+password)
        #password = base64.urlsafe_b64decode(unicode(password))
        password_decode = base64.b64decode(password).decode('ascii')
        # print(password)
        # print("===================1111 "+password)
        credentials = {
            get_user_model().USERNAME_FIELD: username,
            'password': password_decode
        }
        logger.info("Login username="+username)
        logger.info("Login password="+password)
        # Check LDAP
        # res = check_Ldap(username, password_decode)
        # print(res)
        # if res["result"]=="ERROR":
        #     raise exceptions.AuthenticationFailed('Can not found userID in LDAP server.')
        #if username!='admin' and username!='neo':
        #    raise exceptions.AuthenticationFailed(('Only admin or neo'+username))
        user = authenticate(**credentials)
        logger.info("End call authenticate(**credentials)")
        logger.info(user)
        if user is None:
            #loggerDb(request, "Invalid username/password.", "Login Fail", user.seq_id, "NexgenecUser")
            raise exceptions.AuthenticationFailed(('Invalid username/password.'))
        
        logger.info("is_active="+str(user.is_active))
        if user.is_active!='Y':
            #loggerDb(request, "User inactive or deleted.", "Login Fail", user.seq_id, "NexgenecUser")
            raise exceptions.AuthenticationFailed(('User inactive or deleted.'))
        # return UserObj + Auth
        #def loggerDb(request,action_desc,action_id='Insert',field_id=0,table_name=''):
        #loggerDb(request, model_to_dict(user), "Login Successful", user.seq_id, "NexgenecUser")
        return (user, 'OK')  # authentication successful

class MyView(APIView):
    authentication_classes = (SessionAuthentication, MyAppAuthentication)
    permission_classes = (IsAuthenticated,)

    def post(self, request, format=None):
        payload = jwt_payload_handler(request.user)
        content = {
            'token': jwt_encode_handler(payload),
            'user': str(request.user),
            'fullname': str(request.user.full_name),
            'site_id': str(request.user.site_id),
            'operation_id': str(request.user.operation_id),
            'auth': str(request.auth),  # None
        }
        return Response(content)