from django.db import models
from django.db.models import Manager

class OppUsers(models.Model):
    username = models.CharField(unique=True, max_length=50, blank=True, null=True)
    phone = models.CharField(max_length=50, blank=True, null=True)
    email = models.CharField(max_length=50, blank=True, null=True)
    id = models.FloatField(primary_key=True)
    fullname = models.CharField(max_length=100, blank=True, null=True)
    supervisor = models.CharField(max_length=100, blank=True, null=True)
    is_admin = models.CharField(max_length=1, blank=True, null=True)
    del_flag = models.CharField(max_length=1, blank=True, null=True)
    supervisor_email = models.CharField(max_length=128, blank=True, null=True)
    site_cn = models.CharField(max_length=20, blank=True, null=True)
    
    objects = Manager()

    class Meta:
        managed = False
        db_table = 'OPP_USERS'
        app_label = 'app.users'

class EquipmentModel(models.Model):
    #this.displayedColumns = ['FULLNAME', 'USERNAME', 'SUPERVISOR', 'EMAIL', 'ID'];
    FULLNAME = models.CharField(max_length=50, db_column='FULLNAME')
    USERNAME = models.CharField(max_length=50, db_column='USERNAME')
    SUPERVISOR = models.CharField(max_length=50, db_column='SUPERVISOR')
    EMAIL = models.CharField(max_length=50, db_column='EMAIL')
    DEL_FLAG = models.CharField(max_length=1, db_column='DEL_FLAG')
    # ID = models.IntegerField(max_length=50, db_column='ID')
    ID = models.IntegerField(
        primary_key=True,
        unique=False,
        default=None,
        blank=True,
        null=True,
        db_column='ID'
    )

    objects = Manager()

    class Meta:
        managed = False
        db_table = 'OPP_USERS'
        app_label = 'app.equipment'

    def __str__(self):
        return self.USERNAME