# from django.conf.urls import url,patterns
# from . import views, api
from django.urls import path, re_path
from django.conf.urls import url, include
from .views import listUser

urlpatterns = [
    path(r'', listUser),
    re_path(r'^(?P<ID>[-\w]+)/$', listUser),
    #path(r'^listeqp/(?P<pk>\w+)$', listEquipment)
    # url(r'^$', views.account_list,name='account_list'),
    # url(r'^isAvaluableAccount/$', views.isAvaluableAccount,name='is_availuable_acc'),
    # url(r'^api_accounts/$', api.api_account_list,name='api_account_list'),
    # url(r'^api_accounts/(?P<pk>\w+)$', api.api_account_detail,name='api_account_detail'),
    
    # path(r'^eqp/$', views.member_list,name='member_list'),
    #url(r'^member/add$', views.member_add,name='member_add'),
    # url(r'^add$', views.account_add,name='account_add'),
    # url(r'^member/ticket/$', views.ticket_list,name='ticket_list'),
]
