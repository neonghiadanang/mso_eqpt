from django.shortcuts import render
from django.db import connection
from rest_framework import serializers
from django.http import HttpResponse, JsonResponse
from rest_framework.decorators import api_view
from django.views.decorators.csrf import csrf_exempt
import json as simplejson
from .models import OppUsers

from django.core import serializers
from django.forms.models import model_to_dict
from app.common import *

# def dictfetchall(cursor):
#     "Return all rows from a cursor as a dict"
#     columns = [col[0] for col in cursor.description]
#     #print( columns)
#     # zipobj = zip(columns, row)
#     # print( zipobj )
#     # dictobj = dict(zipobj)
#     # print( dictobj )
#     obj = [
#         dict(zip(columns, row))
#         for row in cursor.fetchall()
#     ]
#     #print( obj )
#     return obj
# def isNum(data):
#     try:
#         int(data)
#         return True
#     except ValueError:
#         return False
@api_view(['GET', 'POST', 'DELETE', 'OPTION'])
def listUser(request, ID=0):
    # If using cursor without "with" -- it must be closed explicitly:
    print("call listUser request.method="+request.method)

    if request.method == "GET":
        #ID = request.GET.get('ID','')
        print('ID='+str(ID))
        # list = OppUsers.objects.all
        # response_data = {}
        # cashflow_set = OppUsers.objects.all();
        # i = 0;
        # for e in cashflow_set.iterator():
        #     c = OppUsers(value=e.value, date=str(e.date));
        #     response_data[i] = c;
        # data = [car.simplejson for car in OppUsers.objects.all()]
        if isNum(ID)==True and int(ID)>0:
            obj = OppUsers.objects.filter(id=ID).values()
            print(len(obj))
            data = []
            if len(obj)==0:
                data = []
            else:
                data = list(obj)
            return JsonResponse(
                    data
                    ,safe=False
                    )
        elif len(ID)>0:
            #search by fullname
            obj = OppUsers.objects.filter(fullname__contains=ID).values()
            data = list(obj)
            return JsonResponse(
                    data
                    ,safe=False
                    )
        else:
            listobj = OppUsers.objects.values()
            for e in listobj.iterator():
                #print(e)
                for key in e:
                    value = e[key]
                    #print("model_to_dict(obj) The key and value are ({}) = ({})".format(key, value))
                pass
                # for key in model_to_dict(e):
                #     value = model_to_dict(e)[key]
                #     print("model_to_dict(obj) The key and value are ({}) = ({})".format(key, value))
                #     #obj[key] = jsonObj[key]
                #     #setattr(obj, key, jsonObj[key])
                #     #print("model_to_dict(obj) The key and value are ({}) = ({})".format(key, value))
                # pass
            data = list(OppUsers.objects.values())
            return JsonResponse(
                    data
                    ,safe=False
                    )

        # return HttpResponse(
        #         data
        #         ,content_type='application/json'
        #     )
        # username = request.GET.get('username','')
        # fullname= request.GET.get('fullname','')
        # with connection.cursor() as cursor:
        #     sql ="select * from OPP_USERS where DEL_FLAG='N'"
        #     if len(username)>0:
        #         sql += " and USERNAME LIKE '%"+username+"%' "
        #     if len(fullname)>0:
        #         sql += " and FULLNAME LIKE '%"+fullname+"%' "
        #     print(sql)
        #     cursor.execute(sql)
        #     json_data = dictfetchall(cursor)
        #     # print("json_data")
        #     #print(json_data)
        #     return JsonResponse(json_data, safe=False)
    elif request.method == "POST":
        # print(request)
        # print(request.POST)
        # print(request.data)
        # print(request.data['data'])
        #data = request.data.decode('utf-8')
        #print(data)
        #jsonObj = simplejson.loads(data)
        #received_json_data = simplejson.loads(request.data)
        #print(jsonObj)
        jsonObj = request.data['data']

        obj = EquipmentModel.objects.get(ID=jsonObj['ID'])
        # print(model_to_dict(obj))
        for key in model_to_dict(obj):
            #value = model_to_dict(obj)[key]
            #print("model_to_dict(obj) The key and value are ({}) = ({})".format(key, value))
            #obj[key] = jsonObj[key]
            setattr(obj, key, jsonObj[key])
            #print("model_to_dict(obj) The key and value are ({}) = ({})".format(key, value))
        pass
        obj.save()
        
        # return HttpResponse(
        #     #serializers.serialize('json', [jsonObj])
        #     jsonObj
        #     ,content_type='application/json'
        #     )
        return JsonResponse(
                jsonObj
                ,safe=False
                )
    elif request.method == "DELETE":
        print( request )
        print( ID )
        obj = EquipmentModel.objects.get(ID=ID)
        obj.DEL_FLAG='Y'
        obj.save()
        return JsonResponse(
                None
                ,safe=False
                )
    else:
        print("call listEquipment request.method="+request.method)
        return JsonResponse(None, safe=False)