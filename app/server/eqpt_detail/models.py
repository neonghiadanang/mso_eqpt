from django.db import models

class NexgenecEqptDetail(models.Model):
    seq_id = models.FloatField(primary_key=True)
    eqpt_id = models.CharField(max_length=50, blank=True, null=True)
    eqpt_type = models.CharField(max_length=20, blank=True, null=True)
    updated_by = models.CharField(max_length=30, blank=True, null=True)
    timestamp = models.DateField(blank=True, null=True)
    # site_id = models.CharField(max_length=20, blank=True, null=True)
    # operation_id = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'NEXGENEC_EQPT_DETAIL'
        app_label = 'NEXGENEC_EQPT_DETAIL'
