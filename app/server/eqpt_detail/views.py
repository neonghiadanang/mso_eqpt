from django.db import connection
from django.shortcuts import render
from rest_framework import serializers
from django.http import HttpResponse, JsonResponse
from rest_framework.decorators import api_view
from django.views.decorators.csrf import csrf_exempt
import json as simplejson
from django.core import serializers
from django.forms.models import model_to_dict
from .models import NexgenecEqptDetail
from app.common import *
from django.db.models import Avg, Max, Min, Sum
from django.utils.timezone import localtime, now

from django.contrib.sessions.models import Session
from django.contrib.auth.models import User
from ..nexgenec_action_log.views import loggerDb

import logging
logger = logging.getLogger("logit")

@api_view(['GET', 'POST', 'DELETE'])
def listData(request, ID=''):
    try:
        site_id = request.GET['site_id']
        operation_id = request.GET['operation_id']
        logger.info("Restful NexgenecEqptDetail site_id="+site_id+" operation_id="+operation_id+" request.method="+request.method)
        
        if request.method == "GET":
            eqpt_id = request.GET.get('eqpt_id')
            if len(str(eqpt_id))>0:
                logger.info('NexgenecEqptDetail search eqpt_id= ['+str(eqpt_id)+" " + str(len(str(eqpt_id)))+"]")
                eqpt_type = request.GET.get('eqpt_type')
                logger.info('   search eqpt_type= ['+str(eqpt_type)+" " + str(len(str(eqpt_type)))+"]")
                if eqpt_type is None or len(eqpt_type)==0:
                    obj = NexgenecEqptDetail.objects.filter(eqpt_id__icontains=eqpt_id).order_by('-seq_id')
                else:
                    obj = NexgenecEqptDetail.objects.filter(eqpt_id__icontains=eqpt_id).filter(eqpt_type__icontains=eqpt_type).order_by('-seq_id')
                data = list(obj.values())
                logger.info('NexgenecEqptDetail return:')
                logger.info(data)
                return JsonResponse(
                        data
                        ,safe=False
                        )
            else:
                logger.info('NexgenecEqptDetail show list all')
                data = list(NexgenecEqptDetail.objects.order_by('seq_id').values())
                return JsonResponse(
                        data
                        ,safe=False
                        )

        elif request.method == "POST":
            jsonObj = request.data['data']
            logger.info('NexgenecEqptDetail update object begin:')
            logger.info(jsonObj)
            if jsonObj["seq_id"] is None:
                # new object
                # print(request.user)
                # print(str(request.user))
                logger.info('NexgenecEqptDetail new object:')
                seq_id = NexgenecEqptDetail.objects.all().aggregate(Max('seq_id'))["seq_id__max"]+1
                #obj = NexgenecEqptDetail(seq_id=seq_id, updated_by=str(request.user), timestamp=now(), site_id=site_id,operation_id=operation_id)
                obj = NexgenecEqptDetail(seq_id=seq_id, updated_by=str(request.user), timestamp=now())
                logger.info(model_to_dict(obj))
                for key in model_to_dict(obj):
                    # print(key)
                    # print(jsonObj[key])
                    if jsonObj[key] is not None:
                        setattr(obj, key, jsonObj[key])
                    # elif key in request.GET:
                    #      print(key + "="+request.GET[key])
                    #      if len(request.GET[key])>0:
                    #          setattr(obj, key, request.GET[key])
                    # else:
                    #     setattr(obj, key, jsonObj[key])
                pass
                logger.info(model_to_dict(obj))
                obj.save()
                logger.info('NexgenecEqptDetail new object after:')
                logger.info(model_to_dict(obj))
                #def loggerDb(request,action_desc,action_id='Insert',field_id=0,table_name=''):
                loggerDb(request, model_to_dict(obj), "Create", obj.seq_id, NexgenecEqptDetail.objects.model._meta.db_table)
                #
                return JsonResponse(
                        jsonObj
                        ,safe=False
                        )
            else:
                #Update
                obj = NexgenecEqptDetail.objects.get(seq_id=ID)
                for key in model_to_dict(obj):
                    if key in jsonObj:
                        setattr(obj, key, jsonObj[key])
                    elif key in request.GET:
                        print(key + "="+request.GET[key])
                        if len(request.GET[key])>0:
                            setattr(obj, key, request.GET[key])
                pass
                obj.save()
                logger.info('NexgenecEqptDetail update object after:')
                logger.info(jsonObj)
                
                #def loggerDb(request,action_desc,action_id='Insert',field_id=0,table_name=''):
                loggerDb(request, model_to_dict(obj), "Edit", obj.seq_id, NexgenecEqptDetail.objects.model._meta.db_table)

                return JsonResponse(
                        jsonObj
                        ,safe=False
                        )
        elif request.method == "DELETE":
            logger.info('NexgenecEqptDetail delete object by ID:'+str(ID))
            obj = NexgenecEqptDetail.objects.get(seq_id=ID)
            logger.info('NexgenecEqptDetail load object by ID')
            logger.info(model_to_dict(obj))
            # obj.DEL_FLAG='Y'
            #def loggerDb(request,action_desc,action_id='Insert',field_id=0,table_name=''):
            loggerDb(request, model_to_dict(obj), "Delete", obj.seq_id, NexgenecEqptDetail.objects.model._meta.db_table)
            
            obj.delete()

            return JsonResponse(
                    model_to_dict(obj)
                    ,safe=False
                    )
        else:
            logger.info('NexgenecEqptDetail not support method '+request.method)
            return JsonResponse(None, safe=False)
    except Exception:
        logger.error("Fatal error", exc_info=True)

@api_view(['GET'])
def listEqptMenu(request):
    try:
        logger.info("Restful listEqptMenu Begin")

        mapData = []

        mapData.append({
            "title":"Administrator System",
            "groupTitle" : True
        })
        mapData.append({
            "title":"User/Groups",
            "icon": {
                "class": "fa fa-user-o",
                "bg": "#E1BEE7",
                "color": "rgba(0,0,0,.87)"
            },
            "sub":[
                {
                    "title": "Users",
                    "icon": {
                        "class": "fa fa-user",
                        "bg": "#E1BEE7",
                        "color": "rgba(0,0,0,.87)"
                    },
                    "routing": "users"
                },
                {
                "title": "Groups",
                    "icon": {
                        "class": "fa fa-users",
                        "bg": "#E1BEE7",
                        "color": "rgba(0,0,0,.87)"
                    },
                    "routing": "groups"
                },
                {
                "title": "Action Logger",
                "icon": {
                    "class": "fa fa-book",
                    "bg": "#E1BEE7",
                    "color": "rgba(0,0,0,.87)"
                },
                "routing": "actionlog"
                }
            ]
        })
        mapData.append({
            "title": "Configuration",
            "icon": {
                "class": "fa fa-cog",
                "bg": "#E1BEE7",
                "color": "rgba(0,0,0,.87)"
                },
            "sub": 
            [
                {
                "title": "Machine State List",
                "icon": {
                    "class": "fa fa-comment",
                    "bg": "#E1BEE7",
                    "color": "rgba(0,0,0,.87)"
                },
                "routing": "machinastatelist"
                },
                {
                "title": "Machine Action List",
                "icon": {
                    "class": "fa fa-comment",
                    "bg": "#E1BEE7",
                    "color": "rgba(0,0,0,.87)"
                },
                "routing": "machinaactionlist"
                }
            ]
        })

        # mapData.append({
        #     "title":"Equipment List",
        #     "groupTitle" : True,
        # })
        mapData.append({
            "title":"Version Control",
            "icon": {
                "class": "fa fa-check-square-o",
                "bg": "#E1BEE7",
                "color": "rgba(0,0,0,.87)"
            },
            "routing": "version/"
        })

        mapData.append({
            "title":"Equipment List",
            "icon": {
                "class": "fa fa-th-list",
                "bg": "#E1BEE7",
                "color": "rgba(0,0,0,.87)"
            },
            "routing": "equipmentlist/"
        })

        # Get list eqpt
        mapEqpt = {}
        obj = NexgenecEqptDetail.objects.order_by('eqpt_type').values()
        data = list(obj)
        for item in data:
            eqpt_type = item["eqpt_type"]
            eqpt_id = item["eqpt_id"]
            if eqpt_type not in mapEqpt:
                mapEqpt[eqpt_type] = []
            mapEqpt[eqpt_type].append(eqpt_id)
        #parse list eqpt
        #print(mapEqpt)
        listEqpType = []
        for eqptType in mapEqpt:
            # print(mapEqpt[eqptType])
            listEqp = []
            for eqpt_id in mapEqpt[eqptType]:
                listEqp.append(
                    {
                        "title": eqpt_id,
                        "icon": {
                            "class": "fa fa-heartbeat",
                            "bg": "#E1BEE7",
                            "color": "rgba(0,0,0,.87)"
                        },
                        "routing": "eqptconfig/"+eqpt_id
                    }
                )
                pass
            mapData.append({
                    "title": eqptType,
                    "icon": {
                        "class": "fa fa-stethoscope",
                        "bg": "#E1BEE7",
                        "color": "rgba(0,0,0,.87)"
                    },
                    "sub":listEqp
                }
            )
        # mapData.append(listEqpType)
        return JsonResponse(
                mapData
                ,safe=False
                )
    except Exception:
        logger.error("Fatal error", exc_info=True)