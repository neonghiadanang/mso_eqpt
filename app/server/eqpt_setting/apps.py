from django.apps import AppConfig


class EqptDetailConfig(AppConfig):
    name = 'eqpt_detail'
