from django.db import models

class NexgenecEqptSetting(models.Model):
    seq_id = models.FloatField(primary_key=True)
    eqpt_id = models.CharField(max_length=30, blank=True, null=True)
    key = models.CharField(max_length=50, blank=True, null=True)
    value = models.CharField(max_length=100, blank=True, null=True)
    description = models.CharField(max_length=255, blank=True, null=True)
    site_id = models.CharField(max_length=20, blank=True, null=True)
    operation_id = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'NEXGENEC_EQPT_SETTING'
        app_label = 'NEXGENEC_EQPT_SETTING'
