from django.db import connection
from django.shortcuts import render
from rest_framework import serializers
from django.http import HttpResponse, JsonResponse
from rest_framework.decorators import api_view
from django.views.decorators.csrf import csrf_exempt
import json as simplejson
from django.core import serializers
from django.forms.models import model_to_dict
from django.db.models import Avg, Max, Min, Sum
from .models import NexgenecEqptSetting
from app.common import *
from ..nexgenec_action_log.views import loggerDb

import logging
logger = logging.getLogger("logit")

@api_view(['GET', 'POST', 'DELETE', 'PUT'])
def listData(request, ID=''):
    try:
        site_id = request.GET['site_id']
        operation_id = request.GET['operation_id']
        logger.info("Restful NexgenecEqptSetting site_id="+site_id+" operation_id="+operation_id+" request.method="+request.method)

        if request.method == "GET":
            eqpt_id = request.GET.get('eqpt_id')
            # print("eqpt_id ="+eqpt_id)
            if len(str(eqpt_id))>0:
                logger.info('NexgenecEqptSetting search eqpt_id= ['+str(eqpt_id)+" " + str(len(str(eqpt_id)))+"]")
                key = request.GET.get('key')
                logger.info('   search key= ['+str(key)+" " + str(len(str(key)))+"]")
                if key is None or len(key)==0:
                    obj = NexgenecEqptSetting.objects.filter(eqpt_id=eqpt_id)
                else:
                    obj = NexgenecEqptSetting.objects.filter(eqpt_id=eqpt_id).filter(key__icontains=key)
                data = list(obj.values())
                logger.info('NexgenecEqptSetting return:')
                logger.info(data)
                return JsonResponse(
                        data
                        ,safe=False
                        )
            else:
                logger.info('NexgenecEqptSetting show list all')
                data = list(NexgenecEqptSetting.objects.values())
                return JsonResponse(
                        data
                        ,safe=False
                        )

        elif request.method == "POST":
            jsonObj = request.data['data']
            logger.info('NexgenecEqptSetting update object begin:')
            logger.info(jsonObj)
            if jsonObj["seq_id"] is None:
                # new object
                seq_id = NexgenecEqptSetting.objects.all().aggregate(Max('seq_id'))["seq_id__max"]+1
                obj = NexgenecEqptSetting(seq_id=seq_id, site_id=site_id,operation_id=operation_id)
                for key in model_to_dict(obj):
                    if jsonObj[key] is not None:
                        setattr(obj, key, jsonObj[key])
                pass
                obj.save()
                #def loggerDb(request,action_desc,action_id='Insert',field_id=0,table_name=''):
                loggerDb(request, model_to_dict(obj), "Create", obj.seq_id, NexgenecEqptSetting.objects.model._meta.db_table)
                logger.info('NexgenecEqptSetting new object after:')
                logger.info(jsonObj)
                return JsonResponse(
                        jsonObj
                        ,safe=False
                        )
            else:
                #Update
                obj = NexgenecEqptSetting.objects.get(seq_id=ID)
                for key in model_to_dict(obj):
                    setattr(obj, key, jsonObj[key])
                pass
                obj.save()
                #def loggerDb(request,action_desc,action_id='Insert',field_id=0,table_name=''):
                loggerDb(request, model_to_dict(obj), "Edit", obj.seq_id, NexgenecEqptSetting.objects.model._meta.db_table)
                logger.info('NexgenecEqptSetting update object after:')
                logger.info(jsonObj)
                return JsonResponse(
                        jsonObj
                        ,safe=False
                        )
        elif request.method == "DELETE":
            logger.info('NexgenecEqptSetting delete object by seq_id:'+str(ID))
            obj = NexgenecEqptSetting.objects.get(seq_id=ID)
            logger.info('NexgenecEqptSetting load object by seq_id')
            print( model_to_dict(obj) )
            logger.info(model_to_dict(obj))
            #def loggerDb(request,action_desc,action_id='Insert',field_id=0,table_name=''):
            loggerDb(request, model_to_dict(obj), "Delete", obj.seq_id, NexgenecEqptSetting.objects.model._meta.db_table)

            # obj.DEL_FLAG='Y'
            obj.delete()

            logger.info("Deleted")
            return JsonResponse(
                    model_to_dict(obj)
                    ,safe=False
                    )
        else:
            logger.info('NexgenecEqptSetting not support method '+request.method)
            return JsonResponse(None, safe=False)
    except Exception:
        logger.error("Fatal error", exc_info=True)            