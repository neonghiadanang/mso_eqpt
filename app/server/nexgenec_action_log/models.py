from django.db import models

from django.db import models

class NexgenecActionLog(models.Model):
    id = models.FloatField(primary_key=True)
    username = models.CharField(max_length=20, blank=True, null=True)
    from_ip = models.CharField(max_length=20, blank=True, null=True)
    from_user_agent = models.CharField(max_length=200, blank=True, null=True)
    at_time = models.DateTimeField(blank=True, null=True)
    site_id = models.CharField(max_length=20, blank=True, null=True)
    operation_id = models.CharField(max_length=20, blank=True, null=True)
    field_id = models.FloatField(blank=True, null=True)
    table_name = models.CharField(max_length=20, blank=True, null=True)
    action_id = models.CharField(max_length=20, blank=True, null=True)
    action_desc = models.CharField(max_length=1000, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'NEXGENEC_ACTION_LOG'
        app_label = 'NEXGENEC_ACTION_LOG'