from django.urls import path, re_path
from django.conf.urls import url, include
from django.shortcuts import render
from .models import NexgenecActionLog
from django.utils.timezone import localtime, now
from django.db.models import Avg, Max, Min, Sum
from rest_framework.decorators import api_view
from django.db import connection
from django.shortcuts import render
from rest_framework import serializers
from django.http import HttpResponse, JsonResponse
from rest_framework.decorators import api_view
from django.views.decorators.csrf import csrf_exempt
import json as simplejson
from django.core import serializers
from django.forms.models import model_to_dict

import logging
logger = logging.getLogger("logit")

@api_view(['GET', 'POST', 'DELETE', 'PUT'])
def listData(request, ID=''):
    try:
        user_name = request.user.user_name
        user_name = request.GET.get("username", "")
        search_text = request.GET.get("search_text", "")

        logger.info("Restful NexgenecActionLog user_name="+user_name+" request.method="+request.method)

        if request.method == "GET":
            logger.info('NexgenecActionLog search user_name= ['+str(user_name)+"] search_text=[" + str(search_text)+"]")
            obj = NexgenecActionLog.objects
            if len(user_name)>0:
                logger.info('filter user_name= ['+str(user_name)+"]")
                obj = obj.filter(username=user_name)
            if len(search_text)>0:
                logger.info('filter search_text= ['+str(search_text)+"]")
                obj = obj.filter(action_desc__contains=search_text)
            obj = obj.order_by('-at_time')
            data = list(obj.values())
            #logger.info(data)
            return JsonResponse(
                    data
                    ,safe=False
                    )

        else:
            logger.info('NexgenecActionLog not support method '+request.method)
            return JsonResponse(None, safe=False)
    except Exception:
        logger.error("Fatal error", exc_info=True)

urlpatterns = [
    path(r'', listData),
]
