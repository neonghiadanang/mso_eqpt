from django.shortcuts import render
from .models import NexgenecActionLog
from django.utils.timezone import localtime, now
from django.db.models import Avg, Max, Min, Sum
from rest_framework.decorators import api_view

import logging
logger = logging.getLogger("logit")

# Create your views here.
def loggerDb(request,action_desc,action_id='Insert',field_id=0,table_name=''):
    try:
        # GET IP
        x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
        if x_forwarded_for:
            ip = x_forwarded_for.split(',')[0]
        else:
            ip = request.META.get('REMOTE_ADDR')
        # GET USER AGENT
        user_agent = request.META.get('HTTP_USER_AGENT')
        # GET USER NAME
        # print(request.user)
        username = request.user.user_name
        # print("ip="+ip)
        # print("user_agent="+user_agent)
        # print("username="+username)
        # headers = request.headers
        # print(request.GET)
        site_id = request.GET.get('site_id','MSO')
        operation_id = request.GET.get('operation_id','')
        # print(site_id)
        # print(operation_id)
        id=1
        try:
            id = NexgenecActionLog.objects.all().aggregate(Max('id'))["id__max"]
            if id is None:
                id = 1
            else:
                id += 1
        except ValueError:
            id = 1
        obj = NexgenecActionLog(id=id, username=username,from_ip=ip,from_user_agent=user_agent,at_time=now(),site_id=site_id,operation_id=operation_id,field_id=field_id,table_name=table_name,action_id=action_id,action_desc=action_desc)
        #obj.save(force_insert=True)
        obj.save()
        return obj
    except Exception:
        logger.error("Fatal error", exc_info=True)