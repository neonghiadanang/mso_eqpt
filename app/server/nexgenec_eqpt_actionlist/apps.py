from django.apps import AppConfig


class NexgenecEqptActionlistConfig(AppConfig):
    name = 'nexgenec_eqpt_actionlist'
