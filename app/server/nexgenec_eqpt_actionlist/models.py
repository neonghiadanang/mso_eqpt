from django.db import models
from django.utils.timezone import localtime, now

class NexgenecEqptAction(models.Model):
    seq_id = models.FloatField(primary_key=True, null=True)
    action_name = models.CharField(max_length=50, blank=True, null=True)
    action_desc = models.CharField(max_length=150, blank=True, null=True)
    updated_by = models.CharField(max_length=30, blank=True, null=True)
    timestamp = models.DateField(blank=True, null=True,default=now)

    class Meta:
        managed = False
        db_table = 'NEXGENEC_EQPT_ACTION'
        app_label = 'NEXGENEC_EQPT_ACTION'