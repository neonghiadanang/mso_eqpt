from django.db import connection
from django.shortcuts import render
from rest_framework import serializers
from django.http import HttpResponse, JsonResponse
from rest_framework.decorators import api_view
from django.views.decorators.csrf import csrf_exempt
import json as simplejson
from django.core import serializers
from django.forms.models import model_to_dict
from django.db.models import Avg, Max, Min, Sum
from django.utils.timezone import localtime, now
from .models import NexgenecEqptAction
from app.common import *
from ..nexgenec_action_log.views import loggerDb

import logging
logger = logging.getLogger("logit")

@api_view(['GET', 'POST', 'DELETE', 'PUT'])
def listData(request, ID=''):
    try:
        logger.info("Restful NexgenecEqptAction request.method="+request.method)

        if request.method == "GET":
            logger.info('NexgenecEqptAction show list all')
            name = request.GET.get('name')
            logger.info('   search name= ['+str(name)+" " + str(len(str(name)))+"]")
            if name is None or len(name)==0:
                obj = NexgenecEqptAction.objects
            else:
                obj = NexgenecEqptAction.objects.filter(action_name__icontains=name)

            data = list(obj.values())
            return JsonResponse(
                    data
                    ,safe=False
                    )

        elif request.method == "POST":
            jsonObj = request.data['data']
            logger.info('NexgenecEqptAction update object begin:')
            logger.info(jsonObj)
            if jsonObj["seq_id"] is None:
                # new object
                seq_id = NexgenecEqptAction.objects.all().aggregate(Max('seq_id'))["seq_id__max"]+1
                obj = NexgenecEqptAction(seq_id=seq_id, updated_by=request.user.user_name,
                    action_name=jsonObj['action_name'],
                    action_desc=jsonObj['action_desc']
                    )
                # for key in model_to_dict(obj):
                #     if jsonObj[key] is not None:
                #         setattr(obj, key, jsonObj[key])
                # pass
                obj.save()
                loggerDb(request, model_to_dict(obj), "Create", obj.seq_id, NexgenecEqptAction.objects.model._meta.db_table)
                logger.info('NexgenecEqptAction new object after:')
                logger.info(jsonObj)
                return JsonResponse(
                        jsonObj
                        ,safe=False
                        )
            else:
                #Update
                obj = NexgenecEqptAction.objects.get(seq_id=ID)
                # for key in model_to_dict(obj):
                    # setattr(obj, key, jsonObj[key])
                # pass
                obj.action_name=jsonObj['action_name']
                obj.action_desc=jsonObj['action_desc']
                obj.save()
                #def loggerDb(request,action_desc,action_id='Insert',field_id=0,table_name=''):
                loggerDb(request, model_to_dict(obj), "Edit", obj.seq_id, NexgenecEqptAction.objects.model._meta.db_table)
                logger.info('NexgenecEqptAction update object after:')
                logger.info(jsonObj)
                return JsonResponse(
                        jsonObj
                        ,safe=False
                        )
        elif request.method == "DELETE":
            logger.info('NexgenecEqptAction delete object by seq_id:'+str(ID))
            obj = NexgenecEqptAction.objects.get(seq_id=ID)
            logger.info('NexgenecEqptAction load object by seq_id')
            print( model_to_dict(obj) )
            logger.info(model_to_dict(obj))
            #def loggerDb(request,action_desc,action_id='Insert',field_id=0,table_name=''):
            loggerDb(request, model_to_dict(obj), "Delete", obj.seq_id, NexgenecEqptAction.objects.model._meta.db_table)

            # obj.DEL_FLAG='Y'
            obj.delete()

            logger.info("Deleted")
            return JsonResponse(
                    model_to_dict(obj)
                    ,safe=False
                    )
        else:
            logger.info('NexgenecEqptAction not support method '+request.method)
            return JsonResponse(None, safe=False)
    except Exception:
        logger.error("Fatal error", exc_info=True)