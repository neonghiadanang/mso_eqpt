from django.apps import AppConfig


class NexgenecEqptConfigConfig(AppConfig):
    name = 'nexgenec_eqpt_config'
