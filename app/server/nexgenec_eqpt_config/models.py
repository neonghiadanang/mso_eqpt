from django.db import models
from django.utils.timezone import localtime, now

# Create your models here.
class NexgenecEqptConfig(models.Model):
    seq_id = models.FloatField(primary_key=True)
    eqpt_id = models.CharField(max_length=50, blank=True, null=True)
    section_id = models.CharField(max_length=30, blank=True, null=True)
    order_no = models.BigIntegerField(blank=True, null=True)
    commented = models.CharField(max_length=10, blank=True, null=True)
    key = models.CharField(max_length=50, blank=True, null=True)
    value = models.CharField(max_length=256, blank=True, null=True)
    updated_by = models.CharField(max_length=30, blank=True, null=True)
    timestamp = models.DateTimeField(default=now, null=True)
    site_id = models.CharField(max_length=20, blank=True, null=True, default='MSO')
    operation_id = models.CharField(max_length=20, blank=True, null=True, default='TEST')
    version = models.FloatField(blank=True, null=True, default=1.0)
    old_value = models.CharField(max_length=256, blank=True, null=True)
    is_modified = models.FloatField(blank=True, null=True, default=0.0)
    state = models.CharField(max_length=20, blank=True, null=True, default='Editting')

    class Meta:
        managed = False
        db_table = 'NEXGENEC_EQPT_CONFIG'
        app_label = 'app.NEXGENEC_EQPT_CONFIG'

class NexgenecEqptConnection(models.Model):
    seq_id = models.FloatField(primary_key=True)
    eqpt_id = models.CharField(max_length=50, blank=True, null=True)
    section_id = models.CharField(max_length=30, blank=True, null=True)
    connection_mode = models.CharField(max_length=30, blank=True, null=True)
    connection_type = models.CharField(max_length=30, blank=True, null=True)
    connection_host = models.CharField(max_length=20, blank=True, null=True)
    connection_port = models.CharField(max_length=10, blank=True, null=True)
    environment = models.CharField(max_length=30, blank=True, null=True)
    username = models.CharField(max_length=20, blank=True, null=True)
    password_enc = models.CharField(max_length=20, blank=True, null=True)
    config = models.CharField(max_length=20, blank=True, null=True)
    updated_by = models.CharField(max_length=30, blank=True, null=True)
    timestamp = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'NEXGENEC_EQPT_CONNECTION'
        app_label = 'app.NEXGENEC_EQPT_CONNECTION'

class NexgenecMesSetting(models.Model):
    seq_id = models.FloatField(primary_key=True)
    eqpt_id = models.CharField(max_length=50, blank=True, null=True)
    environment = models.CharField(max_length=30, blank=True, null=True)
    section_id = models.CharField(max_length=30, blank=True, null=True)
    connection_mode = models.CharField(max_length=30, blank=True, null=True)
    connection_host = models.CharField(max_length=20, blank=True, null=True)
    connection_port = models.CharField(max_length=10, blank=True, null=True)
    default_userid = models.CharField(max_length=30, blank=True, null=True)
    default_password = models.CharField(max_length=30, blank=True, null=True)
    connection_type = models.CharField(max_length=30, blank=True, null=True)
    updated_by = models.CharField(max_length=30, blank=True, null=True)
    timestamp = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'NEXGENEC_MES_SETTING'
        app_label = 'app.NEXGENEC_MES_SETTING'
