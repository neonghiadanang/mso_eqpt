from django.urls import path, re_path
from django.conf.urls import url, include
from .views import listDataConfig, listDataConnection, listDataMes, approveConfig

urlpatterns = [
    path(r'config/', listDataConfig),
    re_path(r'^config/(?P<ID>[-\w]+)/$', listDataConfig),    
    path(r'connection/', listDataConnection),
    re_path(r'^connection/(?P<ID>[-\w]+)/$', listDataConnection),  
    path(r'mes/', listDataMes),
    re_path(r'^mes/(?P<ID>[-\w]+)/$', listDataMes),  
]
