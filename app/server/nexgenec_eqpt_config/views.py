from django.db import connection
from django.shortcuts import render
from rest_framework import serializers
from django.http import HttpResponse, JsonResponse
from rest_framework.decorators import api_view
from django.views.decorators.csrf import csrf_exempt
import json as simplejson
from django.core import serializers
from django.forms.models import model_to_dict
from django.utils.timezone import localtime, now
from django.db.models import Avg, Max, Min, Sum
from app.common import *
from ..nexgenec_action_log.views import loggerDb
from .models import NexgenecEqptConfig, NexgenecEqptConnection, NexgenecMesSetting
from ..nexgenec_version_control.models import NexgenecVersionControl

import logging
logger = logging.getLogger("logit")

def approveConfig(request):
    site_id = request.GET.get("site_id", "MSO")
    operation_id = request.GET.get("operation_id", "TEST")
    eqpt_id = request.GET.get('eqpt_id')
    version = request.GET.get('version')
    action = request.GET.get('action')
    commented = request.GET.get('commented')
    approved_by = request.GET.get('approved_by')
    #'Editing/Requesting/Cancelled/Rejected/Approved'
    next_state = 'Approved'
    if action=='Request Approve':
        next_state = 'Requesting'
    elif action=='Clone new version':
        next_state = 'Editing'
    else:
        next_state = 'Approved'

    logger.info("approveConfig site_id="+site_id+" operation_id="+operation_id)
    logger.info("eqpt_id="+eqpt_id+" version="+version)
    logger.info("action="+action+" next_state="+next_state)
    
    if (site_id is not None) and (operation_id is not None) and (eqpt_id is not None) and (version is not None):
        if next_state=='Editing':
            #clone new version to do
            logger.info("approveConfig do clone new version ")
            version = NexgenecEqptConfig.objects.filter(eqpt_id=eqpt_id).aggregate(Max('version'))["version__max"]
            if version is None:
                version=1
            
            seq_id = NexgenecEqptConfig.objects.all().aggregate(Max('seq_id'))["seq_id__max"]
            if seq_id is None:
                seq_id = 0
            seq_id += 1

            logger.info("Clone new version begin. seq_id="+str(seq_id))
            obj = NexgenecEqptConfig.objects.filter(eqpt_id=eqpt_id).filter(version=version).filter(operation_id=operation_id).filter(site_id=site_id)
            for item in obj:
                item.seq_id = seq_id
                item.version = version+1
                item.state = next_state
                item.old_value = ''
                item.is_modified = 0
                item.updated_by = request.user.user_name
                item.updated_time = now()
                item.save()
                seq_id += 1
            logger.info("Clone new version done. seq_id="+str(seq_id))

        else:
            obj = NexgenecEqptConfig.objects.filter(eqpt_id=eqpt_id).filter(version=version).filter(operation_id=operation_id).filter(site_id=site_id)
            for item in obj:
                item.state = next_state
                item.save()

            seq_id = NexgenecVersionControl.objects.all().aggregate(Max('seq_id'))["seq_id__max"]
            if seq_id is None:
                seq_id = 0
            seq_id += 1            
            objnew = NexgenecVersionControl(seq_id=seq_id,eqpt_id=eqpt_id, operation_id=operation_id, site_id=site_id, version=version, requested_by=request.user.user_name,requested_time=now(),state=next_state,data_type='NEXGENEC_EQPT_CONFIG')
            objnew.updated_by = approved_by
            objnew.requested_comment = commented
            objnew.save()
            logger.info('NexgenecVersionControl new object after:')
            logger.info(model_to_dict(objnew))
            loggerDb(request, model_to_dict(objnew), "Create", objnew.seq_id, NexgenecVersionControl.objects.model._meta.db_table)
        return 0
    else:
        return 1

@api_view(['GET', 'POST', 'DELETE', 'PUT'])
def listDataConfig(request, ID=''):
    try:
        site_id = request.GET.get("site_id", "MSO")
        operation_id = request.GET.get("operation_id", "TEST")
        logger.info("Restful NexgenecEqptConfig site_id="+site_id+" operation_id="+operation_id+" request.method="+request.method)

        if request.method == "GET":
            action = request.GET.get('action')
            if action is not None:
                logger.info("Restful NexgenecEqptConfig do approve")
                ret = approveConfig(request)

            eqpt_id = request.GET.get('eqpt_id')
            version = request.GET.get('version', 1)
            section_id = request.GET.get('section', '')
            view = request.GET.get('view', '')
            # print("eqpt_id ="+eqpt_id)
            if len(str(eqpt_id))>0:
                if view=='listmain':
                    logger.info('NexgenecEqptConfig search eqpt_id= ['+str(eqpt_id)+" " + str(len(str(eqpt_id)))+"]")
                    logger.info('  version= ['+str(version) +"]")
                    logger.info('  section_id= ['+str(section_id)+"]")

                    #Insert if nothing
                    obj = NexgenecEqptConfig.objects.filter(eqpt_id=eqpt_id)
                    if obj.values().count()==0:
                        logger.info('NexgenecEqptConfig No setting for eqpt_id= ['+str(eqpt_id))
                        #add default
                        obj = NexgenecEqptConfig.objects.filter(eqpt_id='*')
                        # logger.info(data)
                        seq_id = NexgenecEqptConfig.objects.all().aggregate(Max('seq_id'))["seq_id__max"]+1
                        for item in obj.values():
                            item["seq_id"] = seq_id
                            item["eqpt_id"] = eqpt_id
                            newobj = NexgenecEqptConfig(seq_id=seq_id, eqpt_id=eqpt_id )
                            for key in model_to_dict(newobj):
                                if item[key] is not None:
                                    setattr(newobj, key, item[key])
                            pass
                            seq_id += 1
                            # print("=================================")
                            # print(newobj)
                            newobj.save()
                    #end insert

                    listSection = NexgenecEqptConfig.objects.filter(eqpt_id=eqpt_id).values('section_id').distinct().order_by('section_id')
                    if listSection.count()>0 and len(str(section_id))==0:
                        section_id = list(listSection)[0]["section_id"]
                        logger.info('  default section_id= ['+str(section_id)+"]")

                    logger.info('NexgenecEqptConfig search by :eqpt_id='+eqpt_id+" version="+str(version))
                    obj = NexgenecEqptConfig.objects.filter(eqpt_id=eqpt_id).filter(version=version)
                    if len(str(section_id))>0:
                        logger.info('NexgenecEqptConfig search by :eqpt_id='+eqpt_id+" version="+str(version)+" section_id="+section_id)
                        obj = NexgenecEqptConfig.objects.filter(eqpt_id=eqpt_id).filter(version=version).filter(section_id=section_id)
                    data = list(obj.values())
                    logger.info('NexgenecEqptConfig return:')
                    #logger.info(data)

                    listVersion = NexgenecEqptConfig.objects.filter(eqpt_id=eqpt_id).values('version', 'state').distinct().order_by('version')
                    arrListVersion = list(listVersion)
                    if listVersion.count()==0:
                        arrListVersion = [1.0];
                    arrlistSection = list(listSection)

                    ret = [data,arrListVersion, arrlistSection, section_id]
                    # print(ret)

                    return JsonResponse(
                            ret
                            ,safe=False
                            )
                else:
                    #return all by version
                    logger.info('NexgenecEqptConfig search eqpt_id= ['+str(eqpt_id)+" " + str(len(str(eqpt_id)))+"]")
                    logger.info('  version= ['+str(version) +"]")
                    logger.info('  section_id= ['+str(section_id)+"]")
                    obj = NexgenecEqptConfig.objects.filter(eqpt_id=eqpt_id).filter(version=version).filter(is_modified=1)
                    #data = list(obj.values('value','old_value','section_id','key'))
                    data = list(obj.values())
                    logger.info('NexgenecEqptConfig return:')
                    return JsonResponse(
                            data
                            ,safe=False
                            )

            else:
                logger.info('NexgenecEqptConfig show list all')
                return JsonResponse(
                        None
                        ,safe=False
                        )

        elif request.method == "POST":
            jsonObj = request.data['data']
            logger.info('NexgenecEqptConfig update object begin:')
            logger.info(jsonObj)
            if jsonObj["seq_id"] is None:
                # new object
                eqpt_id = jsonObj["eqpt_id"]
                section_id = jsonObj["section_id"]
                seq_id = NexgenecEqptConfig.objects.all().aggregate(Max('seq_id'))["seq_id__max"]+1
                order_no = NexgenecEqptConfig.objects.filter(eqpt_id=eqpt_id).filter(section_id=section_id).aggregate(Max('order_no'))["order_no__max"]
                if order_no is None:
                    order_no = 0
                order_no += 1
                obj = NexgenecEqptConfig(seq_id=seq_id, order_no=order_no,updated_by=request.user.user_name, site_id=site_id,operation_id=operation_id)

                for key in model_to_dict(obj):
                    if (key in jsonObj) and (jsonObj[key] is not None):
                        setattr(obj, key, jsonObj[key])
                pass
                logger.info(model_to_dict(obj))
                obj.save()
                #def loggerDb(request,action_desc,action_id='Insert',field_id=0,table_name=''):
                loggerDb(request, model_to_dict(obj), "Create", obj.seq_id, NexgenecEqptConfig.objects.model._meta.db_table)
                logger.info('NexgenecEqptConfig new object after:')
                logger.info(jsonObj)
                return JsonResponse(
                        jsonObj
                        ,safe=False
                        )
            else:
                #Update
                obj = NexgenecEqptConfig.objects.get(seq_id=ID)
                for key in model_to_dict(obj):
                    # if (key in jsonObj) and (jsonObj[key] is not None):
                    #     setattr(obj, key, jsonObj[key])
                    if int(obj.is_modified)==0:
                        obj.is_modified =1
                        obj.old_value = obj.value
                    obj.value = jsonObj['value']
                    obj.key = jsonObj['key']
                    obj.commented = jsonObj['commented']
                pass
                obj.save()
                #def loggerDb(request,action_desc,action_id='Insert',field_id=0,table_name=''):
                loggerDb(request, model_to_dict(obj), "Edit", obj.seq_id, NexgenecEqptConfig.objects.model._meta.db_table)
                logger.info('NexgenecEqptConfig update object after:')
                logger.info(jsonObj)
                return JsonResponse(
                        jsonObj
                        ,safe=False
                        )
        elif request.method == "DELETE":
            logger.info('NexgenecEqptConfig delete object by seq_id:'+str(ID))
            obj = NexgenecEqptConfig.objects.get(seq_id=ID)
            logger.info('NexgenecEqptConfig load object by seq_id')
            print( model_to_dict(obj) )
            logger.info(model_to_dict(obj))
            #def loggerDb(request,action_desc,action_id='Insert',field_id=0,table_name=''):
            loggerDb(request, model_to_dict(obj), "Delete", obj.seq_id, NexgenecEqptConfig.objects.model._meta.db_table)

            # obj.DEL_FLAG='Y'
            obj.delete()

            logger.info("Deleted")
            return JsonResponse(
                    model_to_dict(obj)
                    ,safe=False
                    )
        else:
            logger.info('NexgenecEqptConfig not support method '+request.method)
            return JsonResponse(None, safe=False)
    except Exception:
        logger.error("Fatal error", exc_info=True)

@api_view(['GET', 'POST', 'DELETE', 'PUT'])
def listDataConnection(request, ID=''):
    try:
        site_id = request.GET.get("site_id", "MSO")
        operation_id = request.GET.get("operation_id", "TEST")

        logger.info("Restful NexgenecEqptConnection site_id="+site_id+" operation_id="+operation_id+" request.method="+request.method)

        if request.method == "GET":
            eqpt_id = request.GET.get('eqpt_id')
            # print("eqpt_id ="+eqpt_id)
            if len(str(eqpt_id))>0:
                logger.info('NexgenecEqptConnection search eqpt_id= ['+str(eqpt_id)+" " + str(len(str(eqpt_id)))+"]")
                key = request.GET.get('key')
                logger.info('   search key= ['+str(key)+" " + str(len(str(key)))+"]")
                if key is None or len(key)==0:
                    obj = NexgenecEqptConnection.objects.filter(eqpt_id=eqpt_id)
                else:
                    obj = NexgenecEqptConnection.objects.filter(eqpt_id=eqpt_id).filter(key__icontains=key)
                if obj.values().count()==0:
                    logger.info('NexgenecEqptConnection No setting for eqpt_id= ['+str(eqpt_id))
                    #add default
                    obj = NexgenecEqptConnection.objects.filter(eqpt_id='*')
                    # logger.info(data)
                    seq_id = NexgenecEqptConnection.objects.all().aggregate(Max('seq_id'))["seq_id__max"]+1
                    for item in obj.values():
                        item["seq_id"] = seq_id
                        item["eqpt_id"] = eqpt_id
                        newobj = NexgenecEqptConnection(seq_id=seq_id, eqpt_id=eqpt_id)
                        for key in model_to_dict(newobj):
                            # print("Key="+key)
                            # print(item[key])
                            if item[key] is not None:
                                setattr(newobj, key, item[key])
                        pass

                        seq_id += 1
                        # print("=================================")
                        # print(newobj)
                        newobj.save()
                    
                    #reload again
                    obj = NexgenecEqptConnection.objects.filter(eqpt_id=eqpt_id)
                    data = list(obj.values())
                    logger.info('NexgenecEqptConnection return:')
                    logger.info(data)
                    return JsonResponse(
                            data
                            ,safe=False
                            )
                else:
                    data = list(obj.values())
                    logger.info('NexgenecEqptConnection return:')
                    logger.info(data)
                    return JsonResponse(
                            data
                            ,safe=False
                            )
            else:
                logger.info('NexgenecEqptConnection show list all')
                data = list(NexgenecEqptConnection.objects.values())
                return JsonResponse(
                        data
                        ,safe=False
                        )

        elif request.method == "POST":
            jsonObj = request.data['data']
            logger.info('NexgenecEqptConnection update object begin:')
            logger.info(jsonObj)
            if jsonObj["seq_id"] is None:
                # new object
                seq_id = NexgenecEqptConnection.objects.all().aggregate(Max('seq_id'))["seq_id__max"]+1
                obj = NexgenecEqptConnection(seq_id=seq_id, site_id=site_id,operation_id=operation_id)
                for key in model_to_dict(obj):
                    if jsonObj[key] is not None:
                        setattr(obj, key, jsonObj[key])
                pass
                obj.save()
                #def loggerDb(request,action_desc,action_id='Insert',field_id=0,table_name=''):
                loggerDb(request, model_to_dict(obj), "Create", obj.seq_id, NexgenecEqptConnection.objects.model._meta.db_table)
                logger.info('NexgenecEqptConnection new object after:')
                logger.info(jsonObj)
                return JsonResponse(
                        jsonObj
                        ,safe=False
                        )
            else:
                #Update
                obj = NexgenecEqptConnection.objects.get(seq_id=ID)
                for key in model_to_dict(obj):
                    setattr(obj, key, jsonObj[key])
                pass
                obj.save()
                #def loggerDb(request,action_desc,action_id='Insert',field_id=0,table_name=''):
                loggerDb(request, model_to_dict(obj), "Edit", obj.seq_id, NexgenecEqptConnection.objects.model._meta.db_table)
                logger.info('NexgenecEqptConnection update object after:')
                logger.info(jsonObj)
                return JsonResponse(
                        jsonObj
                        ,safe=False
                        )
        elif request.method == "DELETE":
            logger.info('NexgenecEqptConnection delete object by seq_id:'+str(ID))
            obj = NexgenecEqptConnection.objects.get(seq_id=ID)
            logger.info('NexgenecEqptConnection load object by seq_id')
            print( model_to_dict(obj) )
            logger.info(model_to_dict(obj))
            #def loggerDb(request,action_desc,action_id='Insert',field_id=0,table_name=''):
            loggerDb(request, model_to_dict(obj), "Delete", obj.seq_id, NexgenecEqptConnection.objects.model._meta.db_table)

            # obj.DEL_FLAG='Y'
            obj.delete()

            logger.info("Deleted")
            return JsonResponse(
                    model_to_dict(obj)
                    ,safe=False
                    )
        else:
            logger.info('NexgenecEqptConnection not support method '+request.method)
            return JsonResponse(None, safe=False)
    except Exception:
        logger.error("Fatal error", exc_info=True)

@api_view(['GET', 'POST', 'DELETE', 'PUT'])
def listDataMes(request, ID=''):
    try:
        site_id = request.GET.get("site_id", "MSO")
        operation_id = request.GET.get("operation_id", "TEST")

        logger.info("Restful NexgenecMesSetting site_id="+site_id+" operation_id="+operation_id+" request.method="+request.method)

        if request.method == "GET":
            eqpt_id = request.GET.get('eqpt_id')
            print("eqpt_id ="+eqpt_id)
            if len(str(eqpt_id))>0:
                logger.info('NexgenecMesSetting search eqpt_id= ['+str(eqpt_id)+" " + str(len(str(eqpt_id)))+"]")
                obj = NexgenecMesSetting.objects.filter(eqpt_id=eqpt_id)
                print("COUNT="+str(obj.values().count()))
                # logger.info("data.len="+str(len(data)))
                #logger.info(data.count())
                if obj.values().count()==0:
                    logger.info('NexgenecMesSetting No MES seeting for eqpt_id= ['+str(eqpt_id))
                    #add default
                    obj = NexgenecMesSetting.objects.filter(eqpt_id='*')
                    # logger.info(data)
                    seq_id = NexgenecMesSetting.objects.all().aggregate(Max('seq_id'))["seq_id__max"]+1
                    for item in obj.values():
                        item["seq_id"] = seq_id
                        item["eqpt_id"] = eqpt_id
                        newobj = NexgenecMesSetting(seq_id=seq_id, eqpt_id=eqpt_id)
                        for key in model_to_dict(newobj):
                            # print("Key="+key)
                            # print(item[key])
                            if item[key] is not None:
                                setattr(newobj, key, item[key])
                        pass

                        seq_id += 1
                        # print("=================================")
                        # print(newobj)
                        newobj.save()
                    
                    #reload again
                    obj = NexgenecMesSetting.objects.filter(eqpt_id=eqpt_id)
                    data = list(obj.values())
                    logger.info('NexgenecMesSetting return:')
                    logger.info(data)
                    return JsonResponse(
                            data
                            ,safe=False
                            )
                else:
                    data = list(obj.values())
                    logger.info('NexgenecMesSetting return:')
                    logger.info(data)
                    return JsonResponse(
                            data
                            ,safe=False
                            )
            else:
                logger.info('NexgenecMesSetting show list all')
                data = list(NexgenecMesSetting.objects.values())
                return JsonResponse(
                        data
                        ,safe=False
                        )

        elif request.method == "POST":
            jsonObj = request.data['data']
            logger.info('NexgenecMesSetting update object begin:')
            logger.info(jsonObj)
            if jsonObj["seq_id"] is None:
                # new object
                seq_id = NexgenecMesSetting.objects.all().aggregate(Max('seq_id'))["seq_id__max"]+1
                obj = NexgenecMesSetting(seq_id=seq_id)
                for key in model_to_dict(obj):
                    if jsonObj[key] is not None:
                        setattr(obj, key, jsonObj[key])
                pass
                obj.save()
                #def loggerDb(request,action_desc,action_id='Insert',field_id=0,table_name=''):
                loggerDb(request, model_to_dict(obj), "Create", obj.seq_id, NexgenecMesSetting.objects.model._meta.db_table)
                logger.info('NexgenecMesSetting new object after:')
                logger.info(jsonObj)
                return JsonResponse(
                        jsonObj
                        ,safe=False
                        )
            else:
                #Update
                obj = NexgenecMesSetting.objects.get(seq_id=ID)
                for key in model_to_dict(obj):
                    if key in jsonObj:
                        setattr(obj, key, jsonObj[key])
                pass
                obj.save()
                #def loggerDb(request,action_desc,action_id='Insert',field_id=0,table_name=''):
                loggerDb(request, model_to_dict(obj), "Edit", obj.seq_id, NexgenecMesSetting.objects.model._meta.db_table)
                logger.info('NexgenecMesSetting update object after:')
                logger.info(jsonObj)
                return JsonResponse(
                        jsonObj
                        ,safe=False
                        )
        elif request.method == "DELETE":
            logger.info('NexgenecMesSetting delete object by seq_id:'+str(ID))
            obj = NexgenecMesSetting.objects.get(seq_id=ID)
            logger.info('NexgenecMesSetting load object by seq_id')
            print( model_to_dict(obj) )
            logger.info(model_to_dict(obj))
            #def loggerDb(request,action_desc,action_id='Insert',field_id=0,table_name=''):
            loggerDb(request, model_to_dict(obj), "Delete", obj.seq_id, NexgenecMesSetting.objects.model._meta.db_table)

            # obj.DEL_FLAG='Y'
            obj.delete()

            logger.info("Deleted")
            return JsonResponse(
                    model_to_dict(obj)
                    ,safe=False
                    )
        else:
            logger.info('NexgenecMesSetting not support method '+request.method)
            return JsonResponse(None, safe=False)
    except Exception:
        logger.error("Fatal error", exc_info=True)