from django.apps import AppConfig


class NexgenecEqptLoggerConfig(AppConfig):
    name = 'nexgenec_eqpt_logger'
