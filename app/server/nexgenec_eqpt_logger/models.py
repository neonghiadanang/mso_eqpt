from django.db import models
from django.utils.timezone import localtime, now


class NexgenecEqptLogger(models.Model):
    seq_id = models.FloatField(primary_key=True, null=True)
    eqpt_id = models.CharField(max_length=50, blank=True, null=True)
    section_id = models.CharField(max_length=30, blank=True, null=True)
    commented = models.CharField(max_length=10, blank=True, null=True)
    key = models.CharField(max_length=30, blank=True, null=True)
    value = models.CharField(max_length=256, blank=True, null=True)
    order_no = models.BigIntegerField(blank=True, null=True)
    updated_by = models.CharField(max_length=30, blank=True, null=True)
    timestamp = models.DateField(blank=True, null=True,default=now)

    class Meta:
        managed = False
        db_table = 'NEXGENEC_EQPT_LOGGER'
        app_label = 'app.NEXGENEC_EQPT_LOGGER'                        