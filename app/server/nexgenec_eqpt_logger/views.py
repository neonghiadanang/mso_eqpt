from django.db import connection
from django.shortcuts import render
from rest_framework import serializers
from django.http import HttpResponse, JsonResponse
from rest_framework.decorators import api_view
from django.views.decorators.csrf import csrf_exempt
import json as simplejson
from django.core import serializers
from django.forms.models import model_to_dict
from django.db.models import Avg, Max, Min, Sum
from .models import NexgenecEqptLogger
from app.common import *
from ..nexgenec_action_log.views import loggerDb

import logging
logger = logging.getLogger("logit")

@api_view(['GET', 'POST', 'DELETE', 'PUT'])
def listData(request, ID=''):
    try:
        site_id = request.GET.get("site_id", "MSO")
        operation_id = request.GET.get("operation_id", "TEST")
        eqpt_id = request.GET.get('eqpt_id')
        logger.info("Restful NexgenecEqptLogger site_id="+site_id+" operation_id="+operation_id+" request.method="+request.method)

        if request.method == "GET":
            # print("eqpt_id ="+eqpt_id)
            if len(str(eqpt_id))>0:
                logger.info('NexgenecEqptLogger search eqpt_id= ['+str(eqpt_id)+" " + str(len(str(eqpt_id)))+"]")
                obj = NexgenecEqptLogger.objects.filter(eqpt_id=eqpt_id)
                data = list(obj.values())
                if data.count==0:
                    objnew = NexgenecEqptLogger(eqpt_id=eqpt_id)
                    objnew.save()
                    #Create new
                    obj = NexgenecEqptLogger.objects.filter(eqpt_id=eqpt_id)
                    data = list(obj.values())

                logger.info('NexgenecEqptLogger return:')
                logger.info(data)
                return JsonResponse(
                        data
                        ,safe=False
                        )

        elif request.method == "POST":
            jsonObj = request.data['data']
            logger.info('NexgenecEqptLogger update object begin:')
            logger.info(jsonObj)
            if jsonObj["seq_id"] is None:
                # new object
                seq_id = NexgenecEqptLogger.objects.all().aggregate(Max('seq_id'))["seq_id__max"]
                print("seq_id=")
                print(seq_id)
                if seq_id == None:
                    seq_id=1
                else:
                    seq_id+=1

                order_no = NexgenecEqptLogger.objects.filter(eqpt_id=eqpt_id).aggregate(Max('order_no'))["order_no__max"]
                if order_no == None:
                    order_no=1
                else:
                    order_no+=1
                obj = NexgenecEqptLogger(seq_id=seq_id, order_no=order_no)
                setattr(obj, "eqpt_id", jsonObj["eqpt_id"])
                setattr(obj, "section_id", jsonObj["section_id"])
                setattr(obj, "key", jsonObj["key"])
                setattr(obj, "value", jsonObj["value"])
                setattr(obj, "commented", jsonObj["commented"])
                # for key in model_to_dict(obj):
                #     if jsonObj[key] is not None:
                #         setattr(obj, key, jsonObj[key])
                # pass
                obj.save()
                #def loggerDb(request,action_desc,action_id='Insert',field_id=0,table_name=''):
                loggerDb(request, model_to_dict(obj), "Create", obj.seq_id, NexgenecEqptLogger.objects.model._meta.db_table)
                logger.info('NexgenecEqptLogger new object after:')
                logger.info(jsonObj)
                return JsonResponse(
                        jsonObj
                        ,safe=False
                        )
            else:
                #Update
                obj = NexgenecEqptLogger.objects.get(seq_id=ID)
                for key in model_to_dict(obj):
                    setattr(obj, key, jsonObj[key])
                pass
                obj.save()
                #def loggerDb(request,action_desc,action_id='Insert',field_id=0,table_name=''):
                loggerDb(request, model_to_dict(obj), "Edit", obj.seq_id, NexgenecEqptLogger.objects.model._meta.db_table)
                logger.info('NexgenecEqptLogger update object after:')
                logger.info(jsonObj)
                return JsonResponse(
                        jsonObj
                        ,safe=False
                        )
        elif request.method == "DELETE":
            logger.info('NexgenecEqptLogger delete object by seq_id:'+str(ID))
            obj = NexgenecEqptLogger.objects.get(seq_id=ID)
            logger.info('NexgenecEqptLogger load object by seq_id')
            print( model_to_dict(obj) )
            logger.info(model_to_dict(obj))
            #def loggerDb(request,action_desc,action_id='Insert',field_id=0,table_name=''):
            loggerDb(request, model_to_dict(obj), "Delete", obj.seq_id, NexgenecEqptLogger.objects.model._meta.db_table)

            # obj.DEL_FLAG='Y'
            obj.delete()

            logger.info("Deleted")
            return JsonResponse(
                    model_to_dict(obj)
                    ,safe=False
                    )
        else:
            logger.info('NexgenecEqptLogger not support method '+request.method)
            return JsonResponse(None, safe=False)
    except Exception:
        logger.error("Fatal error", exc_info=True)