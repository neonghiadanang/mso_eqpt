from django.apps import AppConfig


class NexgenecEqptStatelistConfig(AppConfig):
    name = 'nexgenec_eqpt_statelist'
