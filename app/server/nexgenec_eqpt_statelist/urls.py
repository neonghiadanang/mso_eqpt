from django.urls import path, re_path
from django.conf.urls import url, include
from .views import listData

urlpatterns = [
    path(r'', listData),
    re_path(r'^(?P<ID>[-\w]+)/$', listData),
]
