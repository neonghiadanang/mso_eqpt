from django.db import connection
from django.shortcuts import render
from rest_framework import serializers
from django.http import HttpResponse, JsonResponse
from rest_framework.decorators import api_view
from django.views.decorators.csrf import csrf_exempt
import json as simplejson
from django.core import serializers
from django.forms.models import model_to_dict
from django.db.models import Avg, Max, Min, Sum
from django.utils.timezone import localtime, now
from .models import NexgenecEqptState
from app.common import *
from ..nexgenec_action_log.views import loggerDb

import logging
logger = logging.getLogger("logit")

@api_view(['GET', 'POST', 'DELETE', 'PUT'])
def listData(request, ID=''):
    try:
        logger.info("Restful NexgenecEqptState request.method="+request.method)

        if request.method == "GET":
            logger.info('NexgenecEqptState show list all')
            name = request.GET.get('name')
            logger.info('   search name= ['+str(name)+" " + str(len(str(name)))+"]")
            if name is None or len(name)==0:
                obj = NexgenecEqptState.objects
            else:
                obj = NexgenecEqptState.objects.filter(state_name__icontains=name)

            data = list(obj.values())
            return JsonResponse(
                    data
                    ,safe=False
                    )

        elif request.method == "POST":
            jsonObj = request.data['data']
            logger.info('NexgenecEqptState update object begin:')
            logger.info(jsonObj)
            if jsonObj["seq_id"] is None:
                # new object
                seq_id = NexgenecEqptState.objects.all().aggregate(Max('seq_id'))["seq_id__max"]+1
                obj = NexgenecEqptState(seq_id=seq_id, updated_by=request.user.user_name, 
                                                # timestamp=now, 
                                                state_desc=jsonObj['state_desc'],
                                                state_name=jsonObj['state_name'])
                # for key in model_to_dict(obj):
                #     if jsonObj[key] is not None:
                #         setattr(obj, key, jsonObj[key])
                # pass
                logger.info(model_to_dict(obj))
                obj.save()
                loggerDb(request, model_to_dict(obj), "Create", obj.seq_id, NexgenecEqptState.objects.model._meta.db_table)
                logger.info('NexgenecEqptState new object after:')
                logger.info(jsonObj)
                return JsonResponse(
                        jsonObj
                        ,safe=False
                        )
            else:
                #Update
                obj = NexgenecEqptState.objects.get(seq_id=ID)
                obj.state_name=jsonObj['state_name']
                obj.state_desc=jsonObj['state_desc']

                # for key in model_to_dict(obj):
                #     setattr(obj, key, jsonObj[key])
                # pass
                obj.save()
                #def loggerDb(request,action_desc,action_id='Insert',field_id=0,table_name=''):
                loggerDb(request, model_to_dict(obj), "Edit", obj.seq_id, NexgenecEqptState.objects.model._meta.db_table)
                logger.info('NexgenecEqptState update object after:')
                logger.info(jsonObj)
                return JsonResponse(
                        jsonObj
                        ,safe=False
                        )
        elif request.method == "DELETE":
            logger.info('NexgenecEqptState delete object by seq_id:'+str(ID))
            obj = NexgenecEqptState.objects.get(seq_id=ID)
            logger.info('NexgenecEqptState load object by seq_id')
            print( model_to_dict(obj) )
            logger.info(model_to_dict(obj))
            #def loggerDb(request,action_desc,action_id='Insert',field_id=0,table_name=''):
            loggerDb(request, model_to_dict(obj), "Delete", obj.seq_id, NexgenecEqptState.objects.model._meta.db_table)

            # obj.DEL_FLAG='Y'
            obj.delete()

            logger.info("Deleted")
            return JsonResponse(
                    model_to_dict(obj)
                    ,safe=False
                    )
        else:
            logger.info('NexgenecEqptState not support method '+request.method)
            return JsonResponse(None, safe=False)
    except Exception:
        logger.error("Fatal error", exc_info=True)