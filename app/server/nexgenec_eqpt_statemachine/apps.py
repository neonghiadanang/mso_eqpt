from django.apps import AppConfig


class NexgenecEqptStatemachineConfig(AppConfig):
    name = 'nexgenec_eqpt_statemachine'
