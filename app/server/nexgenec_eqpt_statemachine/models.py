from django.db import models
from django.utils.timezone import localtime, now
import base64

class NexgenecEqptStatemachine(models.Model):
    seq_id = models.FloatField(primary_key=True)
    eqpt_id = models.CharField(max_length=50, blank=True, null=True)
    statemachine_id = models.CharField(max_length=50, blank=True, null=True)
    order_no = models.BigAutoField(blank=True, null=True)
    commented = models.CharField(max_length=10, blank=True, null=True)
    current_state = models.CharField(max_length=50, blank=True, null=True)
    trigger_action = models.CharField(max_length=50, blank=True, null=True)
    action_macro = models.CharField(max_length=50, blank=True, null=True)
    pass_state = models.CharField(max_length=50, blank=True, null=True)
    failed_state = models.CharField(max_length=50, blank=True, null=True)
    guard_cond = models.CharField(max_length=50, blank=True, null=True)
    group_state = models.CharField(max_length=50, blank=True, null=True)
    guard_value = models.CharField(max_length=256, blank=True, null=True)
    updated_by = models.CharField(max_length=30, blank=True, null=True)
    timestamp = models.DateField(blank=True, null=True, default=now)

    class Meta:
        managed = True
        db_table = 'nexgenec_eqpt_statemachine'
        app_label = 'nexgenec_eqpt_statemachine'


class NexgenecDiagramLayout(models.Model):
    layout_key = models.CharField(primary_key=True, max_length=200)
    eqpt_id = models.CharField(max_length=50)
    statemachine_id = models.CharField(max_length=50)
    update_time = models.DateField(blank=True, default=now)
    update_by = models.CharField(max_length=20, blank=True, null=True)

    # _data = models.TextField(
    #         db_column='layout_json',
    #         blank=True)
    # def set_data(self, data):
    #     self._data = base64.encodestring(data)
    # def get_data(self):
    #     return base64.decodestring(self._data)
    # data = property(get_data, set_data)
    layout_json = models.TextField(db_column='layout_json', blank=True)
    # def set_data(self, data):
    #     self.layout_json = base64.encodestring(data)
    # def get_data(self):
    #     return base64.decodestring(self.layout_json)
    # data = property(get_data, set_data)

    # _layout_json = models.TextField(
    #         db_column='layout_json',
    #         blank=True)
    # def set_layout_json(self, layout_json):
    #     self._layout_json = base64.encodestring(layout_json)
    # def get_layout_json(self):
    #     return base64.decodestring(self._layout_json)
    # layout_json = property(get_layout_json, set_layout_json)

    class Meta:
        managed = False
        db_table = 'NEXGENEC_DIAGRAM_LAYOUT'
        app_label = 'NEXGENEC_DIAGRAM_LAYOUT'
