from django.urls import path, re_path
from django.conf.urls import url, include
from .views import *

urlpatterns = [
    # url(r'^statemachine/(?P<eqpt_id>[\w-]+)/(?P<statemachine_id>[a-zA-Z0-9_\.]+)/$', stateMachine),
    # url(r'^statemachine/(?P<eqpt_id>[\w-]+)/$', stateMachine),
    url(r'^statemachine/', stateMachine),
    path(r'', listData),
    re_path(r'^(?P<ID>[-\w]+)/$', listData),
]
