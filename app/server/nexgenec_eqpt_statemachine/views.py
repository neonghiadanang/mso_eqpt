from django.db import connection
from django.shortcuts import render
from rest_framework import serializers
from django.http import HttpResponse, JsonResponse
from rest_framework.decorators import api_view
from django.views.decorators.csrf import csrf_exempt
import json as simplejson
from django.core import serializers
from django.forms.models import model_to_dict
from .models import NexgenecEqptStatemachine, NexgenecDiagramLayout
from app.common import *
from django.contrib.sessions.models import Session
from django.contrib.auth.models import User
from django.utils.timezone import localtime, now
from ..nexgenec_action_log.views import loggerDb
from django.core.exceptions import ObjectDoesNotExist


import logging
logger = logging.getLogger("logit")

ERROR_COLOR = "yellow"
NORMAL_COLOR = "green"
TRANS_NORMAL_COLOR = "blue"
TRANS_ERROR_COLOR = "red"

@api_view(['GET', 'POST', 'DELETE'])
def listData(request, ID=''):
    try:
        site_id = request.GET['site_id']
        operation_id = request.GET['operation_id']
        logger.info("Restful NexgenecEqptStatemachine site_id="+site_id+" operation_id="+operation_id+" request.method="+request.method)
        if request.method == "GET":
            if isNum(ID)==True and int(ID)>0:
                logger.info('NexgenecEqptStatemachine Find obj by ID='+str(ID))
                obj = NexgenecEqptStatemachine.objects.filter(seq_id=ID).values()
                data = []
                if len(obj)==0:
                    logger.info('NexgenecEqptStatemachine Find return no thing')
                    data = []
                else:
                    logger.info('NexgenecEqptStatemachine return 1 object:')
                    data = list(obj)
                    logger.info(data)
                return JsonResponse(
                        data
                        ,safe=False
                        )
            elif len(str(ID))>0:
                logger.info('NexgenecEqptStatemachine Find obj by String= ['+str(ID)+" " + str(len(str(ID)))+"]")
                obj = NexgenecEqptStatemachine.objects.filter(eqpt_id__contains=ID).values()
                data = list(obj)
                logger.info('NexgenecEqptStatemachine return:')
                logger.info(data)
                return JsonResponse(
                        data
                        ,safe=False
                        )
            else:
                logger.info('NexgenecEqptStatemachine show list all')
                data = list(NexgenecEqptStatemachine.objects.values())
                return JsonResponse(
                        data
                        ,safe=False
                        )

        elif request.method == "POST":
            jsonObj = request.data['data']
            logger.info('NexgenecEqptStatemachine update object begin:')
            logger.info(jsonObj)
            obj = NexgenecEqptStatemachine.objects.get(seq_id=jsonObj['ID'])
            for key in model_to_dict(obj):
                setattr(obj, key, jsonObj[key])
            pass
            obj.save()
            logger.info('NexgenecEqptStatemachine update object after:')
            logger.info(jsonObj)
            return JsonResponse(
                    jsonObj
                    ,safe=False
                    )
        elif request.method == "DELETE":
            logger.info('NexgenecEqptStatemachine delete object by ID:'+str(ID))
            obj = NexgenecEqptStatemachine.objects.get(eqpt_id=ID)
            logger.info('NexgenecEqptStatemachine load object by ID')
            logger.info(list(obj))
            obj.DEL_FLAG='Y'
            obj.save()
            return JsonResponse(
                    list(obj)
                    ,safe=False
                    )
        else:
            logger.info('NexgenecEqptStatemachine not support method '+request.method)
            return JsonResponse(None, safe=False)
    except Exception:
        logger.error("Fatal error", exc_info=True)    

@api_view(['GET', 'POST'])
def stateMachine(request, eqpt_id=None, statemachine_id=None):
    if request.method == "GET":
        return stateMachineGetInfo(request, eqpt_id, statemachine_id)
    elif request.method == "POST":
        return stateMachineSaveInfo(request, eqpt_id, statemachine_id)
    else:
        logger.info('NexgenecEqptStatemachine not support method '+request.method)
        return JsonResponse(None, safe=False)

def stateMachineSaveInfo(request, eqpt_id=None, statemachine_id=None):
    try:
        #Ladder&view_type=simple
        eqpt_id = request.GET.get('eqpt_id',None)
        statemachine_id = request.GET.get('statemachine_id',None)
        view_mode = request.GET.get('view_mode','ladder')
        view_type = request.GET.get('view_type','good')
        view_function = request.GET.get('view_function','simple')
        view_version = request.GET.get('view_version','1.0')
        layout_json = request.data['layout_json']
        #print(layout_json)
        layout_key = eqpt_id +"_"+statemachine_id +"_"+view_mode +"_"+view_type +"_"+view_function +"_"+view_version
        logger.info('stateMachineSaveInfo Find obj by String= ['+str(layout_key)+" " + str(len(str(layout_key)))+"]")
        #obj = NexgenecDiagramLayout.objects.filter(eqpt_id=eqpt_id).filter(statemachine_id=statemachine_id).get(layout_key=layout_key)
        try:
            obj = NexgenecDiagramLayout.objects.get(layout_key=layout_key)

            obj.layout_json = layout_json
            obj.update_by = request.user.user_name
            obj.update_time = now()
            obj.save()
            
            loggerDb(request, layout_key, "Update Layout", 0, "NEXGENEC_DIAGRAM_LAYOUT")
            logger.info('stateMachineSaveInfo Update layout= ['+str(layout_key)+" " + str(len(str(layout_key)))+"]")

            return JsonResponse(
                    {
                        "msg":"Update done"
                    }
                    ,safe=False
                )

        except ObjectDoesNotExist:
            # Nothing, do import
            objnew = NexgenecDiagramLayout(eqpt_id=eqpt_id, statemachine_id=statemachine_id,layout_key=layout_key,update_by = request.user.user_name,layout_json = layout_json)
            objnew.save()
            loggerDb(request, layout_key, "Create Layout", 0, "NEXGENEC_DIAGRAM_LAYOUT")
            logger.info('stateMachineSaveInfo Create new layout= ['+str(layout_key)+" " + str(len(str(layout_key)))+"]")
            return JsonResponse(
                    {
                        "msg":"Create new done"
                    }
                    ,safe=False
                )
    except Exception:
        logger.error("Fatal error", exc_info=True)

def stateMachineGetInfo(request, eqpt_id=None, statemachine_id=None):
    try:
        #Ladder&view_type=simple
        eqpt_id = request.GET.get('eqpt_id',None)
        statemachine_id = request.GET.get('statemachine_id',None)
        view_mode = request.GET.get('view_mode','ladder')
        view_type = request.GET.get('view_type','good')
        view_function = request.GET.get('view_function','simple')
        view_line = request.GET.get('view_line','no')
        view_version = request.GET.get('view_version','1.0')
        if statemachine_id is None or len(statemachine_id)==0:
            #return list statemachine_id
            obj = NexgenecEqptStatemachine.objects.filter(eqpt_id=eqpt_id).values('statemachine_id').distinct().order_by('statemachine_id')
            # print(obj)
            dataListStateMachineId = list(obj)
            if len(dataListStateMachineId)==0:
                return JsonResponse(
                    {
                        "list":[], #[{"statemachine_id":"No Thing"}],
                        "statemachine_id":"No Thing",
                        "model":None
                    }
                    ,safe=False
                )
            statemachine_id = dataListStateMachineId[0]["statemachine_id"]
        

        layout_key = eqpt_id +"_"+statemachine_id +"_"+view_mode +"_"+view_type +"_"+view_function +"_"+view_version
        logger.info('stateMachineGetInfo Find obj by key= ['+str(layout_key)+" " + str(len(str(layout_key)))+"]")
        logger.info('stateMachineGetInfo Find obj by eqpt_id=['+str(eqpt_id)+" " + str(len(str(eqpt_id)))+"]")
        logger.info('statemachine_id=['+str(statemachine_id)+" " + str(len(str(statemachine_id)))+"]")
        
        # if statemachine_id is None or len(statemachine_id)==0:
        #     #return list statemachine_id
        #     obj = NexgenecEqptStatemachine.objects.filter(eqpt_id=eqpt_id).values('statemachine_id').distinct().order_by('statemachine_id')
        #     # print(obj)
        #     dataListStateMachineId = list(obj)
        #     if len(dataListStateMachineId)==0:
        #         return JsonResponse(
        #             {
        #                 "list":[], #[{"statemachine_id":"No Thing"}],
        #                 "statemachine_id":"No Thing",
        #                 "model":None
        #             }
        #             ,safe=False
        #         )
        #     statemachine_id = dataListStateMachineId[0]["statemachine_id"]

        #     #load diagram
        #     jsonModel = None
        #     obj = NexgenecEqptStatemachine.objects.filter(eqpt_id=eqpt_id).filter(statemachine_id=statemachine_id).order_by('-order_no').values()
        #     data = list(obj)
        #     if view_line=='no':
        #         #try to load from layout first
        #         obj = NexgenecDiagramLayout.objects.filter(eqpt_id=eqpt_id).filter(statemachine_id=statemachine_id).filter(layout_key=layout_key)
        #         if len(obj)==0:
        #             model = data
        #         else:
        #             #load model from layout
        #             model = None
        #             jsonModel = obj.layout_json
        #     else:
        #         model = parseModel(data, view_type, view_mode, view_function)
        #     return JsonResponse(
        #             {
        #                 "list":dataListStateMachineId,
        #                 "statemachine_id":statemachine_id,
        #                 "model":model,
        #                 "jsonModel":jsonModel
        #             }
        #             ,safe=False
        #         )
        # else:
        listDiagram = NexgenecEqptStatemachine.objects.filter(eqpt_id=eqpt_id).values('statemachine_id').distinct().order_by('statemachine_id')
        #load diagram
        obj = NexgenecEqptStatemachine.objects.filter(eqpt_id=eqpt_id).filter(statemachine_id=statemachine_id).order_by('-order_no').values()
        data = list(obj)
        jsonModel = None
        if view_line=='no':
            #try to load from layout first
            obj = NexgenecDiagramLayout.objects.filter(eqpt_id=eqpt_id).filter(statemachine_id=statemachine_id).filter(layout_key=layout_key)
            if len(obj)==0:
                model = data
            else:
                #load model from layout
                # print(obj.values()[0])
                model = None
                jsonModel = obj.values()[0]["layout_json"]
        else:
            model = parseModel(data, view_type, view_mode, view_function)
        return JsonResponse(
                {
                    "list":list(listDiagram),
                    "statemachine_id":statemachine_id,
                    "model":model,
                    "jsonModel":jsonModel
                }
                ,safe=False
                )
    except Exception:
        logger.error("Fatal error", exc_info=True)

def parseModel(data,view_type,view_mode,view_function):
    view_mode = view_mode.lower()
    if view_mode=='zizzag':
        return parseModel_Zigzag(data, view_type, view_function)
    return parseModel_Ladder(data, view_type, view_function)

def parseModel_Zigzag(data, view_type, view_function):
    #Object value
    mapState = {}
    mapStateId = {}
    mapStateOKId = {}
    mapStateFailId = {}

    # Codination
    X = 300
    Y = 0
    YError = 0
    nodeIdNormal = 1
    nodeIdError = 1
    nodeId = 1
    mapCorX = [900,1200,1500]
    mapCorXError = [0,300,600]
    for item in data:
        # main state
        stateName = item["current_state"]
        # print( str(nodeId) + " " + stateName)
        # if stateName not in mapState:
        if stateName.upper().find("ERROR")>-1 or stateName.upper().find("REWORK")>-1:
            #error node
            X = mapCorXError[nodeIdError%3]
            if stateName not in mapState:
                nodeIdError += 1
        else:
            X = mapCorX[nodeIdNormal%3]
            if stateName not in mapState:
                nodeIdNormal += 1
        if X>=600:
            Y += 100
            if(YError==0):
                YError = Y
            state = {
                "Name": stateName,
                "Trigger": item["trigger_action"],
                "Function": item["action_macro"],
                "OK": item["pass_state"],
                "Fail": item["failed_state"],
                "GuardCond": item["guard_cond"],
                "GuardVal": item["guard_value"],
                "X": X,
                "Y": Y,
                "nodeId": str(nodeId),
                "color":NORMAL_COLOR
            }
        else:
            YError += 100
            state = {
                "Name": stateName,
                "Trigger": item["trigger_action"],
                "Function": item["action_macro"],
                "OK": item["pass_state"],
                "Fail": item["failed_state"],
                "GuardCond": item["guard_cond"],
                "GuardVal": item["guard_value"],
                "X": X,
                "Y": YError,
                "nodeId": str(nodeId),
                "color":ERROR_COLOR
            }
        
        if stateName not in mapState:
            mapState[stateName] = state
            mapStateId[nodeId] = state
            nodeId += 1
            # print("nodeId= " + str(nodeId)+" stateName="+stateName)
        nameOK = "GOOD "+stateName+"->"+item["pass_state"]+" Trig="+item["trigger_action"]+" Func="+item["action_macro"]
        nameFail = "FAIL "+stateName+"->"+item["pass_state"]+" Trig="+item["trigger_action"]+" Func="+item["action_macro"]
        if view_function=="simple":
            nameOK = "GOOD "+stateName+"->"+item["pass_state"]+" Func="+item["action_macro"]
            nameFail = "FAIL "+stateName+"->"+item["pass_state"]+" Func="+item["action_macro"]

        mapStateOKId[nameOK] = {
            "from":stateName,
            "to":item["pass_state"],
            "Trigger": item["trigger_action"],
            "Function": item["action_macro"],
            "GuardCond": item["guard_cond"],
            "GuardVal": item["guard_value"],
        }
        mapStateFailId[nameFail] = {
            "from":stateName,
            "to":item["failed_state"],
            "Trigger": item["trigger_action"],
            "Function": item["action_macro"],
            "GuardCond": item["guard_cond"],
            "GuardVal": item["guard_value"],
        }

        pass
    # print(mapState)
    for item in data:
        # main state
        stateName = item["pass_state"]
        if stateName not in mapState:
            if stateName.upper().find("ERROR")>-1 or stateName.upper().find("REWORK")>-1:
                #error node
                X = mapCorXError[nodeIdError%3]
                nodeIdError += 1
            else:
                X = mapCorX[nodeIdNormal%3]
                nodeIdNormal += 1
            if X>=600:
                Y += 100
                if(YError==0):
                    YError = Y
                state = {
                    "Name": stateName,
                    "Trigger": "",
                    "Function": "",
                    "OK": "",
                    "Fail": "",
                    "X": X,
                    "Y": Y,
                    "nodeId": str(nodeId)
                    ,"color":NORMAL_COLOR
                }
            else:
                YError += 100
                state = {
                    "Name": stateName,
                    "Trigger": "",
                    "Function": "",
                    "OK": "",
                    "Fail": "",
                    "X": X,
                    "Y": Y,
                    "nodeId": str(nodeId)
                    ,"color":ERROR_COLOR
                }
            
            mapState[stateName] = state
            mapStateId[nodeId] = state
            nodeId += 1

        # main state
        stateName = item["failed_state"]
        if stateName not in mapState:
            if stateName.upper().find("ERROR")>-1 or stateName.upper().find("REWORK")>-1:
                #error node
                X = mapCorXError[nodeIdError%3]
                nodeIdError += 1
            else:
                X = mapCorX[nodeIdNormal%3]
                nodeIdNormal += 1
            if X>=600:
                Y += 100
                if(YError==0):
                    YError = Y
                state = {
                    "Name": stateName,
                    "Trigger": "",
                    "Function": "",
                    "OK": "",
                    "Fail": "",
                    "X": X,
                    "Y": Y,
                    "nodeId": str(nodeId)
                    ,"color":NORMAL_COLOR
                }
            else:
                YError += 100
                state = {
                    "Name": stateName,
                    "Trigger": "",
                    "Function": "",
                    "OK": "",
                    "Fail": "",
                    "X": X,
                    "Y": Y,
                    "nodeId": str(nodeId)
                    ,"color":ERROR_COLOR
                }
            
            mapState[stateName] = state
            mapStateId[nodeId] = state
            nodeId += 1
        pass
    
    return buildJson(data, view_type, view_function, mapState, mapStateId, mapStateOKId,  mapStateFailId)

def parseModel_Ladder(data, view_type, view_function):
    #Object value
    mapState = {}
    mapStateId = {}
    mapStateOKId = {}
    mapStateFailId = {}

    # Codination
    X = 300
    Y = 0
    YError = 0
    nodeIdNormal = 1
    nodeIdError = 1
    nodeId = 1
    # mapCorX = [900,1200,1500]
    # mapCorXError = [0,300,600]
    for item in data:
        # main state
        stateName = item["current_state"]
        # print( str(nodeId) + " " + stateName +" nodeIdError="+str(nodeIdError)+" nodeIdNormal="+str(nodeIdNormal))
        #if stateName not in mapState:
        if stateName.upper().find("ERROR")>-1 or stateName.upper().find("REWORK")>-1:
            #error node
            if stateName not in mapState:
                nodeIdError += 1
            if nodeIdError%20>=10:
                X = 600-100*( 20-(nodeIdError%20) )
            elif nodeIdError%20<10:
                X = 600-100*(nodeIdError%20);
        else:
            if nodeIdNormal%20<=10:
                X = 900+100*(nodeIdNormal%20)
            elif nodeIdNormal%20>10:
                X = 900+100*( 20-(nodeIdNormal%20) )
            else:
                X = 900+100*( 20-(nodeIdNormal%20) )
            if stateName not in mapState:
                nodeIdNormal += 1
        if X>=600:
            Y += 100
            if(YError==0):
                YError = Y
            state = {
                    "Name": stateName,
                    "Trigger": item["trigger_action"],
                    "Function": item["action_macro"],
                    "OK": item["pass_state"],
                    "Fail": item["failed_state"],
                    "GuardCond": item["guard_cond"],
                    "GuardVal": item["guard_value"],
                    "X": X,
                    "Y": Y,
                    "nodeId": str(nodeId)
                    ,"color":NORMAL_COLOR
                }
        else:
            YError += 100
            state = {
                "Name": stateName,
                "Trigger": item["trigger_action"],
                "Function": item["action_macro"],
                "OK": item["pass_state"],
                "Fail": item["failed_state"],
                "GuardCond": item["guard_cond"],
                "GuardVal": item["guard_value"],
                "X": X,
                "Y": YError,
                "nodeId": str(nodeId)
                ,"color":ERROR_COLOR
            }

        # print("----------------> add: "+stateName)
        if stateName not in mapState:
            mapState[stateName] = state
            mapStateId[nodeId] = state
            nodeId += 1

        nameOK = "GOOD "+stateName+"->"+item["pass_state"]+" Trig="+item["trigger_action"]+" Func="+item["action_macro"]
        nameFail = "FAIL "+stateName+"->"+item["pass_state"]+" Trig="+item["trigger_action"]+" Func="+item["action_macro"]
        if view_function=="simple":
            nameOK = "GOOD "+stateName+"->"+item["pass_state"]+" Func="+item["action_macro"]
            nameFail = "FAIL "+stateName+"->"+item["pass_state"]+" Func="+item["action_macro"]

        mapStateOKId[nameOK] = {
            "from":stateName,
            "to":item["pass_state"],
            "Trigger": item["trigger_action"],
            "Function": item["action_macro"],
            "GuardCond": item["guard_cond"],
            "GuardVal": item["guard_value"],
        }
        mapStateFailId[nameFail] = {
            "from":stateName,
            "to":item["failed_state"],
            "Trigger": item["trigger_action"],
            "Function": item["action_macro"],
            "GuardCond": item["guard_cond"],
            "GuardVal": item["guard_value"],
        }
        pass
    # print("===================")
    # print(mapStateOKId)
    # print("===================")
    # print(mapStateFailId)
    # print("===================")
    # print(mapState)
    for item in data:
        # main state
        stateName = item["pass_state"]
        if stateName not in mapState:
            if stateName.upper().find("ERROR")>-1 or stateName.upper().find("REWORK")>-1:
                #error node
                nodeIdError += 1
                if nodeIdError%20>=10:
                    X = 600-100*( 20-(nodeIdError%20) )
                elif nodeIdError%20<10:
                    X = 600-100*(nodeIdError%20);
            else:
                if nodeIdNormal%20<=10:
                    X = 900+100*(nodeIdNormal%20)
                elif nodeIdNormal%20>10:
                    X = 900+100*( 20-(nodeIdNormal%20) );
                else:
                    X = 900+100*( 20-(nodeIdNormal%20) );
                nodeIdNormal += 1
            
            if X>=600:
                Y += 100
                if(YError==0):
                    YError = Y
                state = {
                    "Name": stateName,
                    "Trigger": item["trigger_action"],
                    "Function": item["action_macro"],
                    "GuardCond": item["guard_cond"],
                    "GuardVal": item["guard_value"],
                    "OK": item["pass_state"],
                    "Fail": item["failed_state"],
                    "X": X,
                    "Y": Y,
                    "nodeId": str(nodeId),
                    "color":NORMAL_COLOR
                }
            else:
                YError += 100
                state = {
                    "Name": stateName,
                    "Trigger": item["trigger_action"],
                    "Function": item["action_macro"],
                    "OK": item["pass_state"],
                    "Fail": item["failed_state"],
                    "GuardCond": item["guard_cond"],
                    "GuardVal": item["guard_value"],
                    "X": X,
                    "Y": Y,
                    "nodeId": str(nodeId),
                    "color":ERROR_COLOR
                }
            if stateName not in mapState:
                print("----- Add node 1"+stateName)
                mapState[stateName] = state
                mapStateId[nodeId] = state
                nodeId += 1

        # main state
        stateName = item["failed_state"]
        if stateName not in mapState:
            if stateName.upper().find("ERROR")>-1 or stateName.upper().find("REWORK")>-1:
                #error node
                nodeIdError += 1
                if nodeIdError%20>=10:
                    X = 600-100*( 20-(nodeIdError%20) )
                elif nodeIdError%20<10:
                    X = 600-100*(nodeIdError%20);
            else:
                if nodeIdNormal%20<=10:
                    X = 900+100*(nodeIdNormal%20)
                elif nodeIdNormal%20>10:
                    X = 900+100*( 20-(nodeIdNormal%20) );
                else:
                    X = 900+100*( 20-(nodeIdNormal%20) );
                nodeIdNormal += 1
           
            if X>=600:
                Y += 100
                if(YError==0):
                    YError = Y
                state = {
                    "Name": stateName,
                    "Trigger": item["trigger_action"],
                    "Function": item["action_macro"],
                    "OK": item["pass_state"],
                    "Fail": item["failed_state"],
                    "X": X,
                    "Y": Y,
                    "nodeId": str(nodeId),
                    "color":NORMAL_COLOR
                }
            else:
                YError += 100
                state = {
                    "Name": stateName,
                    "Trigger": item["trigger_action"],
                    "Function": item["action_macro"],
                    "OK": item["pass_state"],
                    "Fail": item["failed_state"],
                    "X": X,
                    "Y": Y,
                    "nodeId": str(nodeId),
                    "color":ERROR_COLOR

                }
            if stateName not in mapState:
                print("----- Add node 2"+stateName)
                mapState[stateName] = state
                mapStateId[nodeId] = state
                nodeId += 1
            # mapState[stateName] = state
            # mapStateId[nodeId] = state
            # nodeId += 1
        pass
    
    return buildJson(data, view_type, view_function, mapState, mapStateId, mapStateOKId,  mapStateFailId)

def buildJson(data, view_type,view_function, mapState, mapStateId, mapStateOKId,  mapStateFailId):
    #second loop
    nodeDataArray = []
    for item in mapState.values():
        #{"key":"1", "loc":"1000 0", "text":"s_Initialize", "category":"Start", "color": "#7CFC00"},
        if item["Name"].upper()=="S_INITIALIZE":
            node = {
                "key": item["Name"]
                ,"loc": str(item["X"])+" "+str(item["Y"])
                ,"text": item["Name"]
                ,"category":"Start"
                ,"color": item["color"]
            }
        elif item["Name"].upper()=="A_NONE":
            node = {
                "key": item["Name"]
                ,"loc": str(item["X"])+" "+str(item["Y"])
                ,"text": item["Name"]
                ,"category":"End"
                ,"color": item["color"]
            }
        else:
            node = {
                "key": item["Name"]
                ,"loc": str(item["X"])+" "+str(item["Y"])
                ,"text": item["Name"]
                ,"color": item["color"]
            }
        nodeDataArray.append(node)
    pass
    listLinkDataArray = []
    # dictLinkArray = {}
    if view_type=='good' or view_type=='all':
        for item in mapStateOKId.values():
            # print(item)
            text_str = item["Trigger"] +"\n"+ item["Function"]
            if item["Trigger"].upper().find("T_")==-1:
                if view_function!="simple":
                    text_str = item["Trigger"]
                else:
                    text_str = item["Function"]
            node = {
                "from": item["from"]
                ,"to": item["to"]
                ,"text": text_str
                ,"Function": item["Function"]
                ,"Trigger": item["Trigger"]
                ,"isPass": "PASS"
                ,"color": TRANS_NORMAL_COLOR
                ,"name": item["from"] +" -> "+item["to"]
            }
            listLinkDataArray.append(node)
            # dictLinkArray[item["name"]] = node
            # print( "add link Good: " + item["name"])
        pass
    if view_type=='fail' or view_type=='all':
        for item in mapStateFailId.values():
            text_str = item["Trigger"] +"\n"+ item["Function"]
            if item["Trigger"].upper().find("T_")==-1:
                if view_function!="simple":
                    text_str = item["Trigger"]
                else:
                    text_str = item["Function"]

            node = {
                "from": item["from"]
                ,"to": item["to"]
                ,"text": text_str
                ,"Function": item["Function"]
                ,"Trigger": item["Trigger"]
                ,"isPass": "FAIL"
                ,"color": TRANS_ERROR_COLOR
                ,"name": item["from"] +" --> "+ item["to"]

            }
            listLinkDataArray.append(node)
            # dictLinkArray[item["name"]] = node
            # print( "add link FAIL: " + item["name"])
        pass
    # linkDataArray = []
    # print( "listLinkDataArray count="+str(len(listLinkDataArray)))
    # print( "dictLinkArray count="+str(len(dictLinkArray.items())))
    # if view_function=='simple':
    #     for key, value in dictLinkArray.items():
    #         linkDataArray.append(value)
    #     # linkDataArray = dictLinkArray.items()
    # else:
    #     linkDataArray = listLinkDataArray
    model = {
        "class": "GraphLinksModel",
        "nodeKeyProperty": "key",
        "nodeDataArray": nodeDataArray,
        "linkDataArray": listLinkDataArray
    }
    return model
