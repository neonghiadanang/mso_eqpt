from django.apps import AppConfig


class NexgenecEqptUploadSettingConfig(AppConfig):
    name = 'nexgenec_eqpt_upload_setting'
