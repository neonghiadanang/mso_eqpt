from django.db import models
from django.utils.timezone import localtime, now
import base64

class NexgenecEqptUploadSetting(models.Model):
    eqpt_id = models.CharField(primary_key=True, max_length=20)
    conn_type = models.CharField(default="SSH",max_length=20, blank=True, null=True)
    conn_ip = models.CharField(max_length=20, blank=True, null=True,default="localhost")
    conn_port = models.CharField(max_length=20, blank=True, null=True,default="22")
    conn_username = models.CharField(max_length=50, blank=True, null=True,default="")
    conn_password= models.CharField(max_length=50, blank=True, null=True,default="")
    conn_folder = models.CharField(max_length=20, blank=True, null=True,default="/")
    updated_by = models.CharField(max_length=20, blank=True, null=True)
    updated_time = models.DateTimeField(blank=True, null=True,default=now)

    class Meta:
        managed = False
        db_table = 'NEXGENEC_EQPT_UPLOAD_SETTING'
        app_label = 'app.NEXGENEC_EQPT_UPLOAD_SETTING'

class NexgenecSourceCode(models.Model):
    seq_id = models.FloatField(primary_key=True)
    eqpt_id = models.CharField(max_length=20, blank=True, null=True)
    src_desc = models.CharField(max_length=20)
    src_version = models.FloatField(blank=True, null=True)
    # src_bin = models.BinaryField(db_column='src_bin', blank=True, null=True)
    #src_zip = models.FileField()
    # _data = models.CharField(
    #         db_column='src_zip',
    #         blank=True)

    # def set_data(self, data):
    #     # self._data = data
    #     #self._data = base64.b64encode(data)
    #     self._data = base64.encodestring(data)

    # def get_data(self):
    #     #return self._data
    #     return base64.b64decode(self._data)
    #     #return base64.decodestring(self._data)

    # src_zip = property(get_data, set_data)

    src_comment = models.CharField(max_length=200)
    uploaded_time = models.DateTimeField(blank=True, default=now)
    uploaded_by = models.CharField(max_length=20, blank=True, null=True)
    approved_time = models.DateTimeField(blank=True, null=True)
    approved_by = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'NEXGENEC_SOURCE_CODE'
        app_label = 'app.NEXGENEC_SOURCE_CODE'        