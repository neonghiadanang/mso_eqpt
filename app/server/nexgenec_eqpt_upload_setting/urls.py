from django.urls import path, re_path
from django.conf.urls import url, include
from .views import *

urlpatterns = [
    # path(r'', listData),
    # re_path(r'^(?P<ID>[-\w]+)/$', listData),
    path(r'conn/', listData),
    re_path(r'^conn/(?P<ID>[-\w]+)/$', listData),  
    path(r'code/', listDataSourceCode),
    re_path(r'^code/(?P<ID>[-\w]+)/$', listDataSourceCode),  
    re_path(r'^download/(?P<ID>[-\w]+)/$', download),  
    re_path(r'^approve/(?P<ID>[-\w]+)/$', approve),  
]
