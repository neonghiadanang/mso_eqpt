from django.db import connection
from django.shortcuts import render
from rest_framework import serializers
from django.http import HttpResponse, JsonResponse
from rest_framework.decorators import api_view
from django.views.decorators.csrf import csrf_exempt
import json as simplejson
from django.core import serializers
from django.forms.models import model_to_dict
from django.db.models import Avg, Max, Min, Sum
from .models import NexgenecEqptUploadSetting, NexgenecSourceCode
from app.common import *
from ..nexgenec_action_log.views import loggerDb
from app.settings import BASE_DIR, UPLOAD_FOLDER
from django.core.files.storage import FileSystemStorage
from django.utils.encoding import smart_str
from django.utils.timezone import localtime, now

# import datetime

import json
import base64

import paramiko
import os
import sys
import subprocess


import logging
logger = logging.getLogger("logit")

@api_view(['GET'])
def download(request, ID):
    logger.info('Download ID: ['+ID+"]")
    obj = NexgenecSourceCode.objects.get(seq_id=ID)
    localfolder = UPLOAD_FOLDER
    file_name = str(int(obj.seq_id)) +"_"+obj.src_desc
    upload_filename = BASE_DIR+"/"+localfolder+file_name
    logger.info('Source file: ['+upload_filename+"]")
    if os.path.exists(upload_filename):
        logger.info("The file ["+upload_filename +"] exist. Continue to download...")
    else:
        logger.info("The file ["+upload_filename +"] does not exist to download")
        return None    
    path_to_file = upload_filename
    response = HttpResponse(open(upload_filename, 'rb').read(), content_type='application/force-download')
    response['Content-Disposition'] = 'attachment; filename=%s' % obj.src_desc
    logger.info("The file ["+upload_filename +"] start to download")
    return response

@api_view(['GET'])
def approve(request, ID):
    logger.info('Approve ID: ['+ID+"]")
    obj = NexgenecSourceCode.objects.get(seq_id=ID)
    obj.approved_by = request.user.user_name
    obj.approved_time = now()
    obj.save()
    return JsonResponse(
                        "Approved"
                        ,safe=False
                        )

@api_view(['GET', 'POST', 'DELETE', 'PUT'])
def listData(request, ID=''):
    try:
        site_id = request.GET.get("site_id", "MSO")
        operation_id = request.GET.get("operation_id", "TEST")
        logger.info("Restful NexgenecEqptUploadSetting site_id="+site_id+" operation_id="+operation_id+" request.method="+request.method)

        if request.method == "GET":
            eqpt_id = request.GET.get('eqpt_id')
            action = request.GET.get('action','')
            # print("eqpt_id ="+eqpt_id)
            if len(str(eqpt_id))>0:
                if action=="deploy":
                    logger.info('NexgenecEqptUploadSetting deploy data to eqpt_id= ['+str(eqpt_id)+" " + str(len(str(eqpt_id)))+"]")
                    # Do deploy
                    data = deploy(request, eqpt_id)
                    return JsonResponse(
                            data
                            ,safe=False
                            )
                else:
                    logger.info('NexgenecEqptUploadSetting search eqpt_id= ['+str(eqpt_id)+" " + str(len(str(eqpt_id)))+"]")
                    obj = NexgenecEqptUploadSetting.objects.filter(eqpt_id=eqpt_id)
                    data = list(obj.values())
                    if len(data)==0:
                        objnew = NexgenecEqptUploadSetting(eqpt_id=eqpt_id)
                        objnew.save()
                        loggerDb(request, model_to_dict(objnew), "Create New", 0, NexgenecEqptUploadSetting.objects.model._meta.db_table)
                        #Create new
                        obj = NexgenecEqptUploadSetting.objects.filter(eqpt_id=eqpt_id)
                        data = list(obj.values())
                    logger.info('NexgenecEqptUploadSetting return:')
                    logger.info(data)
                    return JsonResponse(
                            data
                            ,safe=False
                            )

        elif request.method == "POST":
            jsonObj = request.data['data']
            logger.info('NexgenecEqptUploadSetting update object begin:')
            logger.info(jsonObj)
            eqpt_id = jsonObj["eqpt_id"]
            #Update
            obj = NexgenecEqptUploadSetting.objects.get(eqpt_id=eqpt_id)
            for key in model_to_dict(obj):
                setattr(obj, key, jsonObj[key])
            pass
            obj.save()
            #def loggerDb(request,action_desc,action_id='Insert',field_id=0,table_name=''):
            loggerDb(request, model_to_dict(obj), "Edit ", 0, NexgenecEqptUploadSetting.objects.model._meta.db_table)
            logger.info('NexgenecEqptUploadSetting update object after:')
            logger.info(jsonObj)
            return JsonResponse(
                    jsonObj
                    ,safe=False
                    )
        elif request.method == "DELETE":
            logger.info("Deleted No support")
            return JsonResponse(
                    []
                    ,safe=False
                    )
        else:
            logger.info('NexgenecEqptUploadSetting not support method '+request.method)
            return JsonResponse(None, safe=False)
    except Exception:
        logger.error("Fatal error", exc_info=True)

@api_view(['GET', 'POST', 'DELETE', 'PUT'])
def listDataSourceCode(request, ID=0):
    try:
        site_id = request.GET.get("site_id", "MSO")
        operation_id = request.GET.get("operation_id", "TEST")
        logger.info("Restful NexgenecSourceCode site_id="+site_id+" operation_id="+operation_id+" request.method="+request.method)

        if request.method == "GET":
            eqpt_id = request.GET.get('eqpt_id')
            print("eqpt_id ="+eqpt_id)
            if len(str(eqpt_id))>0:
                logger.info('NexgenecSourceCode search eqpt_id= ['+str(eqpt_id)+" " + str(len(str(eqpt_id)))+"]")
                obj = NexgenecSourceCode.objects.filter(eqpt_id=eqpt_id)
                data = list(obj.values("seq_id", "eqpt_id", "approved_by", "approved_time", "src_comment", "src_desc", "src_version", "uploaded_by", "uploaded_time"))
                logger.info('NexgenecSourceCode return:')
                logger.info(data)
                return JsonResponse(
                        data
                        ,safe=False
                        )
            else:
                return JsonResponse(
                        None
                        ,safe=False
                        )
        elif request.method == "POST":

            #print("ID="+str(ID))
            file = request.FILES.get('file')
            if int(ID)>0:
                #print("111111111")
                if file==None:
                    #print("1111111112222222222")
                    print(request.data)
                    jsonObj = request.data
                else:
                    #print("11111111133333333")
                    jsonObj = json.loads( request.POST['data'] )
            else:
                #print("2222222222222")
                if file==None:
                    return JsonResponse(
                            'No file'
                            ,safe=False
                            )
                jsonObj = json.loads( request.POST['data'] )

            seq_id = jsonObj["seq_id"]
            if seq_id==None:
                #New upload file
                logger.info('NexgenecSourceCode create new object begin:')
                logger.info(jsonObj)
                eqpt_id = jsonObj["eqpt_id"]
                
                seq_id = NexgenecSourceCode.objects.all().aggregate(Max('seq_id'))["seq_id__max"]
                if seq_id is None:
                    seq_id=1
                else:
                    seq_id+=1
                #Update
                obj = NexgenecSourceCode(seq_id=seq_id, eqpt_id=eqpt_id)
                obj.src_desc = file.name
                obj.src_version = 1.0
                obj.src_comment = jsonObj["src_comment"]
                obj.uploaded_by = request.user.user_name
                # src_zip = file.read()
                # obj.src_bin = bytes(file.read())
                obj.save()
                #Save file if exist
                if file!=None:
                    fs = FileSystemStorage()
                    localfolder = UPLOAD_FOLDER
                    upload_filename = BASE_DIR+"/"+localfolder+str(int(seq_id)) +"_"+file.name
                    logger.info('upload_filename: ['+upload_filename+"]")
                    filename = fs.save(upload_filename, file)
                    logger.info('After upload: ['+filename+"]")

                loggerDb(request, "File "+file.name, "Create new", 0, NexgenecSourceCode.objects.model._meta.db_table)
                logger.info('NexgenecSourceCode create new object after:')
                logger.info(jsonObj)
                return JsonResponse(
                        jsonObj
                        ,safe=False
                        )
            else:
                #Update file
                logger.info('NexgenecSourceCode update object begin:')
                logger.info(jsonObj)
                eqpt_id = jsonObj["eqpt_id"]
                seq_id = jsonObj["seq_id"]
                obj = NexgenecSourceCode.objects.get(seq_id=seq_id)
                if file!=None:
                    #delete the old file
                    localfolder = UPLOAD_FOLDER
                    upload_filename = BASE_DIR+"/"+localfolder+str(int(seq_id)) +"_"+obj.src_desc
                    logger.info('Delete if exist file: ['+upload_filename+"]")
                    if os.path.exists(upload_filename):
                        os.remove(upload_filename)
                        logger.info("The file ["+upload_filename +"] had been deleted")
                    else:
                        logger.info("The file ["+upload_filename +"] does not exist")

                    #Save file if exist
                    fs = FileSystemStorage()
                    upload_filename = BASE_DIR+"/"+localfolder+str(seq_id) +"_"+file.name
                    logger.info('Upload new upload_filename: ['+upload_filename+"]")
                    filename = fs.save(upload_filename, file)
                    logger.info('After upload: ['+filename+"]")
                    obj.src_desc = file.name
                    obj.approved_by = ''

                obj.src_comment = jsonObj["src_comment"]
                obj.uploaded_by = request.user.user_name
                obj.save()

                loggerDb(request, "File "+obj.src_desc, "Update", 0, NexgenecSourceCode.objects.model._meta.db_table)
                logger.info('NexgenecSourceCode create new object after:')
                logger.info(jsonObj)
                return JsonResponse(
                        jsonObj
                        ,safe=False
                        )
        elif request.method == "DELETE":
            logger.info("Deleted No support")
            print("ID="+str(ID))
            obj = NexgenecSourceCode.objects.get(seq_id=ID)
            obj.delete()
            return JsonResponse(
                    []
                    ,safe=False
                    )
        else:
            logger.info('NexgenecSourceCode not support method '+request.method)
            return JsonResponse(None, safe=False)
    except Exception:
        logger.error("Fatal error", exc_info=True)



def deploy(request, eqpt_id):
    message = "Begin \n"
    logger.info('NexgenecEqptUploadSetting deploy begin. eqpt_id='+eqpt_id)
    try:
        # localfolder = UPLOAD_FOLDER
        # logger.info('BASE_DIR: ['+BASE_DIR+"]")
        # filezipname = "temp.zip"
        # logger.info('filezipname: ['+filezipname+"]")
        # localfile = BASE_DIR+"/"+localfolder+filezipname
        # logger.info('NexgenecEqptUploadSetting localfile='+localfile)
        seq_id = NexgenecSourceCode.objects.filter(eqpt_id=eqpt_id).extra(where=["LENGTH(approved_by) >0"]).aggregate(Max('seq_id'))["seq_id__max"]
        print(seq_id)
        if seq_id == None:
            logger.info('NexgenecEqptUploadSetting deploy end. eqpt_id='+eqpt_id+" seq_id="+str(seq_id))
            message = "Could not found source file to deploy"
            return message
        else:
            logger.info('NexgenecEqptUploadSetting seq_id='+str(seq_id))

        logger.info('NexgenecEqptUploadSetting load obj seq_id='+str(seq_id))
        obj = NexgenecSourceCode.objects.get(seq_id=seq_id)
        localfolder = UPLOAD_FOLDER
        upload_filename = BASE_DIR+"/"+localfolder+str(int(obj.seq_id)) +"_"+obj.src_desc
        logger.info('Source file: ['+upload_filename+"]")
        if os.path.exists(upload_filename):
            logger.info("The file ["+upload_filename +"] exist. Continue to deploy...")
        else:
            logger.info("The file ["+upload_filename +"] does not exist")
            message = "The file ["+upload_filename +"] does not exist. Could not found source file to deploy"
            return message

        obj = NexgenecEqptUploadSetting.objects.get(eqpt_id=eqpt_id)
        logger.info('NexgenecEqptUploadSetting deploy end. conn_ip='+obj.conn_ip)
        logger.info('NexgenecEqptUploadSetting deploy end. conn_port='+obj.conn_port)
        
        client = paramiko.SSHClient()
        client.load_system_host_keys()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        #client.load_system_host_keys()
        #client.load_host_keys('~/.ssh/known_hosts')
        #client.set_missing_host_key_policy(AutoAddPolicy())
        hostport = obj.conn_ip+":"+obj.conn_port
        username = obj.conn_username
        password = obj.conn_password
        logger.info('Connect to:'+hostport)
        logger.info('Username:'+username)
        logger.info('PAssword:'+password)
        message += "Connect.. "+hostport+"\n"
        client.connect(obj.conn_ip, username=username, password=password)
        sftp_client=client.open_sftp()
        conn_folder=obj.conn_folder
        message += "Connected.. \n"
        logger.info('conn_folder:'+conn_folder)

        filezipname = "temp.zip"
        localfile = upload_filename
        remotefile = conn_folder+"/"+filezipname
        logger.info('localfile: ['+localfile+"]")
        logger.info('remotefile: ['+remotefile+"]")
        message += "Begin copy: "+localfile+" ->"+remotefile+ "\n"
        sftp_client.put(localfile, remotefile)
        logger.info('Copy done: ['+remotefile+"]. Do unzip remote")
        message += "Begin copy done.: "+filezipname+ "\n"
        command = 'cd '+conn_folder+";"
        command += 'unzip '+filezipname+";"
        command += 'unlink '+filezipname
        logger.info('command unzip: ['+command+"]")
        message += "Begin send command: "+command+ "\n"
        stdin, stdout, stderr = client.exec_command(command)

        sftp_client.close()

        logger.info('sent command to host...wait read line')
        console = stdout.readlines()
        logger.info("Sdt out")
        logger.info(console)
        if( len(console)>0):
            message += "Unzip command done "+ "\n"
            logger.info(console)
        console = stderr.readlines()
        logger.info("Sdt error:")
        logger.info(console)
        if( len(console)>0):
            message += "Unzip command error "+ "\n"+str(console)+"\n"
            logger.info(console)
            raise Exception('Error to deploy to host')
        
        logger.info('Close remote host. eqpt_id='+eqpt_id)
        client.close()

        logger.info('NexgenecEqptUploadSetting deploy successful. eqpt_id='+eqpt_id)
        loggerDb(request, "Success\n"+message, "Deploy", 0, "")
        message = "Deploy to equipment ["+eqpt_id+"] successful"
    except Exception as e:
        print(str(e))
        print(e)
        logger.info('NexgenecEqptUploadSetting deploy fail. eqpt_id='+eqpt_id)
        loggerDb(request, "Error\n"+message, "Deploy", 0, "")
        message += "\n Error to deploy." + str(e)
        pass
    logger.info('NexgenecEqptUploadSetting deploy end. eqpt_id='+eqpt_id)
    return message