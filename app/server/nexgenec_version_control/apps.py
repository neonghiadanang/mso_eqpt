from django.apps import AppConfig


class NexgenecVersionControlConfig(AppConfig):
    name = 'nexgenec_version_control'
