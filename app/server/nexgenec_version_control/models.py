from django.db import models
from django.utils.timezone import localtime, now


class NexgenecVersionControl(models.Model):
    seq_id = models.FloatField(primary_key=True)
    site_id = models.CharField(max_length=20, blank=True, null=True)
    operation_id = models.CharField(max_length=20, blank=True, null=True)
    eqpt_id = models.CharField(max_length=20, blank=True, null=True)
    version = models.FloatField(blank=True, null=True)
    requested_by = models.CharField(max_length=20, blank=True, null=True)
    requested_time = models.DateField(blank=True, null=True)
    requested_comment = models.CharField(max_length=256, blank=True, null=True)
    state = models.CharField(max_length=20, blank=True, null=True)
    data_type = models.CharField(max_length=20, blank=True, null=True)
    updated_by = models.CharField(max_length=20, blank=True, null=True)
    updated_time = models.DateField(blank=True, null=True)
    updated_comment = models.CharField(max_length=256, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'NEXGENEC_VERSION_CONTROL'
        app_label = 'app.NEXGENEC_VERSION_CONTROL'