from django.db import connection
from django.shortcuts import render
from rest_framework import serializers
from django.http import HttpResponse, JsonResponse
from rest_framework.decorators import api_view
from django.views.decorators.csrf import csrf_exempt
import json as simplejson
from django.core import serializers
from django.forms.models import model_to_dict
from django.db.models import Avg, Max, Min, Sum
from django.utils.timezone import localtime, now
from .models import NexgenecVersionControl
from app.common import *
from ..nexgenec_eqpt_config.models import NexgenecEqptConfig
from ..nexgenec_action_log.views import loggerDb

import logging
logger = logging.getLogger("logit")

@api_view(['GET', 'POST', 'DELETE', 'PUT'])
def listData(request, ID=''):
    try:
        site_id = request.GET.get("site_id", "MSO")
        operation_id = request.GET.get("operation_id", "TEST")
        logger.info("Restful NexgenecVersionControl site_id="+site_id+" operation_id="+operation_id+" request.method="+request.method)
        action = request.GET.get("action", "")
        if request.method == "GET":
            if len(action)>0:
                #Update approve action
                seq_id = request.GET.get("seq_id", "")
                commented = request.GET.get("commented", "")
                version = request.GET.get("version", "")
                eqpt_id = request.GET.get("eqpt_id", "")
                next_state = action
                logger.info("version="+version+" eqpt_id="+eqpt_id)

                obj = NexgenecVersionControl.objects.get(seq_id=seq_id)
                obj.updated_by = request.user.user_name
                obj.updated_time = now()
                obj.updated_comment = commented
                obj.state = next_state
                obj.save()
                loggerDb(request, model_to_dict(obj), "Approved", obj.seq_id, NexgenecVersionControl.objects.model._meta.db_table)
                logger.info('NexgenecVersionControl update object after approved:')
                logger.info(model_to_dict(obj))

                logger.info('NexgenecVersionControl update equipment config all approved record:')
                objconfig = NexgenecEqptConfig.objects.filter(eqpt_id=eqpt_id).filter(version=version).filter(operation_id=operation_id).filter(site_id=site_id)
                for item in objconfig:
                    item.state = next_state
                    item.save()

                #def loggerDb(request,action_desc,action_id='Insert',field_id=0,table_name=''):
                return JsonResponse(
                        model_to_dict(obj)
                        ,safe=False
                        )

            else:
                logger.info('NexgenecVersionControl show list all')
                state = request.GET.get('state')
                logger.info('   search state= ['+str(state)+" " + str(len(str(state)))+"]")
                if state is None or len(state)==0:
                    obj = NexgenecVersionControl.objects
                else:
                    obj = NexgenecVersionControl.objects.filter(state=state)

                data = list(obj.values())
                return JsonResponse(
                        data
                        ,safe=False
                        )

        elif request.method == "POST":
            jsonObj = request.data['data']
            logger.info('NexgenecVersionControl update object begin:')
            logger.info(jsonObj)
            if jsonObj["seq_id"] is None:
                # new object
                seq_id = NexgenecVersionControl.objects.all().aggregate(Max('seq_id'))["seq_id__max"]+1
                obj = NexgenecVersionControl(seq_id=seq_id, updated_by=request.user.user_name,
                    action_name=jsonObj['action_name'],
                    action_desc=jsonObj['action_desc']
                    )
                # for key in model_to_dict(obj):
                #     if jsonObj[key] is not None:
                #         setattr(obj, key, jsonObj[key])
                # pass
                obj.save()
                loggerDb(request, model_to_dict(obj), "Create", obj.seq_id, NexgenecVersionControl.objects.model._meta.db_table)
                logger.info('NexgenecVersionControl new object after:')
                logger.info(jsonObj)
                return JsonResponse(
                        jsonObj
                        ,safe=False
                        )
            else:
                #Update
                obj = NexgenecVersionControl.objects.get(seq_id=ID)
                # for key in model_to_dict(obj):
                    # setattr(obj, key, jsonObj[key])
                # pass
                obj.action_name=jsonObj['action_name']
                obj.action_desc=jsonObj['action_desc']
                obj.save()
                #def loggerDb(request,action_desc,action_id='Insert',field_id=0,table_name=''):
                loggerDb(request, model_to_dict(obj), "Edit", obj.seq_id, NexgenecVersionControl.objects.model._meta.db_table)
                logger.info('NexgenecVersionControl update object after:')
                logger.info(jsonObj)
                return JsonResponse(
                        jsonObj
                        ,safe=False
                        )
        elif request.method == "DELETE":
            logger.info('NexgenecVersionControl delete object by seq_id:'+str(ID))
            obj = NexgenecVersionControl.objects.get(seq_id=ID)
            logger.info('NexgenecVersionControl load object by seq_id')
            print( model_to_dict(obj) )
            logger.info(model_to_dict(obj))
            #def loggerDb(request,action_desc,action_id='Insert',field_id=0,table_name=''):
            loggerDb(request, model_to_dict(obj), "Delete", obj.seq_id, NexgenecVersionControl.objects.model._meta.db_table)

            # obj.DEL_FLAG='Y'
            obj.delete()

            logger.info("Deleted")
            return JsonResponse(
                    model_to_dict(obj)
                    ,safe=False
                    )
        else:
            logger.info('NexgenecVersionControl not support method '+request.method)
            return JsonResponse(None, safe=False)
    except Exception:
        logger.error("Fatal error", exc_info=True)