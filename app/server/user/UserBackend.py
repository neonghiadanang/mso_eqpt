from django.contrib.auth.backends import ModelBackend
from django.contrib.auth.models import User

from .models import NexgenecUser
from django.core.exceptions import ObjectDoesNotExist

import logging
logger = logging.getLogger("logit")

class UserBackend(ModelBackend):
    def get_by_natural_key(self, username):
        logger.info('Call me UserBackend get_by_natural_key')
        return self.get(username=username)

    def authenticate(self, request, **kwargs):
        logger.info('Call me UserBackend authenticate')
        username = kwargs['username']
        password = kwargs['password']

        logger.info("username = "+username)
        logger.info("password = "+password)
        try:
            logger.info("Start load table...by username="+username)
            #objUser = NexgenecUser.objects.filter(user_name__iexact=username.lower())[1].get()
            objUser = NexgenecUser.objects.get(user_name__iexact=username.lower())
            logger.info(objUser)
            # if customer.user.check_password(password) is True:
            return objUser
        except ObjectDoesNotExist:
            pass

    def get_user(self, user_name):
        logger.info('Call me UserBackend get_user: user_name=' + user_name)
        try:
            return NexgenecUser.objects.get(user_name=username)
        except ObjectDoesNotExist:
            logger.info('UserBackend get_user: Not Exist: user_name=' + user_name)
            return None