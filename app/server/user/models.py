from django.db import models
from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.utils.translation import gettext_lazy as _
from django.utils import timezone

# from .managers import CustomUserManager
from django.contrib.auth.base_user import BaseUserManager
from django.utils.translation import ugettext_lazy as _

class CustomUserManager(BaseUserManager):
    def get_by_natural_key(self, username):
        # logger.info('Call me CustomUserManager get_by_natural_key ==========================')
        return self.get(username=username)
class NexgenecUsergroup(models.Model):
    seq_id = models.FloatField(primary_key=True, null=False)
    group_name = models.CharField(max_length=50, blank=True, null=True)
    group_desc = models.CharField(max_length=60, blank=True, null=True)
    updated_by = models.CharField(max_length=30, blank=True, null=True)
    timestamp = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'NEXGENEC_USERGROUP'
        app_label = 'NEXGENEC_USERGROUP'

class NexgenecUser(AbstractBaseUser, PermissionsMixin):
    seq_id = models.FloatField(primary_key=True)
    user_name = models.CharField(max_length=30, blank=True, null=True)
    full_name = models.CharField(max_length=70, blank=True, null=True)
    updated_by = models.CharField(max_length=30, blank=True, null=True)
    timestamp = models.DateField(blank=True, null=True)
    is_staff = models.CharField(max_length=1, blank=True, null=True)
    is_superuser = models.CharField(max_length=1, blank=True, null=True)
    is_active = models.CharField(max_length=1, blank=True, null=True)
    site_id = models.CharField(max_length=20, blank=True, null=True)
    operation_id = models.CharField(max_length=20, blank=True, null=True)
    groups = models.ManyToManyField(NexgenecUsergroup, verbose_name=_("NexgenecUsergroups"))

    objects = CustomUserManager()
    USERNAME_FIELD = 'user_name'
    USERID_FIELD = 'seq_id'
    REQUIRED_FIELDS = ['user_name']

    class Meta:
        managed = False
        db_table = 'NEXGENEC_USER'
        app_label = 'NexgenecUser'


