from django.shortcuts import render
from rest_framework_jwt.authentication import BaseJSONWebTokenAuthentication
from django.contrib.auth import get_user_model
from django.utils.encoding import smart_text
from django.utils.translation import ugettext as _
from rest_framework import exceptions
from rest_framework.authentication import (
    BaseAuthentication, get_authorization_header
)

from rest_framework_jwt.settings import api_settings
from .models import NexgenecUser, NexgenecUsergroup

jwt_decode_handler = api_settings.JWT_DECODE_HANDLER
jwt_get_username_from_payload = api_settings.JWT_PAYLOAD_GET_USERNAME_HANDLER

from django.db import connection
from django.shortcuts import render
from rest_framework import serializers
from django.http import HttpResponse, JsonResponse
from rest_framework.decorators import api_view
import json as simplejson
from django.core import serializers
from django.forms.models import model_to_dict
from django.db.models import Avg, Max, Min, Sum
from django.utils.timezone import localtime, now

from app.common import *
from ..nexgenec_action_log.views import loggerDb

import logging
logger = logging.getLogger("logit")

@api_view(['GET', 'POST', 'DELETE', 'PUT'])
def listData(request, ID=''):
    try:
        logger.info("Restful NexgenecUser request.method="+request.method)

        if request.method == "GET":
            if len(str(ID))>0:
                logger.info('NexgenecUser show list all')
                user_name = request.GET.get('username')
                logger.info('   search user_name= ['+str(user_name)+" " + str(len(str(user_name)))+"]")

                if ID is None or len(ID)==0:
                    obj = NexgenecUser.objects
                else:
                    if user_name is None or len(user_name)==0:
                        obj = NexgenecUser.objects
                    else:
                        obj = NexgenecUser.objects.filter(user_name__icontains=user_name)

                data = list(obj.values())
                logger.info('NexgenecUser return:')
                logger.info(data)

            else:
                logger.info('NexgenecUser show list all')
                user_name = request.GET.get('username')
                logger.info('   search user_name= ['+str(user_name)+" " + str(len(str(user_name)))+"]")

                if user_name is None or len(user_name)==0:
                    obj = NexgenecUser.objects
                else:
                    obj = NexgenecUser.objects.filter(user_name__icontains=user_name)
                data = list(obj.values())
                logger.info('NexgenecUser return:')
                logger.info(data)
                logger.info('NexgenecUser')

            return JsonResponse(
                    data
                    ,safe=False
                    )

        elif request.method == "POST":
            jsonObj = request.data['data']
            logger.info('NexgenecUser update object begin:')
            logger.info(jsonObj)
            if jsonObj["seq_id"] is None:
                # new object
                seq_id = NexgenecUser.objects.all().aggregate(Max('seq_id'))["seq_id__max"]+1
                # print(seq_id)
                # print(seq_id)
                obj = NexgenecUser(seq_id=seq_id, user_name=jsonObj['user_name'], full_name=jsonObj['full_name'], updated_by=request.user.user_name, is_staff='Y', is_superuser=jsonObj['is_superuser'], is_active=jsonObj['is_active'] )
                # print(obj)

                # for key in model_to_dict(obj):
                #     if jsonObj[key] is not None:
                #         setattr(obj, key, jsonObj[key])
                # pass
                obj.save()
                loggerDb(request, "Add new user "+jsonObj['user_name'], "Create", obj.seq_id, 'NEXGENEC_USER')
                logger.info('NexgenecUser new object after:')
                logger.info(jsonObj)
                return JsonResponse(
                        jsonObj
                        ,safe=False
                        )
            else:
                #Update
                obj = NexgenecUser.objects.get(seq_id=ID)
                #obj.user_name = jsonObj["user_name"]
                obj.full_name = jsonObj["full_name"]
                obj.is_active = jsonObj["is_active"]
                obj.is_superuser = jsonObj["is_superuser"]
                # for key in model_to_dict(obj):
                #     setattr(obj, key, jsonObj[key])
                # pass
                obj.save()
                #def loggerDb(request,action_desc,action_id='Insert',field_id=0,table_name=''):
                loggerDb(request, "Update "+obj.user_name, "Edit", obj.seq_id, NexgenecUser.objects.model._meta.db_table)
                logger.info('NexgenecUser update object after:')
                logger.info(jsonObj)
                return JsonResponse(
                        jsonObj
                        ,safe=False
                        )
        elif request.method == "DELETE":
            logger.info('NexgenecUser delete object by seq_id:'+str(ID))
            obj = NexgenecUser.objects.get(seq_id=ID)
            logger.info('NexgenecUser load object by seq_id')
            # print( model_to_dict(obj) )
            # logger.info(model_to_dict(obj))
            #def loggerDb(request,action_desc,action_id='Insert',field_id=0,table_name=''):
            loggerDb(request, "Delete "+obj.user_name, "Delete", obj.seq_id, NexgenecUser.objects.model._meta.db_table)

            # obj.DEL_FLAG='Y'
            obj.delete()

            logger.info("Deleted")
            return JsonResponse(
                    "Delete Done"
                    ,safe=False
                    )
        else:
            logger.info('NexgenecUser not support method '+request.method)
            return JsonResponse(None, safe=False)
    except Exception:
        logger.error("Fatal error", exc_info=True)



@api_view(['GET', 'POST', 'DELETE', 'PUT'])
def listDataGroup(request, ID=''):
    try:
        logger.info("Restful NexgenecUsergroup request.method="+request.method)

        if request.method == "GET":
            if len(str(ID))>0:
                logger.info('NexgenecUsergroup show list all')
                group_name = request.GET.get('group_name')
                logger.info('   search group_name= ['+str(group_name)+" " + str(len(str(group_name)))+"]")

                if ID is None or len(ID)==0:
                    obj = NexgenecUsergroup.objects
                else:
                    if group_name is None or len(group_name)==0:
                        obj = NexgenecUsergroup.objects
                    else:
                        obj = NexgenecUsergroup.objects.filter(group_name__icontains=group_name)

                data = list(obj.values())
                logger.info('NexgenecUsergroup return:')
                logger.info(data)

            else:
                logger.info('NexgenecUsergroup show list all')
                group_name = request.GET.get('group_name')
                logger.info('   search group_name= ['+str(group_name)+" " + str(len(str(group_name)))+"]")

                if group_name is None or len(group_name)==0:
                    obj = NexgenecUsergroup.objects
                else:
                    obj = NexgenecUsergroup.objects.filter(group_name__icontains=group_name)
                data = list(obj.values())
                logger.info('NexgenecUsergroup return:')
                logger.info(data)

            return JsonResponse(
                    data
                    ,safe=False
                    )

        elif request.method == "POST":
            jsonObj = request.data['data']
            logger.info('NexgenecUsergroup update object begin:')
            logger.info(jsonObj)
            if jsonObj["seq_id"] is None:
                # new object
                seq_id = NexgenecUsergroup.objects.all().aggregate(Max('seq_id'))["seq_id__max"]
                if seq_id==None:
                    seq_id=1
                else:
                    seq_id+=1
                print(seq_id)
                # print(seq_id)
                obj = NexgenecUsergroup(seq_id=seq_id, group_name=jsonObj['group_name'], group_desc=jsonObj['group_desc'], updated_by=request.user.user_name )
                # print(obj)

                obj.save()
                loggerDb(request, "Add new group_name "+jsonObj['group_name'], "Create", obj.seq_id, 'NEXGENEC_USERGROUP')
                logger.info('NexgenecUsergroup new object after:')
                logger.info(jsonObj)
                return JsonResponse(
                        jsonObj
                        ,safe=False
                        )
            else:
                #Update
                obj = NexgenecUsergroup.objects.get(seq_id=ID)
                #obj.user_name = jsonObj["user_name"]
                obj.group_name = jsonObj["group_name"]
                obj.group_desc = jsonObj["group_desc"]

                # for key in model_to_dict(obj):
                #     setattr(obj, key, jsonObj[key])
                # pass
                obj.save()
                #def loggerDb(request,action_desc,action_id='Insert',field_id=0,table_name=''):
                loggerDb(request, "Update "+obj.group_name, "Edit", obj.seq_id, NexgenecUsergroup.objects.model._meta.db_table)
                logger.info('NexgenecUsergroup update object after:')
                logger.info(jsonObj)
                return JsonResponse(
                        jsonObj
                        ,safe=False
                        )
        elif request.method == "DELETE":
            # logger.info('NexgenecUsergroup delete object by seq_id:'+str(ID))
            # obj = NexgenecUsergroup.objects.get(seq_id=ID)
            # logger.info('NexgenecUsergroup load object by seq_id')
            # # print( model_to_dict(obj) )
            # # logger.info(model_to_dict(obj))
            # #def loggerDb(request,action_desc,action_id='Insert',field_id=0,table_name=''):
            # loggerDb(request, "Delete "+obj.user_name, "Delete", obj.seq_id, NexgenecUsergroup.objects.model._meta.db_table)

            # # obj.DEL_FLAG='Y'
            # obj.delete()

            # logger.info("Deleted")
            return JsonResponse(
                    "Delete Done"
                    ,safe=False
                    )
        else:
            logger.info('NexgenecUsergroup not support method '+request.method)
            return JsonResponse(None, safe=False)
    except Exception:
        logger.error("Fatal error", exc_info=True)

