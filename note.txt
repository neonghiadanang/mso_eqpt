Successfully installed Django-2.1.1

"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe" --disable-web-security --disable-gpu --user-data-dir=~/chromeTemp
"D:\workspace3\neo\scripts\activate"
"..\..\neo\scripts\activate"
../../neo/scripts/activate
neo/scripts/activate

../../devenv/scripts/activate



django.db.utils.DatabaseError: DPI-1047: Cannot locate a 32-bit Oracle Client library: "D:\app\Admin\product\11.2.0\dbhome_1\instantclient_19_6\oci.dll is not the correct architecture"


python -c "import django; print(django.get_version())"
django-admin.py version

sudo pip install Django==1.1.2


he problem is that Django 2.0.2 only supports oracle 12g. Check this:

How to make Django 2.0 to use Oracle 11g syntax instead of 12c?

Also, you can check the sql failing, as pointed in the following question (adding to the manage.py a print(query) line)

Unable to create the django_migrations table (ORA-02000: missing ALWAYS keyword)

I've downgrade to Django 1.11 as recommended in the first question, but this leaded me to the error "AttributeError: 'cx_Oracle.Cursor' object has no attribute 'numbersAsStrings'" because I have installed the last cx_Oracle version. (more information here: https://code.djangoproject.com/ticket/28138)

To fix this, I've modify the file C:\Program Files\Python37\lib\site-packages\django\db\backends\oracle\base.py to this:

def __init__(self, connection):
     self.cursor = connection.cursor()
     # Necessary to retrieve decimal values without rounding error.
     self.cursor.numbersAsStrings = True
     self.cursor.outputtypehandler = self._output_type_handler
     # Default arraysize of 1 is highly sub-optimal.
     self.cursor.arraysize = 100
     # https://github.com/django/django/commit/d52577b62b3138674807ac74251fab7faed48331

 @staticmethod
 def _output_type_handler(cursor, name, defaultType, length, precision, scale):
     """
     Called for each db column fetched from cursors. Return numbers as
     strings so that decimal values don't have rounding error.
     """
     if defaultType == Database.NUMBER:
         return cursor.var(
             Database.STRING,
             size=255,
             arraysize=cursor.arraysize,
             outconverter=str,
         )
I've take this code block from here:

https://github.com/cloudera/hue/commit/07d85f46eeec9c8c19d9aa11d131638e2a99e65c#diff-6d9bd161753aad635c23c2e91efafe91

With this, I've been able to migrate the project, at least. I don't know if it will fail while going further.

Hope this helps!

PD: I think your DATABASES setting should be as in http://www.oracle.com/webfolder/technetwork/tutorials/obe/db/oow10/python_django/python_django.htm

DATABASES = {
'default': {
    'ENGINE':   'django.db.backends.oracle',
    'NAME':     'localhost/orcl',
    'USER':     'pythonhol',
    'PASSWORD': 'welcome',
}}



D:\workspace3\mso_eqpt\app\server\eqpt_setting

https://code-maze.com/angular-material-form-validation/

create models

pip uninstall django
pip install Django==1.11.22

cd <django application>


ng g component pages/eqpt-machine-state
ng g component pages/eqpt-config
ng g component pages/eqpt-config
ng g component pages/nexgenecactionlog
ng g component pages/nexgeneceqptaction
ng g component pages/nexgeneceqptstate
ng g component pages/nexgenecgroup

python manage.py startapp nexgenec_eqpt_statemachine server\nexgenec_eqpt_statemachine
python manage.py startapp nexgenec_eqpt_upload_setting server\nexgenec_eqpt_upload_setting
python manage.py startapp nexgenec_eqpt_logger server\nexgenec_eqpt_logger
python manage.py startapp nexgenec_version_control server\nexgenec_version_control

python manage.py startapp nexgenec_eqpt_statelist server\nexgenec_eqpt_statelist
python manage.py startapp nexgenec_eqpt_actionlist server\nexgenec_eqpt_actionlist


py manage.py inspectdb NEXGENEC_EQPT_DETAIL > .\server\eqpt_setting\oraclemodel.py
py manage.py inspectdb NEXGENEC_EQPT_DETAIL > ./server/eqpt_setting/models.py
py manage.py inspectdb NEXGENEC_USER > ./server/app/models.py
py manage.py inspectdb NEXGENEC_EQPT_UPLOAD_SETTING

py manage.py inspectdb NEXGENEC_EQPT_CONFIG
py manage.py inspectdb NEXGENEC_MES_SETTING
py manage.py inspectdb NEXGENEC_EQPT_CONNECTION

py manage.py inspectdb NEXGENEC_EQPT_CONNECTION
py manage.py inspectdb NEXGENEC_EQPT_STATE

py manage.py inspectdb nexgenec_eqpt_statemachine > server\nexgenec_eqpt_statemachine\models.py
py manage.py inspectdb nexgenec_eqpt_upload_setting

NEXGENEC_EQPT_CONFIG

pip uninstall django
pip install Django==2.2.4
https://stackoverflow.com/questions/4914775/django-inspectdb-issue-using-oracle-database




git add *
git add -u :/
git commit -m "Update"
git push

git pull --rebase


#REBASE AT HEAD
git fetch --all
git reset --hard origin/master

git clone https://neonghiadanang@bitbucket.org/neonghiadanang/mso_eqpt.git

npm install -g increase-memory-limit
increase-memory-limit
set NODE_OPTIONS=--max_old_space_size=4096
ng build -c deploy --build-optimizer --aot --prod --sourceMap


npm uninstall @angular-devkit/build-angular
npm install @angular-devkit/build-angular@0.13.0

-->OK gojs run
npm install @angular-devkit/build-angular@0.901.9


ng serve --host 0.0.0.0 --port 4201 
--live-reload-port 49153


    "gojs": "^2.1.8",
    "gojs-angular": "1.0.3",


>>> from django.db.models import Avg, Max, Min, Sum
>>> Product.objects.all().aggregate(Avg('price'))
# {'price__avg': 124.0}


ALTER TABLE NEXGENEC_EQPT_SETTING ADD (SITE_ID VARCHAR2(20) DEFAULT 'MSO' ) ADD (OPERATION_ID VARCHAR2(20) DEFAULT 'TEST' )


Pedigree.prototype.findTwin = function(diagram, a, b){
    var nodeA = diagram.findNodeForKey(a);
    var nodeB = diagram.findNodeForKey(b);
    console.log(nodeA, nodeB);
    if (nodeA !== null && nodeB !== null) {
        var it = nodeA.findLinksBetween(nodeB); // direction matters
        console.log("The links: ", it);
        while (it.next()) {
            var link = it.value;
            console.log(link);
            // Link.data.category === "Twin" means it's a twin relationship
            if (link.data !== null && link.data.category === "Twin") return link;
        }
    }
    return null;
};



npm install ng2-file-upload --save


With raw queries
    qry1 = "SELECT c.car_name, p.p_amount FROM pay p, cars c where p.user_id=%s;"
    cars = Cars.objects.raw(qry1, [user_id])